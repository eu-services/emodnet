/* global legendDijit, myToggleButtonLegend, myToggleButtonTool, item */
quex = [];
alphax = [];
var map;
featureAttributes1 = [];
labtras = [];
labtras2 = [];
hashcheck = false;
hash2 = true;
justloaded2 = false;
listlayerx2ok = [];
layersWithSettings = [];
var lastyear;
var currentYear;
var layers_of_group_list = [];
urlobj = {};
var opt = {};
var allLoaded= false;
var timeFieldStart = [];
var loadedLayers= [];
layerInfo2 = [];

var leg2;
var measurement;
var measurement1;
var measurement2;
var basemapGallery;
var scalebar
var plate
var basemaps = [];
var worldBackgroundLayers,articBackgroundLayers;
var basemapx;
var parameterinurl = false;
var justloaded = false;
var a = 0;
var toggledFish = false;
var toggledTool = false;
var toggledLegend = false;
var toggledMenu = false;
var toggledTime = false;
var embedurl = false;
var fishdata_to_trasl;
var index;
var printer;
var selet = [];
var lastLayer = false;
var contResponse = 0;
var contLayersClicable = 0;
var arraydeferred = [];
var arrayHighChartdeferred = [];
var contDeferred = 0;
var contFeatInfo = 0;
var contGetFeature = 1;
var notFeatureInfo = true;
var featureWMS = {};
var theme_Index = -1;
var package_path = window.location.pathname.substring(0, window.location.pathname.lastIndexOf("/"));

require([
    package_path + "/main_functions.js"
], function (main_functionsx) {

    require([
        "esri",
        "esri/config",
        "esri/map",
        "esri/dijit/Popup",
        "esri/dijit/Scalebar",
        "esri/dijit/BasemapLayer",
        "esri/dijit/Basemap",
        "esri/dijit/Measurement",
        "esri/units",
        "esri/layers/FeatureLayer",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "dojo/json",
        // "dojo/text!" + package_path + "/serviceinfos.json",
        // "dojo/text!" + package_path + "/fishinfos.json",
        // "dojo/text!" + package_path + "/groupinfos.json",
        // "dojo/text!" + package_path + "/themes.json",
        // "dojo/text!" + package_path + "/languages.json",
        "dojo/text!" + package_path + "/languages/languages.json",
        "esri/dijit/BasemapGallery",
        "esri/dijit/Legend",
        "dojo",
        "dojo/dom",
        "dojo/dom-construct",
        "dojo/dom-class",
        "dojo/dom-style",
        "dijit/registry",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/TimeExtent",
        "esri/dijit/TimeSlider",
        "esri/tasks/IdentifyTask",
        "esri/tasks/IdentifyParameters",
        "esri/layers/ImageParameters",
        "dojo/_base/array",
        "dojo/parser",
        "dijit/form/CheckBox",
        "esri/InfoTemplate",
        "esri/dijit/HomeButton",
        "esri/dijit/Print",
        "esri/tasks/PrintTemplate",
        "esri/request",
        package_path + "/layerinit.js",
        package_path + "/main_functions.js",
        "dojo/domReady!"


    ], function (esri, esriConfig, Map, Popup, Scalebar, BasemapLayer, Basemap, Measurement,
        Units, FeatureLayer, SimpleLineSymbol, PictureMarkerSymbol,
        JSON, //serviceinfos, fishinfos, groupinfos, themes, languages, 
        languages,
        BasemapGallery, Legend, dojo, dom, domconstruct, domclass, domstyle,
        registry, Query, QueryTask, ArcGISDynamicMapServiceLayer,
        TimeExtent, TimeSlider, IdentifyTask, IdentifyParameters, ImageParameters, arrayUtils, parser, CheckBox, InfoTemplate,
        HomeButton, Print, PrintTemplate, esriRequest, layerinit, main_functions) {




        translate_measurement = function (langl) {
            for (var l in esri.bundle.widgets.measurement) {
                esri.bundle.widgets.measurement[l] = meas_trasl[l];
            }
            if (measurement){
                measurement.startup();
            }
            if (measurement1){
                measurement1.startup();
            }
            if (measurement2){
                var firstElementMeasureMouseIcon =  $("#coordinatePane .esriMeasurementTableRow").find("td")[0];
                $(firstElementMeasureMouseIcon).empty();
                var firstElementMeasureFlagIcon =  $("#coordinatePane .esriMeasurementTableRow").find("td")[3];
                $(firstElementMeasureFlagIcon).empty();
                measurement2.startup();
            }
        };

        /*
        translate_article = function () {
            for (var x in article) {
                //ll = $('#language').find(":selected").val();
                xx = article[x][lang];
                if (urlobj.p === "a") {
                    if (x == "#wabutton") {
                        xx = article["#projmenutitle"][lang];
                    } else if (x == "#projmenutitle") {
                        xx = article["#wabutton"][lang];
                    }
                }
                if (x != "#LAYERSELECTION1" && x != "#LAYERSELECTION3") {
                    // mylist=xx;
                    $(x).html(xx);
                    $(x).attr("data-placeholder", xx);
                } else {
                    $(x).attr("data-placeholder", xx);
                }
            }
        };
        */

        parseHtmlEntities = function (str) {
            return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
                var num = parseInt(numStr, 10); // read num as normal number
                return String.fromCharCode(num);
            });
        };

        translate_article = function () {
            for (var x in article) {
                //ll = $('#language').find(":selected").val();
                xx = article[x];
                if (urlobj.p === "a") {
                    if (x == "#wabutton") {
                        xx = article["#projmenutitle"][lang];
                    } else if (x == "#projmenutitle") {
                        xx = article["#wabutton"][lang];
                    }
                }
                var attribute_trans = x.substring(x.indexOf("[")+1,x.indexOf("]"));
                if ((attribute_trans || "") != "") {
                    $(x).attr(attribute_trans, parseHtmlEntities(xx));
                } else {
                    if (x != "#LAYERSELECTION1" && x != "#LAYERSELECTION3") {
                        // mylist=xx;
                        $(x).html(xx);
                        $(x).attr("data-placeholder", xx);
                    } else {
                        $(x).attr("data-placeholder", xx);
                    }
                }
            }
        };

        checkEmbedUrl = function () {
            if (window.location.hash.indexOf(";e=t") != -1) {
                embedurl = true;
            }
        };

        var
            identifyTask,
            identifyTaskx,
            identifyTasky,
            identifyParams,
            identifyParamsx,
            identifyParamsy;
        parser.parse();


        var popup = new Popup({
            titleInBody: false
        }, domconstruct.create("div"));

        //Add the dark theme which is customized further in the <style> tag at the top of this page
        domclass.add(popup.domNode, "julie");


        esri.config.defaults.io.useCors = true;
        esriConfig.defaults.io.corsDetection = true;

        esriConfig.defaults.io.proxyUrl = proxyconf;
        esriConfig.defaults.io.alwaysUseProxy = false;
        //      urlUtils.addProxyRule({urlPrefix: "route.arcgis.com*", proxyUrl: "http://localhost:8080/examples/proxy.jsp"});



        esriConfig.defaults.io.corsEnabledServers.push("maratlas.discomap.eea.europa.eu");
        esriConfig.defaults.io.corsEnabledServers.push("discomap.eea.europa.eu");
        // esriConfig.defaults.io.corsEnabledServers.push({host: "discomap.eea.europa.eu", withCredentials: true});
        esriConfig.defaults.io.corsEnabledServers.push("eas.jrc.ec.europa.eu/arcgis/rest");
        esriConfig.defaults.io.corsEnabledServers.push("ows.emodnet-bathymetry.eu");
        //        esriConfig.defaults.io.corsEnabledServers.push("tiles.marinetraffic.com");
        //      esriConfig.defaults.io.corsEnabledServers.push("www.marinetraffic.com");
        esriConfig.defaults.io.corsEnabledServers.push("sampleserver5.arcgisonline.com");
        esriConfig.defaults.io.corsEnabledServers.push("eas.jrc.ec.europa.eu");
        esriConfig.defaults.io.corsEnabledServers.push("194.30.43.115:8032");
        esriConfig.defaults.io.corsEnabledServers.push("staging.european-atlas-of-the-seas.eu");
        //   esriConfig.defaults.io.corsEnabledServers.push("ec.europa.eu");


        $.support.cors = true;


        (function (open) {
            XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
                url = url.replace("f=json", "f=pjson");
                if (url.search("maratlas.discomap.eea.europa.eu") > 0 && url.search("proxy") < 0)
                //if (url.search(window.location.hostname)>0){}else
                    url = url.replace("http:", "https:");  //really important to bypass cors
                open.call(this, method, url, async, user, pass);
                //  this.setRequestHeader("Content-Type","application/geojson");
                //     this.setRequestHeader("Accept","Application/geojson");
                //    this.setRequestHeader('Access-Control-Allow-Origin','localhost');        
                //  this.setRequestHeader('Access-Control-Allow-Methods', 'POST');        
                //   this.setRequestHeader('Access-Control-Allow-Headers', 'Origin,cache-control,content-type,man,messagetype,soapaction');
                //    this.setRequestHeader('Host', 'http://maratlas.discomap.eea.europa.eu');
                //   this.setRequestHeader('Origin', 'http://maratlas.discomap.eea.europa.eu');
            };
        })(XMLHttpRequest.prototype.open);

        // THEMES_CONFIG = JSON.parse(themes); // parse themes file

        //  transldata=JSON.parse(jsontransl);
        urlobj = {};
        checkEmbedUrl();
        main_functions.seturlobj();
        if (urlobj.lang && urlobj.lang !== "undefined") {
            lang = urlobj.lang;

            //translate_article();

        } else {
            // lang = len.options[len.selectedIndex].value;
            lang = "EN";
            urlobj.lang = "EN";
        }

        if (lang == "EN") {
            document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_en.htm";
        } else if (lang == "DE") {
            document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_de.htm";
        } else if (lang == "FR") {
            document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_fr.htm";
        }
        allLoaded = false;
        var fTranslate = $.Deferred();
						
		var fLang = $.Deferred();
		var _langConfig;
		
		var fGroups = $.Deferred();
		var _groupConfig;
		
		var fServices = $.Deferred();
		var _serviceConfig;
		
		var fThemes = $.Deferred();
		var _themesConfig;
		
		var fFish = $.Deferred();
        var _fishConfig;
        
        var fIdiom = $.Deferred();
        var _idiomConfig;
			
		$.getScript( './languages/translation_'  + lang + '.js?_=' + Date.now(), function( data, textStatus, jqxhr ) {				
			fTranslate.resolve();
        });	
        
        $.ajax({
            url: './languages/languages_'  + lang + '.json?_=' + Date.now(),
            crossDomain:true,
            type:'get',
            dataType:'json',
            success: function(data) {    
                _langConfig= data;
                fLang.resolve();	
            }
        });

		$.ajax({
            url: './languages/serviceinfos_'  + lang + '.json?_=' + Date.now(),
            crossDomain:true,
            type:'get',
            dataType:'json',
            success: function(data) {    
                _serviceConfig= data;
                fServices.resolve();	
            }
        });
        
        $.ajax({
            url: './languages/groupinfos_'  + lang + '.json?_=' + Date.now(),
            crossDomain:true,
            type:'get',
            dataType:'json',
            success: function(data) {    
                _groupConfig= data;
                fGroups.resolve();	
            }
        });
        
        $.ajax({
            url: './languages/themes_'  + lang + '.json?_=' + Date.now(),
            crossDomain:true,
            type:'get',
            dataType:'json',
            success: function(data) {    
                _themesConfig= data;
                fThemes.resolve();	
            }
        });
        
        $.ajax({
            url: './languages/fishlist_'  + lang + '.json?_=' + Date.now(),
            crossDomain:true,
            type:'get',
            dataType:'json',
            success: function(data) {    
                _fishConfig= data;
                fFish.resolve();	
            }
        });			
		
			
		//when we are sure all files are loaded change the url 
		$.when(fTranslate,fLang, fGroups, fServices, fThemes, fFish).done(function(){		

			GROUP_CONFIG = _groupConfig; // parse groupinfos file
			SERVICES_CONFIG = _serviceConfig; // List of services
			FISH_CONFIG = _fishConfig; // parse fishinfo file
			LANG_CONFIG = _langConfig; // parse languages file
			THEMES_CONFIG = _themesConfig; // parse themes file
			
			world = GROUP_CONFIG.world;
			arctic = GROUP_CONFIG.arctic;
			worldBackgroundLayers = world.background.LAYERS_LIST.split(";");
			articBackgroundLayers = arctic.background.LAYERS_LIST.split(";");

			translate_article();
            translate_measurement(lang);


            if (urlobj.theme && urlobj.theme !== "undefined") {
            } else {

                if (urlobj.p && urlobj.p !== "w") {
                    urlobj.theme = "100301:0.75";
                } else {
                    urlobj.theme = "2:0.75"; //first layers to load by default  //removed 46:0.5, based on request.
                }
            }

            ///projection world or arctic
            if (urlobj.p && urlobj.p !== "undefined") {
            } else {
                urlobj.p = "w";
            }

            if (urlobj.pos && urlobj.pos !== "undefined") {
            } else {
                urlobj.pos = "";
            }

            if (urlobj.gra && urlobj.gra !== "undefined") {
            } else {
                urlobj.gra = "";
            }

            if (urlobj.bkgd && urlobj.bkgd !== "undefined") {
            } else {
                if (urlobj.p == "w")
                    urlobj.bkgd = worldBackgroundLayers[0];
                else
                    urlobj.bkgd = articBackgroundLayers[0];
            }

            if (urlobj.mode && urlobj.mode !== "undefined") {
            } else {
                urlobj.mode = "";
            }

            if (urlobj.selection && urlobj.selection !== "undefined") {
            } else {
                urlobj.selection = "";
            }

            if (urlobj.c && urlobj.c !== "undefined") {
                parameterinurl = true;
            }
            
            quex = urlobj.theme.split(",");

            //default
            $("#tmbutton").css("background-color", "#759dc0");
            $("#slbutton").css("background-color", "#014489");

            window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z; //set url hash
            que = [];
            main_functions.url2que(quex);

            IDIOMS_CONFIG = JSON.parse(languages); // List of languages
            
            $(".lanValue").removeClass('active');
            $("."+ lang +"").find("a").addClass('active');
            changeLanguageText(lang);
        
            out = [];
            xaa = 0;
            mapservice = [];
            labmapservice = [];
            idlayer = [];
            intlayer = [];
            title = [];
            identifyinfo = [];
            type = [];
            fishlist = [];
            description = [];
            alpha = [];
            translated = [];
            // SERVICES_CONFIG = JSON.parse(serviceinfos); // List of services
            // FISH_CONFIG = JSON.parse(fishinfos); // parse fishinfo file
            activeLayers = [];
            var basemaps = [];
            layerInfo = [];
            layerDefs = [];
            lai = []; //LAYERS ARRAY EMPTY
            lai2 = []; //LABLAYERS ARRAY EMPTY
            speclayer = 0;
            species = 0;
            fish_options = [];
            fishopt2 = [];
        /////////////////////////////////////////////////////////////////////// input of all layer


            //queryTaskTranslation = new QueryTask("https://maratlas.discomap.eea.europa.eu/arcgis/rest/services/Maratlas/translation1/MapServer/1");
            //LANG_CONFIG = JSON.parse(languages); // parse languages file
            /*query = new Query();
            query.returnGeometry = false;
            query.outFields = ["TRANSLATION", "UNIQUEID", "INIT"];*/



            if (urlobj.p === "w") {
                worarc = world;
                
                $("#basemap_next").css({"display":"block"});
                //$('#wabutton_label').text("Arctic");
                //$('#projmenutitle').text("Mercator");
                $('#mercator_button').attr("disabled", true);
            } else {
                worarc = arctic;
                toggledMenu = true;
                $("#basemap_next").css({"display":"none"});
                
                // domstyle.set(registry.byId("slbutton").domNode, "display", "none");
                // domstyle.set(registry.byId("tmbutton").domNode, "display", "none"); //GA: to disable DIY MAp button
                //$('#wabutton_label').text("Mercator");
                //$('#projmenutitle').text("Arctic");
                $('#arctic_button').attr("disabled", true);
            
            }
            main_functions.assigntitle();
            defineBasemaps();
            startupMap();
            startupMeasurement();
            addScaleBar();
            startupBasemapGallery();
            printInfo.then(handlePrintInfo, handleError);
            allLoaded = true;
            if (justloaded && allLoaded ) {
                allLoaded = true;	
                start();
            }	
        });
        $("#wabutton_label").on("click", function () {
            window.location.reload();
        });

        removeMe1 = function () {
            var count = map.layerIds.length;
            var array = [];
            for (var i = 0; i < count; i++) {
                array[i] = map.layerIds[i];
            }
            for (var j = 1; j < count; j++) {
                var layer = map.getLayer(array[j]);
                
				map.removeLayer(layer);
				
            }
            $(".titleButton").click();
        };

        removeAllLayers = function () {
            map.removeAllLayers();
			for (var i=0; i< loadedLayers.length; i++)											
				layerRemoved(loadedLayers[i].id,loadedLayers[i].name);			
			loadedLayers =[];
			
            basemapGallery.select(urlobj.bkgd);
        };

        var justtranslated;
        translation1 = function () {
            //map.graphics.clear();
            justtranslated = true;
            removeMe1();
            labmapservice = [];
            main_functions.intersect_safe(SERVICES_CONFIG, que);
            var translationsOfLayers = [];
            translationsOfLayers = LANG_CONFIG.features.filter(function (item) {
                return (item.attributes.LANGUAGE === lang);
            });
            setTimeout(function () {
                showResults1(translationsOfLayers);
            }, 500);
            //query.where = "LANGUAGE = '" + lang + "'";
            //queryTaskTranslation.execute(query, showResults1);
            //    translated=transldata.features;

            //      var translated0 = transldata[lang];
            //   showResults1(translated0);
        };

        showResults1 = function (results) {
            translated = results;
            for (t in title) {
                var txx = title[t].toString();
                //if (txx.charAt(0) === "{") {
                    for (var attr in results) {
                        featureAttributes = results[attr].attributes;
                        featureAttributes1[attr] = featureAttributes;
                        if (title[t] === featureAttributes["UNIQUEID"]) {
                            index = t;
                            labtras[t] = featureAttributes["TRANSLATION"];
                            labtras2[t] = featureAttributes;
                        }
                    }
                //} else {
                    labtras[t] = title[t];
                //}
            }
            //map.graphics.clear();
            mer = lai.concat(lai2);
            //  while (map.loaded) {
            map.addLayers(mer);
            //       return; } ;
            hashcheck = false;
            hash2 = false;
            //main_functions.listalayercreation2();
            main_functions.listThemesCreationModalSearch();
            main_functions.listThemesCreationModalHome();
            if (justloaded2 == false){
                var miStorage = window.localStorage;
                if (miStorage.length == 0 && !embedurl) {
                    toggleModal();
                } else {
                    if (miStorage.getItem("closeHomepage") != "true" && !embedurl) {
                        toggleModal();
                    }
                }
                justloaded2 = true;
            }
        };

        translation3 = function () {
            labmapservice = [];
            main_functions.intersect_safe(SERVICES_CONFIG, que);
            labtras = [];
            labtras2 = [];
            for (t in title) {
                var txx = title[t].toString();
                if (txx.charAt(0) === "{") {
                    for (var attr in translated) {
                        featureAttributes = translated[attr].attributes;
                        featureAttributes1[attr] = featureAttributes;
                        if (title[t] === featureAttributes["UNIQUEID"]) {
                            index = t;
                            labtras[t] = featureAttributes["TRANSLATION"];
                            labtras2[t] = featureAttributes;
                        }
                    }
                } else {
                    labtras[t] = title[t];
                }
            }
        };

        //////////////////////replaced legend function 2   legendDijit._buildLegendItems_Tools = customBuildLegendItems; // replace original function _buildLegendItems_Tools with customBuildLegendItems
        customBuildLegendItems = function (a, b, c) {
            var d = b.parentLayerId,
                f = this.map.getScale(),
                h = !1,
                k = function (a, b) {
                    var c, d;
                    for (c = 0; c < a.length; c++)
                        if (b.dynamicLayerInfos)
                            for (d = 0; d < b.dynamicLayerInfos[d].length; d++) {
                                if (b.dynamicLayerInfos[d].mapLayerId === a[c].layerId)
                                    return a[c];
                            }
                        else if (b.id === a[c].layerId)
                            return a[c];
                    return {};
                };
            if (!this._respectCurrentMapScale || this._respectCurrentMapScale && this._isLayerInScale(a, b, f)) {
                var l = !0;
                if ("esri.layers.ArcGISDynamicMapServiceLayer" === a.declaredClass || "esri.layers.ArcGISMapServiceLayer" === a.declaredClass) {
                    var g = this._getEffectiveScale(a, b);
                    if (g.minScale && g.minScale < f || g.maxScale && g.maxScale > f)
                        l = !1;
                }
                if (l) {
                    var vvv = a.legendResponse.layers;
                    arr = $.map(vvv, function (n, i) {
                        $.map(n.legend, function (n2, i2) {
                            var xxx = n2.label.toString();
                            if (xxx.charAt(0) === "{") {
                                for (var attr = 0; attr < featureAttributes1.length; attr++) {
                                    var featureAttributes = featureAttributes1[attr];
                                    if (n2.label === featureAttributes.UNIQUEID) {
                                        n2.label1 = featureAttributes.TRANSLATION; //.esriLegendLayerLabel2

                                    }
                                }
                            } else {
                                n2.label1 = n2.label;
                            }
                        });
                    });
                    var f = k(a.legendResponse.layers, b),
                        r = f.legendType,
                        v = f.legend;

                    if (v) {
                        c = domconstruct.create("table", {
                            cellpadding: 0,
                            cellspacing: 0,
                            width: "95%",
                            "class": "esriLegendLayer"
                        },
                        c);
                        var t = domconstruct.create("tbody", {},
                            c);
                        (a._hoverLabel || a._hoverLabels) && this._createHoverAction(c, a, b);
                        arrayUtils.forEach(v, function (c) {
                            if (!(10.1 <= a.version && !c.values && 1 < v.length && (a._hideDefaultSymbol || "\x3call other values\x3e" === c.label || !c.label && !("esri.layers.ArcGISImageServiceLayer" === a.declaredClass && 10.3 <= a.version))))
                                if (c.url && 0 === c.url.indexOf("http") || c.imageData && 0 < c.imageData.length)
                                    h = !0,
                                    this._buildRow_Tools(c, t, a, b.id, r);
                        },
                        this);
                    }
                }
            }
            h && (domstyle.set(dom.byId(this.id + "_" + a.id + "_" + b.id), "display", "block"), -1 < d && (domstyle.set(dom.byId(this.id + "_" + a.id + "_" + d + "_group"), "display", "block"), this._findParentGroup(a.id, a, d)));
            return h;
        };
        //////////////////////replaced legend function 1  old = legendDijit._buildRow_Tools; WITH modfun1

        modfun1 = function (a, b, c, d, f) {
            var xxx = a.label.toString();
            if (xxx.charAt(0) === ".") {
                return;
            }
            var temp = a.label;
            a.label = a.label1;
            old(a, b, c, d, f);
            a.label = temp;
        };
        /////////////////////////////// IDENTIFYTASK FUNCTION MAYBE TO BE REPLACED BY QUERYTASK
        
        execute2 = function (event) {

            boxcontent = 0;
            var conts = 0;
            var conts2 = 0;
            var posxx = null;
            var posxa = [];
            var posax;
            selet = [];
            timeFieldStart = [];
            field4timeseries = [];
            arraydeferred = [];
            contDeferred = 0;
            contGetFeature = 1;
            lastLayer = false;
            contResponse = 0;
            arrayHighChartdeferred = [];
            notFeatureInfo = true;
            wmsURLArray = [];
            for (var i in layerInfo2) { // LAYER LIST ORDERED
                if (layerInfo2[i].title) {
                    if (layerInfo2[i].layer.visible) {
                        selet[i] = layerInfo2[i];
                    }
                }
            }

            identifyParamsy = new IdentifyParameters();
            identifyParamsy.tolerance = 8;
            identifyParamsy.returnGeometry = false;
            identifyParamsy.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
            identifyParamsy.width = map.width;
            identifyParamsy.height = map.height;
            identifyParamsy.geometry = event.mapPoint;
            identifyParamsy.mapExtent = map.extent;

            function identifyCallBackx(lotResults,layer_Id) {
                posxa = [];
                conts2 = conts2 + 1;
                var out = [];

                var tarurl = lotResults.target.url;
                if (lotResults.results.length > 0) {
                    if (posxx === null) {
                        //  posxx = 1;
                        for (var ixy in selet) {
                            if (selet[ixy].layer.idd === layer_Id) {

                                posxa.push(parseInt(ixy));
                            }
                        }

                    }
                }
                ;
                intposxa = Math.max.apply(null, posxa);
                for (var ixla in intlayer) {
                    if (selet[intposxa]) {
                        if (selet[intposxa].title)
                            if (selet[intposxa].idd === intlayer[ixla].ID) {
                                intposya = ixla;
                            }
                    } else
                        intposya = 0;
                }
                if (intposya) {
                    execute3(event, intposya.toString());
                } else {
                    for (var ixla in intlayer) {
                        if (tarurl == intlayer[ixla].MAPSERVICE) {
                            intposya = ixla;
                            break;
                        }
                    }
                    execute3(event, intposya.toString());
                }

                var counter = 1;
                $(".esriPopupWrapper").on('DOMSubtreeModified',  function() {  
                    if (map.infoWindow.isShowing) {
                        if (counter==1){
                            setTimeout(function() {
                                var feature_info = $('.esriPopupWrapper').height();
                                if (feature_info == 0) {
                                    var totalHeight = 0;
                                    $(".esriPopupWrapper").children().each(function(){
                                        totalHeight = totalHeight + $(this).outerHeight(true);
                                    });
                                    $("#measure_container").css({"top":totalHeight+15});
                                } else {
                                    $("#measure_container").css({"top":feature_info+30});
                                }
                            },400);
                            counter++;
                        }
                    }                
                });
            }

            for (var iy = selet.length - 1; iy >= 0; iy--) {

                for (var ixlb in layerInfo2) {
                    if (selet[iy])
                        if (selet[iy].title === layerInfo2[ixlb].title) { //SELECT  layer in layerInfo THAT IS THE LAST OF  selet (LAYER ON TOP)
                            //POSX IS INDEX OF LAST LAYER, IT IS USED FOR TITLE FIELD
                            var ixlc;
                            for (var is in intlayer) {

                                if (intlayer[is].ID == layerInfo2[ixlb].layer.idd)
                                    ixlc = is;
                            }

                            if ((intlayer[ixlc].IDENTIFYINFO || intlayer[ixlc].TYPE == "wms")) {
                                if (intlayer[ixlc].IDENTIFYINFO && intlayer[ixlc].TYPE != "wms")
                                    var typ = intlayer[ixlc].IDENTIFYINFO[0].TYPE;
                                else
                                    var typ = intlayer[ixlc].TYPE;
                                if (typ !== "wms") { //SE NON E'UN WMS

                                    var idenfoy = [];
                                    for (var k in intlayer[ixlc].IDENTIFYINFO) {
                                        idenfoy[k] = intlayer[ixlc].IDENTIFYINFO[k].ID;
                                    }
                                    ;
                                    identifyParamsy.layerIds = idenfoy;
                                    var layerDefAry0 = [];
                                    var species0 = $("#spec").find(":selected").val();
                                    var speciesname0 = $("#spec").find(":selected").text();
                                    var namename0 = "";
                                    if (intlayer[ixlc].TYPE === "fish-dynamic") {
                                        if (speciesname0 && species0) {
                                            namename0 = ": " + speciesname0;
                                            for (var zed in intlayer[ixlc].IDENTIFYINFO) {
                                                y = intlayer[ixlc].IDENTIFYINFO[zed].ID;
                                                layerDefAry0[y] = intlayer[ixlc].FISHLIST.FISHFIELDNAME.split(".")[1] + "= '" + species0 + "'";
                                            }
                                            identifyParamsy.layerDefinitions = layerDefAry0;
                                        }
                                    }

                                    identifyTasky = new IdentifyTask(layerInfo2[ixlb].layer.url);
                                    identifyTasky.layerId= layerInfo2[ixlb].layer.idd;
                                    identifyTasky.execute(identifyParamsy);
                                    identifyTasky.on("complete", function(e) {
                                        identifyCallBackx(e,this.layerId);
                                    });

                                    conts = conts + 1;
                                } else { //if WMS



                                    boxcontent = 0;
                                    var point = event.mapPoint;
                                    var querylayer = [];
                                    var wmsfields;
                                    var wmsfieldslabel;
                                    /*for (var i in intlayer[ixlb].IDENTIFYINFO) {
    
                                            querylayer[i] = intlayer[ixlb].IDENTIFYINFO[i].ID;
                                        }*/
                                    if (layerInfo2[ixlb].layer.url.indexOf("?") != -1) { //IF LAYER URL CONTAINS "?"
                                        var proxys = proxyconf + "?" + layerInfo2[ixlb].layer.url + "&service=wms";
                                    } else {
                                        var proxys = proxyconf + "?" + layerInfo2[ixlb].layer.url + "?service=wms";
                                    }
                                    var getFeatureInfoRequest = function () {
                                        var infoFormat = "text/xml"; //SET INFO FORMAT AS XML
                                        var responseFormat = "xml"; //SET RESPONSE FORMAT AS XML
                                        if (intlayer[ixlc].INFOFORMAT) {
                                            infoFormat = intlayer[ixlc].INFOFORMAT;
                                            responseFormat = "text";
                                        }
                                        var querylayers = (layerInfo2[ixlb].layer.visibleLayers.toString()).split("&");
                                        var content = {
                                            //  version: "1.1.1",
                                            //   styles: "default",
                                            f: "",
                                            REQUEST: "GetFeatureInfo",
                                            //   "VERSION": wmsLayer.version,
                                            //   service: "WMS",
                                            VERSION: layerInfo2[ixlb].layer.version,
                                            SRS: "EPSG:" + layerInfo2[ixlb].layer.spatialReferences[0],
                                            CRS: "EPSG:" + layerInfo2[ixlb].layer.spatialReferences[0],
                                            QUERY_LAYERS: querylayers[0],
                                               //QUERY_LAYERS: layerInfo2[ixlb].layer.visibleLayers.toString(),
                                            LAYERS: layerInfo2[ixlb].layer.visibleLayers,
                                            //      "FORMAT": "jsonp",// json
                                            //      "type": "jsonp",

                                            INFO_FORMAT: infoFormat, //"text/xml", "text/html",  "text/plain", "application/vnd.ogc.wms_xml" "application/vnd.esri.wms_featureinfo_xml",   "application/vnd.esri.wms_raw_xml",
                                            BBOX: map.extent.xmin + "," + map.extent.ymin + "," + map.extent.xmax + "," + map.extent.ymax,
                                            WIDTH: map.width,
                                            HEIGHT: map.height,
                                            X: event.layerX,
                                            Y: event.layerY
                                        };

                                        for (var i=1;i<querylayers.length; i++) {
                                            content[querylayers[i].split("=")[0]] = querylayers[i].split("=")[1];
                                        }

                                        esriRequest({
                                            url: proxys,
                                            content: content,
                                            //  jsonp: "jsoncallback",
                                            //     Tell jQuery we're expecting JSONP
                                            //dataType: "jsonp",
                                            // dataType: "pjson",
                                            handleAs: responseFormat,
                                            //callbackParamName: "jsoncallback",
                                            load: function (wmsresponse) {
                                                contResponse++;
                                                if (contResponse == contLayersClicable) {
                                                    lastLayer = true;
                                                }
                                                var containerxx = [];
                                                featureWMS = {};
                                                if ((wmsresponse.childNodes[0].childNodes[1] || "") != "") {
                                                    //boxcontent = 1;
                                                    if (typeof wmsresponse == "object") {
                                                        for (var i in wmsURLArray) {
                                                            if (wmsresponse.URL.substring(wmsresponse.URL.indexOf("?")+1).indexOf(wmsURLArray[i].URL.substring(0,wmsURLArray[i].URL.indexOf("?"))) > -1) {
                                                                if (wmsresponse.URL.indexOf(wmsURLArray[i].LAYERS) > -1 || wmsresponse.URL.indexOf(encodeURIComponent(wmsURLArray[i].LAYERS[0])) > -1) {
                                                                    ixlbwms = i;
                                                                    break;
                                                                }
                                                            }
                                                        }

                                                        if (wmsresponse.childNodes[0].localName == "info") { //FOR CHEMISTRY LAYERS
                                                            var elements = wmsresponse.childNodes[0].childNodes;
                                                            containerxx.push("<div>");
                                                            for (var i = 0; i < elements.length; i++) {
                                                                if (elements[i].childNodes.length > 0) {
                                                                    var textContent = textContentToLink(elements[i].textContent);
                                                                    containerxx.push("<div><b>" + elements[i].localName + ": </b>" + textContent + "</div>");
                                                                }
                                                            }
                                                            containerxx.push("</div>");
                                                        } else {
                                                            var elements = wmsresponse.childNodes[0].childNodes[1].childNodes[0].childNodes;
                                                            if (intlayer[ixlbwms].IDENTIFYINFO) {
                                                                wmsfields = intlayer[ixlbwms].IDENTIFYINFO;
                                                                containerxx.push("<div>");
                                                                for (var key in wmsfields) {
                                                                    for (var i = 0; i < elements.length; i++) {
                                                                        if (elements[i].localName == wmsfields[key].FIELD) {
                                                                            output = {};
                                                                            var translated = main_functions.translater([wmsfields[key].FIELD_ALIAS]);
                                                                            var wmsfieldslabeltrad = output;
                                                                            var textContent = textContentToLink(elements[i].textContent);
                                                                            containerxx.push("<div><b>" + translated[0] + ": </b>" + textContent + "</div>");
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                containerxx.push("</div>");
                                                            } else {
                                                                containerxx.push("<div>");
                                                                for (var i = 0; i < elements.length; i++) {
                                                                    var textContent = textContentToLink(elements[i].textContent);
                                                                    containerxx.push("<div><b>" + elements[i].localName + ": </b>" + textContent + "</div>");
                                                                }
                                                                containerxx.push("</div>");
                                                            }
                                                        }
                                                    } else {
                                                        //containerxx.push('<div>' + wmsresponse + '</div>');
                                                        if (wmsresponse.indexOf("<body></body>") != -1 || wmsresponse == "") { } //DON'T APPEAR INFOWINDOW WHEN IS EMPTY TEXT
                                                        else {
                                                            wmsresponse = wmsresponse.replace(new RegExp("\"", "g"), "\'");
                                                            containerxx.push("<iframe style=\"width:100%;height:100%;border:0px;\" src=\"data:text/html;charset=UTF-8," + wmsresponse + "\"></iframe>");
                                                        }
                                                    }
                                                    if (containerxx.length > 0) {
                                                        featureWMS = new esri.Graphic();
                                                        var custTemplate0 = new InfoTemplate(lai[ixlbwms].titleok, containerxx.join(""));
                                                        featureWMS.setInfoTemplate(custTemplate0);
                                                        arraydeferred.push(featureWMS);
                                                        if (lastLayer) {
                                                            map.infoWindow.setFeatures(arraydeferred);
                                                            map.infoWindow.show(event.mapPoint);
                                                        }

                                                        arrayHighChartdeferred[contDeferred] = false;
                                                        contDeferred++;
                                                    }
                                                } else {
                                                    if (arraydeferred.length == 0) {
                                                        if (lastLayer) {
                                                            setnotFeatureInfo(event);
                                                        } else {
                                                            checknotFeatureInfo(event);
                                                        }
                                                    }
                                                }

                                            },
                                            error: function (error) {
                                                if (lastLayer) {
                                                    if (arraydeferred.length == 0) {
                                                        errorFeatureInfo(event);
                                                    } else {
                                                        map.infoWindow.setFeatures(arraydeferred);
                                                        map.infoWindow.show(event.mapPoint);
                                                    }
                                                }
                                            }
                                        });
                                    };
                                    getFeatureInfoRequest();
                                    if (boxcontent === 0) {
                                        execute3(event, ixlc);
                                    } else {
                                        return;
                                    }
                                    ;

                                }
                            }
                        }
                }

            }
        };

        textContentToLink = function (value) { //IF TEXT IS URL CONVERT TO LINK
            if (value.indexOf("http") != -1) {
                value = "<a href=\"" + value + "\" target=\"_blank\">" + value + "</a>";
            }
            return value;
        }

        execute3 = function (event, posx) {
            timeFieldStart[posx] = "";
            var posxb;
            for (var is in layerInfo2) {

                if (intlayer[posx].ID == layerInfo2[is].layer.idd)
                    posxb = is;
            }

            if (boxcontent === 0)
                if (intlayer[posx].IDENTIFYINFO) {
                    var typ = intlayer[posx].IDENTIFYINFO[0].TYPE;
                    if (typ === "Array" || typ === "ColumnChart" || typ === "BarChart" || typ === "timeseries" || typ === "timemultiseries"  || typ === "PieChart" || typ === "Compass") {

                        var idenfo = [];
                        fieldaliasestrad = [];
                        fieldaliasestradraw = [];
                        fieldaliasestradraw2 = [];
                        fieldaliasestradraw3 = [];
                        identifyTaskx = new IdentifyTask(layerInfo2[posxb].layer.url);
                        identifyParamsx = new IdentifyParameters();
                        identifyParamsx.tolerance = 8;
                        identifyParamsx.returnGeometry = false;
                        identifyParamsx.returnFieldName = true;
                        identifyParamsx.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                        identifyParamsx.width = map.width;
                        identifyParamsx.height = map.height;
                        identifyParamsx.geometry = event.mapPoint;
                        identifyParamsx.mapExtent = map.extent;
                        for (var k in intlayer[posx].IDENTIFYINFO) {
                            idenfo[k] = intlayer[posx].IDENTIFYINFO[k].ID;
                        }
                        ;
                        identifyParamsx.layerIds = idenfo;
                        var deferredx = identifyTaskx
                            .execute(identifyParamsx)
                            .addCallback(function (responsex) {
                                if (responsex.length > 0) {
                                    for (var k in intlayer[posx].IDENTIFYINFO) {
                                        idenfo[k] = intlayer[posx].IDENTIFYINFO[k].ID;
                                    }
                                    ;
                                    identifyParamsx.layerIds = idenfo;
                                    var x = 0;
                                    var fifiID = identifyParamsx.layerIds.indexOf(responsex[x].layerId);
                                    field4timeseries[posx] = [];
                                    getFieldList = function () {
                                        esriRequest({
                                            url: layerInfo2[posxb].layer.url + "/" + responsex[x].layerId,
                                            content: {
                                                f: "pjson"
                                            },
                                            handleAs: "json",
                                            callbackParamName: "callback",
                                            load: function (response, io) {
                                                var fields = response.fields;
                                                field4timeseries[posx] = response.fields;
                                                fieldaliases = [];
                                                fieldnames = [];
                                                timeFieldStart[posx] = "";
                                                timeFieldStartAlias = "";
                                                timeFieldEnd = "";
                                                timeFieldEndAlias = "";
                                                /*for (var s in fields) {
                                                    if (intlayer[posx].IDENTIFYINFO[fifiID])
                                                        if (intlayer[posx].IDENTIFYINFO[fifiID].FIELDS)
                                                            if ($.inArray(fields[s].name, intlayer[posx].IDENTIFYINFO[fifiID].FIELDS) !== -1) {
                                                                fieldaliases.push(fields[s].alias);
                                                                fieldnames.push(fields[s].name);
                                                            }
                                                }*/
                                                var arrayfieldsNewFormat = false;
                                                if (intlayer[posx].IDENTIFYINFO[fifiID].FIELDS) {
                                                    arrayfields = intlayer[posx].IDENTIFYINFO[fifiID].FIELDS;
                                                    for (var key in arrayfields) {
                                                        for (var s in fields) {
                                                            if ((arrayfields[key].FIELD || '') != '') {
                                                                arrayfieldsNewFormat = true;
                                                                if (fields[s].name == arrayfields[key].FIELD) {
                                                                    fieldaliases.push(arrayfields[key].FIELD_ALIAS);
                                                                    fieldnames.push(fields[s].name);
                                                                    break;
                                                                }
                                                            } else {
                                                                if (fields[s].name == arrayfields[key]) {
                                                                    fieldaliases.push(fields[s].alias);
                                                                    fieldnames.push(fields[s].name);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                output = {};

                                                if (response.timeInfo) {
                                                    timeFieldStart[posx] = response.timeInfo.startTimeField;
                                                    timeFieldEnd = response.timeInfo.endTimeField;
                                                    if (timeFieldStart[posx])
                                                        timeFieldStartAlias = fields.filter(function (item) {
                                                            return (item.name === timeFieldStart[posx]);
                                                        })[0].alias;
                                                    if (timeFieldEnd)
                                                        timeFieldEndAlias = fields.filter(function (item) {
                                                            return (item.name === timeFieldEnd);
                                                        })[0].alias;

                                                }

                                                if (arrayfieldsNewFormat) {
                                                    for (var inx in fieldaliases) {
                                                        fieldaliasestradraw[inx] = fieldaliases[inx]; // sostituisce le keys con la loro traduzione
                                                        if (fieldaliases[inx] == "")
                                                            fieldaliasestradraw2[inx] = "${" + fieldnames[inx] + "} <br/>";
                                                        else
                                                            fieldaliasestradraw2[inx] = "<b>" + fieldaliases[inx] + ": </b> ${" + fieldnames[inx] + "} <br/>"; //LAYOUT POPUP FOR ARRAY
                                                    }
                                                } else {
                                                    main_functions.translater(fieldaliases);
                                                    fieldaliasestradraw2 = [];
                                                    for (var inx in output) {
                                                        fieldaliasestradraw[inx] = output[inx]; // sostituisce le keys con la loro traduzione
                                                        fieldaliasestradraw2[inx] = "<b>" + output[inx] + ": </b> ${" + fieldnames[inx] + "} <br/>"; //LAYOUT POPUP FOR ARRAY
                                                    }
                                                }
                                                fieldaliasestradraw3[posx] = fieldaliasestradraw2.join("");
                                                executeIdentifyTask(event, posx, posxb);
                                            },
                                            error: function (error) {
                                                console.log(error)
                                            }
                                        });
                                    };
                                    var queryTaskxx = new QueryTask(layerInfo2[posxb].layer.url + "/" + responsex[x].layerId);
                                    var queryxx = new Query();
                                    queryxx.returnGeometry = false;
                                    if (intlayer[posx].IDENTIFYINFO[fifiID])
                                        queryxx.outFields = [intlayer[posx].IDENTIFYINFO[fifiID].FIELDS];
                                    queryxx.where = "1=1";

                                    getFieldList();
                                } else {
                                    executeIdentifyTask(event, posx, posxb);
                                }
                            });
                    } else
                        executeIdentifyTask(event, posx, posxb);
                } else
                    executeIdentifyTask(event, posx, posxb);
        };

        changeChartArea = function (contGetFeature) {
            $('.container').hide();
            $('#containerc'+contGetFeature).show();
        };
        checkHighChart = function (contFeatInfo) {
            if (arrayHighChartdeferred[contFeatInfo]) {
                $("#container").show();
                $(".contentPane").empty();
            } else {
                $(".container").hide();
            }
        };
        checknotFeatureInfo = function (event) {
            if (notFeatureInfo) {
                $("#map_graphics_layer g").empty();
                $(".sizer.content #container").empty();
                if (!$(".titleButton.prev").hasClass("hidden"))
                    $(".titleButton.prev").addClass("hidden");
                if (!$(".titleButton.next").hasClass("hidden"))
                    $(".titleButton.next").addClass("hidden");
                map.infoWindow.setTitle("");
                map.infoWindow.setContent(article["loading"]);
                map.infoWindow.show(event.mapPoint);
            }
        };
        setnotFeatureInfo = function (event) {
            if (notFeatureInfo) {
                $("#map_graphics_layer g").empty();
                $(".sizer.content #container").empty();
                if (!$(".titleButton.prev").hasClass("hidden"))
                    $(".titleButton.prev").addClass("hidden");
                if (!$(".titleButton.next").hasClass("hidden"))
                    $(".titleButton.next").addClass("hidden");
                map.infoWindow.setTitle("");
                map.infoWindow.setContent(article["notFeature"]);
                map.infoWindow.show(event.mapPoint);
            }
        };
        errorFeatureInfo = function (event) {
            if (notFeatureInfo) {
                $("#map_graphics_layer g").empty();
                $(".sizer.content #container").empty();
                if (!$(".titleButton.prev").hasClass("hidden"))
                    $(".titleButton.prev").addClass("hidden");
                if (!$(".titleButton.next").hasClass("hidden"))
                    $(".titleButton.next").addClass("hidden");
                map.infoWindow.setTitle("");
                map.infoWindow.setContent(article["notFeature"]);
                map.infoWindow.show(event.mapPoint);
            }
        };
        executeIdentifyTask = function (event, posx, posxb) {

            $(".container").remove();
            //  $("#container").remove() ;
            $(".sizer.content").prepend($("div[id^='containerc']"));
            //$(".sizer.content").prepend($("#container"));
            $(".titleButton.close").on("click", function () {
                $("#map_graphics_layer g").empty();
                $(".container").remove();
                $("#measure_container").css({"top":"165px"});
            });
            $(".titleButton.prev").unbind('click').on("click", function () {
                contGetFeature--;
                changeChartArea(contGetFeature);
                contFeatInfo--;
                checkHighChart(contGetFeature-1);
                var feature_info = $('.esriPopupWrapper').height();
                if (feature_info == 0) {
                    var totalHeight = 0;
                    $(".esriPopupWrapper").children().each(function(){
                        totalHeight = totalHeight + $(this).outerHeight(true);
                    });
                    $("#measure_container").css({"top":totalHeight+15});
                } else {
                    $("#measure_container").css({"top":feature_info+30});
                }
            });
            $(".titleButton.next").unbind('click').on("click", function () {
                contGetFeature++;
                changeChartArea(contGetFeature);
                contFeatInfo++;
                checkHighChart(contGetFeature-1);
                var feature_info = $('.esriPopupWrapper').height();
                if (feature_info == 0) {
                    var totalHeight = 0;
                    $(".esriPopupWrapper").children().each(function(){
                        totalHeight = totalHeight + $(this).outerHeight(true);
                    });
                    $("#measure_container").css({"top":totalHeight+15});
                } else {
                    $("#measure_container").css({"top":feature_info+30});
                }
            });
            var date = [];
            var values = [];
            var valuesc = [];
            var valuescbar = [];
            var val = [];
            var valcompass = [];
            var names = [];
            var valbar = [];
            var valcol = [];
            var valpie = [];
            var coltitle;
            var ALIASBAR = [];
            var deferred;

            if (intlayer[posx].TYPE === "wms") {
                ixlbwms = posx;
                wmsURLArray[posx] = {URL:intlayer[posx].MAPSERVICE,LAYERS:intlayer[posx].LAYERS};
            } else if (boxcontent === 0) {

                identifyTask = new IdentifyTask(layerInfo2[posxb].layer.url);

                identifyParams = new IdentifyParameters();
                identifyParams.tolerance = 8;
                identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
                identifyParams.returnFieldName = true;
                //      identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_TOP;
                identifyParams.width = map.width;
                identifyParams.height = map.height;
                var layerDefAry = [];
                species = $("#spec").find(":selected").val();
                speciesname = $("#spec").find(":selected").text();
                var namename = "";

                if (intlayer[posx].TYPE === "fish-dynamic") {
                    namename = ": " + speciesname;
                    for (var zed in intlayer[posx].IDENTIFYINFO) {
                        y = intlayer[posx].IDENTIFYINFO[zed].ID;

                        layerDefAry[y] = intlayer[posx].FISHLIST.FISHFIELDNAME.split(".")[1] + "= '" + species + "'";

                    }
                    identifyParams.layerDefinitions = layerDefAry; // ????
                }


                identifyParams.returnGeometry = true;
                identifyParams.geometry = event.mapPoint;
                identifyParams.mapExtent = map.extent;
                var idenfo = [];
                for (var k in intlayer[posx].IDENTIFYINFO) {
                    idenfo[k] = intlayer[posx].IDENTIFYINFO[k].ID;
                    if (intlayer[posx].IDENTIFYINFO[k].TYPE == "Url") {
                        identifyParams.timeExtent = this.map.timeExtent;
                    }
                }
                ;
                identifyParams.layerIds = idenfo;
                //Reference the chart theme here too
                var chartempl = [];

                function identifyCallBack(lotResults) {

                    var out = [];
                    var result = lotResults.results[0];
                    var feature = result.feature;
                    var attribute = feature.attributes;
                }


                deferred = identifyTask
                    .execute(identifyParams) //il response individua ID interrogato (layerID)
                    .addCallback(function (response) { //il response individua ID interrogato (layerID)
                        contResponse++;
                        if (contResponse == contLayersClicable) {
                            lastLayer = true;
                        }
                        if (response.length == 0) {
                        } else {
                            notFeatureInfo = false;
                        }

                        if (boxcontent === 0 && response.length > 0) {
                            var x = 0;
                            //var fifiID = identifyParams.layerIds.indexOf(response[x].layerId);
                            var fifiID = $.map(intlayer[posx].IDENTIFYINFO, function(obj,index) { if (obj.ID==response[0].layerId) return index;  });

                            var responsey = response;
                            response = [];
                            response = responsey.filter(function (item) {
                                return (item.layerId == responsey[x].layerId);
                            });

                            var dim = response.length;

                            chartchartarea = function (divse,result,labelField) {

                                $(divse).highcharts({
                                    chart: {
                                        type: "area"
                                    },
                                    title: {
                                        text: lai[posx].titleok + namename
                                    },
                                    subtitle: {
                                        text: "PERIOD: " + result.feature.dates[0].toString() + " - " + result.feature.dates[result.feature.dates.length - 1].toString()
                                    },
                                    xAxis: {
                                        categories: result.feature.dates,
                                        tickmarkPlacement: 'on',
                                        labels: {
                                            autoRotation: [-90]
                                        }
                                    },
                                    yAxis: {
                                        title: {
                                            text: "VALUES"
                                        }
                                    },
                                    plotOptions: {
                                        area: {
                                            fillColor: {
                                                linearGradient: {
                                                    x1: 0,
                                                    y1: 0,
                                                    x2: 0,
                                                    y2: 1
                                                },
                                                stops: [
                                                    [0, Highcharts.getOptions().colors[0]],
                                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
                                                ]
                                            }

                                        },
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: false
                                        }


                                    },
                                    series: [{
                                        name: result.feature.attributes[labelField],
                                        data: result.feature.values
                                    }

                                    ]
                                });
                            };

                            var multiserie = [{
                                name: names[0],
                                data: val[0]
                            }, {
                                name: names[1],
                                data: val[1]
                            }, {
                                name: names[2],
                                data: val[2]
                            }
                            ];

                            chartchartareaspline = function (divse,result,labelField) {								
                                $(divse).highcharts({
									title: {
                                        text: lai[posx].titleok + namename
                                    },
                                    subtitle: {
                                        text: "PERIOD: " + result.feature.dates[0].toString() + " - " + result.feature.dates[result.feature.dates.length - 1].toString()
                                    },
                                    xAxis: {
                                        categories: result.feature.dates,
                                        tickmarkPlacement: 'on',
                                        labels: {
                                            autoRotation: [-90]
                                        }
                                    },
                                    yAxis: {
                                        title: {
                                            text: "VALUES"
                                        }
                                    },
                                    plotOptions: {
										areaspline: {
											fillOpacity: 0.5
										

                                        },
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: false
                                        }


                                    },
                                    series: [{
										type: 'column',
                                        name: result.feature.attributes[labelField] +  " female",
                                        data: result.feature.values
                                    },
									{
										type: 'column',
                                        name: result.feature.attributes[labelField] + " male",
                                        data: result.feature.values2
                                    },
									{
										type: 'spline',
                                        name: result.feature.attributes[labelField] + " total",
                                        data: result.feature.values3
                                    }									
									
                                    ]
                                });
                            };


                            chartchartmulti = function (divse) {

                                $(divse).highcharts({
                                    chart: {
                                        type: "line"
                                    },
                                    title: {
                                        text: lai[posx].titleok + namename
                                    },
                                    subtitle: {
                                        text: "PERIOD: " + date[0].toString() + " - " + date[date.length - 1].toString()
                                    },
                                    xAxis: {
                                        categories: date
                                    },
                                    yAxis: {
                                        title: {
                                            text: "VALUES"
                                        }
                                    },
                                    plotOptions: {
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: false
                                        }
                                    },
                                    series: multiserie
                                });
                            };

                            chartchartpie = function (divse) {

                                $(divse).highcharts({
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false,
                                        type: "pie"
                                    },
                                    title: {
                                        text: pietitle
                                    },
                                    xAxis: {
                                        categories: valpie
                                    },
                                    yAxis: {},
                                    tooltip: {
                                        "width": "380px",
                                        useHTML: true,
                                        formatter: function () {

                                            return valpie[this.point.x].name + " = " + this.y + " " + unit;

                                        }

                                    },
                                    plotOptions: {
                                        pie: {
                                            allowPointSelect: true,
                                            cursor: "pointer",
                                            dataLabels: {
                                                enabled: true,
                                                formatter: function () {
                                                    if (this.y != 0) {
                                                        return this.point.percentage.toFixed(1) + " %";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                style: {
                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || "black"
                                                }
                                            },
                                            showInLegend: true
                                        }
                                    },
                                    legend: {
                                        layout: "vertical",
                                        backgroundColor: "#FFFFFF",
                                        verticalAlign: "bottom"

                                    },
                                    series: [{
                                        name: pietitle,
                                        data: valpie
                                    }]
                                });
                            };

                            chartchartbar = function (divse) {

                                $(divse).highcharts({
                                    chart: {
                                        type: "bar"
                                    },
                                    title: {
                                        text: lai[posx].titleok + namename
                                    },
                                    xAxis: {
                                        categories: fieldaliasestradraw
                                    },
                                    yAxis: {},
                                    plotOptions: {
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: false
                                        }
                                    },
                                    series: [{
                                        name: lai[posx].titleok,
                                        data: valbar
                                    }]
                                });
                            };

                            chartchartcolumn = function (divse) {

                                $(divse).highcharts({
                                    chart: {
                                        type: "column"
                                    },
                                    title: {
                                        text: coltitle
                                    },
                                    xAxis: {
                                        categories: valcol
                                    },
                                    yAxis: {},
                                    plotOptions: {
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: false
                                        }
                                    },
                                    tooltip: {
                                        "width": "260px",
                                        useHTML: true,
                                        formatter: function () {
                                            return "The value for <br/><b>" + valcol[this.point.x] + "</b> is <b>" + this.y + "</b>";
                                        }
                                    },
                                    series: [{
                                        name: lai[posx].titleok,
                                        data: valbar

                                    }]
                                });
                            };

                            chartchartcompass = function (divse) {



                                $(divse).highcharts({
                                    chart: {
                                        polar: true,
                                        type: "area"
                                    },
                                    title: {
                                        text: lai[posx].titleok + namename
                                    },
                                    xAxis: {
                                        categories: ["N", "NE", "E", "SE", "S",
                                            "SW", "W", "NW"
                                        ],
                                        tickmarkPlacement: "on",
                                        lineWidth: 0
                                    },
                                    yAxis: {
                                        gridLineInterpolation: "polygon",
                                        lineWidth: 0,
                                        min: 0
                                    },
                                    plotOptions: {
                                        line: {
                                            dataLabels: {
                                                enabled: true
                                            },
                                            enableMouseTracking: true
                                        }
                                    },
                                    series: [{
                                        name: "Values",
                                        data: valcompass,
                                        pointPlacement: "on"
                                    }]
                                });
                            };

                            if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE) {
                            
                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Compass") {

                                    //var ALIAS = ["W0", "W45", "W90", "W135", "W180", "W225", "W270", "W315"];
                                    for (var l in response) {

                                        valuesc[l] = response[l].feature.attributes;
                                        for (var x in fieldnames) {

                                            valuesc[x] = response[l].feature.attributes[fieldnames[x]];
                                            valcompass[x] = Number(Number(valuesc[x]).toFixed(2));
                                        }
                                    }
                                    response.splice(1, (response.length - 1));
                                } else

                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "BarChart" || intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "ColumnChart" || intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "PieChart") {


                                    var labelfi = intlayer[posx].IDENTIFYINFO[fifiID].FIELDS;
                                    for (var vals in labelfi) {

                                        ALIASBAR[vals] = labelfi[vals].toString();
                                    }

                                    var toltipfield = intlayer[posx].IDENTIFYINFO[fifiID].toptipfield; //check if 

                                    for (var l in response) {

                                        valuescbar[l] = response[l].feature.attributes;
                                        valuescbarkey = Object.keys(response[l].feature.attributes);
                                        output = {};
                                        main_functions.translater(valuescbarkey);
                                        valuescbarkeytrad = output;
                                        for (var x in fieldnames) {

                                            valuescbar[x] = response[l].feature.attributes[fieldnames[x]];
                                            valbar[x] = Number(Number(valuescbar[x]).toFixed(2));

                                            if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "ColumnChart") {
                                                valcol[x] = toltipfield[x]; //set field of toptipfield fo graph
                                                coltitle = intlayer[posx].DESCRIPTION_GRAPH;
                                            } else
                                            if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "PieChart") {
                                                valpie[x] = {
                                                    y: valbar[x],
                                                    name: toltipfield[x]
                                                };
                                                pietitle = intlayer[posx].DESCRIPTION_GRAPH;
                                            }
                                        }
                                    }
                                    // if (intlayer[posx].IDENTIFYINFO[fifiID].TIMEFIELD) {
                                    if (timeFieldStart[posx] !== "") {
                                    } else
                                        response.splice(1, (response.length - 1));
                                } else

                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "timeseries") {
                                    fieldaliases = [];
                                    fieldnames = [];
                                    for (var x in field4timeseries[posx]) {
                                        fieldaliases.push(field4timeseries[posx][x].alias);
                                        fieldnames.push(field4timeseries[posx][x].name);
                                    }
                                    ;

                                    for (var x in intlayer[posx].IDENTIFYINFO) {
                                        if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].VALUEFIELD) != -1) {
                                            var valuefix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].VALUEFIELD)]
                                        }
                                        if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].TIMEFIELD) != -1) {
                                            var timefix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].TIMEFIELD)];
                                        }
                                        if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].LABELFIELD) != -1) {
                                            var labelfix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].LABELFIELD)];
                                            break;
                                        }
                                    }

                                    for (var l in response) {
                                        values[l] = {
                                            data: response[l].feature.attributes[timefix],
                                            value: response[l].feature.attributes[valuefix],
                                            name: response[l].feature.attributes[labelfix]
                                        };
                                        for (var k = 0; k < dim; k++) {
                                            response[l].feature.attributes[response[k].feature.attributes[timefix]] = response[k].feature.attributes[valuefix];
                                        }
                                    }
                                    values.sort(main_functions.dynamicSort("data"));
                                    uniqueNames = [];
                                    names2 = [];
                                    for (var lix in values) {

                                        if (
                                            values[lix].name === response[0].feature.attributes[labelfix]
                                        ) {
                                            date[lix] = values[lix].data;
                                            val[lix] = Number(values[lix].value);
                                            names[lix] = values[lix].name;
                                        }
                                        names2[lix] = values[lix].name;
                                    }
                                    output = [];
                                    main_functions.translater(names); //translater Ã© una funzione per la traduzione generica di un array
                                    names = output;
                                    $.each(names2, function (i, el) {
                                        if ($.inArray(el, uniqueNames) === -1)
                                            uniqueNames.push(el);
                                    });
                                    output = [];
                                    main_functions.translater(uniqueNames); //translater Ã© una funzione per la traduzione generica di un array
                                    uniqueNames = output;

                                    var responsearray = [];
                                    for (var u2 in uniqueNames) {
                                        responsearray[uniqueNames[u2]] = [];
                                        for (var r1 in response) {
                                            if (response[r1].feature.attributes[labelfix] == uniqueNames[u2]) {
                                                responsearray[uniqueNames[u2]].push(response[r1]);
                                            }
                                        }
                                    }
                                    response = [];
                                    for (var ra1 in responsearray) {
                                        responsearray[ra1].sort(main_functions.dynamicSort("feature.attributes.time"));
                                        for (var ra2 = 0; ra2 < responsearray[ra1].length; ra2++) {
                                            responsearray[ra1][ra2].feature.dates = [];
                                            responsearray[ra1][ra2].feature.values = [];
                                            for (var k = 0; k < responsearray[ra1].length; k++) {
                                                responsearray[ra1][ra2].feature.attributes[responsearray[ra1][k].feature.attributes[timefix]] = responsearray[ra1][k].feature.attributes[valuefix];
                                            }
                                            for (var lix in values) {
                                                if (values[lix].name === ra1) {
                                                    responsearray[ra1][ra2].feature.dates.push(values[lix].data);
                                                    responsearray[ra1][ra2].feature.values.push(parseFloat(values[lix].value == "Null" ? 0 : values[lix].value));
                                                }
                                            }
                                        }
                                        responsearray[ra1].splice(1, (responsearray[ra1].length - 1));
                                        response.push(responsearray[ra1][0]);
                                    }
                                } else
                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "timemultiseries" ) {
                                    fieldaliases = [];
                                    fieldnames = [];
                                    for (var x in field4timeseries[posx]) {
                                        fieldaliases.push(field4timeseries[posx][x].alias);
                                        fieldnames.push(field4timeseries[posx][x].name);
                                    }
                                    

                                    for (var x in intlayer[posx].IDENTIFYINFO) {
										var valuefix= [];
                                        for (var j=0; j< intlayer[posx].IDENTIFYINFO[x].SERIES.length; j++) {											
											valuefix.push(fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].SERIES[j].VALUEFIELD)]);
											valuefix[j]= valuefix[j].split('.')[1];
										}
										if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].VALUEFIELD) != -1) {
                                            var valuefix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].VALUEFIELD)]
                                        }
                                        if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].TIMEFIELD) != -1) {
                                            var timefix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].TIMEFIELD)];
                                        }
                                        if (fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].LABELFIELD) != -1) {
                                            var labelfix = fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[x].LABELFIELD)];
                                            break;
                                        }
                                    }
									timefix= timefix.split('.')[1];
									labelfix= labelfix.split('.')[1];

                                    for (var l in response) {
                                        values[l] = {
                                            data: response[l].feature.attributes[timefix],
                                            value: [], //response[l].feature.attributes[valuefix],
                                            name: response[l].feature.attributes[labelfix]
                                        };
										for (j=0; j< valuefix.length; j++) {
											values[l].value.push( response[l].feature.attributes[valuefix[j]] );
										}
										
                                        for (var k = 0; k < dim; k++) {
                                            response[l].feature.attributes[response[k].feature.attributes[timefix]] = response[k].feature.attributes[valuefix];
                                        }
                                    }
                                    values.sort(main_functions.dynamicSort("data"));
                                    uniqueNames = [];
                                    names2 = [];
                                    for (var lix in values) {

                                        if (values[lix].name === response[0].feature.attributes[labelfix]) {
                                            date[lix] = values[lix].data;
                                            val[lix] = Number(values[lix].value);
                                            names[lix] = values[lix].name;
                                        }
                                        names2[lix] = values[lix].name;
                                    }
                                    output = [];
                                    main_functions.translater(names); //translater Ã© una funzione per la traduzione generica di un array
                                    names = output;
                                    $.each(names2, function (i, el) {
                                        if ($.inArray(el, uniqueNames) === -1)
                                            uniqueNames.push(el);
                                    });
                                    output = [];
                                    main_functions.translater(uniqueNames); //translater Ã© una funzione per la traduzione generica di un array
                                    uniqueNames = output;

                                    var responsearray = [];
                                    for (var u2 in uniqueNames) {
                                        responsearray[uniqueNames[u2]] = [];
                                        for (var r1 in response) {
                                            if (response[r1].feature.attributes[labelfix] == uniqueNames[u2]) {
                                                responsearray[uniqueNames[u2]].push(response[r1]);
                                            }
                                        }
                                    }
                                    response = [];
                                    for (var ra1 in responsearray) {
                                        responsearray[ra1].sort(main_functions.dynamicSort("feature.attributes.time"));
                                        for (var ra2 = 0; ra2 < responsearray[ra1].length; ra2++) {
                                            responsearray[ra1][ra2].feature.dates = [];
                                            responsearray[ra1][ra2].feature.values = [];																						
											responsearray[ra1][ra2].feature.values2 = [];
											responsearray[ra1][ra2].feature.values3 = [];
                                            for (var k = 0; k < responsearray[ra1].length; k++) {
                                                responsearray[ra1][ra2].feature.attributes[responsearray[ra1][k].feature.attributes[timefix]] = responsearray[ra1][k].feature.attributes[valuefix];
                                            }
                                            for (var lix in values) {
                                                if (values[lix].name === ra1) {
                                                    responsearray[ra1][ra2].feature.dates.push(values[lix].data);
                                                    responsearray[ra1][ra2].feature.values.push(parseFloat(values[lix].value[0] == "Null" ? 0 : values[lix].value[0]));																										
													responsearray[ra1][ra2].feature.values2.push(parseFloat(values[lix].value[1] == "Null" ? 0 : values[lix].value[1]));
													responsearray[ra1][ra2].feature.values3.push(parseFloat(values[lix].value[2] == "Null" ? 0 : values[lix].value[2]));
													/*
													if (lix %2==0) {
														responsearray[ra1][ra2].feature.values2.push( parseFloat(values[lix].value[0]*0.8));
														responsearray[ra1][ra2].feature.values3.push(parseFloat(values[lix].value[0]) + parseFloat(values[lix].value[0]*0.8));
													}	
													else  {
														responsearray[ra1][ra2].feature.values2.push( parseFloat(values[lix].value[0]*1.2));
														responsearray[ra1][ra2].feature.values3.push(parseFloat(values[lix].value[0]) + parseFloat(values[lix].value[0]*1.2));
													}
													
													*/													
                                                }
                                            }
                                        }
                                        responsearray[ra1].splice(1, (responsearray[ra1].length - 1));
                                        response.push(responsearray[ra1][0]);
                                    }
                                } else

                                ///////////////////////////////////////////////////////                 
                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "html") {
                                    var valuehtmlfield = intlayer[posx].IDENTIFYINFO[fifiID].HTMLFIELD;
                                } else
                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Array")

                                ///////////////////////////////////////////////////////                

                                {
                                    var valuefields = intlayer[posx].IDENTIFYINFO[0].FIELDS;
                                } else
                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Url")

                                ///////////////////////////////////////////////////////                

                                {

                                    if (intlayer[posx].IDENTIFYINFO[0].FIELDS[0].split(".")[1]) {

                                        var valueUrlfields = intlayer[posx].IDENTIFYINFO[0].FIELDS[0].split(".")[1];
                                    } else {
                                        var valueUrlfields = intlayer[posx].IDENTIFYINFO[0].FIELDS;
                                    }
                                }

                                if ((intlayer[posx].IDENTIFYINFO[fifiID].TYPE !== "timeseries" && 
								intlayer[posx].IDENTIFYINFO[fifiID].TYPE !== "timemultiseries"  )   && (intlayer[posx].TYPE === "fish-dynamic" || timeFieldStart[posx] !== "")) {
                                    var responsex = response;
                                    response = [];
                                    response = responsex.filter(function (item) {

                                        if (timeFieldEnd)
                                            return (parseInt(item.feature.attributes[timeFieldStartAlias]) <= currentYear && currentYear <= parseInt(item.feature.attributes[timeFieldEndAlias]));
                                        else
                                            return (parseInt(item.feature.attributes[timeFieldStartAlias]) == currentYear);
                                    });
                                }

                                if (response.length > 0) {
                                    var typ = intlayer[posx].IDENTIFYINFO[fifiID].TYPE;
                                    if (typ === "ColumnChart" || typ === "BarChart" || typ === "timeseries" || typ === "timemultiseries"  || typ === "PieChart" || typ === "Compass") {                                        
                                        if (contDeferred == 0) {
                                            $('#containerc'+(contDeferred+1)).show();
                                        }
                                    } else {
                                        if (contDeferred == 0) {
                                            $('.container').hide();
                                        }
                                    }
                                }

                            }

                            arrayUtils.map(response, function (result) {

                                var typ = intlayer[posx].IDENTIFYINFO[fifiID].TYPE;
                                if (typ === "ColumnChart" || typ === "BarChart" || typ === "timeseries" || typ === "timemultiseries" || typ === "PieChart" || typ === "Compass") {
                                    arrayHighChartdeferred[contDeferred] = true;
                                    contDeferred++;
                                } else {
                                    arrayHighChartdeferred[contDeferred] = false;
                                    contDeferred++;
                                }

                                x = x + 1;
                                var feature = result.feature;
                                var feature2 = Object.keys(feature.attributes);
                                output = {};
                                main_functions.translater(feature2); //translater is a function to translate a generic array usoing translation table in db
                                for (var inn in output) {
                                    feature.attributes[output[inn]] = feature.attributes[feature2[inn]]; // it replaces the keys with their translations
                                    if (output[inn] !== feature2[inn])
                                        delete feature.attributes[feature2[inn]]; // delete the voice only after replaced by translation
                                }

                                output = {};
                                main_functions.translater(feature.attributes);
                                for (var inn2 in feature.attributes) {
                                    feature.attributes[inn2] = output[inn2]; // Translated values
                                }


                                if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE) {

                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "timeseries") {
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
                                        chartchartarea("#"+id_container,result,fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[fifiID].LABELFIELD)]);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else
                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "timemultiseries") {
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
										var labelF= fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[fifiID].LABELFIELD)].split('.')[1];
										//var labelF= fieldnames[fieldnames.indexOf(intlayer[posx].IDENTIFYINFO[fifiID].LABELFIELD)];
                                        chartchartareaspline("#"+id_container,result,labelF);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else		
                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Compass") {
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
                                        chartchartcompass("#"+id_container);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else

                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "BarChart") {
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
                                        chartchartbar("#"+id_container);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else
                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "PieChart") {
                                        unit = intlayer[posx].IDENTIFYINFO[fifiID].UNIT;
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
                                        chartchartpie("#"+id_container);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else
                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "ColumnChart") {
                                        if (arraydeferred.length == 0) {
                                            var e = $('<div class="container" style="width: 400px;"></div>');
                                        } else {
                                            var e = $('<div class="container" style="display:none; width: 400px"></div>');
                                        }
                                        $('#container2').append(e);
                                        var id_container = "containerc" + (contDeferred);
                                        e.attr('id', id_container);
                                        chartchartcolumn("#"+id_container);
                                        $(".sizer.content").prepend(e);
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok,"&nbsp"); //SET TITLE
                                        feature.setInfoTemplate(custTemplate0);
                                    } else

                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Array") {
                                        //  var infoTemplateZAZA = new InfoTemplate("Attributes", "Name : " + fieldaliasestrad[0] + "<br/> State : " + fieldaliasestrad[1] + "<br />Population : " + fieldaliasestrad[2] + "");
                                        var infoTemplateZAZA = new InfoTemplate(lai[posx].titleok, fieldaliasestradraw3[posx]);
                                        feature.setInfoTemplate(infoTemplateZAZA);
                                    } else

                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "html") {
                                        var custTemplate0 = new InfoTemplate(lai[posx].titleok, result.feature.attributes[valuehtmlfield]);
                                        feature.setInfoTemplate(custTemplate0);
                                    } else
                                    if (intlayer[posx].IDENTIFYINFO[fifiID].TYPE === "Url")

                                    ///////////////////////////////////////////////////////                

                                    {

                                        var kurl = result.feature.attributes[valueUrlfields];
                                        kurl = "<a href='"+kurl.replace("%LANG%", lang)+"' target='_blank'>"+kurl+"</a>";
                                        var custTemplateUrl = new InfoTemplate(lai[posx].titleok, kurl);
                                        feature.setInfoTemplate(custTemplateUrl);
                                        /*mywindow = window.open(kurl.replace("%LANG%", lang), "Print_Output", "menubar=1,resizable=1,width=800,height=600");
                                        mywindow.moveTo(200, 200);
                                        map.infoWindow.hide();*/
                                    } else
                                        alert("'type' error in 'serviceinfos' file");

                                }

                                arraydeferred.push(feature);
                            });
                        }
                        if (lastLayer) {
                            if (arraydeferred.length > 0) {
                                map.infoWindow.setFeatures(arraydeferred);
                                contFeatInfo = 0;
                            } else {
                                $("#map_graphics_layer g").empty();
                                $(".sizer.content #container").empty();
                                if (!$(".titleButton.prev").hasClass("hidden"))
                                    $(".titleButton.prev").addClass("hidden");
                                if (!$(".titleButton.next").hasClass("hidden"))
                                    $(".titleButton.next").addClass("hidden");
                                map.infoWindow.setTitle("");
                                map.infoWindow.setContent(article["notFeature"]);
                                map.infoWindow.show(event.mapPoint);
                            }
                        }
                    });

                    isEmpty = function (myObject) {
                        for (var key in myObject) {
                            if (myObject.hasOwnProperty(key)) {
                                return false;
                            }
                        }
                        return true;
                    };
                    // InfoWindow expects an array of features from each deferred
                    // object that you pass. If the response from the task execution
                    // above is not an array of features, then you need to add a callback
                    // like the one above to post-process the response and return an
                    // array of features.

                    //  map.infoWindow.setFeatures([deferred]);
                
                checknotFeatureInfo(event);
                
                if (boxcontent === 0)
                    map.infoWindow.show(event.mapPoint);
                boxcontent = 0;
            }

        };

        mapReady = function () {
            if (urlobj.p == "w" ) {
                maxExtent = new esri.geometry.Extent({
                    "xmin": -20036395.14788131,
                    "ymin": -13372419.930173635,
                    "xmax": 20036395.14788131,
                    "ymax": 13372419.930173635,
                    "spatialReference": new esri.SpatialReference({wkid: 102100})});
            } else {
                maxExtent = map.extent;
            }
            identifyHandler = map.on("click", function (event) {

                if (measurement.activeTool || measurement1.activeTool || measurement2.activeTool) { //disable indentyfy task when measure tool is active
                    var newCenter = map.extent.getCenter();
                    map.centerAt(newCenter);
                } else {
                    execute2(event);
                }
                closeinfolayer();
            });
            ;
            //create identify tasks and setup parameters
            //  main_functions.hideLoading;
        };
        ///////////////////////////////////////////          //BASEMAP DEFINE
        defineBasemaps= function() {

            var base = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === worldBackgroundLayers[1]);
                })[0].MAPSERVICE
                //    content: {                    f: "pjson"                }
            });
            var basem = new Basemap({
                id: worldBackgroundLayers[1],
                itemId: "1",
                //   content: {                    f: "pjson"                },
                layers: [base],
                title: "Base Template",
                thumbnailUrl: "img/terrain.png"
            });


            var oce = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === worldBackgroundLayers[0]);
                })[0].MAPSERVICE
            });
            var ocem = new Basemap({
                id: worldBackgroundLayers[0],
                itemId: "2",
                layers: [oce],
                title: "Water Template",
                thumbnailUrl: "img/oceans.jpg"
            });

            var base2 = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === worldBackgroundLayers[3]);
                })[0].MAPSERVICE
            });
            var basem2 = new Basemap({
                id: worldBackgroundLayers[3],
                itemId: "3",
                layers: [base2],
                title: "Civil Template",
                thumbnailUrl: "img/imagery.jpg"
            });


            var oce2 = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === worldBackgroundLayers[2]);
                })[0].MAPSERVICE
            });
            var ocem2 = new Basemap({
                id: worldBackgroundLayers[2],
                itemId: "4",
                layers: [oce2],
                title: "Ocean Reference",
                thumbnailUrl: "img/ago_downloaded.jpg"
            });


            var arctibase = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === articBackgroundLayers[0]);
                })[0].MAPSERVICE
            });
            var arctibasem = new Basemap({
                id: articBackgroundLayers[0],
                itemId: "5",
                layers: [arctibase],
                title: "Arctic Template",
                thumbnailUrl: "img/oceans.jpg"
            });

            var arctisimplebase = new BasemapLayer({
                url: SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === articBackgroundLayers[1]);
                })[0].MAPSERVICE
            });
            var arctisimplebasem = new Basemap({
                id: articBackgroundLayers[1],
                itemId: "6",
                layers: [arctisimplebase],
                title: "Basic Arctic Template",
                thumbnailUrl: "img/oceansbn.jpg"
            });

            var mapbase = {};

            if (urlobj.p === "w") {
                mapbase = ocem;
                if(parameterinurl){
                    var xyCenter = urlobj.c.split(",");
                    if(!isNaN(xyCenter[1]) && !isNaN(xyCenter[0])){
                        if (urlobj.c && urlobj.c !== "undefined") {
                            var centerPoint = new esri.geometry.Point({
                            "x": xyCenter[0],
                            "y": xyCenter[1],
                            "spatialReference": new esri.SpatialReference({ wkid: 102100 })
                            });
                            mapbase.center = centerPoint;
                        }
                        if (urlobj.z && urlobj.z !== "undefined") {
                            mapbase.zoom = urlobj.z;
                        }
                    }else{
                        mapbase.center = [11, 50]; // long, lat
                        mapbase.zoom = 4;
                    }
                }else{
                    mapbase.center = [11, 50]; // long, lat
                    mapbase.zoom = 4;
                }


                mapbase.lods = [
                    {
                        level: 0,
                        resolution: 39135.7584820001,
                        scale: 147914381.897889
                    },
                    {
                        level: 1,
                        resolution: 19567.8792409999,
                        scale: 73957190.948944
                    },
                    {
                        level: 2,
                        resolution: 9783.93962049996,
                        scale: 36978595.474472
                    },
                    {
                        level: 3,
                        resolution: 2445.98490512499,
                        scale: 9244648.868618
                    },
                    {
                        level: 4,
                        resolution: 1222.99245256249,
                        scale: 4622324.434309
                    },
                    {
                        level: 5,
                        resolution: 305.748113140558,
                        scale: 1155581.108577
                    },
                    {
                        level: 6,
                        resolution: 152.874056570411,
                        scale: 577790.554289
                    },
                    {
                        level: 7,
                        resolution: 38.2185141425366,
                        scale: 144447.638572
                    },
                    {
                        level: 8,
                        resolution: 19.1092570712683,
                        scale: 72223.819286
                    }



                ];
            } //"oceans"; //topo //streets   INITIAL BASEMAP basem; basem2; ocem
            else {
                mapbase = arctibasem;
                if(parameterinurl){
                    var xyCenter = urlobj.c.split(",");
                    if(!isNaN(xyCenter[1]) && !isNaN(xyCenter[0])){
                        if (urlobj.c && urlobj.c !== "undefined") {
                            var xyCenter = urlobj.c.split(",");
                            var centerPoint = new esri.geometry.Point({
                                "x": xyCenter[0],
                                "y": xyCenter[1],
                                "spatialReference": new esri.SpatialReference({ wkid: 102017 })
                            });
                            mapbase.center = centerPoint;
                        }
                        if (urlobj.z && urlobj.z !== "undefined") {
                            mapbase.zoom = urlobj.z;
                        }
                    }else{
                        mapbase.center = [100, 100];      
                        mapbase.zoom = 1;
                    }
                }else{
                    mapbase.center = [100, 100]; // long, lat       
                    mapbase.zoom = 1;
                }
                mapbase.lods = [
                    {
                        level: 0,
                        resolution: 33072.9828126323,
                        scale: 125000000
                    },
                    {
                        level: 1,
                        resolution: 16933.367200067736,
                        scale: 64000000
                    },
                    {
                        level: 2,
                        resolution: 8466.683600033868,
                        scale: 32000000
                    },
                    {
                        level: 3,
                        resolution: 4233.341800016934,
                        scale: 16000000
                    },
                    {
                        level: 4,
                        resolution: 2116.670900008467,
                        scale: 8000000
                    }];


                var startExtent = new esri.geometry.Extent({
                    "xmin": -14227887.35,
                    "ymin": -6665741.89,
                    "xmax": 14020169.55,
                    "ymax": 8113220.94,
                    "spatialReference": new esri.SpatialReference({ wkid: 102017 })
                });

            }

            esriConfig.defaults.geometryService = new esri.tasks.GeometryService("https://maratlas.discomap.eea.europa.eu/arcgis/rest/services/Utilities/Geometry/GeometryServer");

            map = Map("map", {
                zoom: mapbase.zoom, // long, lat
                center: mapbase.center,
                extent: startExtent,
                //lods: mapbase.lods,
                sliderStyle: "large"

            });


            map.on("load", mapReady);

            if (urlobj.p === "w") {
                basemaps.push(ocem);
                basemaps.push(ocem2);
                basemaps.push(basem);
                basemaps.push(basem2);
            }
            else {
                basemaps.push(arctibasem);
                basemaps.push(arctisimplebasem);
            }
        }
        //  add the legend
        /////////////////////////////////////////////CREATE LEGEND FUNCTION
        makelegend = function () {
            layerInfo = [];
            for (var i in lai) {
                for (var h in lai[i].layerInfos) {
                    if (lai[i].layerInfos[h].name.charAt(0) === "{") {
                        for (var attr in featureAttributes1) {
                            var featureAttributes = featureAttributes1[attr];
                            if (lai[i].layerInfos[h].name === featureAttributes.UNIQUEID) {
                                lai[i].layerInfos[h].name1 = featureAttributes.TRANSLATION; //.esriLegendLayerLabel2
                            }
                        }
                    } else {
                        lai[i].layerInfos[h].name1 = lai[i].layerInfos[h].name;
                    }
                }


                layerInfo[i] = {
                    layer: lai[i],
                    title: lai[i].titleok,
                    pos: lai[i].pos
                };
                curLayerConfig= SERVICES_CONFIG.filter(function (item) {
					return (item.ID == lai[i].idd );
				})[0];
				
				if ((curLayerConfig.LEGEND || "") !=="") {
                    lai[i].LEGEND = curLayerConfig.LEGEND;
                }
                
            }

            arrayUtils.map(layerInfo, function (layer, y) {
                if (typeof layer !== "undefined") { // if layer exist
                    {
                        if (typeof layer.title !== "undefined") //if name exist
                        {
                            if (layer.title.substr(layer.title.length - 3) === "lbl") {
                            } //if is not a label layer do nothing
                        } else
                            layerInfo.splice(y, 1); // if is a label layer delete item from legend source
                    }
                }
            });


            if (layerInfo.length > 0) {
                layerInfo.sort(main_functions.dynamicSort("pos"));

                if (typeof legendDijit !== "undefined") {
                    legendDijit.destroy();
                }
                ;

                var ndiv = domconstruct.create("div", {
                    id: "legendDiv"
                });
                document.getElementById("legendPane").appendChild(ndiv);
                legendDijit = new Legend({
                    map: map,
                    layerInfos: layerInfo
                }, "legendDiv");
                legendDijit._buildLegendItems_Tools = customBuildLegendItems; // replace original function _buildLegendItems_Tools with customBuildLegendItems
                old = legendDijit._buildRow_Tools;
                legendDijit._buildRow_Tools = modfun1; // replace original function _buildRow_Tools with  modfun1
                legendDijit.startup();
            }
        };


        ///////////////////////////////////////////// TIME SLIDER FUNCTION
        timeSlider = new TimeSlider({
            style: "width: 100%;"
        }, dom.byId("timeSliderDiv"));

        initSlider = function (lowest, highest, iy) {

            if (iy === "0") {
                map.setTimeSlider(timeSlider);
            } else {
            }

            var timeExtent = new TimeExtent();
            //   timeExtent.startTime = layerInfo[ix].layer.timeInfo.timeExtent.startTime;
            //  timeExtent.endTime = layerInfo[ix].layer.timeInfo.timeExtent.endTime;

            var low= new Date(highest);
            low.setFullYear( (new Date(highest)).getFullYear()-1  );
            
            timeExtent.startTime =  low;
            timeExtent.endTime = new Date(highest);


            var TSFYB = timeExtent.startTime.getUTCFullYear();
            var TSFYE = timeExtent.endTime.getUTCFullYear();

            var d = new Date();
            var n = d.getFullYear();



            timeSlider.setThumbCount(1);
            timeSlider.createTimeStopsByTimeInterval(timeExtent, 1, "esriTimeUnitsYears");


            if (timeExtent.startTime < d && d < timeExtent.endTime)
                timeSlider.setThumbIndexes([n - TSFYB]);
            else
                timeSlider.setThumbIndexes([timeSlider.timeStops.length - 1]);
            timeSlider.loop = false;
            timeSlider.singleThumbAsTimeInstant(true);
            timeSlider.setThumbMovingRate(2000);
            timeSlider.startup();
            //add labels for every other time stop
            var labels = arrayUtils.map(timeSlider.timeStops, function (timeStop, i) {
                if (i % 0 === 0) { //%0 setting to clean all label for the steps
                    return timeStop.getUTCFullYear();
                } else {
                    return "";
                }
            });
            labels[0] = timeSlider.timeStops[0].getUTCFullYear(); //label start timeslider
            labels[timeSlider.timeStops.length - 1] = timeSlider.timeStops[timeSlider.timeStops.length - 1].getUTCFullYear(); //label end timeslider
            lastyear = timeSlider.timeStops[timeSlider.timeStops.length - 1].getUTCFullYear(); //label end timeslider


            timeSlider.setLabels(labels);
            if (timeExtent.startTime < d && d < timeExtent.endTime)
                dom.byId("daterange").innerHTML = "<i>" + n + "<\/i>";
            else {
                dom.byId("daterange").innerHTML = "<i>" + lastyear + "<\/i>";
                currentYear = lastyear;
            }

            timeSlider.next();

            timeSlider.on("time-extent-change", function (evt) {
                var startValString = evt.startTime.getUTCFullYear();
                var endValString = evt.endTime.getUTCFullYear();
                //     dom.byId("daterange").innerHTML = "<i>" + startValString + " <-->  " + endValString + "<\/i>";
                dom.byId("daterange").innerHTML = "<i>" + endValString + "<\/i>";
                currentYear = endValString;
                main_functions.specieslink(); //?????????????????????? update current  year for fishlayer
            });
        };


        ///////////////////////////////////////////// TABLE OF CONTENT FUNCTION

        /////////////////// REORDER FUNCTION ;  layerIds[0] to exclude in reorder

        reor = function (lain, pos) { // reorder function ;  layerIds[0] is baselayer                  
            map.reorderLayer(map.layerIds[lain], pos);
            checkb("0");
            makelegend();
            closeinfolayer();
        };
        ///////////////////////////////////////////////////// CHECK MENU FUNCTIONS
        var myOpentip;
        checkb = function (xa) {


            main_functions.assigntitle();
            main_functions.translater(description);
            descriptiony = output;
            main_functions.assigndescription();

            var scrollPosition = $("#layerPane").scrollTop();

            $("#toggleCB1").html("");

            $("#toggleCB1").sortable({
                start: function (event, ui) {
                    ui.item.startPos = ui.item.index();
                    closeinfolayer();
                },
                stop: function (event, ui) {
                    var start = map.layerIds.length - ui.item.startPos - 1;
                    var stop = map.layerIds.length - 1 - ui.item.index();

                    reor(start, stop);
                }
            });


            layerInfo2 = [];
            ////////////////////////////


            for (var i = 0; i < map.layerIds.length - 1; i++) {
                k = i + 1;
                layerInfo2[i] = {
                    layer: map._layers[map.layerIds[k]],
                    title: map._layers[map.layerIds[k]].titleok,
                    pos: map.layerIds.indexOf(map._layers[map.layerIds[k]].id) - 1, //GET POSITION FROM MAP
                    //  pos:merg[i].pos,
                    idd: map._layers[map.layerIds[k]].idd,
                    alpha: map._layers[map.layerIds[k]].opacity,
                    type: map._layers[map.layerIds[k]].type,
                    identifyinfo: map._layers[map.layerIds[k]].identifyinfo,
                    descriptionx: map._layers[map.layerIds[k]].descriptionx

                };
            }
            /////////////////////////////////CREATION OF Label in TOC from relative layer

            listlayer = [];
            listlayer1 = [];
            listlayerx2 = [];
            listlayer1x2 = [];
            layerInfo2.sort(main_functions.dynamicSort("pos")); //REORDER LAYER TOC BY POSITION IN MAP

            arrayUtils.forEach(layerInfo2, function (listlayerxx, ind) {

                arrayUtils.forEach(lai, function (zz, ind2) {
                    if (listlayerxx.idd === lai[ind2].idd) {
                        lai[ind2].pos = listlayerxx.pos;
                    }
                });
                listlayer[ind] = {
                    idd: listlayerxx.idd,
                    alpha: listlayerxx.alpha

                };
                listlayer1[ind] = {
                    idd: listlayerxx.idd,
                    alpha: listlayerxx.alpha

                };
            });

            var topLayer;
            contLayersClicable = 0;
            ///START foreach layerinfo

            arrayUtils.forEach(layerInfo2, function (layer, ind) {


                var layerName;

                //CREATION OF TITLE FOR FOR LABEL

                if (typeof layer.layer.titleok !== "undefined") {
                    layerName = layer.layer.titleok;
                } else {
                    for (var ixlc in lai) {

                        if (layer.layer.serv === lai[ixlc].serv) {
                            //layerName = "Label-" + labtras2[ind - lai2.length].TRANSLATION;
                            layerName = "Label-" + lai[ixlc].titleok //Label in TOC from relative layer

                            ;
                        }
                    }
                }


                //CREATION OF CHECKBOX
                // var checkBox = new CheckBox({
                //     name: "checkBox" + layer.layer.id,
                //     value: layer.layer.id,
                //     checked: layer.layer.visible
                // });
                // checkBox.on("change", function () {
                //     var targetLayer = map.getLayer(this.value);
                //     var targetLayerServ = map.getLayer(this.value).serv;
                //     targetLayer.setVisibility(!targetLayer.visible);
                //     this.checked = targetLayer.visible;
                //     var targetLayerLab;
                //     for (var cx in lai2) {
                //         if (lai2[cx].serv === targetLayerServ)
                //             if (lai2[cx].id === targetLayer.id) {

                //             } else {
                //                 targetLayerLab = lai2[cx];
                //                 // targetLayerLab.setVisibility(!targetLayerLab.visible);
                //                 if (targetLayer.visible) {
                //                     targetLayerLab.setVisibility(true);
                //                 } else if (!targetLayer.visible) {
                //                     targetLayerLab.setVisibility(false);
                //                     //showinfolayer.setVisibility(false); // to hide info when check box is not selected.
                //                 }

                //             }
                //     }
                //     closeinfolayer();
                //     checkb("1"); //enable or disable time slider

                //     if (targetLayer.lab !== "label")
                //         initsli("1");
                // });
                //////////////////////////////add the check box and label to the toc

                var posizione = map.layerIds.indexOf(layer.layer.id);
                var idxx = layer.layer.id;

                // var checkLabel = domconstruct.create("label", {
                //     "for": checkBox.name,
                //     innerHTML: layerName
                // }, checkBox.domNode, "after");

                domconstruct.place("<div id=toggleCBX" + idxx + " class=toggleCBX ></div>", dom.byId("toggleCB1"), "first");

                //SELECT THE THUMBNAIL OF LAYERS FOR THE DISTINCT TYPES OF LAYERS
                var thumbnailLayerUrl;
                thumbnailLayerUrl = "img/thumbnails/thumb_" + layer.idd + ".png";

                if (layer.layer.lab) {
                    domconstruct.place("<div id=layerContainer" + idxx + " style=\"width: 80%; height: 50px; margin:0 auto\"></div>", dom.byId("toggleCBX" + idxx), "first");
                } else {
                    if (layer.layer.visible && ((layer.identifyinfo && layer.type=="rest")||(layer.type!="rest" && layer.type!="webtiled")) ) {
                        contLayersClicable++;
                        topLayer = idxx;
                    }
                    domconstruct.place("<div id=layerContainer" + idxx + " style=\"width: 80%; height: 100px; margin:0 auto\"></div>", dom.byId("toggleCBX" + idxx), "first");
                }
                for (var lws in layersWithSettings) {
                    var found = false;
                    if (idxx == layersWithSettings[lws]) {
                        found = true;
                        domconstruct.place("<div id=layerThumbnailContainer" + idxx + ' class=\"cont'+idxx+ " map_cont\" style=\"width: 80%; height: 100%; float: left; display: none\"><img class=\"layerThumbnail\" src=\"" + thumbnailLayerUrl + "\" onError=\"this.onerror=null;this.src='img/noimage.png';\" style=\"width: 100%; height: 100%;\"></div>",dom.byId("layerContainer" + idxx ), "first");
                        domconstruct.place("<div id=layerSettingsContainer" + idxx + ' class=\"cont' +idxx+ " set_cont\"></div>",dom.byId("layerContainer" + idxx ), "first");
                        break;
                    }
                }
                if (!found) {
                    domconstruct.place("<div id=layerThumbnailContainer" + idxx + ' class=\"cont'+idxx+ " map_cont\" style=\"width: 80%; height: 100%; float: left\"><img class=\"layerThumbnail\" src=\"" + thumbnailLayerUrl + "\" onError=\"this.onerror=null;this.src='img/noimage.png';\" style=\"width: 100%; height: 100%;\"></div>",dom.byId("layerContainer" + idxx ), "first");
                    domconstruct.place("<div id=layerSettingsContainer" + idxx + ' class=\"cont' +idxx+ " set_cont\" style=\"display: none\"></div>",dom.byId("layerContainer" + idxx ), "first");
                }

                if (layer.layer.lab) {
                    domconstruct.place("<div id=otherSettingsContainer" + idxx + " style=\"width: 100%; height: 50%; display: table;\"></div>",dom.byId("layerSettingsContainer" + idxx ), "first");
                } else {
                    domconstruct.place("<div id=sliderContainer" + idxx + " title='Transparency' class='sliderContainer' style=\"width: 100%; height: 50%\"></div>",dom.byId("layerSettingsContainer" + idxx ), "first");
                
                    domconstruct.place("<br /><input  id=myslider-" + idxx + " value=\"" + layer.layer.opacity + "\" type=\"text\"  data-slider=\"true\" data-slider-theme=\"volume\" data-slider-snap=\"true\" data-slider-step=\"0.01\"  data-slider-equal-steps=\"true\">", dom.byId("sliderContainer" + idxx ), "first");
                    $("#myslider-" + idxx).simpleSlider();
                    $("#myslider-" + idxx + "-slider.slider").after("<span  id=myspan" + idxx + " style=\"position: relative;\">" + ((layer.layer.opacity)*100).toFixed(0) + "%</span>");
                    $("#myspan" + idxx).position({
                       my:        "left top",
                       at:        "left bottom",
                       of:        $("#myslider-" + idxx + "-slider.slider").find(".dragger"),
                       collision: "flip"
                    });
                    $("#myslider-" + idxx).bind("slider:ready slider:changed", function (event, data) {
                        $("#myspan" + idxx).replaceWith("<span  id=myspan" + idxx + " style=\"position: relative;\">" + (data.value*100).toFixed(0) + "%</span>");
                        $("#myspan" + idxx).position({
                            my:        "left top",
                            at:        "left bottom",
                            of:        $("#myslider-" + idxx + "-slider.slider").find(".dragger"),
                            collision: "flip"
                        });
                        var idxxc = event.target.id.substr(event.target.id.indexOf("-") + 1);
                        setopacity(idxxc, data.value.toFixed(2));
                    });
    
                    domconstruct.place("<div id=otherSettingsContainer" + idxx + " style=\"width: 100%; height: 50%; display: table;\"></div>",dom.byId("sliderContainer" + idxx ), "after");
                }
                
                domconstruct.place("<div id=rowSettingsContainer" + idxx + " style=\"display: table-row;\"></div>",dom.byId("otherSettingsContainer" + idxx ), "first");
                //check if layer is hidden
                var targetLayer = map.getLayer(idxx);
                if (targetLayer.visible) {
                    domconstruct.place("<a id=hidebut onclick= hidelayer(\"" + idxx + "\");><div id=hideButton" + idxx + " title='Hide/Show layer' class=hideButton><i class=\"fas fa-eye\"></i></div></a>", dom.byId("rowSettingsContainer" + idxx), "first");
                } else{
                    domconstruct.place("<a id=hidebut onclick= hidelayer(\"" + idxx + "\");><div id=hideButton" + idxx + " title='Hide/Show layer' class=hideButton><i class=\"fas fa-eye-slash\"></i></div></a>", dom.byId("rowSettingsContainer" + idxx), "first");
                }
                if (posizione < (layerInfo2.length)) //first up arrow disabled
                {
                    var pallo2 = posizione + 1;
                    domconstruct.place("<a id=upbut onclick=reor(" + posizione + "," + pallo2 + ");><div id=upButton" + idxx + " title='Move layer up' class=upButton><i class=\"fas fa-arrow-up\"></i></div></a>", dom.byId("hidebut"), "after");
                    arrayUtils.forEach(listlayer, function (layerx, ind) {
                        if (listlayer[ind].idd) {
                            listlayerx2[ind] = listlayer[ind].idd + ":" + listlayer[ind].alpha;
                        } else {
                            listlayerx2[ind] = "";
                        }

                    });
                    listlayerx2ok = $.grep(listlayerx2, function (n) {
                        return (n);
                    }); //remove empty field  

                    main_functions.url2que(listlayerx2ok);
                } else {
                    domconstruct.place("<a id=upbut><div id=upButtonDis" + idxx + " title='Move layer up' class=upButtonDis><i class=\"fas fa-arrow-up\"></i></div></a>", dom.byId("hidebut"), "after"); 
                }

                if (posizione > 1) //last down arrow disabled
                {
                    var pallo = posizione - 1;
                    listlayer1x = listlayer;
                    arrayUtils.forEach(listlayer, function (layerx, ind) {

                        if (listlayer[ind].idd) {
                            listlayer1x2[ind] = listlayer[ind].idd + ":" + listlayer[ind].alpha;
                        } else
                            listlayer1x2[ind] = "";
                    });
                    domconstruct.place("<a id=downbut onclick=reor(" + pallo + "," + posizione + ");><div id=downButton" + idxx + " title='Move layer down' class=downButton><i class=\"fas fa-arrow-down\"></i></div></a>", dom.byId("upbut"), "after");

                    listlayer1x2ok = $.grep(listlayer1x2, function (n) {
                        return (n);
                    }); //remove empty field                       
                }  else {
                    pallo = posizione;
                    domconstruct.place("<a id=downbut><div id=downButtonDis" + idxx + " title='Move layer down' class=downButtonDis><i class=\"fas fa-arrow-down\"></i></div></a>", dom.byId("upbut"), "after");
                }

                if (layer.layer.lab) {
                    domconstruct.place("<div class=deleteButtonLabel><i class=\"fas fa-trash\"></i></div>", dom.byId("downbut"), "after");
                } else {
                    domconstruct.place("<a id=deletebut onclick= removelayer(\"" + layer.layer.idd + "\");><div id=deleteButton" + idxx + " title='Delete layer' class=deleteButton><i class=\"fas fa-trash\"></i></div></a>", dom.byId("downbut"), "after");
                }
                
                
                domconstruct.place("<div id=layerButtonsContainer" + idxx + " style=\"width: 20%; height: 100%; float: right\"></div>", dom.byId("layerThumbnailContainer" + idxx), "after");
                if (layer.layer.lab) {
                } else {
                    domconstruct.place("<a id=infbut onclick= showinfolayer(\"" + idxx + "\");><div id=infoButton" + idxx + " title='Layer description' class=infoButton><i class=\"fas fa-info-circle\"></i></div></a>", dom.byId("layerButtonsContainer" + idxx), "first");
                }
                if (myOpentip && myOpentip.visible) {
                    if (layerName == myOpentip.options.title) {
                        $("#infoButton" + idxx).find("i").attr("class","fas fa-times");
                        $("#infoButton" + idxx).css({"border-style": "solid", "border-width": "2px", "border-color": "#004494"});
                    }
                }
                domconstruct.place("<a id=setbut onclick= showsettings(\"" + idxx + "\"); style=\"cursor:pointer\"><div id=settingButton" + idxx + " title='Layer settings' class=settingButton><i class=\"fas fa-cog\" style=\"width: 100%;\"></i></div></a>", dom.byId("layerButtonsContainer" + idxx), "last");
                if (found) {
                    $("#settingButton" + idxx).find("i").attr("class","fas fa-times");
                    $("#settingButton" + idxx).css({"border-style": "solid", "border-width": "2px", "border-color": "#004494"});
                }
                 domconstruct.place("<div class=layerNameContainer><span>" + layerName + "</span></div>", dom.byId("layerContainer" + idxx), "after");
                domconstruct.place("<hr style=\"width: 80%; border: lightgrey solid 0.5px;\">", dom.byId("toggleCBX" + idxx), "last");


                translate_article();

            }); ///STOP foreach layerinfo

            checktopLayerCursor = function () {
                if ((topLayer || "") != "") {
                    $("#map_container").attr("style","cursor:pointer !important");
                } else {
                    $("#map_container").attr("style","cursor:context-menu !important");
                }
            };

            if (measurement.activeTool==null && measurement1.activeTool==null && measurement2.activeTool==null) {
                checktopLayerCursor();
            }

            $("#layerPane").scrollTop(scrollPosition);

            showlayerquantity();
            urlupdateok();

            if (xa === "1")
                main_functions.checkslider();
            else {
            }
            ;
            main_functions.checkfishdim();
            /*if( $("#buttonshow i").hasClass("fa-angle-left")){
                moveShareButton();
            }*/
        }; ///end checkb
           
        showlayerquantity = function () {
            var lay_num=$(".toggleCBX").length; // layer number
            document.getElementById('lay_num').innerHTML = lay_num;
            document.getElementById('lay_ind').innerHTML = lay_num;
        }

        showinfolayer = function (xpinfo) {
           
            var infoxxx = lai.filter(function (item, indx, arr) {
                return (item.id === xpinfo);
            })[0];

            if ($('#infoButton'+xpinfo).find("i").hasClass('fas fa-info-circle')){
                closeinfolayer();
                myOpentip = new Opentip($("#container2"), {
                    showOn: "click",
                    hideOn: "click",
                    extends: "alert",
                    removeElementsOnHide: true,
                    title: infoxxx.titleok,
                    hideTrigger: "closeButton",
                    closeButtonCrossSize:0,
                    closeButtonRadius:0,
                    background: "#ffffff",
                    borderWidth: 0,
                    shadow:false,
                    stem: "false",
                    target: $("#toggleCBX" + xpinfo),
                    targetJoint: "top right"
                });
                if (infoxxx) {
                    myOpentip.setContent(infoxxx.descriptiont); // Updates Opentips content
                    
                    myOpentip.show();
                    $(".ot-close").attr("href", "javascript:closeinfolayer();");
                    $("#infoButton" + xpinfo).find("i").attr("class","fas fa-times");
                    $("#infoButton" + xpinfo).css({"border-style": "solid", "border-width": "2px", "border-color": "#004494"});
                    $(".opentip-container").css({"left": "365px"});
                }
                else {
                    closeinfolayer();
                }
            }
            else{
                closeinfolayer();
            }
            
        };
        closeinfolayer = function () {
            if (myOpentip)
                myOpentip.hide(); // Hides the tooltip immediately
            $(".infoButton").find("i").attr("class","fas fa-info-circle");
            $(".infoButton").css({"border-style": "", "border-width": "", "border-color": ""});
        };
        formatValue = function(itemText, text) {
            if (text == 0) {
                return (itemText.value).split(",")[0];
            } else {
                return (itemText.value).split(",")[1];
            }
  
        };
        showinfolayerModal = function (xpinfo) {
            xpinfo = xpinfo.toString();
            var infoxxx = SERVICES_CONFIG.filter(function (item) {
                return (item.ID === xpinfo);
            })[0];
            //TRANSLATE TITLE
            traslatedElement = LANG_CONFIG.features.filter(function (item) {
                return (item.attributes.LANGUAGE === lang && item.attributes.UNIQUEID === infoxxx.TITLE);
            });
            if (traslatedElement[0])
                infoxxx.TITLE = traslatedElement[0].attributes.TRANSLATION;
            //TRANSLATE DESCRIPTION
            traslatedElement = LANG_CONFIG.features.filter(function (item) {
                return (item.attributes.LANGUAGE === lang && item.attributes.UNIQUEID === infoxxx.DESCRIPTION);
            });
            if (traslatedElement[0])
                infoxxx.DESCRIPTION = traslatedElement[0].attributes.TRANSLATION;
            if ((infoxxx.DESCRIPTION || "") != "") {
                if (infoxxx.DESCRIPTION.indexOf("<u>") == -1) {
                    layerDescription = infoxxx.DESCRIPTION;
                } else {
                    layerDescription = infoxxx.DESCRIPTION.substring(0, infoxxx.DESCRIPTION.indexOf("<u>"));
                }
            } else {
                layerDescription = "";
            }
            closeinfolayer();
            if ($("#multisearch1").children().hasClass("k-state-border-down")) {
                myOpentip = new Opentip($("#container2"), {
                    showOn: "click",
                    hideOn: "click",
                    extends: "alert",
                    removeElementsOnHide: true,
                    //title: infoxxx.TITLE,
                    hideTrigger: "closeButton",
                    closeButtonCrossSize:0,
                    closeButtonRadius:0,
                    background: "#014489",
                    borderWidth: 0,
                    target: $("#aaaa" + xpinfo),
                    //targetJoint: "top center",
                    //tipJoint: "bottom center"
                });
            } else if ($("#multisearch2").children().hasClass("k-state-border-down")) {
                myOpentip = new Opentip($("#container2"), {
                    showOn: "click",
                    hideOn: "click",
                    extends: "alert",
                    removeElementsOnHide: true,
                    //title: infoxxx.TITLE,
                    hideTrigger: "closeButton",
                    closeButtonCrossSize:0,
                    closeButtonRadius:0,
                    background: "#014489",
                    borderWidth: 0,
                    //target: $("#aaab" + xpinfo),
                    //targetJoint: "top center",
                    //tipJoint: "bottom center"
                });    
            } else {
                if ($(".modal").hasClass("show-modal")) {
                    myOpentip = new Opentip($("#container2"), {
                    showOn: "click",
                    hideOn: "click",
                    extends: "alert",
                    removeElementsOnHide: true,
                    //title: infoxxx.TITLE,
                    hideTrigger: "closeButton",
                    closeButtonCrossSize:0,
                    closeButtonRadius:0,
                    background: "#014489",
                    borderWidth: 0,
                    target: $("#" + xpinfo + "Cont").find("i"),
                    //targetJoint: "top center",
                    //tipJoint: "bottom center"
                    });
                } else {
                    myOpentip = new Opentip($("#container2"), {
                    showOn: "click",
                    hideOn: "click",
                    extends: "alert",
                    removeElementsOnHide: true,
                    //title: infoxxx.TITLE,
                    hideTrigger: "closeButton",
                    closeButtonCrossSize:0,
                    closeButtonRadius:0,
                    background: "#014489",
                    borderWidth: 0,
                    target: $("#" + xpinfo + "Cont1").find("i"),
                    //targetJoint: "top center",
                    //tipJoint: "bottom center"
                    });
                }
            }
            if (infoxxx) {

                myOpentip.setContent(layerDescription); // Updates Opentips content
                myOpentip.show();	
                $(".ot-close").attr("href", "javascript:closeinfolayer();");	
 
                //if opentip is to be resized to relocate the content
                //save original positions and sizes
                var top=  $("#"+ myOpentip.container[0].id).position().top;	
                var left=  $("#"+ myOpentip.container[0].id).position().left;	

                var heightBefore = $("#"+ myOpentip.container[0].id).height();	
                var widthBefore = $("#"+ myOpentip.container[0].id).width();	
 
                $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("opentipOther");
                $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("infolayer");
 
                //relocate the opentip widget
                var heightAfter= $("#"+ myOpentip.container[0].id).height();	
                var widthAfter= $("#"+ myOpentip.container[0].id).width();	
                
                if (myOpentip.currentStem.vertical == "bottom") 
                if (heightBefore > heightAfter) 
                $("#"+ myOpentip.container[0].id).css("top", top +  heightBefore - heightAfter);
                
                if (myOpentip.currentStem.horizontal == "right") 
                if (widthBefore > widthAfter) 
                $("#"+ myOpentip.container[0].id).css("left", left + widthBefore- widthAfter   );
 
            }
            else {
                closeinfolayer();
            }       
        };
        showthemelayer = function (thName) {
            var thName2 = "."+thName+" .theme_title";
            var aaa = $(thName2).find("a")[0].href;
            var ccc = aaa.substring(aaa.indexOf("('")+2,aaa.indexOf("','"));
            var ddd = ccc.split(",");
            var eee = [];
            for (var i in ddd) {
                eee.push(ddd[i].split(":")[0]);
            }
            var listOfLayersNames = [];
            for (var i in eee) {
                var infoxxx = SERVICES_CONFIG.filter(function (item) {
                    return (item.ID === eee[i]);
                })[0];
                if ((infoxxx|| "") != "")
                    listOfLayersNames.push(infoxxx.TRASL);          
            }
            var textOfLayers = article["listLayers"];
            for (var i in listOfLayersNames) {
                textOfLayers = textOfLayers + "<br> - " + listOfLayersNames[i];
            }

            closeinfolayer();
            if ($(".modal").hasClass("show-modal")) {
                var thName3 = ".modal-content ." + thName + " .theme_img_des";
                myOpentip = new Opentip($("#container2"), {
                showOn: "click",
                hideOn: "click",
                extends: "alert",
                removeElementsOnHide: true,
                //title: infoxxx.TITLE,
                hideTrigger: "closeButton",
                closeButtonCrossSize:0,
                closeButtonRadius:0,
                background: "#014489",
                borderWidth: 0,
                target: $(thName3).find("a"),
                //targetJoint: "bottom center",
                //tipJoint: "bottom center"
                });
            } else {
                var thName3 = ".modalSearch-content ." + thName + " .theme_img_des";
                myOpentip = new Opentip($("#container2"), {
                showOn: "click",
                hideOn: "click",
                extends: "alert",
                removeElementsOnHide: true,
                //title: infoxxx.TITLE,
                hideTrigger: "closeButton",
                closeButtonCrossSize:0,
                closeButtonRadius:0,
                background: "#014489",
                borderWidth: 0,
                target: $(thName3).find("a"),
                //targetJoint: "bottom center",
                //tipJoint: "bottom center"
                });
            }
            if (infoxxx) {

                myOpentip.setContent(textOfLayers); // Updates Opentips content
                myOpentip.show();
                $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("opentipOther2");	
                $(".ot-close").attr("href", "javascript:closeinfolayer();");
                	
                $(".opentip-container canvas").remove();
 
                //if opentip is to be resized to relocate the content
                //save original positions and sizes
                var top=  $("#"+ myOpentip.container[0].id).position().top;	
                var left=  $("#"+ myOpentip.container[0].id).position().left;	

                var heightBefore = $("#"+ myOpentip.container[0].id).height();	
                var widthBefore = $("#"+ myOpentip.container[0].id).width();	
 
                $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("opentipOther");
 
                //relocate the opentip widget
                var heightAfter= $("#"+ myOpentip.container[0].id).height();	
                var widthAfter= $("#"+ myOpentip.container[0].id).width();	
                
                if (myOpentip.currentStem.vertical == "bottom") 
                if (heightBefore > heightAfter) 
                $("#"+ myOpentip.container[0].id).css("top", top +  heightBefore - heightAfter);
                
                if (myOpentip.currentStem.horizontal == "right") 
                if (widthBefore > widthAfter) 
                $("#"+ myOpentip.container[0].id).css("left", left + widthBefore- widthAfter   );
 
            }
            else {
                closeinfolayer();
            }       
        };
        showsettings = function(xpsettings){
            
            var settingxxx = lai.filter(function (item, indx, arr) {
                return (item.id === xpsettings);
            })[0];
             //check if it is in the array
             var trigg = true;
             for (var lws in layersWithSettings) {
                if (xpsettings == layersWithSettings[lws]) {
                    trigg = false;
                    break;
                }
            }

            if (trigg === true) {
              
                    $('.cont'+xpsettings+'.map_cont').toggle();
                    $('.cont'+xpsettings+'.set_cont').toggle();
                    $("#settingButton" + xpsettings).find("i").attr("class","fas fa-times");
                    $("#settingButton" + xpsettings).css({"border-style": "solid", "border-width": "2px", "border-color": "#004494"});

                    //check if it is repeated
                    for (var lws in layersWithSettings) {
                        var foundinArray = false;
                        if (xpsettings == layersWithSettings[lws]) {
                            foundinArray = true;
                            break;
                        }
                    }
                    if (!foundinArray) {
                        layersWithSettings.push(xpsettings); //add in array
                    }

                
            } // Shows the tooltip immediately 
            else {
                $('.cont'+xpsettings+'.map_cont').toggle();
                $('.cont'+xpsettings+'.set_cont').toggle();
                $("#settingButton" + xpsettings).find("i").attr("class","fas fa-cog");
                $("#settingButton" + xpsettings).css({"border-style": "", "border-width": "", "border-color": ""});

                //reomove for the array
                for (var lws in layersWithSettings) {
                    if (xpsettings == layersWithSettings[lws]) {
                        layersWithSettings.splice(lws,1);
                        break;
                    }
                }
           
            }

        };
        changeSliderMain = function () {
            main_functions.changeSliderCss2();
        }
        hidelayer = function (xpinfo) {
            var targetLayer = map.getLayer(xpinfo);
            var targetLayerServ = map.getLayer(xpinfo).serv;
            targetLayer.setVisibility(!targetLayer.visible);
            yay = "1";
            //translation2(lang);
            showResults2traslated();
            if (targetLayer.visible) {
                $("#hideButton" + xpinfo).find("i").attr("class","fas fa-eye");
            } else {
                $("#hideButton" + xpinfo).find("i").attr("class","fas fa-eye-slash");
            }
            var targetLayerLab;
            for (var cx in lai2) {
                if (lai2[cx].serv === targetLayerServ)
                    if (lai2[cx].id === targetLayer.id) {

                    } else {
                        targetLayerLab = lai2[cx];
                        // targetLayerLab.setVisibility(!targetLayerLab.visible);
                        if (targetLayer.visible) {
                            targetLayerLab.setVisibility(true);
                        } else if (!targetLayer.visible) {
                            targetLayerLab.setVisibility(false);
                            //showinfolayer.setVisibility(false); // to hide info when check box is not selected.
                        }

                    }
            }
            closeinfolayer();
            checkb("1"); //enable or disable time slider

            if (targetLayer.lab !== "label")
                initsli("1");
        };
        /* $("#opentip-1").focusout(function() {
             $('#opentip-1').hide();
             });*/

        //to change color of buttons
        $("#tmbutton").click(function () {
            $("#tmbutton").css("background-color", "#014489");
            $("#slbutton").css("background-color", "#759dc0");
        });
        $("#slbutton").click(function () {
            $("#slbutton").css("background-color", "#014489");
            $("#tmbutton").css("background-color", "#759dc0");
        });

        removelayer = function (xp) {

            var toremove = lai.filter(function (item, indx, arr) {
                return (item.idd === xp);
            })[0];
            if (toremove) {
                var toremovex = toremove.pos;
                map.removeLayer(toremove);
				
				numPos=-1;
				for (var i=0; i< loadedLayers.length; i++) {
					if (loadedLayers[i].id == toremove.idd) {
						numPos = i;
						break;
					}
				}				
				
				if (numPos>-1) {
					layerRemoved(loadedLayers[numPos], toremove.titleok );
					loadedLayers.splice(numPos,1)
				}
				
                listlayerx2ok.splice(toremovex, 1);
                for (var i = lai.length - 1; i >= 0; i--) {
                    if (lai[i].idd === xp) {
                        lai[i].setVisibility(false);
                        yay = "1";
                        translation2(lang);
                        // lai.splice(i,1);
                    }
                }

                for (var l in lai2) {
                    if (
                        lai2[l].iddx === (toremove.idd + "x")) { //remove unused label
                        map.removeLayer(lai2[l]);
                        lai2[l].setVisibility(false);
                        //   lai2.splice(l,1);
                    }
                }
                que = [];
                main_functions.url2que(listlayerx2ok);
                checkb("1");
                window.location.replace("" + "#lang=" + lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + listlayerx2ok.toString() + ";c=" + urlobj.c + ";z=" + urlobj.z);
                urlobj.theme = listlayerx2ok.toString();
                //     urlupdateok();
            }
            $("#predefinedmapCont").hide();
            map.infoWindow.hide();
            closeinfolayer();
            //makelegend();
            $(".titleButton").click();
            if( $("#buttonshow i").hasClass("fa-angle-left")){
                moveShareButton();
            }

        };

        setopacity = function (xpalfa, alfa) {

            var alfaxxx = lai.filter(function (item) {
                return (item.id === xpalfa);
            })[0];


            alfaxxx.setOpacity(alfa);
            var temptheme = urlobj.theme.split(",")
            var urltheme;
            for (var i in temptheme) {
                var themetemp = temptheme[i].split(":");
                if (themetemp[0] == alfaxxx.idd) {
                    urltheme = themetemp[0] + ":" + alfa;
                    temptheme[i] = urltheme;
                }
            }
            ;


            urlobj.theme = temptheme.toString();
            window.history.pushState(urltheme, "Opacity change", "" + "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z);


            makelegend(); //update theopacity in legend

        };

        /////////////////////////////// SET the timelayer to use for timeslider depending on timeextent ( max )
        initsli = function (xb) {
            click = 0;
            var timediff = [];
            var timeext2 = [];
            /*       for (var i = 0; i < layerInfo.length; i++) {
                 if (layerInfo[i].layer.timeInfo) {
                 if (layerInfo[i].layer.visible) {
                 if (layerInfo[i].lab !== "label") {
                 start = layerInfo[i].layer.timeInfo.timeExtent.startTime.getFullYear();
                 end = layerInfo[i].layer.timeInfo.timeExtent.endTime.getFullYear();
                 //   lastyear = end;
                 layerInfo[i].layer.timediff = end - start;
                 click = click + 1;
                 timediff[i] = layerInfo[i].layer.timediff;
                 timeext2[i] = 1;
                 } else {
                 timediff[i] = 0;
                 timeext2[i] = 0;
                 }
                 } else {
                 timediff[i] = 0;
                 timeext2[i] = 0;
                 }
                 } else {
                 timediff[i] = 0;
                 timeext2[i] = 0;
                 }
                 ;
                 }
                 ;*/
            var start = [];
            var end = [];

            for (var i = 0; i < layerInfo.length; i++) {
                if (layerInfo[i].layer.timeInfo) {
                    if (layerInfo[i].layer.visible) {
                        if (layerInfo[i].lab !== "label") {

                            var endx = layerInfo[i].layer.timeInfo.timeExtent.endTime
                            var startx = layerInfo[i].layer.timeInfo.timeExtent.startTime

                            start.push(startx);
                            end.push(endx);
                            //   lastyear = end;
                            layerInfo[i].layer.timediff = new Date(endx - startx);
                            click = click + 1;
                            //    timediff[i] = layerInfo[i].layer.timediff;
                            //  timeext2[i] = 1;
                        } else {
                            //   start[i] = start[i - 1];
                            //    end[i] = end[i - 1];
                        }
                    } else {
                        //  start[i] = start[i - 1];
                        //    end[i] = end[i - 1];
                    }
                } else {
                    //    start[i] = start[i - 1];
                    //    end[i] = end[i - 1];
                    ;
                }
                ;
            }


            //   var highest = Math.max.apply(this, $.map(layerInfo, function (o) { return o.layer.timeInfo.timeExtent.endTime; }));
            //  var lowest = Math.min.apply(this, $.map(layerInfo, function (o) { return o.layer.timeInfo.timeExtent.startTime;  }));
            // var highest2 = Math.max.apply(this, $.map(layerInfo, function (o) {return o.layer.timeInfo.timeExtent.endTime.getFullYear();}));
            var highest = new Date(Math.max.apply(null, end));
            var lowest = new Date(Math.min.apply(null, start));

            lastyear = highest.getFullYear();

            //   var larges = Math.max.apply(Math, timediff);
            //  var larges2 = Math.max.apply(Math, timeext2);
            //     var large = timediff.indexOf(larges);
            var larges2 = highest - lowest;
            if (larges2 > 0)
                initSlider(lowest, highest, xb);
        };


        ///////////////////////////////////////////// FUNCTION TO START
        startupMap = function() {	
            $(".logo-med").attr('title','EMODnet');
            esriConfig.defaults.map.logoLink = "http://www.emodnet.eu/";

            map.on("layers-add-result", function (e) {
                console.log("Layers have been loaded:");
                for (var i = 0; i < e.layers.length; i++) {
                    var result = (e.layers[i].error == undefined) ? "OK" : e.layers[i].error.stack;
                    console.log(" - " + e.layers[i].layer.id + " - " + e.layers[i].layer.url + ": " + result);
                    if (result=="OK") {	
                        var numPos=-1
                        for (var j=0; j< loadedLayers.length; j++) {
                            if (loadedLayers[j].id == e.layers[i].layer.idd) {
                                numPos = j;
                                break;
                            }
                        }
                    
                        if (numPos==-1) {
                            loadedLayers.push( {id:e.layers[i].layer.idd, name: labtras[i]});
                            layerLoaded(e.layers[i].layer.idd,labtras[i]); 
                        }
                    }
                }

                checkb("1");
                makelegend();
                initsli("0"); ///// 0 mean that timeslider div is not populated

                if (fish_options.length > 0)
                    main_functions.fishtrasl();
                var home = new HomeButton({
                    map: map,
                    extent: map.extent
                }, "HomeButton");
                home.startup();	
            });
            map.on("update-end", main_functions.hideLoading);
			map.on("zoom-start", main_functions.showLoading);
			map.on("zoom-end", main_functions.hideLoading);
			map.on("update-start", main_functions.showLoading);
			//map.on("layers-add-result", main_functions.hideLoading);
            //map.on("unload", main_functions.hideLoading);

            $( ".theme" ).mouseleave(function() {
                closeinfolayer();
              });


			dojo.connect(map, "onUpdateEnd", function () {
                checkb("1");
                limitMapExtent(map);
                //  urlupdateok();
    
                numVisibLayersLayers=0;
                for (var i=0; i<layerInfo.length; i++) { 
                    if ((map.getLayer(layerInfo[i].layer.id) || "") != "") 
                        if (map.getLayer(layerInfo[i].layer.id).visible) numVisibLayersLayers++;	
                }
                
                var noLegendElem = document.getElementById("legendDiv_msg");
                if ((noLegendElem || '') != '') {
                    if (numVisibLayersLayers > 0) {
                        noLegendElem.innerHTML = "";
                        $(noLegendElem).hide();
                    } else {
                        noLegendElem.innerHTML = article["#legendDiv_msg"];
                        $(noLegendElem).show();
                    }
                    //noLegendElem.innerHTML =  numVisibLayersLayers > 0? "":  article["#legendDiv_msg"];
                }

                /*if( $("#buttonshow i").hasClass("fa-angle-left")){
                    moveShareButton();
                }*/

            });  		
		}

        startupMeasurement = function() {	
            //////////////////// style for measure of distance
            var pms = new PictureMarkerSymbol("img/flag.png", 24, 24);
            pms.setOffset(9, 11);
            var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DOT,
                new dojo.Color([255, 0, 0, .55]), 4);
                ///////////////////////////// disable identify task when measure tool is clicked
            $(".distance").click(function (e) {
                e.preventDefault();
                dojo.disconnect(identifyHandler);
                return false;
            });
            /////////// MEASUREMENT FUNCTION
            measurement = new Measurement({
                defaultAreaUnit: Units.SQUARE_KILOMETERS,
                lineSymbol: sls,
                pointSymbol: pms,
                map: map
            }, dom.byId("measure_areaPane"));
            measurement.startup();
            measurement.hideTool("distance");
            measurement.showTool("area");
            measurement.hideTool("location");
        
            
            
            measurement1 = new Measurement({
                defaultLengthUnit: Units.KILOMETERS,
                lineSymbol: sls,
                pointSymbol: pms,
                map: map
            }, dom.byId("measure_distancePane"));
            measurement1.startup();
            measurement1.showTool("distance");
            measurement1.hideTool("area");
            measurement1.hideTool("location");



            measurement2 = new Measurement({
                defaultLengthUnit: Units.DEGREES,
                lineSymbol: sls,
                pointSymbol: pms,
                map: map
            }, dom.byId("measure_coordinatePane"));
            measurement2.startup();
            measurement2.hideTool("distance");
            measurement2.hideTool("area");
            measurement2.showTool("location");
            
        }

        /////////////////////////////add a scalebar
        addScaleBar = function() {
            scalebar = new Scalebar({
                map: map,
                attachTo: "bottom-left",
                scalebarUnit: "metric"
            }, dojo.byId("scalebar"));
        }

        startupBasemapGallery = function() {		
            basemapGallery = new BasemapGallery({
                showArcGISBasemaps: false,
                basemaps: basemaps,
                map: map
            }, "basemapGallery");



            if (urlobj.bkgd && urlobj.bkgd !== "undefined") {
                if (urlobj.bkgd != worldBackgroundLayers[0]) {
                    basemapGallery.select(urlobj.bkgd);
                }
            } else {

                if (urlobj.p == "w")
                    urlobj.bkgd = worldBackgroundLayers[0];
                else
                    urlobj.bkgd = articBackgroundLayers[0];
            }

            //     translation1(); //FIRST INTERSECTion of QUE WITH LIST LAYER 
            //   main_functions.listalayercreation2();  //CREATION OF LIST OF LAYERS (PRESENT IN SERVICEINFOS)IN SELECTOR

            basemapGallery.startup(); //CREATE A BASEMAP GALLERY

            //  setTimeout(function () {
            //  map._params.sliderStyle = "large";
            //   map._mapParams.sliderStyle = "large";
            //     }, 100);
            basemapGallery.on("error", function (msg) {
                console.log("basemap gallery error:  ", msg);
            });
        }



        //Use the ImageParameters to set map service layer definitions and map service visible layers before adding to the client map.
        var imageParameters = new ImageParameters();
        imageParameters.transparent = true;
        imageParameters.mode = FeatureLayer.MODE_SNAPSHOT;
        // imageParameters.outFields = ["*"];
        fishallinupdate = function () {
            quex = urlobj.theme.split(",");
            que = [];
            main_functions.url2que(quex);
            main_functions.fishupdate();
        };
        //  $("#spec").change(                fishallinupdate                ); // CHANGE THE FISH SPECIES BASING IT ON SPEC SELECTION

        $("#spec").on("change", function () {
            fishallinupdate();
        });

        //translate_article();

        layersupdate2 = function (xx) { //filter fish code

            var layerDefs = [];
            var codefield = intlayer[speclayer].FISHLIST.FISHFIELDNAME;
            layerDefs[0] = codefield + "='" + species + "'";
            imageParameters.transparent = true;
            imageParameters.mode = FeatureLayer.MODE_SNAPSHOT;
            imageParameters.outFields = ["*"];
            imageParameters.layerDefinitions = layerDefs;

            alphay = 0;
            if (alphax[speclayer]) {
                alphay = alphax[speclayer];
            } else {
                alphay = 1;
            }

            lais = new ArcGISDynamicMapServiceLayer(intlayer[speclayer].MAPSERVICE, {
                "imageParameters": imageParameters,
                "opacity": alphay
            });
            lais.DisableClientCaching = true;
            lais.serv = speclayer;
            lais.id = xx;
            lai[speclayer] = lais;
        };

        /*  layinit = function () {
             
             var uniqueQue = [];
             $.each(que, function (i, el) {
             if ($.inArray(el, uniqueQue) === -1)
             uniqueQue.push(el);
             });
             
             que = uniqueQue;
             
             layerinit.layerinit(intlayer, imageParameters, labmapservice, que, alphax);
             };
             if (urlobj.theme) 
             layinit();*/

        ///////////////////////////////////////ACCESSORI VARI////////////////////////



        translation2 = function (lan) {
            //var newquea = urlobj.theme.split(',');
            var newquea = [];
            for (i in lai) {
                //lai[i].idd = labtras[i];
                newquea.push(lai[i].idd);
            }
            arrayUtils.forEach(SERVICES_CONFIG, function (serviceInfos) {
                
                for (var index2 = 0; index2 < newquea.length; index2++) {
                    //var serviceID = newquea[index2].split(':')[0];
                    var serviceID = newquea[index2];
                    if (serviceInfos.ID === serviceID) {
                        idlayer[index2] = serviceInfos.ID;
                        mapservice[index2] = serviceInfos.MAPSERVICE;
                        title[index2] = serviceInfos.TITLE;
                        labtras[index2]= serviceInfos.TITLE;
                        description[index2] = serviceInfos.DESCRIPTION;
                        descriptiony[index2] = serviceInfos.DESCRIPTION;
                        identifyinfo[index2] = serviceInfos.IDENTIFYINFO;
                    }
                }
            });
            //query.where = "LANGUAGE = '" + lan + "'";
            if (justtranslated !== true) {
                var translationsOfLayers = [];
                translationsOfLayers = LANG_CONFIG.features.filter(function (item) {
                    return (item.attributes.LANGUAGE === lan);
                });
                showResults2(translationsOfLayers);
                //queryTaskTranslation.execute(query, showResults2);
            } 
            else {
                showResults2traslated();
                justtranslated = false;
            }
        };

        showResults2 = function (results) {
            translated = results;
            for (var attr = 0; attr < results.length; attr++) {

                featureAttributes = results[attr].attributes;
                featureAttributes1[attr] = featureAttributes;
                /*
                for (t = 0; t < title.length; t++) {
                    if (title[t] === featureAttributes["UNIQUEID"]) {
                        index = t;
                        labtras[t] = featureAttributes["TRANSLATION"];
                        labtras2[t] = featureAttributes;
                    }
                    ;
                }
                */
            }
            output = [];
            main_functions.assigntitle();
            checkb(yay);
            makelegend();
            if (fish_options)
                main_functions.fishtrasl();

            //main_functions.listalayercreation2();
            main_functions.listThemesCreationModalSearch();
            main_functions.listThemesCreationModalHome();
        };

        showResults2traslated = function () {

            for (var attr = 0; attr < translated.length; attr++) {

                featureAttributes = translated[attr].attributes;
                featureAttributes1[attr] = featureAttributes;
                for (t = 0; t < title.length; t++) {
                    if (title[t] === featureAttributes["UNIQUEID"]) {
                        index = t;
                        labtras[t] = featureAttributes["TRANSLATION"];
                        labtras2[t] = featureAttributes;
                    }
                    ;
                }
            }
            output = [];
            main_functions.assigntitle();
            checkb(yay);
            makelegend();
            if (fish_options)
                main_functions.fishtrasl();
            //main_functions.listalayercreation2();
            main_functions.listThemesCreationModalSearch();
            main_functions.listThemesCreationModalHome();
        };


        ////////////////////////////////PRINT FUNCTION


        // get print templates from the export web map task
        var printUrl = "https://maratlas.discomap.eea.europa.eu/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";
        //   var printUrl = "https://maratlas.discomap.eea.europa.eu/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export Web Map Task";
        //   var printUrl = "http://eas.jrc.ec.europa.eu/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task";

        var canvaslegend, canvaslegend2, layouttemplate;

        esriRequest.setRequestPreCallback(function (ioArgs) {
            if (ioArgs.url === printUrl + "/execute")
                if (ioArgs.content && ioArgs.content.Web_Map_as_JSON) { //Intercept Print Call

                    var webMapAsJson = ioArgs.content.Web_Map_as_JSON;
                    var webMapObj = JSON.parse(webMapAsJson);
                    // This is where you do you magic.
                    // Loop through webMapObj.operationalLayers.
                    // It has the layer id that you want to change

                    ioArgs.content.Web_Map_as_JSON = JSON.stringify(webMapObj);

                    var w;
                    var h;
                    var pw;
                    var ph;

                    switch (ioArgs.content.Layout_Template) {
                    case "A3 Landscape":
                        w = "1192";
                        h = "280";
                        break;
                    case "A3 Portrait":
                        w = "740";
                        h = "550";
                        break;
                    case "A4 Landscape":
                        w = "780";
                        h = "165";
                        break;
                    case "A4 Portrait":
                        w = "450";
                        h = "250";
                        break;
                    case "MAP_ONLY":
                        w = "0";
                        h = "0";
                        break;
                    }

                    $("#container3").width(w);
                    var legdiv = $("#legendDiv").html();


                    var newMode = $("<div id=\"legendAux\" style=\"position: absolute; top:0; z-index: -9000;\"></div>").appendTo("body");


                    $("#legendDiv3").empty();
                    $("#legendDiv3").remove();
                    $("#container3").empty();
                    $("<div id=\"legendDiv3\" style=\"width:" + w + " ;\">" + legdiv + "</div>").appendTo("#container3");

                    if (legdiv.length > 0) {
                        $("#container3").width($("#legendDiv3").width());
                        $("#container3").height($("#legendDiv3").height());
                    } else {
                        $("#container3").width(10);
                        $("#container3").height(10);
                        $("#legendDiv3").width(10);
                        $("#legendDiv3").height(10);
                    }
                    var ContentPane_4 = $("#dijit_layout_ContentPane_4").text();
                    var ContentPane_9 = $("#dijit_layout_ContentPane_9").text();

                    if (ContentPane_4.trim() != "" || ContentPane_9.trim() != "") {
                        if(ContentPane_4.trim() != ""){
                            $(newMode).html("<div style=\"display:inline-block; width:" + w + "px ;\">" + $("#legendDiv3")[0].outerHTML + "</div>" + "<div>" + "<div style=\"display:inline-block ; font-weight:bold;\">" + $("#dijit_layout_ContentPane_3").html() + ":&nbsp; " + "</div>" + "<div style=\"display:inline-block;\">" + $("#dijit_layout_ContentPane_4").text() + "</div>" + "</div>");
                        } else{
                            $(newMode).html("<div style=\"display:inline-block; width:" + w + "px ;\">" + $("#legendDiv3")[0].outerHTML + "</div>" + "<div>" + "<div style=\"display:inline-block ; font-weight:bold;\">" + $("#dijit_layout_ContentPane_8").html() + ":&nbsp; " + "</div>" + "<div style=\"display:inline-block;\">" + $("#dijit_layout_ContentPane_9").text() + "</div>" + "</div>");
                        }

                    }
                    else {
                        $(newMode).html("<div style=\"display:inline-block; width:" + w + "px ;\">" + $("#legendDiv3")[0].outerHTML + "</div>");
                    }


                    html2canvas(newMode, {
                        onrendered: function (canvas) {

                            layouttemplate = ioArgs.content.Layout_Template;

                            canvaslegend = Canvas2Image.convertToPNG(canvas);

                            canvaslegend2 = canvas;

                            canvas = null;
                            $(newMode).remove();

                        }
                    });

                }

            return ioArgs;
        });


        function handlePrintInfo() {
            var layoutTemplate, templateNames, mapOnlyIndex;
            var layoutst = [
                "A3 Landscape",
                "A3 Portrait",
                "A4 Landscape",
                "A4 Portrait",
                "MAP_ONLY"
            ];
                // 
            templateNames = layoutst;
            // remove the MAP_ONLY template then add it to the end of the list of templates 
            mapOnlyIndex = arrayUtils.indexOf(templateNames, "MAP_ONLY");
            if (mapOnlyIndex > -1) {
                var mapOnly = templateNames.splice(mapOnlyIndex, mapOnlyIndex + 1)[0];
                templateNames.push(mapOnly);
            }

        }


        var printInfo = esriRequest({
            "url": printUrl,
            "content": {
                "f": "pjson"
            }
        });


        function handleError(err) {
            console.log("Something broke: ", err);
        }

        //printInfo.then(handlePrintInfo, handleError);




        var lastprint = function (evt) {
            var canvasx;
            var myurl = evt.result.url;
            myurl = myurl.replace(":443", "");
            console.log("The url to the print image is : " + myurl);

            function toDataUrl(url, callback, outputFormat) {
                url = url.replace(":443", "");
                var pw;
                var ph;
                var or;
                var h;
                var w;

                switch (layouttemplate) {
                case "A3 Landscape":
                    pw = "38";
                    ph = "915";
                    or = "l";
                    eclogow = 0.58;
                    eclogoh = 0.72;
                    eclogowJPG = 0.77;
                    eclogohJPG = 0.95;
                    break;
                case "A3 Portrait":
                    pw = "38";
                    ph = "1205";
                    or = "p";
                    eclogow = 0.51;
                    eclogoh = 0.725;
                    eclogowJPG = 0.68;
                    eclogohJPG = 0.96;
                    break;
                case "A4 Landscape":
                    pw = "38";
                    ph = "680";
                    or = "l";
                    eclogow = 0.54;
                    eclogoh = 0.715;
                    eclogowJPG = 0.72;
                    eclogohJPG = 0.95;
                    break;
                case "A4 Portrait":
                    pw = "38";
                    ph = "940";
                    or = "p";
                    eclogow = 0.45;
                    eclogoh = 0.72;
                    eclogowJPG = 0.60;
                    eclogohJPG = 0.95;
                    break;
                case "MAP_ONLY":
                    pw = "0";
                    ph = "0";
                    or = "l";
                    eclogow = 0.01;
                    eclogoh = 0.705;
                    eclogowJPG = 0.012;
                    eclogohJPG = 0.925;
                    break;
                }

                var img = new Image();
                img.crossOrigin = "Anonymous";
                //    img.crossOrigin = "Use-Credentials";

                img.onload = function () {
                    var format = $("input[name=pdgp]:checked").val();
                    w = this.width;
                    h = this.height;
                    var canvas = document.createElement("CANVAS");
                    var ctx = canvas.getContext("2d");
                    canvas.style.opacity = "1";
                    var dataURL;
                    var pdfonepage;
                    var legdraw;

                    if (layouttemplate == "MAP_ONLY")
                        var hh = parseInt(ph);
                    else {
                        var hh = parseInt(ph)
                        if ((canvaslegend.height || "") !="")  hh += parseInt(canvaslegend.height);
                    }    
                                    
                    if (hh > h) {
                        if (format === "PDF") {
                            ctx.canvas.height = h;
                            legdraw = false;
                            pdfonepage = false;
                        } else {
                            ctx.canvas.height = hh;
                            legdraw = true;
                        }
                    } else {
                        ctx.canvas.height = h;
                        pdfonepage = true;
                        legdraw = true;
                    }


                    ctx.canvas.width = w;
                    ctx.fillStyle = "#FFF";
                    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

                    //   ctx.webkitImageSmoothingEnabled = false;
                    //   ctx.mozImageSmoothingEnabled = false;
                    //   ctx.imageSmoothingEnabled = false;
                    if (layouttemplate == "MAP_ONLY") {
                        ctx.drawImage(this, 15, 15, w - 30, h - 30);
                        // ctx.drawImage(this, 0, 0);
                    } else {
                        
                        ctx.drawImage(this, 0, 0);
                    }

                    if (layouttemplate != "MAP_ONLY")
                        if (canvaslegend && legdraw === true) {
                            ctx.drawImage(canvaslegend, pw, ph);
                        }
                    ;

                    function toDataURL2() {
                        dataURL = canvas.toDataURL(outputFormat, 1);
                        var URI = dataURL;
                        callback(dataURL, w, h, pdfonepage);
                        canvasx = canvas;
                        canvas = null;
                    }

                    if (format === "jpg") {
                        var image_logo = document.createElement("IMG");
                        image_logo.setAttribute("src","img/logo-europe2.png");
                        image_logo.onload = function () {
                            ctx.drawImage(image_logo, w*eclogowJPG, h*eclogohJPG);
                            toDataURL2();
                        };
                    } else {
                        toDataURL2();
                    }
                    
                    
                    function convertURIToImageData(URI) {
                        return new Promise(function (resolve, reject) {
                            if (URI == null)
                                return reject();
                            var canvas = document.createElement("canvas"),
                                context = canvas.getContext("2d"),
                                image = new Image();
                            image.addEventListener("load", function () {
                                canvas.width = image.width;
                                canvas.height = image.height;
                                context.drawImage(image, 0, 0, canvas.width, canvas.height);
                                resolve(context.getImageData(0, 0, canvas.width, canvas.height));
                            }, false);
                            image.src = URI;
                        });
                    }
                };
                img.src = url;
            }

            toDataUrl(myurl, function (base64Img, w, h, pdfonepage) {
                var or1;
                var format = $("input[name=pdgp]:checked").val();
                if (format === "PDF") {
                    if (w > h) {
                        or1 = "l";
                    } else {

                        or1 = "p";
                    }


                    //  var docjspdf = new jsPDF(or1, "pt", layouttemplate.substring(0, 2));
                    var docjspdf = new jsPDF(or1, "pt", [w * 0.75, h * 0.75]);
                    docjspdf.addImage(base64Img, "jpg", 0, 0);

                    var eclogoBase64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACCIAAAIiCAYAAAAXG/uuAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAArnlJREFUeNrs3X2MpNtdJ/aZ9d3gOEA3f+yugkS6VhEgy6t035VCQFqp6navgqIgT1+JSMtKq6nxS7xZILcvBsKixVPjlQgJLNMjIFrL9k6NkEBJHN0exEZh4+6uThwZgZTbs4rX4SVRDShOAAV6MLA2wXS66/7O+unHXe+nqp6q/nz+qPNUTfU55znP85x6aqTzrdvn5+e3AABm6fDopBabu5cPO9v1faMClbk+G7HZiOuzZVQAAAAAgGn8BUMAAAAAAAAAAOTykiEAAOagHWXHUEBl7V0+HB6d9K7Xne1615AAAAAAAJOQiAAAAAAAAAAAZCMRAQCYmcOjk1Zs1qPsGBWorLUo21E2DAkAAAAAMAmJCAAAAAAAAABANhIRAIDsDo9OtmLzvtGApVOP63j3stzZrh8YEgAAAABgHBIRAAAAAAAAAIBsJCIAALPQ7vP6maGB5bqOD49Oapflznbd9QsAAAAAjEQiAgAAAAAAAACQjUQEACCbw6OT/djcLP3Ts8uHne36vlGCpbEWZbpum4YEAAAAABiFRAQAAAAAAAAAIBuJCADA1A6PThqx+VqftzSNEiytu3Gdty/Lne16x5AAAAAAAINIRAAAAAAAAAAAspGIAABM7PDoZD02233e8uDyYWe7fmq0YOml67xmKAAAAACAQSQiAAAAAAAAAADZSEQAAKbRjnKj9Pqzy4ed7XrLEMHK6F3nh0cnLdc3AAAAADCIRAQAAAAAAAAAIBuJCADA2A6PTvZi806ftzSNEqys+zEPtC/Lne1615AAAAAAAEUSEQAAAAAAAACAbCQiAAAjOzw62YrNVp+3PLh82NmunxotWHntKBuGAgAAAAAokogAAAAAAAAAAGRz+/z83CgAAAMdHp2sx2Ynys3SW55dPuxs17eMFizd9d2IzeMJq7gX13/baAIAAAAAlyQiAAAAAAAAAADZvGQIAIAR7EdZTkJ4EeWuIYKbPT8cHp0cXJY72/UzQwIAAAAAN5tEBAAAAAAAAAAgG4kIAEBfh0cnzdi82+ctrcuHne1612jBjbUWZUpOaRoSAAAAALjZJCIAAAAAAAAAANncPj8/NwoAwBWHRydbsdmJcq30lpPLh53tesNowdJf7+k6Ps5U5SsxP3SMLgAAAADcTBIRAAAAAAAAAIBsXjIEAEByeHSyHpvtKMtJCC+i3DVaQB/tmE96ySo72/UzQwIAAAAAN4tEBAAAAAAAAAAgG4kIAEBRO8rNPv/evHywwhkYYCPKvShbhgQAAAAAbhaJCAAAAAAAAABANhIRAIDL33JPK5fv9HnLk8uHne36gdECRnQ/5peDmD9ODQkAAAAA3AwSEQAAAAAAAACAbCQiAMANdnh00ojNh33e8izKPaMFK6s74/r3o2wYagAAAAC4GSQiAAAAAAAAAADZ3D4/PzcKAHDDHB6dbMVmJ8q1Pm99+fLBb7vDjZgXWrF5f0ZNvB7zyb7RBgAAAIDVJhEBAAAAAAAAAMhGIgIA3CCHRyfrsdmJcrPPW61chps7T6T5oZ656hdR1mJ+OTPaAAAAALCaJCIAAAAAAAAAANm8ZAgA4EZpR9kvCeHp5YMkBLjRmlGeRrmWqd610jy0a6gBAAAAYDVJRAAAAAAAAAAAsrl9fn5uFABgxR0enaSEg9f6vOV5lFuXD7l/u/2i/fXYbEb9EhegOvPDVmzW4vo8iNeb8frjGTX9SrTXcRQAAAAAYLVIRAAAAAAAAAAAsnnJEADA6iqsaH5tyFt7v9WeOwmh4CDKjqMClZMSS9oxb9RiPkjPd+Pf72RuN9U/kyQWAAAAAGBxJCIAAAAAAAAAANlIRACAFXR4dNKIzWG/7f765cPOdv10Rv1oxWY9yo6jA5W1FmU7ypSE0IyyW3rftDai3Iuy5RAAAAAAwGqQiAAAAAAAAAAAZCMRAQBWSPqt9QsHQ9765PJhZ7u+P6N+NGLzvqMCS+dOXMd7xXni4nkz/v2NzO3dj/oPor1ThwAAAAAAlptEBAAAAAAAAAAgG4kIALACDo9O1mMzJSH0+w33Z1HuzakfZVY6w/JoxXWdkgoO4vmj+PfXMrfXjnLL0AMAAADAcpOIAAAAAAAAAABkIxEBAJZYIYGgE+VGn7e+iHL38mFnu342oy71S2R4Eu0eOGqwNNJ13I6yEWWrOJ8MmHfGtRnz2l7MF/sOAQAAAAAsJ4kIAAAAAAAAAEA2EhEAYLl1otwc8r7G5cPOdr07i04cHp20YrNe+qfnUe45VLC06nGdX0kquHieEhHezNxeK+o/mOW8BQAAAADMjkQEAAAAAAAAACAbiQgAsIQOj07asTksCeHe5cPOdv10Rv1IK6Lv93nLbrR/5qjB0mvFdd8pzisXzx8MmQfGtRblfnEeAQAAAACWh0QEAAAAAAAAACCb2+fn50YBAJZEIQnh7pC39lYo72zXWzPqRy02U9LC2jzbB7Jez43YPB7xT57F9b1VqifNB5uZu/hqtHfgaAEAAADAcpCIAAAAAAAAAABkIxEBAJbAGEkITy4fdrbrzRn1Yz02O1GWVz6fRPsNRw2WZn5J1+vxmH96Jfnkop6t0vywlqmLz6PcivbOHDUAAAAAqDaJCAAAAAAAAABANi8ZAgCorsOjk1ZsDktCSL/Z3pxxl/ajLCchvIhy11GDG+N+zFMHMf+cluath5na2Ygy1btn6AEAAACg2iQiAAAAAAAAAADZSEQAgAo6PDppxub9IW99FmVjxv1JK5D7JTP0+uu32+FGake5FfPAfswbKSGlnqmd16LedrRzaugBAAAAoJokIgAAAAAAAAAA2UhEAIAKKSQhPB7y1hdR9lYczyqJ4KI/jdjs91vvj6L9A0cPbqzNmC/2Yz7YK85PF7pRrmVqrx3llqEHAAAAgGqSiAAAAAAAAAAAZHP7/PzcKADAgk2QhNC4fJjVb6Rf9KcWm6n+8krmZ9G+Fcmw/PNPIzaPM1X5SswPnag/JSO8kbnrr0c7+44iAAAAAFSLRAQAAAAAAAAAIBuJCACwQIdHJ+m31B+O+CdXVhrPoD/rsZnq3yy9JSUybEU/uo4iLP081IjNXIkIz0vzxFm0047X72Zqx3wEAAAAABUlEQEAAAAAAAAAyOYlQwAA8zfByuB7lw+zSkIoSP3a7PPvzehH11EE+tiIshXlXqlslN43qbXSvNUw9AAAAABQDRIRAAAAAAAAAIBsbp+fnxsFAJiTKZIQ2jPu135svtbnLY+iH3uOIqzcvNSIzeMZNfFqzB8HM27vSjsAAAAAwOJIRAAAAAAAAAAAspGIAABzMEESwoPLh53temvG/WrG5uM+b3kW/dhyFGHl56nT2NzMXPWLKGsxn5xFe8OSWLK0AwAAAADMn0QEAAAAAAAAACCblwwBAOR3eHSyHpvpt8rrI/7pk8uHOSQhNGKzXxJCWlm862jCjZGu95SMsJap3lRPu9jOxTy3V5qPNjO1k+bPPYcUAAAAABZDIgIAAAAAAAAAkM3t8/NzowAAmRSSEDpRjrrCNyUhNGfcv61S//qteH41+nPgqMKNm8dSMsIbM2riyvxSmJfezNzOK9FOx1EFAAAAgPmSiAAAAAAAAAAAZCMRAQAyKKzoTQkCGyP+6bySEFJSQzfKfkkID6I/LUcVbvy8luaB+5mrfhFlLeabs2hvL15/mKmdZ1H/lqMJAAAAAPMlEQEAAAAAAAAAyEYiAgBM4fDopBGbKQlhbcQ/nXcSQifKzT5vPYn+NBxVoDSPpPmjnrnqa+edGbQn6QUAAAAA5kwiAgAAAAAAAACQjUQEAJjA4dFJMzYfj/mnVUtCeB7lVvTrbB79mnU7wFjX5VZspuuz02c+6Ua5lrkLr0e7+9FeLV4/zdTei9I813XUAQAAAGC2JCIAAAAAAAAAANm8ZAgAYHSHRyft2Lw75p/OJQmhYD/KfkkIaYXwbvRr1kkIab9rUbacTVAZKfHgIK7XWnFeSOXF67vxvuPM7bei/oNor1uaN96Ysv6UqJDm74ZDDgAAAACzJREBAAAAAAAAAMhGIgIADFD4bfROlJtjVjHXJIQxEhua0a/TGfcn/fb8fqkEqiclBxxE2Sj+48V80Ynr+vV46WHmdtvFdi/aSwkNT+P1O1O2U4/6dov1AwAAAAD5SUQAAAAAAAAAALK5fX5+bhQAoOTw6KQRm2nF7NqYVVQ1CeFB9Ks14/6kJImUuLAxz/aBiea743Hmi4u/S/PjncxdutJuYT7pTjgfl72IshbtnDkLAAAAACAviQgAAAAAAAAAQDYvGQIA+LLDo5NWbN6fsIpHlw872/W9OfV3PzaHJSGkhIbWnIayE+VG6XUrj2F53I95phPzR6f0780oT/tc79O2exDtnsbz3fj34ynrT4kKaT7cc6gBAAAAIC+JCAAAAAAAAABANrfPz8+NAgA31uHRSS0221HWJ6zq3uXDzna9Pad+N2Pz8ZC3Pot+bc2pX2n/ywkNJ9GPhrMOKjcPpuuyX9LAiyhrcR2flf4+zS9vZu7atfNXIQnmtUztvBLtdJwNAAAAAJCHRAQAAAAAAAAAIJuXDAEAN1Hht8bbUa5NWFWlkxAuNObUr/Qb6+UkhOdR7jrrYGml+bET5ZWEgov57zTmgXsjzk+j2ox6W9FOK15vlea3zSnbSfN3zaEGAAAAgDwkIgAAAAAAAAAA2dw+Pz83CgCsvMOjk/XYbEd5Z8Kq0m+lNy4f0krgOfS/GZuPR+xfLfp3NuN+paSDN/q85eV5jhMw0XXciM3jEf/kUVzXe33qS/Ps3cxdfSXa7UQ7KZnhzUz1P4j6W84KAAAAAJiORAQAAAAAAAAAIBuJCACstMKK/f0oNyas6lmUzcuHCichNObRv8JK5E6Ua6W33It+tJ2FUPl5shGbx2P+6atxnR+U6lsvzQ+bmbr6PMqtaPcs2mvF6/cztfNXo/6uswMAAAAAJiMRAQAAAAAAAADIRiICACulsBK3HeWdKat8GmXz8iGtwJ3DfjRjs2pJCGl8u1GWkxCeRD+azkZYmnmzEZvjJiKk+SclFHRL9Q5LTpnUo2hvr9Reaqc+Zf0nUX/D2QEAAAAAk5GIAAAAAAAAAABkIxEBgJVQSBDYj3LalbcPLh92tuutOe9H6v9rQ966qCSETpTl33x/Fv3YcjbC0s2fjdg8nrCKgdf/GAkv43ol2u1EO7V4/TTT58C9qL/tLAEAAACA8UhEAAAAAAAAAACykYgAwFIq/PZ4ShCY9jfBU8LA7uVDWmE7x/1px+bdEf9krit1L/qXVhhv9hm3WvTnzNkJSzefNmLzeMqqHsU8sNennVETX0b1PMqt4vyTMYHB/AYAAAAAE5KIAAAAAAAAAABkIxEBgKVweHSyHptpRe3dTFWfRJmSEM7mvF/tMfdn3kkIw/p35TfagaWcXxuxeZypyldjXjjo016/hJVJPY32dkvtpPbvTFn/k6i/6WwBAAAAgNFIRAAAAAAAAAAAspGIAEAlFRIQ9krlWqYmHlw+7GzXWwvar7RStz7in1YtCeH16M++sxWWfr5txGauRIQXUTZinjgttVeLzdPM8/qVJIbCfNvN1I4EGAAAAAAYkUQEAAAAAAAAACAbiQgAVMrh0UkzNtNK+1wrZZ9F2au/vEJ3DvuVVuZ2ohz1t9HnnYSQxv9xn7f4rXRYvXm3EZvHmatO824j5o2zUru7sflGpvZSEkOt2F7G/Xse9dacNQAAAAAwmEQEAAAAAAAAACAbiQgALFRhBX4ryo1MVaeVsb1khZ3temtB+7cVmwdj7l/VkhCeRX+2nLWwcvNwIzaPZ9TEwCSVi/bT/Hw/U3tPo73dUjspaee1Ket/sMjPFQAAAABYBhIRAAAAAAAAAIBsJCIAMBeHRyfrsbkXZTPKjcxNnRTr39mudxe0v43YTEkIayP+adWSEK79zXVgJefn7pjz1bgGzm8X/ejEZj1Te69He/ul/TzN9PnzctR/6iwCAAAAgKskIgAAAAAAAAAA2UhEAGAmDo9OarHZjDIlIeReafu8WP/Odv1gwfud9vPhmH867ySERmz2+034lITQiH5Z8QurP29vxWZnRvP1wHllBskMqb2taK9b2s83p6z/JOptOHsAAAAA4CqJCAAAAAAAAABANhIRAMiisMK+GeXdGTf5IMreb3/vbNfPFrz/7Qn3e95JCKOueJ5rv4BKzedpHn88oyaeRdm4bv4eIbFlXNcmF1y004rN+1PW/3rUv+/sAQAAAIC3SEQAAAAAAAAAALKRiADAWAq/4d2Mci/KjRk3/STK1uVD+q3vBY5DLTYPotwcs4qqJiFY2QukeaMVm/dn1MSTmG+afdpPny8PM7V37fx20c7phPN48iLKWtR/5uwBAAAA4KaTiAAAAAAAAAAAZCMRAYCBDo9OdmOzGeWdOTV9EmXr8mFnu96pyHg0YjMlIayN+Kdpxexe7E97Tv0dNQlh4Mpk4EZ/DqT56u6MmhiYxHLR/kHmz5+Xo73TqL8Wr5+OOa+XPY16d501AAAAANx0EhEAAAAAAAAAgGwkIgDQU0g+KJdrc+pCJRMQCuPTis1xfys9JSE0Yr9O59TfUZMQTqJfDVcBMGReSfPX5oyaePm6efKi3fXYTK9vTNnOs2hnq9TOXmw+nLL+V6r4OQYAAAAA8yQRAQAAAAAAAADIRiICwA1RWFHaiHJRyQdJ1RMQarHZjrI+ZhVVT0J4VurfmasEbuznQ5o3tmI+aA/5HMmVTFD2vNSPsz79fDNTew+inVapnYPYvDOL/QAAAACAm0AiAgAAAAAAAACQjUQEgBVTWDHaiDIlHtQX3LWnUe5fPlT1t7Mvxi+NVzvKcZMiFpI0UPUkhPJ5edHuvqsVKjPvpc+L4yhfjuv0dMr5ZqrPi4v2d/u034zNx5nau7K/heSH7pT7d23iAgAAAADcBBIRAAAAAAAAAIBsJCIALJlrEg/K5dqCu/giyvQb263Lh53terei45lWvrajnPQ3wU+i3I39nVfSQKM03mtDjksj+nc65/O1E2VKxGi5mqEy82CaR45L88XWoPn7mr/L7fVof79P+2nevjtlO89L+3sW9adEhjemrP/lec67AAAAAFAFEhEAAAAAAAAAgGxeMgQA1VBYmd+IcqvP87WKdT2tJE0rVtuXD/NKBJhivHeL/Z1iXJ/E/jbn3P/U3rDfSK9KEsKaqxyWRrpeD+J6blw3r18878S/3xtxPhrXw6i/02f+2it9Pm5O2M5GlK1ivRftpf1/FK+/NmH9+6XPcwAAAABYeRIRAAAAAAAAAIBsbp+fnxsFgBkqrAyvRVlOOkivb1R8V9LK+oMo25cPaUXsEhyH9WK/L9yZssp7sf/tOe9HMzaXNQnhQfSnZXaAysyP6fPouM9bTuK6bQypJ82HdzN38Xnx87OczDCDBJZXip9vhc+P0yk/r1+PeveddQAAAACsOokIAAAAAAAAAEA2EhEARlRYEVlOOKj1eX1zyXf5aZQHxbK8EnUJjlv6DfFWlJOulF1IwsA1+/Gwiv0sXB/dPuO80PEDBl6/jdg8HvLWJ3H9NofU14nNeuauDkxmGCMxZtT5vlb83CskL7yZs14AAAAAWEUSEQAAAAAAAACAbCQiADfGNYkGSWPI8/qKD82zKNtRpuSD7pIe53T80m9wT5tMcRLlbozL2Zz3Jx2XYb+5vugkhM6Q8b4X/WqbjaCy8+bxiH8y8HoeY16Y1INov9Wn/TT/vzZlO0+jnd1S/and+znrBQAAAIBVIhEBAAAAAAAAAMhGIgJQeYdHJ7XYTOWoyQbp/RtGsedplAdRdi4fljX54JrzI62AvZOp6oErbme4P+ul/ZGEAMz6ek6fn8dj/umrcX0fDJmf07y0lrnrr0T7nT7tnw6Zn6bazwz1Dxw/AAAAAFhmEhEAAAAAAAAAgGwkIgAzU1hhmdRKZb9kg7rRm8jzKMuJBwcrdl6l86YV5WuZx68Z49ZZ0H6ldoetsK16EsJCEiWAqT6vx01EGGkeuqg/fc6/mbnrL4r3FRftn5XaTfcb0yYyXNtOhvqfF++Dyv0HAAAAgGUmEQEAAAAAAAAAyEYiAvCvFFY6lxMKGqXn6d/XS+WmUZyptHKyc125s13vrvh5uVcqc/3W+KMoWzGOZ3Pev63S8Ry2X8+K1+W8+jtGEsKT6FfTJQtLM8+mz/njCasYaWX/RTtpXniceRdOot1Gn3Z3Y/ONWbRzUX/6XHo4zefQRb17zkYAAAAAVoVEBAAAAAAAAAAgG4kIsAIKK6rLCQWjJhusGcVKOIky/dZ0p/h8VRMPrjmfZ52AkFbuNmNcOwvaz2Zs7o+4f5IQgFld5+n+4HjKqkaapy7aa8Xm/cy78iDabc243dejnf1S/QexeWfCel+Oek+dlQAAAAAsO4kIAAAAAAAAAEA2EhFggQorEJNRkw3qRm+ppKSDbpSnxXJRK/IrdB3UYrMVZfot79xJHQ+i3I9xP1vQ/qYVtK+N+CdVT0J4Fv3acqnD0t+PHGeqcqSElIt227F5N/MuvRrtH/RptzPl/dSL4v1ZSiwqzJvdCT/HzKcAAAAArAyJCAAAAAAAAABANhIRYAyFlW7llWqN0vN+yQabRnElpBXqaWV6J8qUdNC9fPAbz32vo3S97EV5Z0ZNpSSKZhyP7oLnjbQyd9QVuCOtKJ5hfztD5q2FJDUAM52XjzNX/Sjmh70p55txXZtYcE276fVJE3iuTTC4qD8l+7wxYb2vR737zk4AAAAAlpVEBAAAAAAAAAAgG4kI3AiHRyflhIJaqUwapefp79aM4kpJKyVTYsFZ6Xn3unJRK+qX+LpL11szyrQidmNGTT4vttPvt8EXMO8cjLnfkhCAec9X6f7neEZN3Iv5oj1k/jnN/DlxbWLBNfP0m1O28yDaaZXqT4kGr014n7Ll/gMAAACAZSURAQAAAAAAAADIRiIClVZYoZeMmmxQN3o3QlqZXU40SM87xTfvbNc7hmym12v6TexmlHdm3GRaMdqK47tfkXFIyQ8Px/zThfwmeGFFcLo+1oaMdy36KQkBVu9+63jGTb0c88fplPPRuB5Fu3uZ5+2B+5ch6eFp1LfrLAUAAABg2UhEAAAAAAAAAACykYhAVodHJ7XYrJX+qVF63i/ZYMMo3ihphXU5ySA9715X+q3khV/naWVmuVyb0/myXywXvTK/sOL1IMpxE1kG/nb6DPs9bhJCI/p56iqAlZ3f0zx0d8bz+MD5pPA580bm9l+Ndg/6tJtenzTR51nUv9Vnvn1zFv0GAAAAgCqSiAAAAAAAAAAAZCMRgZ7CSq1yQkGt9NZG6Xn6uzWjeKM9izKtTO+U/r1T/HcrqpdmXlhU8kFSyQSEa8anPea4LDRhQBICMMI8ka73zRnfNzQGzesX/WjG5uPMnytb0W631F66D077P2lS16Oof69Ufys2749Z3/NSv8+cpQAAAABUnUQEAAAAAAAAACAbiQhLqrBia6v0T8OSDdLrm0aRgrTSrjti2VspaEXe0s8jaV5IK/sbUd5Z8HnYjrJqCQjrxX7dGv831J8Vx7u8EncO/U/HN/3GuCQEYNh815nxfeOzmGe2hvSnPeG8O1G7YyTHDPNKtNMp1T9p4sS1SQsAAAAAUEUSEQAAAAAAAACAbCQizFlhBXIqx0022DCKDHBSet6JMq0oPy0+t9L5xsw3jT7loueTlBCQkg/aFR3HNF7tCcftaZTN2M+zOfe/GZvDfmNdEgJQnj/mlYzwJOad5pD+pH7U59HuGPNnP8+L9/Vp/i98Pqd5dtzEhZfN0wAAAABUnUQEAAAAAAAAACAbiQgjKvxWbDmhoFZ6vV+ywZpRZARphfi1CQa3vjLRoGPIbuR8VJ5vGn2eV23eeRJlu8rnb2F821HembCqB7GfrQXtRzM2JSEAuebFNG/PKhnh9ZiH9ufcj3vRbrtPu+n1u9N8/pWTF6ZIXHgW9W05OwEAAACoKokIAAAAAAAAAEA2K5uIcM2K4aRRel4rlenvNp0eTCCtLO6XZNC9rtzZrncNHYXfjK6V5qutUrlR8V1Jv4mdVrS24zw/q/j4N0v9Xpvw+m/G/h4saD/asXl3xP42or+SEIBR76+7E86ToxqWUFAr3V9N24+B82HGJIZXr/t8uKg/PR83gWehyTsAAAAAMIhEBAAAAAAAAAAgm8olIlyzInjcZIMNh5UMTkrPO32e91Z4W0lMaR4rz1vleW2rNL/Vl3RX0wrStJJzf5muh4vjtFXs9xTH4VmUzUXsf+F8S/shCQGY1/yZ7odyJyOMNE8V+vFmpnafldo9y7zfL4r3A6n+KZImXhTvKyRsAQAAAFAlEhEAAAAAAAAAgGyyJSIcHp00Si/VSmW/ZIO6w8AMpN+o70aZVtKdDXpe9d+wZz4KKx7X+8xfN20+KycfHMT1crBkx3Xc5IBhnkS5t4j5Y4LfLJeEAAybT26NM5/NMRlh4Ir/i340Y/NxpnafRHvNPu3txuYbE9b/NOrf7fN96njM+k6ivoazGQAAAICqkIgAAAAAAAAAAGRz+5OHnUbptfLzWqlMK6Y2DR8zlFbAjZRkcCuSD/w27s1yeHRSnp+ScnJBv/nNfHZVShJJSQeduK4OlvT8SMd3r1ROumL3RbGei3FpL2i/xl2BXIkkhMLxSCubOy45qMx8mT4f90vzRVWSEZ6N0q+LfrRi836mdu8Nmu8v2kvj9dqE9b8e9e9nqvfVZf7cBgAAAGC1SEQAAAAAAAAAALK5TEQ4NwzMUFrBllaudUr/fuW5FbKrqbBSctSEgq3S67UoN4xmFk9L118nrr/TFTnfWrE5bQJCchJlM8apu6D9asbm/oj7lebf3QX3e710vh1Ef1ouRajMvJk+j49L80cjrtdxkxHenOXn10V/dof0ox2bd6dsb6REmYv20uubE9a/VZynr5k3N8esrzbOcQMAAACAWZCIAAAAAAAAAABk85IhYIj0m/HdPmVaAXZWfG4F1nKSXLAy0gr+TrFctcSRworRZpR7mc+/BzFurQXv57i/FT7RSuYZHp903qUVvX67HKovXa/7pXl2oJQYcHH934uXHmfu152ovx3t9evXXuk+ZXPC9taK81a6T7pmXm2U7o/Xxqy/fd28XUjCeXPM+lqlcQAAAACAuZOIAAAAAAAAAABkc/uTh51zw3AjpBXSV5ILrnnevXxY1G+JM5rDo5NabNZK/zRuosGm0Vwqz0vXa6f4fNUSD64579N5u1cq1zI1kZIEmjGepwvez5QcUB/xT58Ux6WCSQhJJZImgCvXbbpPOB40vwxIIOhXb3r/4xl1/V70qz1kPkrz+bSJOU+jvd0Jx3GYa+fHi3rT593DMet75SbcHwAAAABQTRIRAAAAAAAAAIBsJCIsj7RSt1+iQaf4ZiufqqGwEm+r9E+1Upk0Ss8lF9ys67tbur47xeeLWuG+wOsnXR9pJWgzylwJCC+ibMX47i94f9M8kZIQRl25+yj6v7fg/g9LQkher8J4A1eu33T/MWwlf1WTEV6Nfh0MmV87mT5HBs5jF+21YvP+hPW/HPWflupN/R81KedZ1LPlLAcAAABg3iQiAAAAAAAAAADZSESYn/Tb7t0Ryxu5AnrRCisCk36JBlulf0/qRpFbX5lg0rnuOpdc8hXXX/rN7WaUd2bU1NMo9+I4dBe832l/08raUVfqDvxt9Dn2f9QkhIlWUgNzvf85HvFPqpaMkBJuGtGv0yGfM29kavflIe0dTPh59qy0P2dRX634PWGMz4sHUU/L2Q4AAADAvEhEAAAAAAAAAACykYgwvpMo00rn00HP+62QYjqFFWG10j81Ss+HJRqsGU1GuN6TTp/rvRvXe9eQjXX9NkvlxoyPYyuOU2fB+5/mpZSAcHfEPx1pxe8C9iONpyQEWN55Od0/HY/5p5MmI7THnP9G9aJ4f9gvWSxjMsPz4n1lub3CPHk64efco6h3r1TvpMkOL/t+AgAAAMC8SEQAAAAAAAAAALK5iYkI5d9uLycadIpv9hvu0ymsBCsnEtRKZdIoPU9/v2k0GUNaodjtc73fcr3P9fpPKzebUdbndPxbcVzbFRmPNN8djDmvXftb4QvcjzSft4fshyQEWJ75Ot1/HU9YxYO43ltjtpvmkdzJCCPNmxnbfxrt7A6ZN9+csP5XrrtPmaD/lfo8AQAAAGC1SUQAAAAAAAAAALJZxkSE8krnYWVvBbQVP6MprNhaL/1To/S8ViqT9PdrRpMM1/dISQau84XPG+Xkg1TemfN504rzoF2x8WnG5v6Y8+OTKPeqcH4XPh86Q/ZDEgIs3zye7vOOp6zq3iTz8AyTEUaajy7aT/PatIk9A5MhLtrZi82HY9b7onjfnT4PCp+/6T5pY8T6HkU9e85+AAAAAGZFIgIAAAAAAAAAkM0iExFOoiyveL72+c52/dTh+kqFFWxJrVQm5felFVSbRpEMXpSu26RTel6+zrtxfXcN4VLMN2le2S3NK3fm3JWqJyCk+TUlIIy7wvdelfZLEgLcqPvJ40xVVi0ZYWACQGHe7mS6P34l2utk3s+nUe9un3n6zTHrezXqO3AVAAAAAJCbRAQAAAAAAAAAIJtpEhGeRdkv0aBTfHO/FUE3RWElca30T43S87Qia6v0enq+5rRlBoZdz91SKalkNeep9dK8lMq08nJjQV1LCTr7cd4dVHT80ni1xxyv58Vxrsp1dbE/zdh8POStkhBg+ef/NH8dZ6560mSE9L2hPs/+FO7XT6e8735RvO+/aO+sz+dt2s/NHPtxUW8rNu+P2c+tqK/ragAAAAAgF4kIAAAAAAAAAEA2l4kIndjuDil7K4PKK3pWVeG3VtdLZb+kgvXS63WnF3PwvHSd9ks0uLLC+qYnlNxUhZWead5qlMrNinT1SZQpAeG0ouOZ5v1WlK+NWUVKetit0ufrxX6l31B/OMpxkoQAK/H5kD4HjmfUxFjJCBkSA4Z5ZdD9UOF7wJtTtnMS7TSGtJP6MWoCw8AkgwkSJZ4V7wduyvc9AAAAAGZLIgIAAAAAAAAAkM3t8/PzldiRwkqupFYqk37v23A6sAAnpeed0vNyskH38sFv+FKa/8qJLY3S862KznMp0WM/ynac32cVH+9Gsb8TjOuD2M9WxfYr7c/dIW+VhACr9zmS5rXjGTdVlWSEF8XPy37JOxftp3nu8ZTtDZz3p2jn2sSFQgJS2q9RkxaeRn27rgoAAAAApiURAQAAAAAAAADIZu6JCIUVOrXSP6UVu+ulcqvP+9YcPhYoreTu9inTiu4riQZV/a17qqmwQrVf4kGaR6ue6JJWnh5Eub9M10NhRW5Kbrg74XyxW6X9nmClsSQEWN3Pm/T58uacPg8a48yHM0xGeF78fO2XyHPRfis270/Z3ivRTqdPO+lz5rUx6702ceGivpRs8MaY9ZnvAQAAAJiaRAQAAAAAAAAAIJuhiQiFFVLrpX9qlJ7XSmVSN8xU0Enpeaf0/EqSQXpe9d+up5oKyQbl+bOcdJDmz40l3+WnUbbjujlY0uO2F5utKNcmHIdmleaPwuf6wYjn21i/6Q4s9edVMzYfz7ipqiUjPCv1p18yQpoH70653ymBodunndMJ9+/l68ZzikSH16O+fVcHAAAAAOOSiAAAAAAAAAAAZHP7k4edTmynFUabhoUKS7/l2+1TXkkwSM+X5bfoqZZrkgxqpTIpv2/Vk2DSis6DUtmJ6+1syY/3/oSfh2lcmjEOBxXbv5Tw8HDEP5GEAD7/0jy2NqOmJk1GqJXu93L170n0ozmk/UkTC5Jn0c5W5v1L98lb130eX9SbjuedMfvr8wAAAACAsUlEAAAAAAAAAACyuUxEODcMzNFJ6fmV5IJ+z5d1hTWzdU1iQTLu63Wjea20srKceHCwYudPa8rzIM1rzRifbkX2LyUdtaMcdQWsla9Amke2ivP/reolI8yqfw+iH60h82vq58aE7TyKdvaGfE4dj1nv06h3t0+/03iNm+jg8wEAAACAkUlEAAAAAAAAAACykYjAqNJKtZESDJKd7XrH0N1cEguW5rpO12k5+aC7oudja8rz6kWxnotx2q/ofraj3Bhxf3bN20CfeeWmJiMMTADI2O6r0c5Bn3bS59b9HP0vJCN0J+z3wCQHAAAAALgkEQEAAAAAAAAAyEYiws3xLMp+SQbd68pVWxHNWwor4bb6vKUx5usSC6rpeZSd0nXfiev7dMXP83S+tjKdp0+j3Kvi/DjBitmJVh4DN/r+Yd7JCLWYn84W3L+XB82Thc+b4yn3d2vQ58tFO50xP88GzvMZxutJ1Nt0dQAAAABQJhEBAAAAAAAAAMhGIsLyOCk97wx53lvxNOoKMqplQGLBpK9vGtWV8qJ4nd/6ysSDdP13b9h104zNvUzn/fNiff1+u3uB+5uu9/aY+5sScnZv4nkCZL1P6cz4PiPNV41x7mtnkIwwUoJM4XPo8Sz2tzDuqf2NTPVOO14THScAAAAAVptEBAAAAAAAAAAgG4kIs5dW1HajTCuETgc991vd1SaxgBl5VpovyokH3Zgfujf8+qvFZko+aEaZ67fAH0XZivE+q9j+t2Lz/ph/mpJ1dqu4X0Al7mtujTM/LEEywm5svjHPfly0ux+br03YzpOov9mn/nQ/+eaM6u1M+Ln6ovQ503F1AQAAANxcEhEAAAAAAAAAgGwkIgyXVj71SzLoXlf6ze3FKqyYrpX+SWIB8/CiNF+U54nT0nwhAeX66zhdf2lFazPKeuamTor1V23+vhiHRmy2o9wYs4qBK2ABCvNMWsnfiHljVZIR0vz3OGc/LtrfGtLuQWzembCde9FOO/N+Dat32mSE5EG003KVAQAAANw8EhEAAAAAAAAAgGxuQiLCSel5p/S8nHDQe+63s2drQGLBpK9vGFXmMH+U54sr84nfQp54PignH6TyzoyafB5ls4rHrbASNa1MnjQB4vXYv31nGTBk3mnE5nGUz0rz5OmI9aT5/GDK+WuYqiQjDEycyZAU8aK0n6d92mnH5t0x639l0OdgxqSLic4nAAAAAJabRAQAAAAAAAAAIJsqJyKUf2P9bMjzHiuS85BYwJJKK927pdfL80J5HjmL+cMKvdnOK2mlf6NU3pnTedGK49yu6HzbivLuhFWlz81dn4fAmPNQmo+P+8wrjUk+J6dYqT+qqiQjPIj2W33aWy/dn6zl3M8pkgtGTVxI9e9nOp6PSp/LkugAAAAAVpBEBAAAAAAAAAAgm1kmIpRXJg8rrUgeQ2Fl8Xrpn2qlctTXJRYswP/1f/9pr/xvOr9/o8fhi186/6PL8kt/fuvPLss/Pb/1xXj9C8X3/f6f/vmVFXN/8Kd/3vu7/y/+jsV6x0u3335Z/uWv+gu9eWntX3ur/Ddeut1b+fkX/8Ktt8+jHxfnQ++8+dyffKn3OdT94y/9P1Uap7/89rfGZeMdb+vNu+/4i7fXc1w///zszz5zWf7Jn129boCe3v3l53/h7+wZimvvKxuxedznLVVPRngS/WqO2a/0/lzJCPeiH+0h9++dKNdy7ucU9afvbVtR/9mQcUvX0cMpxyudV3uDxg0AAACA5SQRAQAAAAAAAADIZlAiQvot0rQi5nTI8+7lw852vWtYByYWTPr6mlFdPb/ZfWvh8vcf/K7BAIDZObl8+Pwv/J2Gobj2vjWNy/GQt6YV7Ltx398Zs512bFYtGWE/Nl/L1I+Xox+nfdrbjc03Jqz/9ah/P3P96ftfI+ofloyQvqek47o55bil9vcmOb8AAAAAqBaJCAAAAAAAAABANpeJCI3Y7q3YGbbyZVVILKAKJCIAwFxIRBh8X5zG5XjMP70X3x/aY7a3F5sPZ7RLkyYjpP2YNrEhJUc0oh/9khFS/x5P2M6w5IVJx3msZIRCe63Y3Mv0/egkylb0o+NqBQAAAFgeEhEAAAAAAAAAgGxun5+fV7JjhZVZZRILWBkSEQBgLiQijHbffTxhFZMmIzRj8/GMdm3RyQgjJQtM0d7z4vedGdQ/aTJCLTZbmcax3J/9Sc43AAAAAOZLIgIAAAAAAAAAkM3EiQgDEgvGfb3uMLCC0m8Dl3+zt1N88sn/5cXbL8uf/tUX/5khA4CZkYgw2n398ZRVSUa43rNof2tIe50Jvx89jfp3Z7Q/KYmgGe2cTnh+tTJ//0uJEGm/UlLCmasaAAAAYPEkIgAAAAAAAAAA2dz+5GGnFduNPu+RWMBNklZWdaNMK6oGJhuk9+9s17vjNPY17/7ZdN0dG3oAmBmJCANkTERIJk0gSO+fVTLCo+jX3pj9asfm3VmOy0U766X7zM0x63896t+f0f68KH5vHDcZodB+SobYyzSu147zrUhKuOhnx1UOAAAAMH8SEQAAAAAAAACAbC4TEc4NAyvgpPS8WyqvTTZY9AopiQgAML/7BIkI15tBIkIyaTJCWjGf7tPWMvfrXvSrPWa/DmLzzpTtD0xmuGinVrpvHXf/XxnlPjdD0sNE4zhgf5ulciPT8U6JZ+n4taPfp65+AAAAgNmRiAAAAAAAAAAAZCMRgUVJvzFbXonUKT3vlspessGqrGCSiAAAcyERYYAZJiIkK5GMcNGf9VJ/NmfZ/hT7n+6za1H/2ZD9Su3fneb4Xtgbpb0xjv9ubJbLXOfBsyjbxXGWlAAAAACQh0QEAAAAAAAAACAbiQiMK60cOiuVIyUb7GzXu4bwyyQiAMBcSEQYYA6JCOX7yEbcF56N2L9VT0Z4Jdrv9GmvGZuPJxnvi3q3Rtyvvdh8OOXxbUa7pzM6X2edlPA8yoNi2e/4AAAAAHA9iQgAAAAAAAAAQDYSEW6Ok9LztEJpYLKBlT+zJREBAOZ3HyQR4XqFxIE359TktMkIaaX6RuZ+LSoZ4UVpPE77tDdpYsGTqLc54n6lhIE0DpMmDbwe7e7P6TxO1/ducTxvTZ9YUT5OndJ52In97JpNAAAAAL5MIgIAAAAAAAAAkI1EhOpKK27KK6I6pefXJhvM6jdZyUsiAgDMhUSEERwenTRjM61gX5txk5MmI+RKIuhnUckIz6PcGjQeF+2lft0ds/4HUW9rxP2qxebBlPuVjnNzEd9TCsennJSQylzJGs9L58GVUmICAAAAcNNIRAAAAAAAAAAAspGIkF9a8XNWKkdKNhh1JRjVUvhN2rJaqUx6K7P+t1//l72/+wf//e9tGkUAmBmJCOPd12yV7ldnnYyQksAacT98OmI/Z52M8PKC+jNSUsRFeymp4M6Y9Y+V+FBorxWb96cc10dRtqrw/aeQ/NDoU84qMeG0+FyiHQAAALBqJCIAAAAAAAAAANlIRPiyk9LztCJlYLLBzna9Y+iqp7CyqVb6p7RSbavPn26V3nerVM9Gzn7+ZvcLvfL7D37XQQOAGd/nSUQY+35q1okDZVVLRlh0f55Gu7szamfSZIR0v9yecv/S+LaiH/sVvw4apTKNQ31G30tPS8c3Jeh1zU4AAADAMpCIAAAAAAAAAABks8yJCOk3Nrul1zul5+Vkg977rSSZr8JKokmTCGp9/q6+zOMiEQEA5kIiQp77uLRi/e6Mm5w2iSB3Pyftz1bp+8nahO0/iXabQ/a7O2E7EyUjFNpvxebelPuZvt+1punPAq+TRul7Tfn5RubzsVP6vnvl+cX4nZm9AAAAgEWSiAAAAAAAAAAAZLOIRIRnUV5JKLg1YrKBlR15FVZqjZtE0Ojzenr/htEdTiICLJ/1t3/prQ+xL7zNYMDykIiQ9/4xrXx/OOOm0srvvfge0B6zn+n9q5KMMDC5IEM70yYj5E6kWOqEhGvGp1b6HlVOTtjM3OTz0vlwWvpe3TGbAQAAALMkEQEAAAAAAAAAyGacRIST0vO0omJgsoGVFuMprJSp9XlLo8/r/ZIN0vNNo1s9EhFg+fzAt32uV376/3xrev3U77zDoED1SUSYzX3rbmy2o1ybcZMTrdi/gckI6Tw/nuc4D/he08o0/s9L9R1EP89W7LpqlL73lZMTcl9nJ6Xv96fF8/RifLtmOwAAAGASEhEAAAAAAAAAgGwuExHSSqYryQZWPlxVWJlSNiyJYGvI360Z3ZtLIgIsn1/54V/olf/Dp/+dXvmh45pBgeqTiDDb++R0X3sQ5caMm7zpyQgjtXvRTjM2H0+4fw+i/lam8yR9YLYyHYc0Dum47t+E77GFcWyUvlem57mT8NI4n5bO207x9VVLpgAAAACmJxEBAAAAAAAAAMjm9vn5+VJ2vLCSaNIkgvLf1aLccFowLxIRYHm8a/2LvfKXf+b9vfKzn/nOXvktP/pugwPVJxFhPvfn6f46JSPUZ9zko8uHne363pj9bMbm40z9eBZlI/pzNmI/liUZ4UmUe+Ps3wj7XyvWe6E54TiUPS2eh+MmZ6zQ9dgonh+3vjI5IXcyX7oOTktlZ9D5CQAAAKwuiQgAAAAAAAAAQDbZEhEKK6D6JRE0+rxeK5VldYeJVSURAZbHD3zb53rlh77nh668/q3f/dFe+ZmzrzJIUF0SERbg4vtBOzbvzrip3or9ne16c8z+pfcvezLC8+L3sH7tZtjftH+70U438/myXqz/1peTEjanrDolR7SL5U1foV9IpGiUvsdvzfh7+EmUp6Xz/nQW5xUAAACwOBIRAAAAAAAAAIBsbn/ysNPq82+NPq+nFRJrhg+GSiuwyiuuupcPn/5fP9978l/8T39w11BBtX3iPb/aK79956euvP7hn/6xXvnjn/56gwTVJRFhgMJvyaeV7nuZ62/G5uMZ78pNT0YYqd0M7aT722a0czDj83Or2N6tLycmbExZdUqSSP1vx/6cmhWunR+2Sv9PsJXpOPQ7Lv0SEzqOCgAAACwHiQgAAAAAAAAAQDaXiQjnhgH+lbQCp1t6Pa3IOSuVp9e9b9SVb1/z7p9txOaxoYdqWn/7l3rlb3/83rX//kuH39srv/Of/LsGC6pLIsIAhRXPx8XxuvXlhISzzO2kFehrszze4/b/on9ppX07U/8Wmoxw0d7WiO2k4zHpyvYnUe7lPF/GGKdm8Xjfyp+UcBD71TFbXHscarHZLzGhPqOmn5WukysJChfHq+voAAAAwGJJRAAAAAAAAAAAspGIwLJLK2HKK686pefdUtmz6JVNEhGg+j6w+Xu98id+8IMD3/cN733rp8XPvvA2gwbVIxFhgGsSEZIXUTbivuk0U3u12EwrzjdnfJ+Y+j+vRIJc/Zj2PvFJtNcc0s566XhMuoI9JQk0F3mfXTh+u6Vy2vPsRWmcOsXSCvyRj0s6r8vPNzI3+aJ0nMqJCR1HBQAAAGZLIgIAAAAAAAAAkI1EBOatnGCQyvIKu9Pr3pdrJV5VSERglb1r/Yu98q+u/Wmv/MXnX7OQfnzHxuen+vvmzv/eK79956cGvu/DP/1jvfKzn5tsP1OSwqd+5x0LGaeU/PCRZ3/JycsqkogwwBgr7+/F/Vg7U7tpJX6q786M7z8b0f+FJiNctL815jg1Y/PxhO2OlIxQaK8Vm/en3N8nUe6NM+4zPM/T+bZbPB9u5VuR/6x0vlwpF73/SzAPrZeOR78EhbUZzQ+d0vfQdNy6jg4AAABMRiICAAAAAAAAAJCNRASGOSk975bKpHPd+6wgGUwiAqvsB77tc73ynf/W7/fK9/zXf22h/fjQ9/xQJcfpj/7wm3rl+3/stV457+SIlFzxyf/8x3vlv/mf/AMnLyt7PyMR4XpjJCIkY62wH6Mfrdi8P6NdfR7lbvT/dMR+5U5GmGj8FpCMkM6LdpSTJga8iLIV7e9X9DqoxWajTzmrxIRT35vGOk7peuyXmLCZuckXxeN1cZx2HYX+3vf+D6TrqHnDdr13HX/sox85cBYAAAB8mUQEAAAAAAAAACAbiQirI63UKK/sSs/PSuW17/PbpfMlEYFV9j/+p51e+dfe+aleueiV9t+x8fle+dEfetQrv/prf32h/fnsZ76zV/77P/kfvjU5f+FtC+lHOTHiAx9+a3x+7te+zknMKpGIMMAEiQhJWuHdyHkfWVj5n1bO5/5N+Belfo+ajJB+w74T5bQrrxeVjPAg2m2Nud/p/a9Nud/Pi/Vd9KO9JNdJLTbLK/Ebmc6H56Xvaael8833tcnmtUbpuKVyooSLi/G/bXT7e9/7P5ASAe7c0CG4d/nwsY9+pO1sAAAAkIgAAAAAAAAAAGQkEWFx0oqXbun1Tul597r37WzXO4Zw+UlEYBWtv/1LvfK3P37vyutVWWmf+vfPvu+f9sp3vusTc23/4z//I71y7xe/sRLH61d++BeujEPV+geZSEQYYIpEhCQlDOzmvE8t/BZ8qk8ywtV+pPdPmoxwL9ptT3hc2pn2P30v2ov+HKzI9VROTphqJX5BSiI5va70PXHk41QrHZfy8apf93cSEQZ73/s/0Bk0fjflfuNjH/2I+w0AAIBbEhEAAAAAAAAAgIwkIowurTw5K5XllVOn171v1BVWrKbCipta8fX/7p/9v72VNz/7L/74oVFiVXxg8/d65U/84AevvF7Vlfb73/EbvfK93/UPZ1L/H/3hN/XK1j/+QK/8yLO/VIn9ftf6F3vlL//M+6+8/rnfavTKb/7773Eys0okIgy+T0njkiuh6fW4/93P1L/cCQT9jJUQcNOTEa5pvxXltCv+nxfrm7RfS/C9oN9K/PR82gQQyQl5jteV45NrXltVEhEkIgAAABRJRAAAAAAAAAAAslnlRIST0vNuqUw6pecSDFZQYcXaVp+3DPv3Wqns9+9jrQD7ze4XeuX3H/yug8TK+MR7frVXfvvOT115veor7f/2N/9Br/xHez/TK7/6a399qvo++5nv7JXf9/G/2Ss/9TvvqNT+/sC3fa5Xfuh7fujaf/8Pvu8fV7LfMM19oUSEvvdJaVyOM1f9JMq9uL8+y9TfdmzendGQTJqMcBBlPce4LWEywnrxeBfKaVf2p4SEtBK9nfN8qvB1WU5MKCcn5EoGkZxANhIRJCIAAAAUSUQAAAAAAAAAALKpQiLCiyjLCQTp+VmpLL+ve/mws13vOpzVV/hN1Fqft6SVPut9/r0xpImlWnkhEYFVsv72L/XK3/74vYHvq/pK+w+/8tbHyevv+9BU9XzXD7+VrPCLz7+mkvv5Kz/8C73yne/6xLX//vGf/5FeufeL3+jkZhVIRBh8f5bG5XhGTaQV17s579sv+p1W3D+cUb8nSgjImNjwINpvjdn+bmymfoybSPAo2t2b8vik+/lmlKm+jUzHJyVu7Ed/T2/49dsvQUFyAnMjEUEiAgAAQJFEBAAAAAAAAAAgm0kSEdJvdHZLr3dKz7t93pdWTJwZ/sUp/ObouMkD6f1bff69FuWGUR5OIgKr5AObv9crf+IHPzjwfVVfaT8sKWBUDz/24V75oeNapfbvXetf7JW//DPvH/i+z/3WWx8D3/z33+PkZhVIRBh8X5jG5XjGTaUktJSM0Mnc/4Mo1zL3+0n0tzlmv9qxOW0ywqTJDFul72lr89jvEfqV6ktlrpXT6XvqfvF8uOnJeYVkiq3S97xyckKu72+SE24wiQgSEQAAAIokIgAAAAAAAAAA2VwmIrRiu1sqe6xYmK3Do5NabNb6vGXa5IJNo1xdEhGokpRo0Pq7H+mVX/21v17Jfs4qUWH97V/qlb/98XtZ6vvsZ76zV37Lj747az//xl/5k175k+/9ZK+cNrlhVn7p8Ht75ft+7q/3yrMvvM1FxiJJRBh8P5rG5XjOTT+I7xutTPuR7pvbM7oPvtHJCJcfvdH+Webzb6tY/61IzLiVL9nipHReHMxiP1ZgHpCcwNQkItx6evnwsY9+ZNfZAAAAIBEBAAAAAAAAAMjo9vn5uVG4de0KkLJaqSwbllxQN8qUSUSgiqq24v6P/vCbeuUH97+7V/7cr33dTNpJiRA/8YMfHPi+lHTwDd/wz3vlsOSIb/3uj/bKz5x9Vdb+pgSHn7zz2V75H737v6zE+fPhn/6xXvnjn/56FxNVIhFhtPvgdpR35tyFp1E2Lx+mXale2J+DGd2HT5QQcNGvZmw+nrL9SZMRaqVxGTcxIq1gb+Q4TiP0d7d4XszgvDwpjUdKSuiaFcb63twoPZeccINlTERI1+eyHd/evPyxj37EPAIAAHBLIgIAAAAAAAAAkFFlExEKvxVaThjIlVyw5vCzaBIRqLJFr7hPyQP3/qtv75W5EwXKPvGeX+2V377zU9f++8d//kd65d4vfuOV8flv/+P/uVd+67/3T679u3klBKREh9bf/UivHJbUkMvnfqvRK9+7/7d65ad+5x0uHqpIIsJ49+FpJXp7zvfNaeVz8/JhZ7t+mml/9mPztRn1txH9XZZkhGkTMF5EuRvtd+Z0Xq4X27315ZX4u5nP03Rc034dzHM/V2gekZxwA2VMRHhw+fCxj36kZVQBAACWl0QEAAAAAAAAACCboYkIhd8SrfV5S6PP66MmF2w4DNxUEhFYJvNacV9OHpi1lGzw2x+/d+X1P/rDb+qVH9z/7l75c7/2dQPr2f+O3+iV7/2uf3jl9ZTs8C0/+u657M+71r/YKx//vV/qle981ydm0s4vHX5vr3zfz/31Xnn2hbe5SKgyiQgTKHwPaEdZn1PTacV98/JhZ7t+kGl/mrH5OHN/lzIZodCPVmzen7D9B9F+a8Hna7q+y4kJm7OYT259OTGhE/vfMWtMdNwkJ6wQiQgAAAAUSUQAAAAAAAAAALK5/cnDTloBsGk4YL7+xW/8y//jsvzhf/p7/7bRYFn8jb/yJ73yJ9/7yV457Yr7cZMHcktJDz/xgx/slSnB4Ps+/jd75ad+5x1j1fe3v/kPeuU/2vuZXpmSI77hvW8teJ13ckC/pIZJffinf6xX/vinv97FwDKRiJDB4dHJXmw+nHPTWVfcX+xHWmHdiXItUz/TSutm9Pd0xP6kFfztKfvzSrTbmXBcpu3HSWn/uxU5b2ux2ehT5k7ouzYx4daXV9yfmU0mOo6VSE64OH77jkZ/EhEAAAAokogAAAAAAAAAAGRzmYhwbhigr/QbxeUVbWd9Xu+Wyiuvl1eGfc27f7YRm8eGmmXzHRuf75U//6PfPVU9Dz/24V75oePaQvbjE+/51V75h3/8r/fK73v6zrcu8imTC961/sVe+fjv/dL/z979QMlV3QeeLxZmYTHQRSYMJ5oduuJzQFZkT3fi5d8anyokbAWbo+7JEYfAWUcF6nY7xl4VYLw4J0iFyHEYGFApNg7tbqTS+KwIi3ZpcRRnmVVL1YN2ZTPeUJ2xogg8nmrngIO9NiUgjMgelq2+fX/RrUu9+vfu+1NV38859K+quvu9++6975/o3++p+OSfX6vi9OIlkWznT//0j1SUCg2dkkoRV319I5MfvYiKCA4ZFQWKOo6EOY414/q6qupzOyTDuuR4O+T6MaPbWe6wX6U9Q2Gst0m/zOmY7rIded2OQszns1yAeGXapx2vsmGmfeJMxn2Jo4yTcbXH0Y5d7e+18TmL3vVGRQQAAAAAgImKCAAAAAAAAAAAAAAAwBkqIqBfLOlYsT6veHwumUd1mXRhZyBREQG9rHDTKypuufVBX8uJOtNeKjscXLqwL9Zju231GypOb9vqZHnX3Dmj4vHquewE6CVURAjQ/OEFyXjfGtIq5bpPKiOUfbZfKgDIdmx21M6eroxgtCenX+a7bI9UAMhFcb3tcJ7L8cNphn0b/VZuFv1WBuH45TmuKR3T1v0iFRGaoCIC4Gv/keNP0jzO1/aDakTtSXocH5MdLqra6PwV1Xb18LxIWeNgk/6t6P4t92l/pDzmZacGor8a9FvK2o9HHa2iYvVnib23q3Fp998rShxPAfQSKiIAAAAAAAAAAAAAAABnqIiAsHlVLmhYoSBx5i/8Eub3/WZ2xQUVEdDLTv7xbhVXXVZysjwy7YOx+5YfqnjzxoedLG/HNx9S8ZFjq+hc9BIqIoTAyCwu6jgc8Col818y7YuOtkMqAOyMsp0xrIyQssa324znA1Z/VPp0PwircsKSdT9VtuYNlRPcjOtoP92HBoWKCL77zz7Odiur+68S0XYkre1IdrmonN6OsqN2Zc3+6YJknuZ8tmNcv7TjUKPr19r6MiGNl90/IwFPFTl/zZnzJerM9Fp/FKzzdijz1pgXWfP6rYvrP/s60O7fUo8dD3PWfhLU/cUp6/pJ+msu5v00as0XiamQ9uNWFj3mYaVH9+9CN/PCGKdsQPN50WpfMQEAMUJFBAAAAAAAAAAAAAAA4AwVEeCFygUhoCICetF1l76j4l889oWmP3fi+CYVf/ifPqxiq4x8Mu2D8dM//SMVL7jo5Ybff+0nK4eh2ed+W8W7f29305+Xcb3q6xvpXPQSKiKEaP7wgp0JORbSqnfp68+co+0Yt7ZjyFE7b9ftLHbYn3K93WlmU1CVI7L6ZcFn/+zVMa/bVxmQ/cSulJDSMWN97mrenbLu50rW+/Ig9T+CQUUE3/0n27u9l/uvth2u/p3D6XY4nJ+/qdtVbrG+pHn+TZzJhO0oA7a2nrMcj4/drpzj842T6/aE44oYbfSLnHdf8rmovbrd2Zj2/y7zuisuz5ZvUBEmHZP5uGT1VzHifrLnzXCiN+219vNqSP3n9/+DNa1UY+zf4+a8iWCcFs12RFWBAgAEFREAAAAAAAAAAAAAAIAz59AFPY/KBQBCtenKV5t+f+fsDhW3HUnVff79v3lUxfwXplW0M+5vvv4HKj5yjEx7F25b/UbDfhbPz39ZxYl9v7VyMjh9torfPXGvinu++LyKa9bur/s9eb82uUHF49Vz6WwAdYxn0asMjAArC9i26vVl9PuM1Z5Ot2POWp48C9RvRssevVxZT7Gd/jTaIdfz7VZGGOpmvW30T1EvT/ql28y+zWasLU8yIfN6PaU+3U/K1n1bQ7X+kAuqlDmvEx+spDDS5jxIW9Fen105oWHk/hHAAEs2+jDulQaMjH9X1zNBkfPTS7rdYVX4SDpaTirm82KreT0hFUyiqoxQW39WvyzEaX8xDJvX0bX2yn1NNsx+M/ppZ58cRzeb94vGPOzJ60ujolFcjvsj5nV7r/cvgN5HRQQAAAAAAAAAAAAAAOAMFRHCY2eWiIoVE9bP1f1lZb9mBAHoHTd+4oW692+/eYWKkw+t/GH9waULG/7e9OIlKh7P363iY1sOqSgZ9mTau3XDaOPKFTu++ZCKjxxb1fD70u9XfX2lMkXhpjUqbrn1wbqf+8yaX6z8vMdyAPSO+cMLkqmV1NebFZfLNyoLSCZeUcegnv06Yl5fSyWBbjO45feM9s85av8eq33ZFu3wWxnBXu+oXm7O5/jK/UpeL7dovk+cyXhql/TrEb08qQBXMPvf9TyNK2M7K9a4e+3PMj/sigmjbc7bdisnyMsFq3125QTuXwH0taAzuo1M1lKXvz9qnT+GeqyLt+vtSOl+yPbIvMhY171xq0AxYs2L0ZD7R/aXrT02H8fMfguxokQy0Z+GrP5MhdSf/b5/2/1bjGI/BwBBRQQAAAAAAAAAAAAAAOAMFRG8LVjv5S/x2qpoQOYHgLAlz3tv5WB1+uxAln/dpe+ouOqylcPb975/h4o3f/uTHa336Ovnq+iVcU+mvRs3ZlYSdk8c36Ti3U/eUNf/7codvFzFF1/ZpeKjucdVvHqNrrgQ8DgFPa8BKHWZ/vOHF/L6erbgciVGZndGr0cy8YN61qlkgLyk13eXn+0yMv+l/UX9frPPdm7Wy5P1ZNtsx6jPdmzVvy/jP24t3+84Z835lDjzzNSsNT6tDFvzZKde7gFz3ibOVEqoDvLObNyHNr0frfVfytr/R835nThTSaFVhpddOWGztR55uWjdN5etdpYZPwC9cuu9/GVicqpkHf9ipQ8qITS8Xqptl3oT48oIdZWdesCI7te87td8wPMya16H9rC6ihIhVEYoJ/rbkHVdn4n5ffORXpyvsv/V5mmRUzmAMFERAQAAAAAAAAAAAAAAOHPWofnS+z2+DZJZUbViW5ULEmRewAEjk6zVM7vqfu7YX76VWo7/+t+/sZleRLekUsGmK1cy1CWD3bXCTa+o+Obf/xMVtx1JOV3+bavfUPGG0ZXtuOPpjzK4Xbhp+C0Vf+ealUdq331gpeKEq4oCUqHgmc+/oGKnFTG6nXcvvvKrKu47eTGDjG6olNy3nvtchq5oeB0j/XKkUb8ldAa7keke1HVUUceRgDd5r445F/cBtfZn9cs9jtp3wOr3apvtkP7r9rpySUepjFAOaLyT5vYlzlRKGHbcf1RKcDteXhUTRh3vt6es+/mS9b4S5PyEPw4zxB9Y/hJ0Zm4M+0+2d3sv95/x7OwjcdoOh/NTjlNhVRi4S/dDoc3tTFrHzeE+3WU66pcQ522v+3XdrxXHxwW5XnipT/ttl+63XMDH1/cHZB7+pu7PMv3n1JLu1xRXrQDCREUEAAAAAAAAAAAAAADgzDkhrsuuXCBK1vumFQ2CysRCvHVbccDhzwXyl+6X/Mo/YXDhm1RCuPETKxnqQVVEyB/68MpBOqDMd8l0/+7SRQyqDweXLtQxmIoSMv6f+pNMKNsj8/qiD11VN08AhCJtXpfXrsfy+nq84HIlRmbzqF6PLD+oZ8duttY37uc+o/Z7Rb0cO4O72+vHMXM5UrGiVUZ/7ftZ/fP2drZLMiZf0st5QC8373i8ZTsKZpRxSOiKDInuKzuMWXGPXr7cj9qVEsisb2+8Sh737/Z9m1wgpKz7qVHruOJlyPq5tMd65OWC9e8HduWEEqMIwLGhkNeX7PDnXVcaiit1fTIxOaWuw2Znpql85EbOiq4UAm63XVHJ674mKFv1fCzo+VgJaD0LIW1P1LIBzcNBN6znaSrgeQoAdaiIAAAAAAAAAAAAAAAAnGlUEUGeDVqxPpe/KGyrogGZJb2lwbM/vaSs6PfnAq04AAwCyRhfddnK4fi6S39XxaOvn+90PUFVQohqPYi36y59p25ef0K+8fRH6RwgfHKdtlNfN0rGelZf91dcrqy2vJxej2SuF3V0ndk3Yt7nGJURSl22W5Yj17dz1nq6bV9JLzej19NuZQTZjj1drn+7x3gHcp9XW25dpYLaeiUDyq6UMOazP0es7Ttl3dfWRe5rOx7HtvafWr+nrPtBiRnrPrLVft9u5YRF6981KtZ4l9vZvxAaNf7GM9tjZXZmusQQIW5q+4v8u15QGcR2ZSGvfydOWMfzUZ/n71bXp7K9+T4b0qgy/LMu51FtXmYDau9eHaUCQbnN9oxa88X1vMxb/eiaXVmiaWXpxAf//061k/5q0H/29Zt9ne7q3/fHAz6excVCyPu33b+FBACEgIoIAAAAAAAAAAAAAADAmXPWr0ufRTeEx8j8SLX4UfnLwlbPgsu0+L78/gi9D/QXO2NcfPo3fqbi0ddTdBJ61rUfrk+sCbriB4COSIaGVADIL8fafYXTjArJrDYqDBR1DCqj7ohe3wN6/fku213Ry5HrdOmXzV22T67j65bbKlO/9v2i/vmK/mjO2t5O1/+Si/7poB+r1rjL9sj9zbh1P9RtJtaQNa/GrPs3r4oJZXOeorv9JHEmU2/O4/7ZrtyXse6nR9u83x3x+Lnt1vrsCpH2eM8xeqHY7PO4GaiJyalT5vzjGctwZMk83hjHn2Kbv5/r8jzo5ZR5fu2iEkjJ2m9S1vHe1b9T9npFhANWv5TaOa40qICx3eV1sVSkcVABJuu4v27X7Sp288tGJYBxvZ3Svj2O2rdZLzen1+e00lJteXPNrpuCZszLuuu3APbv4UR/WLL6pWjNw1b7t8zPnY7bleKUCyBMVEQAAAAAAAAAAAAAAADOnNOvG0blAQD9Tiof2H772r9ScduRFJ2EnnXz9T9o+PmmK19V8ejBy+kkIHqScbdTX3/XPcuzVcZ+u4zM+HFrPUWrHa5s1+uR+4Ss1Y5O253Vy6uYy/fR3yW9vEw7/dygsoTfTCXpn6w13qFkZnlVSjDuA+3MeTu6rpggL+WFnckqmfQVDhm+xrtkRa9/B8hY9/kp632rZ+4OW9H+eSpKwjwuyPxi/0Y7ljyOZyo6qKyRddTOU+Z5s9tnydtk+yTT3tj+ERf7Y225UrlhLubjX3f90u24G5n2eb39CZ/XmbZMO+ddL0aGvKtn3d+lt7voclBkeUYGuqvM8/FG16n9yti/pULHERfLdViZI2inrPusgp/jp7F/F3Q/yHHiWUftHeWUDCBMVEQAAAAAAAAAAAAAAADO+K6I0OCZjV6oPAAADknlA9uatftVXJvcoOLx6rl0FnrG2uS7dfPYduMnXlAxR0UEII4k4+klfZ/wwHJcvy6dd7kSybw3Mt+L1vpdkcz3kl5fVq+/3GW783o5Zavd3WboS7ukIkGxxfor5n1Z7fdkXLrNnJOM8Wf18qQiQF6vrxTFJDTGR2LBun9NWfedo1ZM+5z/Erda6z1ltatkvadygpvxL1n96/XvGF4VE+x5MUSvAujQAR3nzOORg4oHDU1MTo1a52W/5Nn25SDaK5m+UsGg5j87WnTG6veoLZnXRa4z+Rv0a173a9bRfMj4/P1xl/1Y275CwP0nmec5R/03UBURjH4s6X5c1B/16/+/sSucFMzjWwD9Oqf7dSGg+14ACBQVEQAAAAAAAAAAAAAAgDPnzB9eKLX4Gf7CCgBipFXGuPjMml+oePzYKjoNPUPmrZdVl61ctlx36e+qePT18+k0IL5Upn3tfkMygrLLX7qtKGAzMsczej2SwZTX0VUms2TylMz1tKpA0KTdUtEhoz+SzL1OM69k+/bo5SU6aZdRoUHWX7S2t1Ny33hELzcWFRKazJum/WRkzLeKQx2OV7rZfXaTygkV83NX+9GgalA5Y85jPFL6ZYpeA2CxKx+oGFRGbBMZR8uRzPNiGI02nim/V3+0OSb94Nei3r6onsEu83Grz+X4bf+44+0Ji1Re2Nkn8zEqJZ/3FXEV9f5dbHYf0QGuawGEiooIAAAAAAAAAAAAAADAmXMSVDwAgFDdtvoNFT+y6lRXv/8bv/7ztn5uYuP/ruKF563raj1vnf6vVXyEigrowL3Xvqbn3T909fu3fPpwWz+X++xxFT/9ny/paj1/89pKYuq+kxczaEDwJBPmpeUv84cXHliOkpHvSm15Bb38kv6oaK3fL7sCwaheb67L9pbN5STOZHx1e38m7Up10r9GZvio/n35vZy13Z3qiQoJbfRL08oDte1Kmv2XOJMBl7JiusN51qpygryUFxWrveVe6Oe4MypoVOgNANq/Wv4iz+yOAVeZuVFtj6zXb0WEuGReVyNev5z3/VZEGBrQeenqukn138TkVFIfL6qJweJqezOOx6Vf9m+/hhMAECIqIgAAAAAAAAAAAAAAAGfOoQuAUCzqWPeXk3//X/4/2Qc/QRcNju8uXaTiVZf/PypuufXBQNaz6rKSindNlDr6vRPHN6l4+7c2MFjo2MxLl6r4zOdfUPGaq3cHsp4N67+xEjv8vSeful9FKiEAkdq+/GX+8II8Oza7/MXVM++bZPhvd7wdW/XyJeNrXK+/2mF75eczenlF/X6zz/5N6eVnO2xP3mpH3md7hF0hQa6PC3q9xV6czMb4lazYkIxL4kylhIw5X43PRzrs13SjcTIqJyzpWG4WjQoAAIDG4pbZnHK0nFJE7Xe63onJKXU+nZ2ZLg/o/Cw77s+M7s9Smz8v83HIxfrbXa8rMm9q2+FqkaMR71/oI7X5WXE8PwEgFFREAAAAAAAAAAAAAAAAzlARAYNuweNz+Qti+y/dK1as0+kzWMcL38nol0cYisFRPX22irmDl6v44iu7VHw097iKF1z0ciTtkkxxaRfgZ35/6k9WDm87rk+peNfEtkja8/abV6h4T+FOFamEADTedSNar2R8v7T8Zf7wwgP6eirvciVGhr9cpxV1dPVszLR5fVhbT0avt9xle7NWe/d02a7NejmSiSXtqrbZDrnelfbIuOTN5TsY/z16+QVrfApWO/qCsT0Sm94/yHxKnMl4HfWIrTIPh6045rE++z6pbLW33M19DwAgMGlHy4nkfDs7M62uSyYmp061eT5rJTnIkyEGGdMpR8s51SdDMtDzEQCAZVREAAAAAAAAAAAAAAAAzlARAXEjzy6teHy/5PF50woGPOsUcSYZ2otfu1fFPV98XsU1a/cHul4yxRGGbUdSKr7445WKH49+4RkVV11WCnS9J45vUvH2b21Q8Xj1XAYD8CCZ+/OHF35TfySZ6emQm7Jdt2Ncv8+a7XOwnSW9fMkgz+u41VH7JYNPKjzcrtdb7LK9Rb2csnUd3Gmm4Ih5vSz922m/hlAhYcgaj616PYv6vfTj3CBd37dbeaDWT5JxV1cBI/HBSgojba463ew4YFROWLLuxxpG7sfg+N8Fola15jnQ82ZnpqOez2VH158pRrPuODoc8nqTjudDVBYczcdR8/o1LBOTUxmP68Gw9g/2QwDAP6IiAgAAAAAAAAAAAAAAcIaKCGiXV0ZCq0yAksfnkhlTpWuBFZKxfdXXN6pYuGmNiltufdDpesgURxQOLl2o4tEHVhJmZ2/7mIob1n/D6XqefOp+FXMHL6fTgQ4ZGfKZ5S9GZQKpkBBWRpVkbEtlgQd0+/KOtlOuP3N6+XK9WtRxyNF27NHLz5jr6/T616hYkbKur0c6bM+w+fu15Ul7il32o9wXZM3lyXbK5w7mjWznTjMOeqWEJvO61OI+LKH7TzL0JKbM/d/4fKjNeSVxzGN98nLBun+sWPeHJY7GkP16dmY6T1cAjU1MTqXohYbol/rzq9/rsNF2risa/LxfaT3P32comx4HMtZ192Z6BQAQN1REAAAAAAAAAAAAAAAAzlARofdJJpCdWVWxoq3k8TmVCoCYkIzuF1/ZpeL0Nn+PsH5+/ssqbtp9JZ2LyFRPn103Dws/W6lg4Lfyx45vPqTiI8dW0cmAI7XrQZVhblQMsDPeh0JqynbdDqnQkNXtKzvezpT+qKjjmKP2S2bSqLkdnWbuy/W5UWGhYC2/XTJuTio22O2ryZvRGDc7+p0/rSolzJnR1Xzpo/27bN7/ean1Z9Kcv4kPPmt41BqPVtJWtNcnL+2KfCWrvRXGFcCASzlazim6Ek0k6YL4mJicSgZ0vwIAQGCoiAAAAAAAAAAAAAAAAJyhIkJwOq1UID/XMKODZ2UCAPrRRR96l04AYs7OdJ8/vFDU7yUjP6xMHMm4fkm3Y5fZLr8VvYzfH9fLz1rb6SqDv2wuXyoydNHOrF6O3D/s7LJddsUGaZfTihOJMxUKEno9QVdKkCgVNSTjs2S1p6TbWWFvbzrfSlZsqNbPo+Z8SpzJ2M1Yn7ca52ErtqqgsOBxv10y9zsq/wHAB1BZBoi5icmpUeu6ZoheAQD0CioiAAAAAAAAAAAAAAAAZwaxIoL9rEnRtCJBwiPzg0oFAIJ2w+irTpazYf03VEzu27Ny0Dt9Np2LyN2YmXOynE9f+bKKjxxbRacCATMyx6VyQEa/l8oBIyE1ZavVjqzL6/Pacop6ubK8oo5pn4uWDKZn9fIf0OvLd9nOgl6O3MfMWetpl11xwle72mi3V6WEjDmuiTOZ9COO+n3MirLeJeu+r2y+d1UhYgCOD+UW99XS3/KM41FrnFPW5+2Oe9pj/9xurfdUo/FNWJUUuM8HAAAO+Lp+nJicSlrXK1RCAAD0HCoiAAAAAAAAAAAAAAAAZ+JUEcFppYIEz4AE0CfazRh/+80rVLzgopeb/twtq3+p4vTiJXQuInPb6jfamq/tzutrrt6tYvK8T65cPFDxAwiNkTmsMpjnDy/k9Pu8jkFn7sgz5I/o9e8y1+/3fsCoAJGxtm+no/Zv18vN6Pfj3bRbxsFYTlHHEZ/tksoEOWu8g55PdesxMugzHnHE0TzabEVZv1cmvV05gfvP9sa5avVj03lV6/9R8ziTOFM5IWN93up4I9/3qqAg65OXi+a/U9TaPc7oAQCAFk61c33ThmJI91MAAASGiggAAAAAAAAAAAAAAMCZdioi2JkftpLH5/Lz1UafkymCMDV4Bmkrnf68yHTyww9v/Gf/3XL86nM/Y5DwAa0yxiVT/J7CnSouvn6+inu++LyKa9bub/h7n/p4RUUqIiBKN4y+2vT73/v+HSrm/u21Kv760D+o+OgXnlFx1WWNLz+o+AFEr3adX9DXX0X9UV7HrSE1QdYzrtuR1e0qOd4+WZ5sp9+M/LR5vySVCGrrK3fYPvl9uS4t6Li5y3bJdknFib3muBoVI4KeV3L/OGdFr+v9jBXbzZj30m4m/ZJ1P9wwhtVvfXRcKbf4dwnp/5R+mfIY/1SH++uIo/0bAOIqRRcAziyZ9yGzM9Nd/f+PickpuX4ZC6iddRWfWl1fdSHT7HoZADBYqIgAAAAAAAAAAAAAAACcWa6IcL1+XVn+QmYGGmmQWdJKWD8/3Mv9+l/xp0Bowitj/MTxTSre/q0NKh6vnlv3/au+vlHFwk1rVNxy64N13//ElSsVExK7r6STEZkbM3MNP985u0PFbUfqTwcyz48+sJLQO3vbx1TcsP4bdT9HxQ8gPowM9py+npTM/KKOQWfIyHWiZPLv0u/zVvu63T7JHBrVy8/r99sdtfslvdzb9fqKXfZ/Vi9nzur/bisDSGWFcWtcCy761cF8K1nR675m1IoZR/cZw1Yc82iHXXnQjnJ/XuJo0tE8qJj9l2jxbGajcojXvPBbSQMAglJxfL0ENBJVReNF8z6iV/bH2ZlpV/tl1nH7FszlOmxnQxOTU/mQ7vcAAD2A/w0KAAAAAAAAAAAAAACcOYcMi3AZGRftavfn7WeitvvzPOsSiCk7Y/zJp+5XMXfw8rZ+X37u0H98XMWZ+1YSQS+46GUVb1v9hor7Tl5MZyM0Mu9kHr795hUqTj608kj3g0sXNv396umzVdykK3rce+IhFbd96T4VpUJCct+eup8HED0jUzmjr4vH9XvJqA86I2+rjpLJn9PtmnO0fXm9XFnenKPt2mPdR0i7qx22b04vJ6U/KurY7bNnJUN8u9muuFRIaGMeVqxxsu/b7PsrO0o/pn32X7rZcmrtkJfyzOFWFRTKHG06mg9t/XuIsd8AQCxIRvPE5BSdUY/zoL/rk7j0Z1XP89KAjt+4o+Us6n7MsEsAAKJCRQQAAAAAAAAAAAAAAODMOb3a8A4qC3RaKSBlxVZ4ZiQApyRjXNz6BysVDVplinuR31tz19dUfObzL6h4w+irKlIRAWGSefe979+h4s3f/qSK3VYueOTYKhWP/fgJFR/bckjFW1b/UsXpxUvodCCmjEoEkqmf1+9zAV9fS4WCZ/V6D+j3Wd2uqs/tKuvlyn2CbNdWn+3ebN5/SEUJI8O/3fbJ9o1b91VFq386ZVdI2K6Xv9fsh07bG+H8lH4qWdHr/tS+j8xY72U+jPictxLHPNohL+VFxYol832vjEcM5gP9BKCvTUxOqfNV0M+ObyLlaDlVRjNSriooJAd0P3T9/xmyTEkAQNSoiAAAAAAAAAAAAAAAAJw5h8oCABBPUsHA1TPuZTmf+pOVw/69175GJyN0h8r/XMV9Jz/qdLlHXz9fxU8/9lkVr7v0HTobaJORuS+VCPLLX8LOAK6tL6/bU9AfSdwc8Kols7yi15/V7ZnzuT1Vs19ry5XlFXXstvKAZNSXXbS39nsl874tgMoUm81YW/6C2Q+19Rf7YT8y9heJpTb3u5R1P2x/3m0FhbQVxXarHfJywZxXxnaUzei3YggAwLkln9cVCeu8U4loO4YdLWegz1NGRn0iov501f8jAzqESZfHhdmZ6XICAICIUREBAAAAAAAAAAAAAAA4c07tvyN0AwDEx76TF4eynkeOraKz0XfzWyp/HFy6kM4G2ieZN3bm+gP6vapMEFYmtLGerG5HUb/P65gOaNWS+f+sXu8Bsx1+t18qDxiZ8H4rPtjt3aXXk/PZzrxenrQvZ0W/FRLqMvWN9cyZ0W9FirirbV9dpQFj+xtqUkEhY30+7GJcmrTjlNXuhhUUjEobAKI7r2MwVHwe/xPW+STU4/fE5FTG5fLIAG+7MnFQ/VlxPD9GB2xcXe0PVEIAAMQGFREAAAAAAAAAAAAAAIAz59AFAAAAABqQZ8mrTHjJXJeM+bAYmdUZ3Y6sfi/tGA5o1WM6VvR687o9BZ/bY1d8kEz4oo7dVhzYqpeX0e/H9foqPtuZN8c/4b5Cgvy+XZFDMu8HqlJCk/HotIKCzIOUFe3Ph7scr6YVFGrrl5dL5n6UOJNpWzEjFRQAp0bpgoFSbnY87sC4dX0VlnFHy1lkKjjtz6Vufml2Zlqd1ycmp1xtT9a67kRnxwUAACJHRQQAAAAAAAAAAAAAAOAMFREAAAAANCMZ0KpCgl2RYP26dDHMxsj6jEoCObN9AW7/Tr1eyTTL6vZUfG7PnF5uSn8k/TnW5SJHdCyb4+W3koBdISFxplJC1uyPhP+MTLvfvSollHScM9/7HY9+0WmFgQArKAxbkQoKQPDUfjYxOZVcjrMz09UwVlpbX8Y6TyAcrjKfR8xxrM2bUsDzJWldP8SlH3qS0Z/jMenPBUfXhVm9ffkwj2fwLUkXAAAEFREAAAAAAAAAAAAAAIAzVEQAOiPPnPP6C9ySx+fy83V/Ubz48t/Lsxt30rUAAKBHSGbznuUv84cX8vp9dvlLWJnKdoZ+rR1F/b6g41hAq06b13Wy/bX2FBxtz7hermS0yXYNdbhI+fln9fL26vc5a31+x6FottOo7JAztyfReSZ9q+0aazTOtfUvWdfldZGKCZ7j2NF+SwUFIBCuM7pz5nnStYnJqXFrPWmGMBJz5nWZAwU9vup4HmAGeqHL65tW/TCoco77s+RoPPweF4as+ZJll2/LaJgrqx0vUtb5ZjNDAAAQVEQAAAAAAAAAAAAAAADOUBEBvWrB4/OGlQcMFSva1O+5yhBrZbzwHUYSAAD0OslcPrL8Zf7wglyn5fV1VSmMRhiZ7lJRIKPfSwbViONVSobWTr2+rH6f1e0p+9yeOb3clP7Ib2aZZCZlzPa6Hh9jHHJmrK1v1OyfhPtKCfZ83GzFhG6HXTGhbL73O26DggoKQCBc/zvE9uUvE5NTaj7Pzkx3Na+NTNesFYcZsuhJxYLaOC34vE5IWNdLc3q54+Z6ulVbTtK6LnOVMX3KOp5HLRnmymr9mjX3d4f89ueceZ3swGa9vQnz+jLAih29bjTgeZexzgdUQAAAeKIiAgAAAAAAAAAAAAAAcIaKCPCrLyoTAHGwNvmuip9Z8wsVHzm2ik4BfO5Pk9f9RMXcwcvpFCA8koknFRL26vd5fX1XCaMRRkbzqG5HVr93/UxiIRmEL+n1PaDbkfe5HXI9nNHLtZ/53el22BUsDuj32SCvv41KA3alhJR+P25upxFdj1Orign2fU5dxQTjPqXCrt7V/tiWuFZQqG1HhtEMhToeGRmXvUIdH2ZnpnN+fj8AR3R/yvF+zpzXhpR53kwEV7kGwSg2O675uJ6r6PlTtOaPzPe66waj8oE9j7IBndfnGrUjQiO6H6oe1xESq7rdHe33teWOWtdTrjPRF7tpl632+zJvFhzPy7oKX/a87LbdDeZtwnqf9LguSZjj4bffjPnht8LFsNk/tXZlu+yXUev6KxfyeSHFoR0Aeh8VEQAAAAAAAAAAAAAAgDNUROgfVCYAepxUQvj0lS+rSEUEwP/+9N//yxMrH1ARAYhSXea5USEhF+Z1Zm09Rb1+yeTL67g1oFVu1+sbt7a35HM7CtZ2FHXsNtNszLwfqC03b64nhHGR+5CCFRO6PXYmlkT5PKiMrLQVt1rtOmXdZzWMRiUIdDYvOtpPQqyggHD0ar+r44WRgdrR/i8Z3bXfXwpo+8esiD5Smz9FPX/yjufPkHUerDsf1tYX9abnYzokQx7XE9tb9F9dJR4f569uFQIanyMBnSe2m7FBf9r/Xj5qjY8rUikg63M5lSDuw4wKQ3L/4HX/ZV93D0W8H0n7pb0ls/21414pAQCIPSoiAAAAAAAAAAAAAAAAZ6iI4N6ijl5/WVjy+LytygU8ixToXzdf/wMV16zdr2LyvE+uHBxOn03nAD73p7XJDSoer55L5wDRkwoJqlLA/OGFukz4oCskGMvPWesv6ph2vMoRHY/o9e3S7/N+tte4L8jo5ebM5SY6z2CSn99pLS+r11eKYrIYFQXK5jwRtXYmzX5IfDCTK6Wj60xCr0zHhNU+eblg3tclvCsoUImuu3nS0fwMsIICoG7lfP5+yTpfAp2Q8/ezfb6dDyx/mZ2ZrvTZdkVVEWZJ92fR5UIlY31icmohoOvsVsJa36ij/qro/nJdGUeWs7VH9wuviiwHdL+Nc+gHgPiiIgIAAAAAAAAAAAAAAHCmnysi2M/stMnnXVUu4FmfAFxZm3xXRcncFres/qWK04uX0EmAz/3pM2t+oeLxY6voJCA+JKNcnu0qFQry+nq7EEYjGlQWyOj3RR1dZ6RJJo9UhMjpdsz53I6CXt6c1f5uM8Fku6WSg2SyyfiU4jCJjAoCc1asY1ROGLViyuNz18/EtSsnbPZop30fK/1cMWNc+r9X+aigAISh2Ow4ATQzOzOtzoMTk1N7+3QeLertzDPaTuUCXn7Wur4Z6rP+GwnoPLCdqdlUki4AgPijIgIAAAAAAAAAAAAAAHAmzIoICx6fSwaLV4WBihXrkAkCoNdJprbtUx9fOexREQHwvz/dfP0PVHzk2EY6CfC+3nb9LNJOSWbUzuUvUikgcSYDvxhGI4z7i1SjdiTcZXBJPz+r13NAv8/qdlS7bL+MZ0YvV56ZWvTZfsnkj3WFhCb9Iv1ZsmJDEVZQGLL6O+3RPnm5aO3HZStW9PZT0c/NcQEIXAyeqd6tU9b5ZiujGamcdZ4a6fHtWTSvb+DMLn3cmQtyJbXlV/RxLWNdh/VVZYTa9o3q7fV73VWw9uOhHttPs9b90xi7GgAMLioiAAAAAAAAAAAAAAAAZ5YrItiVCuQv9uwMnLYqFxgZOEDfapAh1ZXP/caH1O9/56//nk4dYJKpbduw/hsqJvftWTkInz6bzgK63J/WrN2v4trkBhWPV8+lswDNuH5P6eucrH6f1zGqCgmy3j26XZIRlNPtLoXUPwW9/qLVL64zPcfM+6ra+vLm+n20f04vL+W4/V4VEop6vcUe3y86qqBg3CdkzP0p8cEKCvLeVWbqiBXHPNolLxfMeWbEEvf1QOzkrP0zrhmxu6zzy2hA50l0YHZmWp3HjAx0uZ7Y3GObIuetcXO74Nte3Z+5kOdl2ZqXcT++Rb3/5sz7oRg6ZR5fau3Om9+stX+u2fUpAGAwUBEBAAAAAAAAAAAAAAA4c876dekM3QCbkcnjl/wlfNLnclJW9Luc4Tj088gVH1KRigiDaW3yXRUlU9vLZ4bfVHHfyYvpNMBD8rz32tqfrtP70/HqJXQa4MHIZC/q68Jx/V4ycqJ6VrVkfNsZ+Hnd7lLA/VI1+6G2/oLZTw77RTLCdjbq/1o7yjFtf9qMUtHBWH5Rt6PS5/tPR/Ow1k/2/VLG4/7H1fxKeyxvu9UueSkvZN5VrPdla34BcCTGmcN7zfOvPPtd1NrL4MVrHsnxOavHZ846P8ctE/2UNb8KjGL/9atxfJPrnF6t2CF2mdvlsJ+K1nF1T0zmUcGMXpVKjPZnI76PBABEiIoIAAAAAAAAAAAAAADAmXMGbYONZ6OmfC7K1XIk82XU0Sbyl4VASK679J2VnVhnYnfqho/9XVs/9zuf+JGKb57+iK/2Hly6kEFDbE2N/FzFf/4r3VWI+W//2dtt/dyWzx7T6/mXvtq77UiKQcPAWL8uLZlzc/p6OqPf5yO+/pT1SoWEugzNoDPvjeVnrH4p6jjseDtf0ut5QL8v6HZUHbff1bjK9m83Y209B8z5ZFTgGNT9y86cK7W4n7XvH0et+1L78yFH8y/dol2SIVe2YqXR51RQ6FvlAf93CXs/cCLCzOFF67xW1O1ptf+62r9dn8crPudnIOMbttr4zVnzya58NRJykxateT3X5jyLq0VzO4z+DbtfO8pcj3A+2hU78tb16Lij6xm/lqzrtFKY89WoLFAJ+T7sgLWd3V63jwd0/ip2uV90O58qMTvOdHtcWeCSFUCYqIgAAAAAAAAAAAAAAACcOevQfCnj8b2Mo3XYz7z0u5whhg394keV0yp+Ze5ndEYPkooIj205pGKrZ9OH7e03r1DxnsKdKu47eTGDhtiSyiLPfP4FFa+5encs96fJh7aqSIWRnqP+4v+t5z6XoSvcMyqO5XWMy7NdpUKCyvALOwO71i85q19c38csWds357j9Gav9QWVcSWZSXeUN19vDfvqP45myov35cEhNooJCH5uYnHL170C9Rs3XsDKOa/0s/TvusT/L90da7H+y35XMWNuOis/22cebdlX1+ssB9VumF8Y3wv1XxmvUIya7Gc8Gx/tSnPrTmBdHXNx31LYr47G/jnrsr/Z+km5x/Vfx6F/p11KfzcuMx3xst9+8KprY/ddwvsZ1v29Q2WS0w+NupdF5IOj506Dd7R5XSn7GpcF+2JG47Fd+t2NQzmcA4oOKCAAAAAAAAAAAAAAAwJnligjv0w1ANKiI0B8kkzt/w49V3HLrg5G258TxTSre/q0NKh6vnssgoefsuL6i4l0T2yJtx/e+f4eKN3/7kypWT5/N4PQmKiKEyKiQIBUBsjpGVdms4TNyw8qwrvWHZKzkddwa5DyX/q5tX8XxdmSs7UiHNG5SGaFkvidDPvB5K+NtZ1zZmXYjIe/HVFAAADgXdEUEAACAQUVFBAAAAAAAAAAAAAAA4AwVEYAIURGhP922+g0VH809ruIFF70cynqffOp+FXMHL2cQ0DduGn5LxZn7doW6P+2c3aHitiMpBqE/UBEhQkZFgJwVo66QkF/+sn5duhByf8iBRdY7FtCqdlnbWQ1oO/I6joc8rlIBoq5iQm07y+x1kezndqUE+73EdMhNk3lSsWLJfO+6gggAoLdQEQEAACAYVEQAAAAAAAAAAAAAAADOUBEBiBAVEfrb2uS7Ku754vMqrlm73+ny337zChXvKdyp4r6TF9Pp6FvJ895T8ZnPv6DiNVfvDmR/mnxo5dHtB5cupNP7CxURYsSokCAZ9HkdhyNq0pLZjvXr0sWQ+0PmpVRIGHG8ilAqQDQY11xA29Pu9pYaRSomxOY4kNIvJWas9xKjqqAgFUTKZqzNnzlGDwD6DxURAAAAgkFFBAAAAAAAAAAAAAAA4AwVEYAIURFhsBRuekXFLbc+6Gs5J45vUvH2b21Q8Xj1XDoXA+fea19TcduX7nOyP336sc+qWD19Np3bn6iI0APmDy9k9cuoMumFVEhQ7Vm/Ll2KqB+kcsGQ41Usmv0c9PYZme/jZr9GOL52xYRyo/e1fqmyV8bq+CAVN0atmPL4fCiIdtTmxVmMBgD0HyoiAAAABIOKCAAAAAAAAAAAAAAAwJlz6AIgUJJx1fBZtKf/4X3J7Bmhq/rfi6/8qopbfC7n//qrNSpSCQGD7MRrFzpZzg//04dVpBICEL3169JF/VLF+cMLGf0+r2NYz4of1vGIbseC2Y6gKwhIP9TWK8+iz1nRb6b3iLV9B8zl19Zfcbw9sryCGRtUShgPaZyl/8asuN38oVr7Fq3r+LoYdqUMjg//WKGiZMWG4lJBAQAAAACAQUZFBAAAAAAAAAAAAAAA4MxZh+ZL79MN6ANNKw8YKlb0Istp+mxYv5lQF278Tka/PMIQ9r/dt/xQxZs3PuxrOa/9ZGXarP7aHXQqBlbhpldU3HLrg76W8/abV6j4a7//h3Rqf1MZ7W8997kMXdG75g8vSOayVAbYHOV8qsnq68FKSNuf0i/zAW//A3Ko1dtXjWi8pUKC7Lcy/umYTc0l6/6iZN1PVHQ/ltmLe+I4I/MtZcWMHkfOIwDQhyYmp+T47vff59R14uzMNOcLAACABBURAAAAAAAAAAAAAACAQ1REQKd6svJAXFERYbD89E//SMULLnrZyfJuvPsJFY++fj6di4Fz8o93q7jqMjenh6kdu1Tcd/JiOrc/URGhDzWoECAZ9GE/632v2Y4QKyRIhYCCjq4rBZyylh9phYQG2y/7s8RRKw7HdOp6VVCoWvdHlTDnEwAAg4yKCAAAAMGgIgIAAAAAAAAAAAAAAHDmHLogtqg8APSJ21a/oWKrSghPPnW/ii++8qsqPpp7vOnvbbryVRWPHrycTsbAuO7Sd1RsVQlh5+yOlf3px/90ZX/6wjNNf++G0ZX9iYoIGERGZnlRx4L5Pi4Z8A2uW+X6N6u3I6nf56wYdIWEzWastWOvuf6g+q+2XLm+z3iMo9+KANJv283tqa0nFhUSjPuWUpvzWyolpDzeh1VBYdiK6Rbtl5eL1n1f2eN+sBzn/RYAAAAAAAwOKiIAAAAAAAAAAAAAAABnzjo0X3p/wPvAfkanl7YqCiSoPIAOXLjxOxn98gi90b923/JDFW/e+HDd52+/eYWKkw9tVfHg0oV131+bfFfFPV98XsU1a/fXff+1n6xMn9Vfu4NOxsAo3PSKiltufbCj/Sl53nsqzt72lypuWP+Nhr//a7//h3Ryf1IpxW8997kMXfFBRsa4fT0iFbrmdMzr69hKj2yXVEgYN9ufCC/zXfovkgoCte3PBrzdsn1Fa/sqfbI/yPwZbfF+pBeOf8b9KZUUAACwTExOeV0Pd3XenZ2Z5r4DAAAgQUUEAAAAAAAAAAAAAADgUJgVEVxXHii1+L76fePZqUDsUBFhMPz0T/9IxQsuelnF731/pYLBzd/+5MrB6vTZbS3HKxP8xrufUPHo6+fT2eh7J/94t4qrLiv52p/uvfY1Fbd96b66z6d27FJx38mL6ez+QkWEJppURGjan4kzGfBzPba9Wf0yp2NYGe39XiFB7NWxqLevNCD7UUq/lNjrFRWopAAAGBhURAAAAAgGFREAAAAAAAAAAAAAAIAzyxUR8i1+ptTi+1QeALpERYT+dtvqN1Sc3rbyzPqdsztU3HYk5Wu5Nw2/peLMfSuZ20//+S0q5g5eTqejb1136Tsq/sVjX1BxxzcfUvGRY6ucLPfJ3J+p+H+Wr1Lxjqc/Sqf3FyoiNNFFRQSbVD4r6hhqpr/D7Zf7onRIq14y11vrr2LI2521tns44O0smPOEDPoPjEe7FRTkfSrgcfN1vE1QSQEA0EOoiAAAABAMKiIAAAAAAAAAAAAAAABnznr//ffpBSAiVETob/IM+hOvXajiwaULnS4/ed57KuZv+LGKVERAP5sa+bmKx//uQyoeff38QPanu6/9WxX9Vi5B7FARoQkHFRG87NVRKiSUe6Q/JOM8p+PmkFYdlwoJst0jAa/ygI5zZiRDvuvxkxOXxFaVFdIx2wQqKQAAIkNFBAAAgGBQEQEAAAAAAAAAAAAAADhDRQQ41eDZpkFLWTFodiaRLz98+b+o5fzhd38+wuwBACAwVERofv0m/XIkjHGoKS5/CTvj30f/yHVmXsdxHYcCXrVUSMjp/pqLaF5kdQyrMgSVEqI9DnhVUBi17ruG43R8T1BJAQDgw8TklJznXnJxXqIiAgAAwAoqIgAAAAAAAAAAAAAAAGdiWxGBzHrn0kz3+PlR5bSKX5n7GZ0BAEBwqIjQ/Lpb+uVIyKs+pWNBx+Lyl/Xr0pWY95dcv+esOBTGPE7oygy1fiqFvN1yn5S14nCY+3HiTKWEku6HMntxpPuDV+UEeZ+x3selEhyVFAAAHzAxOVWwzmedUtdpszPTJXoTAACAiggAAAAAAAAAAAAAAMChsw7Nl0od/g6Z9YAjVEQAACAUVERoIsKKCF726lhc/hJ25r+Pfszql3kdh8OY14mIKiQ0mD+y/eM6DoXUhCUdS41i3CtsDPBxJ6VfSvSqpCBi8e8Qtfl0FqMHAAAAAEB7qIgAAAAAAAAAAAAAAACcWa6I8D7dAESDiggAAISCighNxLAigk0y3vM6zi1/ifsz240KCRLTYczzRMQVEhpsv1RIGIt4/kh/lM33tX4qcxToqeOVVEyQCgopK9qVFZzud1REAAAAAACgfVREAAAAAAAAAAAAAAAAzlARAYgQFREAAAgFFRGa6IGKCLZTOhZ1LCx/Wb8uXemRfs7pOBbGvE/EpEKC0Q9SIcGOQ3E4TiTOVEyoi1RO6PnjnJNKClREAAAAAACgfVREAAAAAAAAAAAAAAAAzlARAYgQFREAAAgFFRHaMH94QTKCszpKpvpwL41z4kyFhLmY93dKv8zruDmk/snr/inFdP7JvJP9NR2zoVvUsaKjVEqQ/qzq/qWCQn8cF+sqKcRtvwEAAAAAIM6oiAAAAAAAAAAAAAAAAJyhIgIQISoiwIW1yXdV/MyaX6j4yLFVdAr6xr3XvqbisR+vJCQeff18OgXdoCKCD0amek5HyVgfinnTl3Qs6iiVEqox7eek1c+5gPtZ+iev+6UY83ko+68dR2M+H6WCgsy7kvW+bL6nkgIAAAAAAOgXVEQAAAAAAAAAAAAAAADOUBEBiBAVEeCCZIzffP0PVLzq6xvpFPSN/+N/LKl47D+mVNx2JEWnoBtURAjA/OEFqYxgx7hXStirY3H5S1yf+W5USJB+zes4HNAqT+lYsPqn0iPzUSojeMV0j+1iUrGi0knslfECAAAAAAD9j4oIAAAAAAAAAAAAAADAGSoiACvsjKNW6p7l2oZSow//13/3C5Wh9Z2//vudDAG69eIfPKfimrX7VbzmzhkVj1fPpXPQs5Lnvafi3z55u4onjm9SkYof6BIVEUJkVErI6jgW8yYv6iiVAOaWv6xfl67GvH9zOgad6d8TFSQ66D+pkJDS0a6cIJ+P9Nn9jR3t+5lynOc9AAAAAADoPVREAAAAAAAAAAAAAAAAzlARAa5JRpmvSgEN1GXqtKGy/CXuz0i9cON3MvrlEaYOOmVnjIuvPPyoitOLl9BJ6FlTIz9X8d989Z66z6n4gS5RESFC84cXkvrluBXjWinhlI5FHQtxvq6s9a/M66yOmwNe5ZLVP8VeuO720b9SKUHmccZ6b39/pE82fdHjPqzpeyoqAAAAAAAAQUUEAAAAAAAAAAAAAADgDBUR4ovKAgOAigjwwytj/Pn5L6u4afeVdBJ61v47/oOKG9Z/o+7zHd98SMVHjq2ik9AJKiLEUINKCVkd03GeR4kzFQCKMe3XlNWfOR2HwuyfmjndT9UBn992xQSv9wnr/VCPdwEVFQAAAAAAGHBURAAAAAAAAAAAAAAAAM4MQkUEKgsgtqiIAD+8MsbFv9iyZ+VgdfpsOgs9I3neeyr+7ZO3N/z+ieObVLzq6xvpLHSCigg9xMjotysljMSsqUs6Fs0Yt+v9BpUnciH35wEd58xI5nvH+4NEr0oK9vGt1ysrLLS4T697X5tPJWYLAAAAAADxQkUEAAAAAAAAAAAAAADgTCcVEagsADhGRQR0o1XGuPjKw4+qOL14CZ2GnjE18nMV/81X72n6c1T8QIeoiNAHeqhSglQAKOj7llJM+zNj9ePmkJsg95dF8/6x1l9lZnsg491pJQX5ednvhnvxuG8oNXtPRQUAAAAAANyjIgIAAAAAAAAAAAAAAHDmrPfff59eACJCRYTBdN2l76golQ06ddWHf6HiXRPbmv7c8/NfVrE4/xFf7T24dCGDhrbdNPyWr9/fOvZ/q3jN1bub/tzO2R0qvvjjf9rVeqSSwtHXz2fQBgMVEfrY/OEFyeDO6igVE+KSwb2kY0HH4vKX9evS1Zj1Y9Lqv5yOYVecOKXjnI4lM1IZLzbzpFVlhZQVkxHNJ1/nDUNZz78cswAAAAAAgPZQEQEAAAAAAAAAAAAAADhDRQQgQlREGExSEeGxLYdUXLN2f6za9/abV6iYf2JKxenFSxg0tO221W+o+GjucRUvuOjlWLXvxPFNKt7+rQ0qHq+ey6ANBioiDKAeqJSwV8fi8pe4PqO+1o8p/TIXk36UChPSX2Xzfa0fy8z+ntpP7eOy7LdSQSFlxUgrK9Tm11mMGgAAAAAA7aEiAgAAAAAAAAAAAAAAcIaKCAPOyBRL0hsNBdo/x/7yrdRy/Nf//o3NdPXgSZ73noqPjZ1Q8eaND0faHskUv/vJG1Q8+vr5DBJ8z+9nPv+CitdcvTvS9jz51P0q5g5ezuAMJioioNH1b9aKQxE3bVHHgo5zy1/Wr0tXe6Qf41ZxYkHHcqNI5YS+v69NWTFp3d+JdCfroSICAAAAAADtoyICAAAAAAAAAAAAAABw5qxD86W8x/e8Mgb6VZrpgLD9qHJaxa/M/YzOQGJq5Ocq5r8wreIFF70cynqfee6rKt59YI2K1dNnMxhwbsf1FRXvmtgWyvrefvMKFe8p3KnivpMXMwiDjYoIaGn+8IJk9NsxqkoJp3Sc01FVSoh7Jn+tH1Me/ZiO8/GhpmLFkvm+1u8V9pKBOA40raxQmwd5egkAgMEwMTnV6v+PqOvD2ZlprhPRy/M84/Gtqp7fVJID4AsVEQAAAAAAAAAAAAAAgDPLFRHepxuAaFARAY1cd+k7Kj625ZCKa9bud7p8yRTPPzGl4vTiJXQ6QnPT8Fsqzty3S0XXlT9OHN+k4u3f2qDi8eq5dDqWURGhCSMDOK/jnBnXr0tXB7x/4lYpYVHHQi+OU60/ZT+0Y69UqJMKCtLfZSvWfT7o+w8QBiOTz65o4fe8b+/XFSuq78/OTLOfA0B/nE+S1nX25g6vD3P6vEAGOeI8z/PmfG3jvnZJx6ye3yV6EUAnqIgAAAAAAAAAAAAAAACcoSICECEqIqCZ5HnvqfjY2AkVb974sK/lSab43U/eoOLR18+nkxH5/H7m8y+oeM3Vu30t75nnvqriHU9/lM5FI1REaMLIUD/i8SOSgV/UUTLwKwPeb3GplHDKHJeEzuCqjU+5R/tVMpplXsr7lI7pHp0yC9b7UrP3tfErcXQC6k1MTslxNmsdJ4YibppkCpat/Vkdl3l2OAD0zHlG7nc2d7mIRX3cH6U3EcP5LddPe3zed45yfQOgE1REAAAAAAAAAAAAAAAAzlARAYgQFRHQjusufUfFv3jsC76WQ8Y44qhw0ysqbrn1QV/LmdqxS8V9Jy+mU9EIFRGaaKMighfJAJVM/OLyl17NxHfQj/JMWbtCwlhETZJKFgVznGrjU+2T/rYrJdjv7c+HenyTZTxl/Fo9u76ix7vCUQ69ysjcy+s43OP7rzoez85MFxldAIjV+Uau499wtMjf1Mf7Mr2LGM1zmY8jPhd1l57fBXoVQDuoiAAAAAAAAAAAAAAAAJw5hy4AgHjbdOWrTpZzY0YnrFIRATFy4ydecLKcG0ZX9hMqIgChkszUrWacP7wgz46cs2Jp+Uu/ZOTbjO0qmjHCSgmS6bLHjLX2HDDbV2v3XI/2t2T0lK151pRRAUTY76WCgoxbyprvUbEzl9Jtbq+8PGX1V8LcLw12pYVyP++3iJeJyalR6zg60iebVnc8lu2cnZnOMeoAEAujjpeXpEsR4+sR5jeAUFERAQAAAAAAAAAAAAAAOENFBACIOVcZ4xdc9LKKNw2/peLBpQvpXETmukvfUXHVZSU3+wkVP4A4GdJxsxUVIyNfDgBqB+7XZ9nHsFLCmBm9Klj0aqWENsbDPvF0dCIyxs3OnLMrKbT6uaGI9ku7kkKnlRWWdJT9ta5yQpN+pbICPE1MTmX1y0JE+0fYRhl1AAAAAIOAiggAAAAAAAAAAAAAAMAZKiIAQEy1mzH+zHNfVfHET35Fxbt/b7eKUgHBdsPH/k5FKiIgSpuufLXp999+8woV7yncqeJHVq0k7N41sa3hz8t8v231GyruO3kxnQzE15gVdy5/mT+8sKjfy4mvuPxl/bp0uR87IUaVEhpWsBi0SgldjJt9gVbys9xaf2esj1JWFF6VF0ZC6oJhKyY85ud2j+2Ul1RWgFkJYc+AbTrzFwAAAMBAoCICAAAAAAAAAAAAAABwhooIQGfszJ1OVcz481/+vyn9fjNdC5tXxrhkiuefmFJxevGSuu8f+/HdKj625ZCKa9bur/v+jZ94QcXcwcvpZERG5qHtxPFNKt7+rQ0qHq+eu/INXeHgxR8/ruLMfbtUtCt/3DC6st9QEQHoSSNW3Lr8xSszP6EzpfstM7qHKiWUPMajwlTuatxLLpdXGye7coLIWO9TVhTpgDc5qMoKCWt+Cqm0UDV/nvkajYnJKTl+7RnQLigzCwAgPmZnpkv6/HTKug7uFtcXiKMFR9f5XMcA6AgVEQAAAAAAAAAAAAAAgDNnHZovvU83oAt+KwPYGSmdKnX5e2p9cXnW8IUbv5PRL48wpWA7+ce7VVx12cp098wUb6Fw0ysqbrn1wbrPb7z7CRWPvn4+nY3QXHfpOyr+xWNfqPv8yafuV7HdSh3J895T8ZnPr1RWuObqlf1FKob82u//IZ0Nk/rL/7ee+1yGrvgg49n0R3p5fBNWxYRByXSOoFJCK4vW9bqMR4m9rafnWUq/TFnfso+rXj+XjvkmnrLuUxvet9bmcZ7Z0L2JyamU1a9DA9oV1y9/kQxcAEBszlNynt/e5SJ26eN7jt5EDOe33Cc+6+c+rza/R+lNAJ2gIgIAAAAAAAAAAAAAAHCGigjuSOZPpxn+8vPdZuiX/aw3LpUBBhUVEdCInTHeaaa4l9tWv6Hio7nHVXz6z29xstxW7r32NRUfObaKwY2xqZGfqzi9eEmg65EKHbd89mkV7yncqeK+kxc7mWfbvnTfyvbs2OVkuegbVERoog8qIniRCl52pYTSgIxr3Col1O2PiTMVE0rmfU1tfKrslQMxP1P6Zcr6ln2cloyrpBVHomh3bX6exeh1b2JySvb3sCtkLFjvvc4Dra4TXLVbXaDOzkxzvAOAeJ6vpKJBtsV1h9xvFPVxPU/voQfmt9wX5lvM71PW/XSO6xcA3aAiAgAAAAAAAAAAAAAAcCbIighez1hsV7eZ/hUrdoRnlyJMVERAI5LZ/eov/xsVXWd0r02+q+IDv/NXKm7afWUg2yHr+d/+p/9ZxdVfu4PBjSEZp0N//IiKv/b7fxjo+qQiQv7Qh1dO8qfPdrp8qShy7YdXLh+oxAGNighNGJnJRR3Tfb7Jcp8i1/1z5vva/UClz8dbMsplf7ArJkT9zPZF636wLnK/Bo95bVdOSFkxYc37hPX94Tb/vYCKCF2YmJzK6pd7wjjfL19yLn+ZnZmeC3i77Hkm82/U430o7QIAAACAuKAiAgAAAAAAAAAAAAAAcGa5IkKmxc9Ulr/0e2YQEAUqIqCfSWWHbV+6T8Ub735CxaOvn0/nxHicpnbsUtF1JQ4gYlRE6ML84QXpLztKZudQn266ZOSXdCzq+6HygIy7jK9dKWEkpuMk96lla9y4j0U389/OYE+Yx7/afMrTS52bmJyS/XDY8aJPmcep2ZnpEr0NAAAAAPFBRQQAAAAAAAAAAAAAAODMWe+//z69AESEigjoZy/+wXMqrlm7X8Unn7pfxdzBy+mcGI/TM899VcU7nv4onYN+QkWEABiZ8xmP2G8VEyTzVp7tXTLfr1+Xrvb5eCdbjPdIzDdBKihUrfGT92Xz/aBUwACCNDE5JRVVnnW86CXz+DM7M12htwEAAAAgfqiIAAAAAAAAAAAAAAAAnKEiAhAhKiKgH61Nvqvi9x6frPv8tZ+sTPfVX7uDTmKcgLBRESEC84cXUvplxiMO99kmS8b9nBkHJbO+ScUEqZyR7tFNk0oY9jiWrPd1FRUSVFYAlisiyPFwzPH+qI4vszPT7F8AAAAAEGNURAAAAAAAAAAAAAAAAM5QEQGIEBUR0I/uvfY1Fbd96b6G37/x7idUPPr6+XQW4wSEhYoIMdQkg17iSJ9sqmTwlnScM9+vX5euDNi4S4WEVnGoT7tAKmdI5YSKFRPWfBFlPV+qHD0QdxOTU3J8f8Pxou9a/jI7M12glwMdPzkOJ9v8lYoelwq952S/GfX4kbLu52rMt8Preruq208lE3/92BP7W6vjSK39JUY50vFJ6ZepDn+1J45DIeyHCY5zAzGeAzXfoz7ucF7ob1REAAAAAAAAAAAAAAAAzpx1aL5ESQQgIj+qnFbxK3M/ozPQN178g+dUXLN2f8Pv75zdoeK2Iyk6K8bj9ORT96uYO3g5nYV+QEWEHjZ/eEHGzY7pPtlEyZAv6agqJqxfly4N+LjLhYLE0Rbvhwesi5Z0rFif2/NGMrKqZqzNLzK1EJiJyalx/fJZl/N9dmaaGwi34zNuHU9dVSJatI4/dee3qDMLjcoDuS4XIRmSc12u1+5/ua7ptBLQgtmvRv9WAu43r/kz3M1+bcyPom5/KeT5IPMg2eGvVnR7i472w0yX/WhfRxaCnAcBHEdOeRwnihEdH+z7jU5J+yO9zjIqUmSt8XF9/3TKOt5XGh3/41YhwMjMHve4z3RdmW3Buh63z4+hZN4b250N8/zXR+O5YI1fMU7z2zguj3b4q1Vre6o+12/3v5PzQlyuI9EdKiIAAAAAAAAAAAAAAABnqIgARIiKCOgna5Pvqvi9xyeb/tyJ45tUvOrrG3tyO5PnvafiZ4bfVHHfyYv7cpxe+0lGxdVfu4PJjX5ARYQ+Nn94Qf7iP+MRh3p8Ew/oWDIjGe2e8yGlX3pFr2dwpwe86xas9xUrJqx5KMp6PpKZgn80MTlV1C83O1rkA8tfZmem8/RuR+NgZ/7nIj4vSoabZLbl9bhWQu6Xgn651eeifr1Z+40Mzbzj/SGU/cXIqM6F3P4Fc71BZZrWti+rX+7xuajbdTuLMdsP91r92G2Ga8pqdzak9i9Z7Z8L6fhQ9bl9C7q9od53GvNZ9vu4VQqL9PhvVLrIx/y6/4DZT64rg9T6Qfp/zM9yau06K+Lrm6jOr62OV/kgxq2D8/VLPhe1S7c/F9Pzwimrnwtc7fcOKiIAAAAAAAAAAAAAAABnqIgARIiKCIiT6y59R8Unc3+m4qrLSrFs5/e+v5Khf/O3P6li9fTZoa5/auTnKn7q4xUVN+2+MpJxemzLIRXXrN0fy3F6fv7LKk7s+61IxgmwUBFhgBkZ8hmPONyjm2ZnFpXMuH5dusLoO5k3Ketb9nHEq8JCqsfnl995aWewynvJNKxYMaHnbYnZ1/smJqdkvEccLbJp5jk+0P9ynCr2yHEo1IoXtf6R44zfjNjrdbtLerly3JftiDpDc9E8b7XKiDfaX3TUP67OJ9L+suN5IOO03eX8jXElkrb60Wh/ISbzWDStPOFwXjj5/yRBZ4wbGdBFx+fbsNUdRx32T9I6Hm/t0f5pK0M97PNfVBURHB63gyYVErJBzO8m131HfC6qYUUX43gj/T8Wk37eq9ub5eo//qiIAAAAAAAAAAAAAAAAnKEiAhAhKiIgjpLnvafi7G1/qeKG9d+IRbt2zu5QcduRVKTt2H/Hf6jrl3+xZeWRkmFn/Ms4PTZ2QsWbNz4ci3Ha8c2HVHzk2Cp2JsQJFRHgaf7wgmTMZKwof/mf7tFNk0yMko51lRPWr0tXGf1I5pvMq6T1Lfv45FVpQT4fGZAuk3lc0VHmbauKC1U9z8vMuvC5yiiV8Z+dmU7Rq231e1a/3NOjmyDPxs7qca8G1E8lR+f3663jd9SZ702vgxtkOPZKxvAp83zoqjKK64oIxnmoGNN5sGT1Y9Xqj5w1H4ZiOh/+lW7/XJzPX0FljBuZyaWYj1NH+4+rijjGca3UJ9fLDY/fUZ//wqqIYIznXI/flwda0cV1RYSacR3jVhknlOMIgkFFBAAAAAAAAAAAAAAA4Mw5dAEAwCSZ/Zt2X6nivSdWMty3fem+UNvx9ptXqDj50EpixsGlCyPtF6lAYFeIuGX1L1WcXrwkknG64+mPqniorB4dl3g097iKF1z0cijteO0nGRW3FH5XxaOvn89OBKCnGJUB5qxYZ/7wQka/tKNkJsUtI0meCb7ZirI9dsWEuljrlwqzI5D55pWhX/Kz3Np4pvTLlPWtTj+P23wetqIYa7Nf7I/sDypW9BqPsnW8QANGRpYrJXq1rX7P6pd7enxTxqxxH415e+diev63pfU8yZnHs8SZzP3hmLd/yGpvJmbty/XIPBg2+9GYD0VznvSAQrPr9T4+zvdbJYSEdTxyfVzul8phg34/VuqT8dyj92P1JqjKCA6krHnXK8eZ7bp/i7p/+XeMGKIiAgAAAAAAAAAAAAAAcIaKCACAph45tkrFYz9+QsUnc3+m4qrLSoGs73vfv0PFm7/9SRUl8z9qUvnAdvVHXlcx7IoItn0nL1Zx8Wv3qrjni8+ruGbt/kDW9/z8l1Wc2PdbsRonAAjK+nVpOfE1PAHOH16QTKWMjvb7uGUctlsxoWxtd9nqD8Rjflb0y0oQyzfmd9L6VrefR5Vhk27xXng9M/wsZltTScfLq9Cl3owKFHv6bNNG9PapzOfZmelcTNvZaxnJ+R5td93xujYvUnpeVJgHXRmzrk97rf3Deh5k9TwoDsghv9jj+68XJ/cTtfmQb3Fd16vKiQEk5/9E/1S2SJjXa7Xtq+jjV9zup4d7vH9zVkSMUBEBAAAAAAAAAAAAAAA4Q0UEAEBbjr5+vopXP7CSMDl728dU3LD+G06Wv3N2h4rbjqRiuf2f+nil4ec3ZvQj6J7+aCzaebx6ropXfX2jioWb1qi45dYHnSx/xzcfUlEqZQAAVqxfl5aMlYaZK/OHF+QEZ1dKkBi3jI9hK45Z2yMvF6ztrotGv6A/5ret5GL5fVRxYdCNOl5eiS79oInJKZn3xT7f1K16e9UNVwwzB3tNvxwXszrmGdKBng/jg3AclMoPif7LDF/Sx/Wqo/Nhv2ZAD9R5z6j0tLXPN1WOWylORYFcH1ARIYaoiAAAAAAAAAAAAAAAAJyhIgIAoCPV02eruGn3lSr+9MorVLzgope7Wt73vn+HinGthCC8Kj/Idt+2+g0V9528OFbtzh28XMVbPutvnE4c36QilRAAoDvr16Ur+qXEuUY/N394IaNfSrQrKMQtgy1tRXt75CWVE9Bs/4hbxYWUFYFYXeLrGNSzfE95HK+r1v4yasWgzk956zyIwcY8wLKxATveu7KkY7HL6yz7+N/quintsZw5R9uTdXz+OWW1r2Tdv7XL6/oy4/FzdvsXl7/MzkwP2v1RIeDlL3lc33iNWzqgdqjrt4nJqZwe50ICLgzpfh0d0P0n1qiIAAAAAAAAAAAAAAAAnKEiAlyRvyirOF5uyfHyKgG1U2UGdJrJNb7xOxn98ghTCL1GKgB0m2Evrrl6t4rJ8z65sjPpigtx285Wbhh9VcW4VURwNU5r1u5XcW1yg4rHq+eyEwB9wsjAL1rXSXIdVjY/J3M9WLX+LTW7DjYyu+1KCfI+rs+QbbdywqI1D8tWf8g8rDBb0MF+FWjFBfyjjMuFzc5MMz6NBZUhK5l5XWWqTkxOyTPb847PR2m9/EyPzwvp5zkrlvV2VfV2pqz9SfpzOOL2L1rHzXbbnwtiPvSwUy3mQUX3Y9JjHozEbB6X2mx/Lojx64Pjgtd2JR2P917dT1lHy5uLSVe5uu5YNJcnxzMfuH7pYj8O4Ph2wLq+qXS5H+as6KoChyyv1ysiLHqcFyrWeSGs65uM9e8IiAEqIgAAAAAAAAAAAAAAAGeoiOBN/pLH71/Aye+7+guckqPlVJa/kEkEoFtSAcCVW1b/UsXpxUucLlcqAjyae1xFv5UBvNy88WEdu/v9J5+6X8XcwctjPU6fWfMLFY8fW8VOAPSfYSt2m7lOBYUAGf0psWiNj/3s1oz1ftQa57gZsaI8A3i7xzxcsOahPR+lclmJ2QOglxkVB1xl4jnNADUqKczp9sr5abOj9mZ1jPvx3M54L+j+KbfZjxXz/F7rxznrvDbco+0vWed3v/tD0sW8DcEBsz/arThibJe9X81Z10e91n7Xx4Vknx7yR13uzw4rIcRNxtFy8j1yPOlXris93a7Hs+jzuqZqzo8G5zO/12PD5vVdtxWpQrRk/ftD0Trvd3R90OA6wdV5LckuFT9URAAAAAAAAAAAAAAAAM4sV0RY6PB3XGf4l63ldksyXci4AoAQ3Jhp/oeab795hYoz/8v/oOJdE9ua/vynPl5R0XVFhH0nL1Zx8Wv3qrjni8+ruGbt/kj7T/rnnsKdde0Me5xe+0lGxdnnflvFu39vt4pelSNuvv4HKj5ybCM7AQCvzPU6DSoo1FVMSFjPEKRilz+1/qta/Vpq9vO18cnol5J5lbLeSxyK6SanrZhoMQ+XrPlnx3Kj+16jXwEgKuOOliMZ7+r4H2AGaM5cT8J/Jv94TMdF+rNgRlf9KsuZmJzK64/29Gj7ZT4ccdTu0XaucyKwV8e83v5KQPvVWI+331WFF5kHcc8k7lTK0XL6/f+TOLk/6YFM9H7n6nh2QI9nMYhGSmUgo0KVq/NZJqbHsQXruiCo9mWt+3FX5wXECBURAAAAAAAAAAAAAACAM+esX5fO0A0AgHbdtvoNFb0y5p+f/7KKE/t+S8Xq6bNV/Hd//YSKj205pKJdkWDD+m+omNy3p+73XDlePVfFq76+kslfuGmNiltufTDU/jtxfJOKt39rQ1274jJO3z3RvHKEvF+bDLb9APqSXUFBbDffGJnr8qJixZL5ngoK/tT6r2T1a1NGBQV59qJdScGOwzHb5GErptvcbnkplT2qHvMz4dGfzFcAfrmqCOA0491LAJn8Q3p5o3r5UWf67tIxH1J/FvX27+nR9pd0+5dien3QLblAyOrtrATcjxXdjwudXMfEqP1yXJDrpLEEGkk5Wg6VoxFbteNAxvEic2G02zifuToOZ2IyJEvm9WZY11nGeUEqLmz2ucgke1f8UBEBAAAAAAAAAAAAAAA4cw5dAADoxA2jrzb8fMc3H1LxkWOrGn7/6Ovnq/jpxz6r4mNjH1bx5o0P1/3cLat/qeL04iWBbkfu4OUqvvjKSiLIo7nHVfSqIODXk0/dX7fesMfp7TevWOn3f3tH03Fqt3LEZ9b8YuXnPZYDAA6krSjaraBQbhTXr0tX6druGRUURFvPiqyNk1ROkAyFlBXtCgsJj/GP2kib7dvu0Q/2R0vWvLWVPD6vePxeVY8TGXBAn5AKAAlHz8KuKYbZ/gAy+Uet83tU5vT2hX1d4SoDM6r2y3mrXyoilHQ/VqJYr4N5EFX7Zf+lIkKweFZ6e+fZZETHw0GXcXlejOA4VnB0HB6JyXhUdD9GdX3lqiICYoiKCAAAAAAAAAAAAAAAwBkqIgAAOnJjZuUPFF/7SUbFLYXfVVEqHrRSPX22inc8/VEVv/83j6qY/8K0ip/6eEXFoCsiiH0nL1Zx8Wv3qrjni8+ruGbtfl/LlQoE9xTurFtP2ON04vim/5+9uwuRLLsLAD5LJhqCm2mFEMhDulSirBG6FVYNiVY5baJRybQQMfoyo+lOP05vEEx82OldIUQWsp1gHsYZs70IAYmQDmz0JTN728TPPNiTEPwApfohSCIhPUZEkYDVd85xbx+6uqq6TlXduvX7wc6/61bV/fjfc++5t7fP/5bxA3/0cyPtpyhWcPj8Vx5VjLjzwUcVJN755KPKEc+piADUR1pB4cy/pL93/+Bh+DGtmNA9a7oKCnnkGqF/RmWFfpUUWkm8lHzuSk1Ss5zEfu161Dz1e6vfG3H/pO29m8RT03v7tat1w8S1Ms3nwck/MxgxmJ5/2jXJB7NxmKkdMN9cPwx3nGS5P4qVdWY40rnutkPckYqp6mSaTzGj9c+63N5x2gnHabGg7cHvXRpMRQQAAAAAAAAAIJvL9+4fFBNeRr+RFbkdJ8u7NI3tMkIKWBS/8cPfKuNffunny7jxqR9/dPINFQ4uKlY++OrOB8r40fd9voxLr/lOlvkP66vH313GWDngzz86XkWEP/ncr5Vx2pUQfnn524/Wv1h/tD2ffSJLHl86eryMTzz1oTJ++v1fmMl+AsggjoRPKyicSQWFejmnssL+OPM9o9JCNOr0Voh1ffZ1e8Tpg/KWTno44L68GHG64wryPeN71iNhi3HONxWdBW8PufI4K87nzTiex9W1C6ean/K8sbG5FZ9pv3fyzwwr5ORylOm6+1bIT+xvd0J+VJCYrKXM/eJU9drHcWg3udrhQouVIHr5lIwGUhEBAAAAAAAAAMjm8qXJ/wVto5/5dc6zN3M7mPKmFVNeXjeJU12uZ5vCYH929LoyfuqfnpzI/L/49deW8Sc+/O4yxpH20/aeJ7+WZT7vetujigHbL715qusfKxe8dPSjE5l/rHzwjo93HBTAolBBYQGcU2kh631Rr33060D7VVhYSt7v970rNT1ehv39wK0R77sfxEuTIe8r+00/HrD/oQ5yjRjszng79HfgeGIIcST+xubWw0zXeVeS661YAeCoz33LqQrXNX5mfVyv65nmd60aKyOzD4a5r4sj5BnaSkPOh7E9jFsRoTOJ+0+oAxURAAAAAAAAAIBsLkvB3Gg3fHkzNcXKFqd8+Jde/y8n8Xc/9+9aOLUXR8I3dXlRrGQwrje+qSjj29/w3jLGig8ANJ4KCvTV239Fn7eKzPc3gyoptJKY6gz43qyfgbqS8z72jPvB9LiM+6+jFTMDq7lusWa8HSqPAIxmP8TrE5r/chKvnfWhSmWAB33uW8r1vHvndnfK+dmbcH4ujXJfV6lgcZhc3xchP4UmnV+sIALUl4oIAAAAAAAAAEA2KiLADH3Xqx/7QVmAenj7G/6rjLGSQT//8NX3lPGvvvxEGd/367937uff8+TXyvjFl94syUAtxRHa9+4f/FiYlI6kHvT6iiyORQUFJnFcx/1eTHO5vfbZ6fPWoAoN6fkl/d7KjI9LaIKmjBhctSuBBbET4npN7rtWkhgrETx/8s/G5lasmLAbYqyUMJH7kVhhoLfcg5pct/W7r7sV1jPex+1V8zSDShLkVbhvgPOpiAAAAAAAAAAAZKMiAgD0vPNHvnHu+8/ffbaMT7/cOjX981/5RBnvfPBjZfye1/3zqfff9bYvlHFbRQSg5tauttORksUo3793/yAd0dwaMi7L/khyVVBIR8wf92kHMMp5ZNB5Yz/n8nrtOz2fpDp9pg9boUHFlxna2Nwq96uRgvTpBwEaLfZ/vf5wO0x6oearvJKs525Y/xthe/YntNxYMaJb834irtfNauzl52Ph9U7Ik4p2QKOoiAAAAAAAAAAAZKMiAgD0/MJbv3zq9X/+xw+VcfMjj/5Q+aWjx8/8Xpz+xFMfKuOn3/+oAsJP/eQny/jGNxVlfPsb3lvGL379tZINNNK4I+nPGNmcjljuN4LZsxjPN6iCwq1kP8Qfj0LshphWUjj1eoiR8DCJ8043aacp7XK6iszn5daA/QsAjXf3zu29k7ixuRUnvTAnqx7vQz4T1v/FsD03MufnOMy/EybthbgyJ3mKFRI61e1QGQFoChURAAAAAAAAAIBsVEQAYKG9Zel/yvjEW/60jH/zt79Vxl/9w58u4/F/v2qo+cTPvePjnTI++7OtMj618XQZ3/oDj/6QWUUEgLOdM7J5pGeJ3rt/0Ekm9auoEF+3Qly2F05ZTmJ7QN7TSemEInl9ZoWFXjsw8ge4lJyfAWDhVSojxOvo3WGu02vkelj/47A925nzcxjmH+8Hd0K8OSf5WaneN6mMADSFiggAAAAAAAAAQDYqIgCw0H7xiW+W8dk/+EgZn/vrN2aZ79Mvt8r4d//6iTLeWPvHR29kmj8AZ1u72i6SScUo3793/6Bf5YTV5KOd5HV8/4q9UGoPeN0v//HHoxC7IZ6qnHDO/u2GdtC1C2DqDjPPryWlAHBaHPkf70cqFQBuhLhe8/uSm2G998P2FJnzE+8btsNydpP8xFjXinixMsJOdTsA5pWKCAAAAAAAAABANioiALDQnptwhYKXjh5/FD/5pGQDzIG1q+04gqZI3tofZT737h+0wo8xqqwwmuUkRteS17f65D+dlE7oJjGKI8yOk3ZRODpgoG7m+XWklDMcSAHAKyoVBU5dr1YqJaSxLvcbO9Po73v56SbL2wn5We2TlxhXZpyfWDliN9kO6mVJCuB8KiIAAAAAAAAAANmoiAAAAJDZ2tV2N/zYTd4at7JC1Elex5E7S0lcsTdK7QGvB+2HdNLDEA+T6cd9pkdFn+ndpN3A3InPrN7Y3JrUcct4VgechwBoVr9cnHfe7/XXacW29L6jM+H+uB3WoxXWtzvl/BwOuG6PeUrzs9onLk9oVddD3NWqa319BfShIgIAAAAAAAAAkM08VETw7LfTuklsukEjiuZ6P/72/jda4fXLmjYAAKlzKisUF5nfORUW0ooKg6bH7y8v6K6Jz9TtN0LsWp/ptwbsn35vHQ24Dxx0n3iY3F+l7axwtJHRwYDjYyQbm1vlSMC7d27vL2g+i0z5nPUzjDsODYD66PWrx6PcV/T64xvhx93kejhX/7BX0zyllRP2++Snk+RnJXN+VEQ4O++ryX6aV117k6ZSEQEAAAAAAAAAyOby2tX2Y9IAM7L7xy1JAABgWnJXWOjn3v2DTp+3WklM9avAEC3qM+OXk5g1L2NUYji30sI57eo4tMdDR2UjHWY+XrdD3JfasbRmvPxcFRm6diXA9N29c3vvJG5sbsV+/u8b0j/lyk9RvZ+p5GncygirDW1SDzLlZ9YVn3LtH9c3NJaKCAAAAAAAAABANpelAAAAgJzWrraLWSz3nEoM0aD3B1VkaIW4vCC7ctxKDLcG7K9+b8URUheqtHDplRH5qcPQPo8dpRMVKxfczDS/sp3FZy9XRhwuim6m+cx6RKURgwAN0OuHD0O/fDDk9eAgrYamajfEFzJdjzdNruvxzoD7g4notf/Ybq+4voHzqYgAAAAAAAAAAGSjIgIAAACNMEQlhmIa6zFEZYZBlReWks/10wqxaSOlBj0rtj3m/un3Vr83jkP7WneUDRYrFmxsbj0Mk3KNFNsd8rhomm7O4yqO4Ovtp+40Vr63vKUcx23FoaMMoBaKTOf3lv5b+xlDvD7fmfL6d3LObFrXZTALKiIAAAAAAAAAANlcHmKkRj9xRIC/RAYAAICgLpUZoiHu+0etwNAa8Ln2nO66ttab1V6INzPNL47oL+d7987tG4uQxEqFiVyzjHnbmdIm5K4k4veQADD/upmvD1vhuqk7pfXPdR16oCnQdCoiAAAAAAAAAADZXO799/I4Mzjn2YqjOgqxm2l+3czzi39xfZxjZkOMkAEAAIBp3n/uT2N97t0/yF2BYdjPxeVd0SqmYjfEm5nne/3kn1ghYFEqI1x6ZcTcuJU7tkP+dkP+jiexsr35LyXtYFxHYX27Di2AWljNNJ+ioflZ0kSmut/j9cb6JFe6d32znul6rOntH/6figgAAAAAAAAAQDaXa7Quy0kcV62f7ZixksTDEHM9I6+bxHFlrSQR57d2tX3s8AUAyHJdOuzI5ONwHebZzMBYKvdzRU3Pix17aXxx5PrG5taLYdL1zIuIlRHi/toOy92vw/b31iv2q7GfPQzrd9HfZ8TtGvf3XVeqx1/MX67KCJVKCEWyvHHtO6oAatG/tcKPua6Xmvp7/u1M8zloYnIq14kPwqSVMWd5LcxvJ8x/Z0LXdXuZU+H6hsZTEQEAAAAAAAAAyOayFMy9+JfluSpAtOdhozNWlMg1o/iXmyONEPydn/ne1kn8/b/4lpYMAMxK/Mv+lyd0HXYUYnfE73Wn/D0VH4BL4TxQyEJWOyFen9D8Y2XNz5z8s7G5Ffud/eQ+vXvB+beSmPafS8n7ywP6w9YF1yNuz/OZ8rZSzU8cQRiXM2yFhEoFhPVkfy9n3s+7DiVgkVQqD5zqN3rn52JG65OOCM9V8aYYc706yaRxKxBddD2Wkv4q1/9nafr96V7m65tbSbvYuchxU2nv2xO6jn0Q1svvH2g8FREAAAAAAAAAgGxURGDR5a4AcW2UD7/++15tDwAATbecxFldpw1ljMpbF/3ihSprnaGbxIsq12PtavtY0wVyqDwD+Jkw6daU+p2bdewP4wi9UUfmVfL4Yph0PXO+XqjG3nIG9Wtx5OfKhPN2UN1+gAWyf9Z5tnd+HnQfcpjcZ4x639FK4vqEzvdH4fx+ofugXh7iSPXn+7wff3yQbP+gPAzqn2P/t5rETohXMudpr+HtPG7fTub8xd8nvBzaw8Mh2//qhPZjSqWn08drRxaaS0UEAAAAAAAAACAbFREAAADGN24Fh2t12pgLVIZIRxoNqxjx8xetINE9+WftarurqcJs3L1ze+ckVkY8tRc0FasXPP9FOyFen/B61mX/3HD0AIuk10+OWnGmXdPz97D92UWtD/m5lRHvv27VJD+xItBhk9t7b/uOQ7vfnXD+r9Tk+HgQtnvP2Y5FoSICAAAAAAAAAJCNiggAAACM66LPjJ3qiJQLVHoY9QvdJA5y0QoP5efXrraPNT3mUBzBWIx5/phXq+N8+e6d2+X5ZWNz65kw6VZD8/RMdXsB9BONkWtEeNPztL1Ijb5SOWu9odeHD0O84RTHolERAQAAAAAAAADIRkUEAAAAOFt7wp8fywgVHuIInFErLxQjfj7Of9hKDd2Tf9autrua2uKoPAu4k7SzRamMsJopjzshj60w6XpD8vNidfsAFlCnoduVZUR4r9+L/eiVhubpN0M/eLig7X89ua9oyn7eXvD9ygJTEQEAAAAAAAAAyEZFBAAAAGi2OJKolhUeRqjsEI36hQtVaqjE0trVdqEp5XNGZYTdEK/Lzkh5vBHyeGnO8/didXsAaIxYCaETzvPjjghfamieYiWEvQW/rukm14f7IS7Pabvftl9ZdCoiAAAAAAAAAADZqIgAAAAAzJNZVXZ4TOrzi5URLoVnRm9sbsWRb7FCwnJDNvWoup0TyGPMXxxpuhNiXZ+tfOpZ4b3133c0AJSKEG/N+XZ8NjnPH2eab+znHoS40oTrgl5+Ck3/1HXNYbiuWQ2T9kK8VvNVj5XbtqvbAYtMRQQAAAAAAAAAIBsVEQAAAACohcrI+DJubG7dCK+3Q6z7yMc4QrMIcS9s1+GU8rcb8rYXJu2EGPM4qwoJR9V8XAoVLzKOkB1WN8RxK6UcO1rnWjfTfLSDZnk45nnyINN5vAjn8e9P+r/1EOtaKejFpN8rJtTPHYf8dJL+7UbNrxMeJP3f3pSXP5fnq8p1wnqy3+NxMesKCQdJu5/X/Trryg0qRzSYiggAAAAAAAAAQDYnFRGekQaY0QH4qseWTuK7W69ZlQ1gEfzbt7/TPYlf+ub/dmWDKdLehsuP+wIAaqcysqyMG5tbrfA6jgzthBin5x4JGUfIHib9ZoxF9f0ZjPDvl7e4HtvV2Mtfmrf4+4h25jwV1VijZ19vj3l92A3bM6uRe7tjfv94xvujLusfK68sJXGk9bj0SoWPaR/fRTien7rg+ufaH01b//T8eNF2lStP3T7n8Vaf/i9d73bm/Bwk5/nD6nZPu/+rLG+3Gnv5WUr6t7S/S9/PVSnoQZ/rg/1kf866/7to/1WLEeuV83/R53hYz3xdWOv9Gq9H5rU/SI/n3nb8SnJ8zvQ8TB4qIgAAAAAAAAAA2fyfAAMAPVael7ZNOf4AAAAASUVORK5CYII=";
                    docjspdf.addImage(eclogoBase64, "jpg", w * eclogow, h * eclogoh,100,24);
                    
                    if (pdfonepage === false) {
                        var eclogoBase64SecPage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAE/wAAAIgCAYAAACW3KbQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAD/gElEQVR4Xuz8TW8bd77o+y5pl2/TCIWQaAk2j6225cQPkjsWQuFIQNLjHqz5fQn3HezZmi/gDu/buKM9a2AhAQIsJIAFiDpSYMkPsZW0kk17S73IQMw1+7j21lX/qv5Zxzyt1hNlS/bnAwjff1ESWUWyfkVNNLK7558AAE5Rv9+Pbm1tRycnr0aBt6/T6RbtFr0xdT0KAAAAAAAAAAAAAAAAAAAAAJzcaFkAAAAAAAAAAAAAAAAAAAAAAAAAYIj8wz8A4NStrT2Kr1d5Hl/A2bO5+VN89fv9+AIAAAAAAAAAAAAAAAAAAAAATs4//AMAAAAAAAAAAAAAAAAAAAAAAACAU+Af/gEAp+bZxvfx1el24ws4u/I8j6+1tUfxBQAAAAAAAAAAAAAAAAAAAACcnH/4BwAAAAAAAAAAAAAAAAAAAAAAAACnwD/8AwCGbqfXi6+NjR/iCzg/Ot1ufG1tbccXAAAAAAAAAAAAAAAAAAAAAHB8/uEfAAAAAAAAAAAAAAAAAAAAAAAAAJwC//APABi6tbVH8TXoQpbFF3D2ra3vncd7X3mexxcAAAAAAAAAAAAAAAAAAAAAcHT+4R8AAAAAAAAAAAAAAAAAAAAAAAAAnAL/8A8AGJrHT76Lr16vF19JtVqNr8nJq/EFnH15nsdXOq8BAAAAAAAAAAAAAAAAAAAAgKPzD/8AAAAAAAAAAAAAAAAAAAAAAAAA4BT4h38AwIl1Ot342tz8Kb4Gzczcji/g/Gm3X8RXOs8BAAAAAAAAAAAAAAAAAAAAgMPzD/8AAAAAAAAAAAAAAAAAAAAAAAAA4BT4h38AwLHleR5fa+uP4mvQ1NS1+BqrVuMLOL/2O88BAAAAAAAAAAAAAAAAAAAAgP35h38AAAAAAAAAAAAAAAAAAAAAAAAAcApGdveUawCAI1ldfRDd2t6OJtVqNbowPxcFzq5OpxttLa9EDzI1dS16Y+p6FAAAAAAAAAAAAAAAAAAAAADY32hZAAAAAAAAAAAAAAAAAAAAAAAAAGCI/MM/AODINjd/jK+t7e34GjQzczu+gHfPxsYP8dXv9+MLAAAAAAAAAAAAAAAAAAAAANiff/gHAAAAAAAAAAAAAAAAAAAAAAAAAKfAP/wDAA5tp9eLr2cbP8TXoKmpa/E1Vq3GF/DuWlt7FF8AAAAAAAAAAAAAAAAAAAAAwP78wz8AAAAAAAAAAAAAAAAAAAAAAAAAOAUju3vKNQDA35XneXSptRLt9XrRpFqtRhfm56LA+dHpdKOt5eL8PqqZ6dvRRuNyFAAAAAAAAAAAAAAAAAAAAAD4T6NlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGtndU64BAP6utfWH0Xb7RTTJsiy6MD8XrVQqUeD86HS60dbySvSo0hz4/LOFaNoGAAAAAAAAAAAAAAAAAAAAAP7pn0bLAgAAAAAAAAAAAAAAAAAAAAAAAABD5B/+AQD7arefl18v4mvQjalr8VWpVOILeP/keR5fj598F18AAAAAAAAAAAAAAAAAAAAAwH/yD/8AAAAAAAAAAAAAAAAAAAAAAAAA4BSM7O4p1wAAYafXi7ZaK9E8z6NJvVaLNpuzUeD86nS60dZycb6fVPPTYi7U68WcAAAAAAAAAAAAAAAAAAAAAID32WhZAAAAAAAAAAAAAAAAAAAAAAAAAGCIRnb3lGsA4D2X53l0qbUS7fV60STLsujnny1E0zZwfnU63WhruTjvT6pSqUQX5uei5gQAAAAAAAAAAAAAAAAAAAAA77PRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI3s7inXAMB7bnX1QXRrezs66N4nd6MTE+NR4PzrdLrR1vJKdFimpq5Fb0xdjwIAAAAAAAAAAAAAAAAAAADA+2i0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1APCe2tz8Mfr4ydPooEbjUnRm+k4UeHd0Ot1oa3klOmzz83PRsWo1CgAAAAAAAAAAAAAAAAAAAADvk9GyAAAAAAAAAAAAAAAAAAAAAAAAAMAQjezuKdcAwHum0+lGW8sr0UHVajU615yNZlkWBd4d/X4/+vU396PDVq/Vos1yjgAAAAAAAAAAAAAAAAAAAADA+2S0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1APCe2On1oq3WSjTP8+ig+fm56Fi1GgXeXc82vo9ubPwQHbZbNz+KTk5ejQIAAAAAAAAAAAAAAAAAAADA+2C0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1APCOy/M8utRaifZ6veigWzc/ik5OXo0C749WOR863W50WLIsi37+2UI0bQMAAAAAAAAAAAAAAAAAAADAu2y0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1APCOW119EN3a3o4Omhgfj967dzcKvH/6/X70/uJSNM/z6LCYMwAAAAAAAAAAAAAAAAAAAAC8T0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNLK7p1wDAO+ox0++i25u/hQdVKlUogvzc9Esy6LDkud5tN1+Hp2cvBoF3r6dXi/af9mPTkyMR9P5urb+KDpszU9no/V6LQoAAAAAAAAAAAAAAAAAAAAA76LRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI3s7inXAMA7pt1+Hl1bfxTdz/z8XHSsWo0OW6u1Eq3VP4zemLoeBd6+TqcbXf32QfTzzxaiWZZFV1eL27e2t6PDUqlUogvl/EmPBwAAAAAAAAAAAAAAAAAAAADvktGyAAAAAAAAAAAAAAAAAAAAAAAAAMAQjezuKdcAwDui0+lGW8sr0f3cuvlRdHLyanTYnm18H93Y+CE6NXUtemPqehR4+wbnxcT4ePTevbvRPM+jX39zP5q2h8VcAAAAAAAAAAAAAAAAAAAAAOBdNloWAAAAAAAAAAAAAAAAAAAAAAAAABiikd095RoAOOd2er1oq7USzfM8OqjRuBSdmb4THbZOpxttLRf7kUxNXYvemLoeBd6+/c7XWzc/ik5OXo1ubW1HV799EB22+fm56Fi1GgUAAAAAAAAAAAAAAAAAAACAd8FoWQAAAAAAAAAAAAAAAAAAAAAAAABgiEZ295RrAOCcyvM8en9xKdrv96ODqtVqdK45G82yLDosaT++/uZ+NG0n9z65G52YGI8Cb1+n0422lleiSZoPC/Nz0UqlEn385Lvo5uZP0WFJ8yk9HgAAAAAAAAAAAAAAAAAAAAC8C0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNLK7p1wDAOdMnufRpdZKtNfrRQdlWRZdmJ+LViqV6LC1yv3odLvRpNG4FJ2ZvhMFzo5OpzhfW8vF+TuoXqtFm83ZaJo79xeXov1+Pzost25+FJ2cvBoFAAAAAAAAAAAAAAAAAAAAgPNstCwAAAAAAAAAAAAAAAAAAAAAAAAAMEQju3vKNQBwztxfXIr2er3ofubn56Jj1Wp02J5tfB/d2PghmlQqlehC+fhZlkWBs6PT6UZbyyvR/dy6+VF0cvJqdKecO4vlHBqWNCfS3EhzBAAAAAAAAAAAAAAAAAAAAADOo9GyAAAAAAAAAAAAAAAAAAAAAAAAAMAQjezuKdcAwDmxtv4w2m6/iO5nZvp2tNG4HB22ra3t6Oq3D6KD5ufnomPVahQ4ezqdbrS1vBLdT5Zl0WZzNprO62cb30c3Nn6IDsvE+Hj03r27UQAAAAAAAAAAAAAAAAAAAAA4j0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNLK7p1wDAGfc2vrDaLv9Irqfqalr0RtT16PD1u/3o/cXl6J5nkeT0358YHg6nW60tbwSPUi1Wo0uzM9FkzQPer1edFjufXI3OjExHgUAAAAAAAAAAAAAAAAAAACA82S0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1AHBGra0/jLbbL6L7aTQuRWem70SHLc/z6FJrJdrr9aJJvVaLNpuzUeDs63S60dZycV4f1tTUteiNqevRnXIetMr5kObFSVUqlejC/Fw0y7IoAAAAAAAAAAAAAAAAAAAAAJwHo2UBAAAAAAAAAAAAAAAAAAAAAAAAgCEa2d1TrgGAM+bZxvfRjY0fovupVqvRhfm56GlZW38YbbdfRJMsy6Kff7YQTdvA2dfpdKOt5ZXoUc2Xc2esnEObmz9GHz95Gh2Wyckr0Vs3P44CAAAAAAAAAAAAAAAAAAAAwHkwWhYAAAAAAAAAAAAAAAAAAAAAAAAAGKKR3T3lGgA4I9rt59G19UfR/VSr1ehcczaaZVl02DY3f4w+fvI0OujeJ3ejExPjUeD86HS60dbySvSo0hxamJ+LJq1WcX+dbnH/wzJfPs5Y+bgAAAAAAAAAAAAAAAAAAAAAcJaNlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5BgDesnb7eXRt/VF0P1mWRRfm56KVSiU6bJ1ON9paXokOmpy8Er118+MocP4cdJ4f1uA8yPM8+vU396Np+6Sq1Wo0zT8AAAAAAAAAAAAAAAAAAAAAOMtGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzSyu6dcAwBvSbv9PLq2/ii6nyzLos3mbHSsWo0OW7/fj95fXIrmeR5NquXjLszPRYHzq9PpRlvLK9GTan5azKd6vRbd2tqOrn77IDost25+FJ2cvBoFAAAAAAAAAAAAAAAAAAAAgLNotCwAAAAAAAAAAAAAAAAAAAAAAAAAMEQju3vKNQDwhm1u/hh9/ORp9CDNT2ej9XotOmx5nkeXWivRXq8XTbIsiy7Mz0UrlUoUOL86nW60tVyc9yeV5kKaE2lurK0/jLbbL6InZR4BAAAAAAAAAAAAAAAAAAAAcB6MlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5BgDekLX1h9F2+0X0IDPTt6ONxuXoaVldfRDd2t6ODrr3yd3oxMR4FDj/Op1utLW8Eh2Wyckr0Vs3P47meR69v7gU7ff70ZOq12rRZnM2CgAAAAAAAAAAAAAAAAAAAABnyWhZAAAAAAAAAAAAAAAAAAAAAAAAAGCIRnb3lGsA4JStrT+MttsvogeZmb4dbTQuR0/L4yffRTc3f4oOmpy8Er118+Mo8O7odLrR1vJKdNjufXI3OjExHj2txxt8HAAAAAAAAAAAAAAAAAAAAAA4C0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNLK7p1wDAKdkbf1htN1+ET3I1NS16I2p69HT0m4/j66tP4oOqlar0YX5uSjw7rq/uBTt9XrRYcmyLPr5ZwvRtP34yXfRzc2foie13+MAAAAAAAAAAAAAAAAAAAAAwNs0WhYAAAAAAAAAAAAAAAAAAAAAAAAAGKKR3T3lGgAYkjzPo6urD6Kdbjd6kEbjUnRm+k70tHQ6xf60lleig7Isiy7Mz0UrlUoUeHf1+/3o/cWlaJpjwzIxPh69d+9uNEmP1+v1oic1OXkleuvmx1EAAAAAAAAAAAAAAAAAAAAAeJtGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzSyu6dcAwAnlOd5dKm1Eu31etGDNBqXojPTd6KnZafcn1a5f2l/B9375G50YmI8Crw/tra2o6vfPogO2+B8SXNpcXEpOizNT2ej9XotCgAAAAAAAAAAAAAAAAAAAABvw2hZAAAAAAAAAAAAAAAAAAAAAAAAAGCIRnb3lGsA4Jh2er3o6uqDaL/fjx6k0bgUnZm+Ez0teZ5Hv/7mfjRtD5qauha9MXU9Cry/nm18H93Y+CE6LFmWRT//bCGatjc3f4w+fvI0elLVajW6MD8XBQAAAAAAAAAAAAAAAAAAAIC3YbQsAAAAAAAAAAAAAAAAAAAAAAAAADBEI7t7yjUAcESdTje6+u2DaJ7n0YM0GpeiM9N3oqcl7c9SayXa6/Wig+q1WrTZnI0CJK1yfnS6xbwblv3mzrAfb2rqWvTG1PUoAAAAAAAAAAAAAAAAAAAAALxJo2UBAAAAAAAAAAAAAAAAAAAAAAAAgCEa2d1TrgGAQ2q3n0fX1h9FD6vRuBSdmb4TPS15nkeXWivRXq8XHVSpVKIL83PRLMuipyXt12k/DnB4O+V8yF8V52e9Xosm6bz9+pv70bQ9LLdufhSdnLwa7ff70fuLS9GTPl6aN2nOpbkHAAAAAAAAAAAAAAAAAAAAAG/CaFkAAAAAAAAAAAAAAAAAAAAAAAAAYIhGdveUawDgAGvrD6Pt9ovoYTUal6Iz03eip+2g/cyyLNpszkbHqtXoaWm3n0df9vvRG1PXo8Db1+l0o6vfPoh+/tlCNM2JJP1ca3klOizpcRbm56KVSiW6tbUdTft1UvVaLZrmHgAAAAAAAAAAAAAAAAAAAAC8CaNlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGtndU64BgAF5nkeXWivRXq8XPaxG41J0ZvpO9LStrT+Mttsvovu598nd6MTEePS07JTPV6t8/iYnr0RvTF2PAm9fp9ONtpaL87Req0WbzdnooM3NH6OPnzyNDst+j7u6+iC6tb0dPak3Nf8AAAAAAAAAAAAAAAAAAAAA4G9GywIAAAAAAAAAAAAAAAAAAAAAAAAAQzSyu6dcAwClTqcbXf32QTTP8+hhNRqXojPTd6KnbW39YbTdfhHdz9TUteiNqevR05Ker/uLS9F+vx99U48PHF6ad63llWhy0Pm6ulrMx63t7eiwDD5umidff3M/etR5PCjLsujnny1E0zYAAAAAAAAAAAAAAAAAAAAAnIbRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI3s7inXAPDee7bxfXRj44foUU1OXoneuvlx9LQ9fvJddHPzp+h+Go1L0ZnpO9HTdn9xKdrr9aLJrZsfRScnr0aBt6/T6UZbyyvRQc1PZ6P1ei2a5HkeTed7v9+PDsv8/Fx0rFqNHrSfR/Wm5zUAAAAAAAAAAAAAAAAAAAAA76fRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI3s7inXAPDe6ff70bW1R9FOtxs9qpnp29FG43L0tLXbz6Nr68V+76darUYX5ueip21t/WG03X4RTeq1WrTZnI0CZ0enU8y91vJKdFCWZdHPP1uIpu1kp9eLLi4uRYdlv/n1+Ml30c3Nn6In1fy0mEv1ejGnAAAAAAAAAAAAAAAAAAAAAGCYRssCAAAAAAAAAAAAAAAAAAAAAAAAAEM0srunXAPAe2Nrazu6tv4omud59Khmpm9HG43L0dPWbj+Ppv3eT7Vajc41Z6NZlkVPy+bmj9HHT55Gk0qlEl2Yn4ue9n4AR9fpdKOt5ZXoftJcSefzoMPOp6OamroWvTF1PZrm9VKr2N9erxc9rjSnPv9sIQoAAAAAAAAAAAAAAAAAAAAAwzRaFgAAAAAAAAAAAAAAAAAAAAAAAAAYopHdPeUaAN5ZeZ5H19YeRbe2t6NHlWVZtNmcjY5Vq9HT1m4/j66tF/u/n7R/n3+2EE3bp2Vrq3geV799EB00Pz8XfVPPE3B0nU432lpeiR5kcvJK9NbNj6OD1tYfRtvtF9FhaX5azN16vRbd6fWii4tL0ZOamroWvTF1PQoAAAAAAAAAAAAAAAAAAAAAwzBaFgAAAAAAAAAAAAAAAAAAAAAAAAAYopHdPeUaAN45W1vb0cdPnkb7/X70qKrVanRm5nZ0rNw+be328+ja+qPofrIsizabs9HT3r+dXi/aaq1E8zyPJjPTxfPUaFyOAmdXp9ONtpaL8/mw7n1yNzoxMR5N0jxYKudDr5wXJ1WpVKIL83PRNPeebXwf3dj4IXpSn3+2EE2PBwAAAAAAAAAAAAAAAAAAAAAnMVoWAAAAAAAAAAAAAAAAAAAAAAAAABiikd095RoAzr08z6Nra4+iW9vb0eOaGB+PzszcjmZZFj1t7fbz6Np6cRz7SfvTbM5Gx6rV6GlJz+/X39yPpu2k0bgUnZm+EwXOvk6nG20tr0QPK82fhfm5aKVSiSY7vV601Srud3BeHNfk5JXorZsfR5P0OJ1ucTzHVa/VommuAgAAAAAAAAAAAAAAAAAAAMBJjJYFAAAAAAAAAAAAAAAAAAAAAAAAAIZoZHdPuQaAc6vdfh59/ORpNM/z6HFNTV2L3pi6Hn1THj/5Lrq5+VN0P1mWRZvN2ehYtRo9Len5XGqtRHu9XjSplo+/MD8XBc6PTqcbbS0X5/dRHXT+p/m8tv4oOizNT4v5V6/Xov1+P3p/cSl60uvAzPTtaKNxOQoAAAAAAAAAAAAAAAAAAAAAxzFaFgAAAAAAAAAAAAAAAAAAAAAAAAAYopHdPeUaAM6NnV4v+uTx02in240eV5Zl0Xuf3I3W67Xom7K2/jDabr+IHmRm+na00bgcPW33F5eivfJ5T9Lz9vlnC9G0DZwfnU4xP1vLK9Hjmpy8Er118+PooMdPvotubv4UPalKpRJdmJ+LpvnTbj+Prq0/ih6X+QYAAAAAAAAAAAAAAAAAAADAMIyWBQAAAAAAAAAAAAAAAAAAAAAAAACGaGR3T7kGgDMrz/Po4yffRdvtF9GTqtdq0Xv37kazLIu+KWvrD6OHPZ6Z6dvRRuNy9LQdtH/NT2ej9XrxPALnT6fTjbaWV6Inde+TYp5OTIxHB91fXIr2er3oSU2MF4+T5niyuvogurW9HT2uRuNSdGb6ThQAAAAAAAAAAAAAAAAAAAAAjmK0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1AJwZeZ5H/7z5Y3Rz86douv2kpqauRW9MXY++KWn/V1cfRDvdbvQgM9O3o43G5ehpW1t/GG23X0QH3br5UXRy8moUOL86nWIOtZZXoieVZVm02ZyNjlWr0aTf70fvLy5FhzXX731yNzoxMR5N9/v1N/ejJ32c5qfF8dTrtSgAAAAAAAAAAAAAAAAAAAAAHMZoWQAAAAAAAAAAAAAAAAAAAAAAAABgiEZ295RrAHjr2u3n0cdPnkbzPI+eVLVajc7M3I6OldtvSjqOpdZKtNfrRQ8yM13sb6NxOXra0vO/tv4oOqjRuBSdmb4TBc6/TqcbbS0X82lY0tyda85GsyyLJltb29HVbx9ETyrd/+efLUTT9rCOr1KpRNP9AwAAAAAAAAAAAAAAAAAAAMBhjJYFAAAAAAAAAAAAAAAAAAAAAAAAAIZoZHdPuQaAN67dfh59tvFDtN/vR08qy7Lo5OSV6I2p69E3bafXi66uPoge9vhmpm9HG43L0dOWXoe19UfRQdVqNbowPxcF3h2dTjfaWl6JDlujcSk6M30nOujZxvfRjfI6cFIT4+PRe/fuRpPHT76Lbm7+FD2uqalr0bd1XQEAAAAAAAAAAAAAAAAAAADgfBktCwAAAAAAAAAAAAAAAAAAAAAAAAAM0cjunnINAKcmz/Ponzd/jLbbL6L9fj86LPVaLTozcztaqVSib1qn042ufvsgmo7/IDPTxX43Gpejp63dfh5dW38UHZRlWfTzzxaiaRt4d6T59PU396OHnVdHddB8a7VWop1uMT9P6tbNj6KTk1ej6bjuLy5FT3r9mZ+fi45Vq1EAAAAAAAAAAAAAAAAAAAAA+HtGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzSyu6dcA8DQ9Pv96H9vP49ubv4UzfM8OiyVSiV66+ZH0YmJ8ejbsrn5Y/Txk6fRw5qZvh1tNC5HT1un0422lleig7Isizabs9GxajUKvLt2er1oq1XMhWHP64PmSnq8r7+5Hz3p46fHW5ifi6brRTrOxcWl6HHVa7VoOh4AAAAAAAAAAAAAAAAAAAAA+HtGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzSyu6dcA8CxdTrdaPv586LtF9HTMjV1Lfq7yavRLMuib8va+sPoUY97Zvp2tNG4HD1tO71etNVaieZ5Hh30pvcLODva7WKOr60/ig5btVqNzjVno4PzO11PWsvFnDqpeq0WbZaPlzzb+D66sfFD9Lhu3fwoOllejwAAAAAAAAAAAAAAAAAAAADg/2q0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1ABwoz/Nou/08+ufNn6L9fj96WhqNS9EbU9ejlUol+rak411ZfRDt9XrRw5qZvh1tNC5HT9tOuX+t1ko0vY6Dbt38KDo5eTUKvL+ebXwf3dj4ITpsaa7PTN+JDtrc/DH6+MnT6EntN9/uLy5FjzrHkyzLop9/thBN2wAAAAAAAAAAAAAAAAAAAADwN6NlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGtndU64B4P9ma2s72m6/iG5tF9unrV6rRaemrkXr9WL7bet0utHVbx9E8zyPHiTLsuitmx9FG43L0dO20+tFW62V6H7722hcis5M34kCJGvrD6PpOjBsaS5OTl6NDlpdLebtsK4/8/Nz0bFqNdrv96P3F5eih53rgybGx6P37t2NAgAAAAAAAAAAAAAAAAAAAMDfjJYFAAAAAAAAAAAAAAAAAAAAAAAAAIZoZHdPuQbgPba1tV10u+zWX6J5nkdPW71Wi05NXYvW68X2WfFs4/voxsYP0cPKsizabM5Gx6rV6Gnb6fWirdZKdL/XMT3vaf8A9nN/cSnaK+fLsM3Pz0UH52SaX+nx+/1+9Liq5f0vlI+XbG7+GH385Gn0uJqfFvP0rF3HAAAAAAAAAAAAAAAAAAAAAHg7RssCAAAAAAAAAAAAAAAAAAAAAAAAAEM0srunXAPwDsvzPNrpdKNb29tFt/4STd9/U+q1WnRq6lq0Xi+2z4p+vx9dW3sU7XSL5+2wsiyLNpuz0bFqNXradnq9aKu1Et3vda2W+zNX7l/aX+D9k+ZGb6doo3E5OijNk/uLS9E0J4elUqlEF+bnooNzKe3nYvn4J5WuPzemrkeT1dUH0XSdPKqDjgMAAAAAAAAAAAAAAAAAAACA98toWQAAAAAAAAAAAAAAAAAAAAAAAABgiEZ295RrAN4BO71etNvpRre2/hLtdIvtt2VifDw6OXklWq/XomfN1tZ2dG39UTTP8+hhVavV6FxzNpplWfS0pde91VqJ7rffb3v/0vtycvJqFHj7OuV52Vou5sf8/Fx0rJwXgw47b44rXS/u3bsbHdRuP4+mOX1Sg8ebjufrb+5Hj3t8U1PXojemrkcBAAAAAAAAAAAAAAAAAAAAeD+NlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5BuAc2On1ot1ON9rp/Fy0W2zneR59W7Isi05M/DZ6Y+p6tFKpRM+a9HytrT2Kbm1vR4+qXqtF7927G03Pw2nrlO+D1W8fRPd7/dP+NJuz0bFqNXra0vu11VqJTk5eiab3BfD2pTnSWi7O0zQvFubnovvN78HfG7ZbNz+KTk5ejQ5aW38YbbdfRI8rHV863nT8W1vF9SDN1+OaL+/3Tc1dAAAAAAAAAAAAAAAAAAAAAM6W0bIAAAAAAAAAAAAAAAAAAAAAAAAAwBCN7O4p1wC8RXmeRzudbnSn14t2Oz9H03b6ubOiUqlEfzd5JdpoXI5mWRY9q7a2tqNr64+ix31eG41L0ZnpO9E3pd1+Hk37v5/0OjSbs9GxajV62tL7tdVaiabnd2rqWvTG1PUo8Pal605ruThfk2o5L+bK+bHfXD/sPDqu+fm56OD8SnNlqZwzvXLuHNdkeR27dfPjaPL4yXfRzc2fokdVr9WiaQ4DAAAAAAAAAAAAAAAAAAAA8H4ZLQsAAAAAAAAAAAAAAAAAAAAAAAAADNHI7p5yDcAp2On1ov2X/Wja7nZ+jr7sF7f3y55VWZZFJyZ+G21cvhyt12vRsy7P8+ja2qPo1vZ29Lhmpm9HG43ieXhT2u3n0bX14jj2k16vZnM2OlatRk9ben+3WivR9LwnU1PXojemrkeBt6/T6UZby8V5O6heK+Z8mif7WVt/GG23X0SHpVKpRBfm56JpviUHzZ2jan5aHGe6vqX7u7+4FD3u9frWzY+ik5NXowAAAAAAAAAAAAAAAAAAAAC8H0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNLK7p1wD8A/keR7d2elF+/1+9GXZ3s4v0V+3e8XPnVcT4+NFJ35bttjOsix6Xmxu/hh9tvFDNL2OR5WOu9mcjY5Vq9E3JR3H4ydPo/t5W/uZntevv7kfHXye3/bzB+yv0+lGW8sr0f00GpeiM9N3ovtptYr76XSL+x2Weq0WTXNkULv9PLq2/ih6XGleff7ZQjRt75TX9cXFpehR7Xe/AAAAAAAAAAAAAAAAAAAAALzbRssCAAAAAAAAAAAAAAAAAAAAAAAAAEM0srunXAO80/I8j+7s9KJJp9stV4Vu5+dyVRj8/rumWq1G/7fGpejExHi0UqlEz5tOp3i9Hj95Gu31Xn+9j6peq0Xv3bsbzbIs+qasrT+MttsvovtJ+9VszkbHytf1tKXzaqm1Et3v+Z6Zvh1tNC5HgbMjzc3WcnEeH+Sg8/mwc+G4pqauRW9MXY8Oevzku+jm5k/R45oYL66Haf4nzza+j25s/BA9qv3uFwAAAAAAAAAAAAAAAAAAAIB302hZAAAAAAAAAAAAAAAAAAAAAAAAAGCIRnb3lGuAM6nf70dfviya53l0p9eLJt3Oz+Wq8LL8vfT777uJ8fGiE7+N1uu1aKVSiZ5X6fV9/PhpdGt7O3pSU1PXojemrkfflPT+fvzku2i7/SK6nyzLos3mbHSsWo2etrSfS62VaG/gfExmpm9HG43LUeDs6XS60dZycT4f1r1P7kYnJorry6A0n+8vLkXT3BiW5qfF3EvXs0HpcfebT4e133Ge9P4Pev4AAAAAAAAAAAAAAAAAAAAAeDeMlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5BhiqTqdbrgr9fj/6smye59Hezi/RpNN9/fc4nEqlEp2Y+G20XqtFJybGo++K9L55tvF9dHPzp+hJpedvZvp2tF4vnr83JR3XUmsl2uv1ovvJsizabM5Gx6rV6Gk77H5OTV2L3pi6HgXOrnS9bi0X5/VhHXYO7ZRzYnFxKTos6fE//2whmraT9Lnjfvm4aX4d1X6Pc9L7T9edhfm56OD+AwAAAAAAAAAAAAAAAAAAAPBuGC0LAAAAAAAAAAAAAAAAAAAAAAAAAAzRyO6ecg285/I8j+7s9KJJp9stV4Xezi/R9POvyvZ6r/8ew1WpVKL1+odFa7Wi9aLp+++a9D778+aP0c3Nn6Lp9pOanLwSvTF1PZplWfRN2SnPm1ZrJXrQcVWr1ehcczb6pvY37ddSuZ/7ne+NxqXozPSdKHD2dTrFdb61XJzfR5WuPwvzc9H95lK7/Ty6tv4oOizpetgs5+Kgra3t6Oq3D6LHtd/jbJbXp8dPnkaPKl2Hbt38OAoAAAAAAAAAAAAAAAAAAADAu2W0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1cE7t9HrR/FVeNC+abk+6nZ/LVeHX3yt/nrerXqtFq2MfRNP22Fg1WqlUou+69H788+aP0c3Nn6LDep+m53Fm+na0Xi+e5zet3X4effzkafSg46tWi/fBXHM2mmVZ9LSl/VpqrUR7A3MlaTQuRWem70SB86PT6UZby8V5flyHnVPPNr6Pbmz8EB2Wqalr0RtT16ODhvW4t25+FJ2cvBpNVlcfRLe2t6NHNT8/Fx0rn0cAAAAAAAAAAAAAAAAAAAAA3g2jZQEAAAAAAAAAAAAAAAAAAAAAAACAIRrZ3VOugTes0+mWq0Kv14u+yvNoXra380s06XRf/z3OtnqtFq1c/E10rFqNVsvW68X331f9fj/6bOP76NbWX6Lp/T8sU1PXor+bvBrNsiz6pj1+8l10c/On6EHS+2SuORt9U/udnv+l1ko0zadBaf8W5ueiwPmTPo+0lovz/aQajUvRmek70f2srT+MttsvosNy75O70YmJ8eigVjnXjvt5Ks3hNPcqlUo0zc2vv7kfPep1zDwFAAAAAAAAAAAAAAAAAAAAeDeNlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5Bg6Q53l0Z6cXTTrdbrkq9HZ+iaaff1W213v99zifqtVq9EKWRWv1D6Nj5e2Vi5Vo2uZ1nU5xvmxu/hTd2t6ODlu9VovOzNyOVirF6/KmpTmwuvogOjgv9tNoXIrOTN+Jvilpf5daK9H95lY6D+aas9GsPB+A8yfN5dZycd4Py+Tkleitmx9HBx123hxVmkcL83PRwfmfHvfrb+5H0/ZRpTmYHifZ2iqua6vfFnP/qG7d/Cg6OXk1CgAAAAAAAAAAAAAAAAAAAMD5NloWAAAAAAAAAAAAAAAAAAAAAAAAABiikd095RreWTu9XjR/lUf7/X70Zdmk2/m5XBV+/b28+D3eDVmWRceq1Wjaro59EL1YqUQrZS9efH2bw0nnTbv9PPrnzZ+i6fwbtvT63Lr5UXRiYjz6tqT5sbr6IHrY4240LkVnpu9E35T0ei21VqK9cv8HVcvzZq45G03nD3B+dTrdaGu5OP+HbWb6drTRuBwdlObP/cWl6LCuE2leLczPRQelOb1YPu5xTU1di96Yuh5NHj/5LrpZXv8OK83VtN8+fwAAAAAAAAAAAAAAAAAAAACcb6NlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGtndU67hzOl0uuWq0Ov1oq/yPNrv94u+/Gs06XRf/z3eTdVqNXohy6LVsQ+iWbldr9WiSb3++jbDtbW1HW23X0S3tovt05Je5xtT16KTk1ejb9vm5o/Rx0+eRg/r1s2Pom/6OHbKudpqrUTzcr4OSs/3558tRNM2cP6lz1ut5WIOnJb5+bnoWHn9HnTYeXRUk5NXordufhwddNy5PWjw+NL+319ciqbPrYc1MT4evXfvbhQAAAAAAAAAAAAAAAAAAACA82m0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCO7e8o1nFi/34++fFk06XS75arQ2/klmud59GX5e+n3eT9kWRYdq1ajabs69kH0YqUSrZS9ePH1bd6Ora3tottlt/4STefzaUnvj8nJK9HfTV6NptvflnTcq6sPooPz7iAz07ejjcbl6Juy0+tFW62V6H6vX3p+m83ZaDpfgXfP2vrDaLv9Ijpsh50n6Tqz+m0xV4fl3id3oxMT49FBaY6n69tRVcvjWZifiyZp3i4uLkWP6qD9BgAAAAAAAAAAAAAAAAAAAOBsGy0LAAAAAAAAAAAAAAAAAAAAAAAAAAzRyO6ecs17bKfXi+av8mi/34++LJt0Oz+Xq8Kvv5cXv8f7qVqtRi9kWbRW/zCa1Gu1aHah+P5Y+fOcbVtb20W3y279JfqmzvesfD9NTl6J/m7yajTd/ral52dt/VH0sM9L2v9mczb6ps+HNLdbrZXofvv9tvcTeHvuLy5Fe+W8GLb0uWGunC/7zfV2+3k0zdmTSo+zMD8XrVQq0STNw3T86fPwUaXr1q2bH0eTZxvfRzc2fogeVtrPtN9n5ToIAAAAAAAAAAAAAAAAAAAAwOGMlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+55hzJ8zy6s9OLJr1esf2q/H6/3y/68q/RdHv6OfibSqUSvVi2cvE3RQdvLzs2Vo1mWRblfErzYWtrO9rp/Bzd2i6237T0/mo0LkV/N3k1elbeZ2nuPn7yXbTdfhE9rGq1OG9m792NpuN9UzqdbnT12wfRdDyD0vPdbM5Gx8r9Bt4faT4stVaip/W5Mc3Fhfm56H7W1h9Gjzp393PQ4+6Ux9sqj3+/eXmQ5qfFHK3Xa9Hk/uJS9KjP6+Tkleitmx9HAQAAAAAAAAAAAAAAAAAAADgfRssCAAAAAAAAAAAAAAAAAAAAAAAAAEM0srunXPMG9Pv96MuXRfM8j+70etGkt/NLNH3/Zfl76ffh76nXauWqUKt/GL2QZdFqtRrNLhTbY+U276Y0LzqdbtFu2c7P0bc9T9L78XeTV6KNxuXoWZOev7X1R9GjPm8T4+PRmZnb0aw8H9+Udvt5NO3/ftJ+NZuzUfMBSJ9Dl1or0d7A59VhaTQuRWem70T30yr3I13PTuqgxz3s/NxPpVKJLszPRdOcTdeR+4tL0fQ8H9Z8eX/mNAAAAAAAAAAAAAAAAAAAAMD5MFoWAAAAAAAAAAAAAAAAAAAAAAAAABiikd095Zp/YKfXi+av8mi/34++LJvnxe29nV+iya+/V34f/pFqtRq9kGXR6tgH0azcHiu/n7br9VqU90uaJzs7xXzpdLvRNH/S9lmbO43GpaKXL0fP6vs3PW9ra4+iW9vb0aOamroWvTF1PfqmtdvPo2vrxXHsJ82TZnM2muYMQJLm4lJrJdorP98O262bH0UnJ69GB53WfsxM3442GsX1adDa+sNou/0ielTp+jczfSeaHHZOD0qfFxfm56IAAAAAAAAAAAAAAAAAAAAAnG2jZQEAAAAAAAAAAAAAAAAAAAAAAACAIRrZ3VOu3yl5nkd3dnrRpNPtlqtCv98v+vKv0Vfl7/V6r/8eHEaWZdGxajWatqtjH0QvVirRStmLF1/f5v2W5tHLl0XTvOrt/BLdKedS+rmzKr2ffzd5JdpoXI6m8+GsarefRx8/eRpN15HDSsc3M307OjExHn3T1tYfRtvtF9H9pP1tNmejaW4B7CfNxa+/uR896pw8rDRH0/VjULoO3l9cip50Pw6ah+n+l1or0eP+nXDvk7vRwevD6uqD6Nb2dvSwpqauRW9MXY8CAAAAAAAAAAAAAAAAAAAAcDaNlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5PhP6/X705cuieZ5Hd3q9aNLt/FyuCi/L30u/DydRr9XKVaFW/7BcFdL3swtZdKxajcLf/Dq3doq59etcK9vb+SWafq7T7UbPmywr3v8TE7+NTk5ejZ6X8yFdV548fho97utQLY93ZuZ29E0ff3ofPX7yXbTdfhHdT3rdms3ZqPkFHFWan63WSjTNoWE57JxK+7G4uBQ9qTTP58rHTfuRnPS40/19/tlCNG2n+/n6m/vRw95v+v2F+blopVKJAgAAAAAAAAAAAAAAAAAAAHC2jJYFAAAAAAAAAAAAAAAAAAAAAAAAAIZoZHdPuT6RTqdbrgr9fj/6smye59Hezi/RpNN9/fdgGCqVSvRi2erYB9Esy6Jj1Wr01+2x17d5v+30etH8VTG30vz69fb3bJ6l82Ji4rdFx8eLThQ9L9Lr9vjJd9F2+0X0uBqNS9FbNz+Ovun5kY5nqbUS7ZXvz/2k/Ws2Z6NpDgKkeZIcdp6l62KrnEOD93NSaT8W5uei6fPdoHb7eXRt/VH0pNJ8n5m+Ex20tbUdXf32QfSo0nX03r270ST9PdVaLp7Pw6rXatE03wEAAAAAAAAAAAAAAAAAAAA4W0bLAgAAAAAAAAAAAAAAAAAAAAAAAABDNPIf/9HZLdeh0+2Wq0K/3y/68q/RV3ke7fV6UTgNWZZFx6rVaNqujn0QvZC2y+9fvFiJVipFeT+k+fTyZdEkzac0r5Ju5+dyVTDPXpfOn4mJ30brtVp0YmI8et7k5ev7580fo5ubP0XT7UeV5tCtmx9FG43L0Tdtp3y/tlor0YOOJ+13szkbTXP1TUv7ubNT7H+9Xry/gLev0yk+/z9+8jQ6V86LND8OctS5dFTp895B+/Vs4/voxsYP0ZOamb4d3W/eP37yXTRdX44qXU8mJ69Gk+Pe771P7kbP63UbAAAAAAAAAAAAAAAAAAAA4F01WhYAAAAAAAAAAAAAAAAAAAAAAAAAGKKRL778ardcw9BVq9XohSyL1uofRpN6rVauCvX669u8G3Z6vWj+Ko8mnW63XBXyvPh+b+eXaPKy34/2y3IyE+Pj0Xp5PtbK826sPF/Pu2cb30c3N3+KpvfVcaU5NTNzO1qpVKJvWrv9PPr4ydPoQceV5u/svbvRt7XfaT+XWivRiYnfRm9MXY8Cb1+nU1yPW8vFeZrmx1xzNpqVn+MOkq73i4tL0WFL16975Vzbz9r6w2i7/SJ6XOm4m+XzsN918n55vL3y+A8r3f/C/Fw0zenBuXnY+0339/lnC9HDvm4AAAAAAAAAAAAAAAAAAAAAnK7RsgAAAAAAAAAAAAAAAAAAAAAAAADAEI188eVXu+Ua/m8qlUr0YtnKxd8ULbfHqtVolmXRsbHXtzlfdnq9aP4qjyadbrdcFfK8+H5v55do8rLfj/bL8nbUa7Vorf5hNG3X60XfFel92G4/j/5586fosN5/U1PXojemrkfflsdPvotulsd3kGo5l+eas9G3NY/T67PUWon2yvlyVp5X4D91OsV1vrVcnK9Jo3EpOjN9J3pYaS6vrT+KDttB+7Xf/Dmu9Ll3YX4uOjhX0+N9/c39aNo+rHSdbpZzO0mfyxYXl6KHNTl5JXrr5sdRAAAAAAAAAAAAAAAAAAAAAN6u0bIAAAAAAAAAAAAAAAAAAAAAAAAAwBCNfPHlV7vlmndYvVaLZlkWrY59EL2QtqvV6MWLlWilUpSzqd/vR1++LJr0er3oqzyPJt3Oz+WqkL6ffp7zIZ2XY+X5Wq9/GE3nb71enOfvqrx83/5588fo5uZP0XT7SaXncWbmdjQ9z29aOp7V1QfRTrcbPUijcSl66+bH0TTv37S0/0utlejgnJmauha9MXU9Crx9nU4xZ1rLxXk7KM2Xmek70cNqt59H19YfRYdtZrqY143G5eigNI/uLy5F0+en45oYH4/eu3c3Ouig5/Eg+83HzfK69/jJ0+hhNT+djb7rnw8AAAAAAAAAAAAAAAAAAAAAzrrRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI188eVXu+WaM6xarUYvZFm0OvZBNCu367VaNKnXX9/m7cjzPLqz04sm/X4/+rJs0u38XK4Kr8rf7/Ve/33eLen8vlipRNP5nc7rsbHi++l8f1+k8+TPmz9G2+0X0XRenVR6Pm9MXYtOTl6Nvi075Xm+uvogmo7/IJOTV6K3bn4cfVvS67LUWonuN7du3fwo+rafb+A/dTrdaGu5OH/302hcis5M34keVrv9PLq2/ig6bPc+uRudmBiPDkrztVXOp5NeRw6aY882vo9ubPwQPar5+bnoWPn5IEn73+kWr9dB0ueLhfL+AAAAAAAAAAAAAAAAAAAAAHg7RssCAAAAAAAAAAAAAAAAAAAAAAAAAEM08sWXX+2Wa05RpVKJXixbufibooO3lx0bq0azLIvyZnQ63XJVyPM8utPrRZPezi/R9P2k033993k/VavF+XuhPH9r9Q+jg+d5vV6LUtja2o622y+iW9vF9rBNjI9Hb936KJpej7el3X4effzkaXRwruxnZvp2tNG4HH1b0v4utVaivYF5mTQal6Iz03eiwNmRPv+0lovz+CDHPZ/TvFtbfxQdlvR5udmcjY6V1+FB6Tqz+u2D6EnNz89F93u81dXicY56PUufI+bK40nH1+/3o/cXl6KHvV5MTV2L3pi6HgUAAAAAAAAAAAAAAAAAAADgzRotCwAAAAAAAAAAAAAAAAAAAAAAAAAM0cgXX361W645hHqtFs2yLFod+yB6IW1Xq9HsQrE9Vm4zXP1+P/ryZdGk0+2Wq0Ke59Hezi/RZKfXi6bvw9+TzvekVv8wOni+X7xYiVYqRfnH0vn739vPo+32i2i6fdjS6zg1dS1ar7/+ur5pae48fvJdNB3/QdJ1p9mcjb7t60s6jqXWSrRXztVBjcal6Mz0nShw9nQ6xeen1nJxPh/Wcc/vtfWH0cPOv8NKc/LzzxaiaXtQu7z+rK0/ih5Xuu4vzM9FBx8vzcn7i0vRo17nJievRG/d/DiabG1tR1e/fRA9rPlyP/19AgAAAAAAAAAAAAAAAAAAAPBmjZYFAAAAAAAAAAAAAAAAAAAAAAAAAIZo5Isvv9ot1++FarUavZBl0erYB9Gs3K7XatGkXn99m6PJ8zy6s9OLJv1+P/qybNLt/FyuCq/K3+/1Xv99+EcqlUr0Ytl0fqfzPXG+n650/m9tbUfb7RfRTrcbPS3p9b8xdS3aaFyOvm1p7q2sPogedq6l69Zcczaa3s9vy06532trj6L7HUejcSk6M30nCpxdnU4xl1vLK9Gjmirn7Y2p69HDWlt/GE3Xh2E57Nwc1uNPjI9H7927Gx2U5ubi4lL0qJqfFscx+DnlqPt/1q4nAAAAAAAAAAAAAAAAAAAAAO+L0bIAAAAAAAAAAAAAAAAAAAAAAAAAwBCNfPHlV7vl+lyoVCrRi2UrF39TdPD2smNj1WiWZVH+sZ1eL5q/yqNJp9stV4V+v1/05V+jya+/n7/++3AYg+dxOm+rYx9Ek3qtVq4KzvO3K53vW1vbZf9SdLvYPm3pfXNj6lq00bgcPSva7efRx0+eRg87HxuNS9FbNz+Ovu33d5rvrdZKdL/jSPs9M30nCpx9nU7xOa+1XJzfxzUzfTt61Dm8tv4w2m6/iA7LYedRmmuDn3ePaqq8Dt2Yuh4dtLn5YzRdDw4rzf/PP1uIpu00h+8vLkXT5/ODTE5eiabrCwAAAAAAAAAAAAAAAAAAAACna7QsAAAAAAAAAAAAAAAAAAAAAAAAADBEI198+dVuuX6j6rVaNMuyaHXsg+iFtF2tRrMLxfZYuc3rOp1uuSr0+/3oy7JJt/NzuSq8yvNor9eLwkmk83jwPK3VPyxXhfT99PMXL1ailUpRzrY0X7a2tqOdcq5sbRfbb0p6v9yYuhZtNC5Hz4q8nK+Pn3wXbbdfRA9rZvp29Kwc1055nWi1VqLp+AY1GpeiM9N3osD5kT5PtpaL8/ykjjvH1tYfRo86Nw8yOXkleuvmx9FBaa4tlXPupJ+Pm5/ORuv14u+dQcc9zonx8ei9e3ejSZrTi4tL0cO690lxPxMTxf0CAAAAAAAAAAAAAAAAAAAAcDpGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzTyxZdf7ZbrI6lWq9ELWRatjn0Qzcrteq0WTer117ffN/1+P/ryZdGk0+2Wq0Ke59Hezi/RZKfXi6bvwzAddD5frFSilbLZheL2sfL3eDek+dLpFHMpzaetrb9E0xx709L1ZHLySnRiYjx61qTnbW39UfSwz1c6r+7duxs9K+dVu/08mo5nP43GpejM9J0ocP6k+dVaXokOy8z07WijcTl6WK1WsR+Dn5NP6qD9SXP7/uJS9Lifu9Pnp88/W4im7STd71J5nL3yc/5h7Xcczza+j25s/BA9SNqvhfm5aLoeAQAAAAAAAAAAAAAAAAAAADBco2UBAAAAAAAAAAAAAAAAAAAAAAAAgCEaWVr6P3b/tqhc/E3cUKlUohfLpu2xsWo0y7Lou26n14vmr/KiedF0e9Lb+SWavp90ut1yBadn8HxN52d17IPohbRdLc7fpF6vlSveJ/1+P7qzU8yxNKc6nZ+jvYH59rY0Gpeik5NXo2MD79+zIs39ZxvfRzc3f4oeVr1WnIf37t2NnpXr6+bmj9HHT55G95Nep5npO1Hg/Op0iutBa3klOmwz07ejjcbl6EHSfF1qFfsz7OtT89PZ6H6fh9Ln/cXFpehxpTnfbBaPNyg9Tqs8zsG/J/aTrhcL83PR9HkwSfd32L9H0ufEuXI/35e/9wAAAAAAAAAAAAAAAAAAAADelNGyAAAAAAAAAAAAAAAAAAAAAAAAAMAQjezuKdfnWqfTLVeFfr8ffVk26XZ+LleF9P308/Am1Wu1clWo1T8sV4WxajWaZVn04sVKtFIpCn+T53l0Z6cX7XSLedjb+SW60ytuP2tzLr2Pfzd5JdpoXI6m9/tZla43a+uPokd9XqemrkVvTF2PnhVr6w+j7faL6H4ajUvRmek7UeD8S3OttbwSPS0z07ejad4fJF3fllrFfvXK69lJpetMszkbTZ+3BrXbz6Np3h/XQXP/uI+TPkem40jSden+4lI0PY8HmRgfj967dzcKAAAAAAAAAAAAAAAAAAAAwHCMlgUAAAAAAAAAAAAAAAAAAAAAAAAAhmhkd0+5fiP6/X705cuiSa/Xi77K82hetrfzSzTZKX8ufR/ehkqlEr1YtnLxN0XL7QtZFq1Wq9HsQrE9Vm7DYXQ63Wiad2n+dTs/R1+W8zTN1bMqK8+HiYnfRicnr0bPy/mQnv/HT76LttsvooeV5sK9e3ejZ+W403EttVai6Tq8n0bjUnRm+k4UeHek68vi4lL0tKTrQbM5Gz3sPDzqvDqsNJ8X5ueiaf8GPdv4Prqx8UP0uJqfFsddr9eig9J1ZnPzp+hhTU1di96Yuh5Ntra2o6vfPogelnkPAAAAAAAAAAAAAAAAAAAAMFyjZQEAAAAAAAAAAAAAAAAAAAAAAACAIRrZ3VOu/66dXi+av8qjSafbLVeFfr9f9OVfo8ngz8FZUK/VylWhVv+wXBXGqtVolmXRsbHXt+EoOp3X52Cai3lezNXezi/Rl2mOlj2vJsbHo43GpejERLF93mxu/hh9tvFDNL1eh5Weh5mZ29GzMj/SdX119UH0oPfbzHSx/43G5Sjw7mq3n0fX1h9FT0uah83mbDR97jpImsNLrZVor5xnJ1UtH3+u3J/95vXa+sNou/0ielTpfhfm56KVSiU66P7iUvSoxzdf3u/g8/ls4/voRnk9O6xbNz+KTk5ejQIAAAAAAAAAAAAAAAAAAABwPKNlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGlla+j92/7Z4ledxQ6/Xi8JZVKlUohfLVi7+pmi5fSHLotVqNZpdKLbHym04ik6nW64K/X4/+rJs0u38XK4Kne7rv/euycrzbGLit0XHx6P1ei2avn/epNf78ZOn0aNeD9Nxz0zfjk5MFM/LWbG5+WM0Hd9B0nE0GpejwPsjzcPVbx9E8/LvhGFLc7PZnI0e9vNauh7fX1yKDmv/Go1L0ZnpO9H9pMc97t9N6XPqwvxcdNBxjy99Hk73O3g9Xl0tXs+t7e3oYbkeAAAAAAAAAAAAAAAAAAAAAJzMaFkAAAAAAAAAAAAAAAAAAAAAAAAAYIhGvvjyq91yDaeuXquVq0J17INolmXRsWo1+uv22Ovb8H/V6XTL1es63b9/e7fzc7l63X4//76rVCrRiYnfRtP5OzExHj3v0vtnY+OH6HHfB+l5mZm5HU3P29uW53l0be1RdGt7O3qQmeniOBqNy1Hg/bXT60VbrZVomivDlj7nNZuz0fR58CCntX9TU9eiN6auRwelx7m/uBTt9/vRo5qcvBK9dfPj6KB0nWotF8d3WBPjxXX63r270STt91L5fPXK5++wXB8AAAAAAAAAAAAAAAAAAAAAjme0LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCNffPnVbrmGfWVZFh2rVqNpuzr2QfRC2i6/n9TrtXLF+6jT6Zar13W6f//2bufncvW6/X6ek0nncb1WnKcTE7+NpvO2UqlE3xXp/bix8UP0uO+r9LzdmLoWnZy8Gj0r0nGurT+K9vv96H7S8dz75G7U3AYG7fR60VZrJZrneXTY0jxqNmej6XPnQU5r/2amb0cbjcvRQcN63DR/JybGo4OebXwfTdevw9pv/9N+fv3N/ehR93ty8kr01s2PowAAAAAAAAAAAAAAAAAAAAD8Y6NlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGvniy692yzXvsGq1Gr2QZdHq2AfRrNy+WKlEK2UvXnx9m3dLnufRnZ1edFCn2y1Xr+t2fi5Xr9vv53m70vlbr38YHSvnQK1ei6btd1WnU7wvNzZ+iJ70fToxPh69deuj6Fmbj882vo+m4z1Imv/N5mz0XX8/ACe30ys+N7RaK9H0eWLY0nz6/LOFaNo+yGnt3/z8XHS/OZmuN63l4nGPKh3fQvk4+11f0nEd9np20Jw/6fPVaFyKzkzfiQIAAAAAAAAAAAAAAAAAAADw942WBQAAAAAAAAAAAAAAAAAAAAAAAACGaOSLL7/aLdecYfVarVwVavUPy1Vh8PtjY9VolmVRzpc8z6M7O71o8uvtvb9/e2/nl2jyKt0+8POcb+m8HqsW53maB2k7nf+VSiX6vmi3n0f/vPlT9KTv+/T83br5UXRiYjx6VqQ5sLb2KHrY462W75PZe3ej79v7BDi59LljqbUSPa3PGWlezTVno4f9XJvmY6vcv7S/x5Uet1nuR7reDkrXobX1Yi4f1UHHm47j/uJStN/vRw9y0P2e9Pk67usEAAAAAAAAAAAAAAAAAAAA8L4YLQsAAAAAAAAAAAAAAAAAAAAAAAAADNHIF19+tVuuOQWVSiV6sWyWZdHq2AfRC2m7Wo1mF4rtsXKbsynP8+jOTi+a/Hp77+/f3tv5JZq8SrcP/DzvpzQH0rxIc6Jeq0UvXixuT3PlfdXv96N/3vwx2m6/iKbz7KQmJ69Eb0xdj6a5fVY82/g+urHxQ/Sw0vvo3r270bN2XMDbMzg/Dzsf0u8ttVaip/V5Jl0f55qz0cPu39bWdnT12wfRkzrsfjx+8l10c/On6FE1GpeiM9N3ooPS58zFxaXoYR32flvl63nU62p6Pu59Ulxn6vXiugMAAAAAAAAAAAAAAAAAAADwvhstCwAAAAAAAAAAAAAAAAAAAAAAAAAM0cgXX361W675O6rVavRClkWrYx9Es3L7YqUSrZS9ePH1bd6Ofr8fffmyaJLneXSn14sm6fbezi/R5FW6feDn4R9J82GsnB+Vi78pWs6F/7z99W1el87Lra3taLv9ItrpdqPDUq/VojMzt6NnbX53OsXxrq0/iqb5dliNxqXozPSdKMCgNGceP3kanWvORtP17CBpXi+1VqKn9bkpfS4/6v6128+jaY6eVNqPhfm56H5WVx9Et7aL69hRzUwX16VG43J00HGP66D7TZ+TW+XrmV7fo5qauha9MXU9CgAAAAAAAAAAAAAAAAAAAPC+Gi0LAAAAAAAAAAAAAAAAAAAAAAAAAAzRyBdffrVbrt9J9VqtXBVq9Q/LVWGsWo1mWRYdG3t9m9PR7/ejL18WTX69vWySbu+//Gs0ST+Xvg/DNDg/qmMfRNN8GPx+vf76NoeT53l0a2u77F+Kbhfbw1apVKIz07ejZ+112+n1ok8eP412ut3oUd26+VF0cvJqFGA/nU4xZ1rLK9Fq+fl4ZqaYk+nz8kHSPF9dfRA97vw6SNq/ueZs9LCf29vt59G19UfRk2o0LkVnpu9EB6XnY6lVPK+9cr4fVjquZnmc+70Oa+sPo+32i+hhNT8t7ne/6+BJ9z857vsJAAAAAAAAAAAAAAAAAAAA4F0xWhYAAAAAAAAAAAAAAAAAAAAAAAAAGKKRL778ardcnylZlkXHqtVo2q6OfRC9kLbL7yf1eq1ccRL9fj/68mXR5Nfbyybp9v7Lv0aT9HPp+3CaKpVK9GLZpFb/sFwVBudKduH1ecPp2On1ot1ON9rp/Bzd2t6Onpb0vrgxdS3aaFyOnhVpPj7b+D7abr+IHlV6P9/75G7U9RA4rE45l1vLK9EkzZVmczZ61Ovk2vrD6HHn2kHS3wFz5f6l/T1Iu/08urb+KHpSU+X15cbU9eigPM+jX39zP5q2D+ug40z3t9QqXr9eeb09yGFf33T/j598Fz3p6zk5eSWanq/Dvm4AAAAAAAAAAAAAAAAAAAAA59VoWQAAAAAAAAAAAAAAAAAAAAAAAABgiEa++PKr3XI9VJVKJXqxbOXib4oO3l42u5BFx6rVKP/YTq8XzV/l0aTf70dflk3S7f2Xf40m6efS93mzfmr/n9H/71f/EX1fjWb/pViMlP+DNCs7+vr/JP1f5ZxI/uevvzdSlLfrf/2vSFbOpZGyu/9n2f/5P6OnbeS/lO+LD4rrS/6b/0f0rPgv5fMy+ksxd//Xq1fR40rnz6sPy+vnwHkD/NM/3ZuqR//f/6//PcrrOp1utLW8Eh2UZcX1t9mcjR718/ra+sNou/0iOmyNxqXozPSd6GG128+ja+uPoic1M3072mhcjg5Kn99breJ5zvPXP8cf5KDjPO79p7/HFubnoun13s/m5o/Rx0+eRo8rPc6tmx9F93veAAAAAAAAAAAAAAAAAAAAAM47/xUIAAAAAAAAAAAAAAAAAAAAAAAAAE7ByBdffrVbrl9TrVajF7IsWh37IJqV22Pl99P2xYuVaKVS9H230+tF81d5NOmVt7/KB27f+SWaD9z+6/0M3M674bvv+9H/+t/+RxQAGL4//P5S9E//+scor+t0utHW8kp0P+lz/71P7kbr9Vr0sNbWH0bb7RfRYWs0itd5ZvpO9LAeP/kuurn5U/Sk5ufnounvpUFbW9vR1W8fRI/q1s2PopOTV6ODjnv/6e+/ueZsNL3e+0l/p6ytPYqmv3OOKz1+Or6jvr8AAAAAAAAAAAAAAAAAAAAAzqrRsgAAAAAAAAAAAAAAAAAAAAAAAADAEI38x390dv+2GBurxg1ZlkXfdTu9XjR/lUeTXnn7q3zg9p1fovnA7b/ez8DtcBjffd+P/tf/9j+iAMDw/eH3l6J/+tc/Rnldp9ONtpZXooc1M3072mhcjh7W5uaP0cdPnkaHrdEoXu+Z6TvRw1pbfxhtt19Ejyv9PdVszkbHqsXfWYPa7efRtfVH0aOan5+L7nf/x32eq+X9zZX7f9i/D59tfB/d3PwpetK/j+q1WnRq6lq0Xi+2AQAAAAAAAAAAAAAAAAAAAM6b0bIAAAAAAAAAAAAAAAAAAAAAAAAAwBCN7O4p12dKp9MtV6/r9XrRV3keTXo7v0Tzgdt3yp8fvB3Ogu++70f/63/7H1EAYPj+8PtL0T/96x+jvC597m4tr0SPamb6drTRuBw9rHb7eXRt/VF02BqN4nWfmb4TPay19YfRdvtF9Liq1Wp0rjkbzbIsOui4j1epVKIL83PRYd//Yfd/UL9ffL59tvF99KTPY5L253eTV6JHfb8BAAAAAAAAAAAAAAAAAAAAvC2jZQEAAAAAAAAAAAAAAAAAAAAAAACAIRrZ3VOuj6TT6Zar13W6f//2bufncvW6/X4ezrMsy6Jj1Wo0qdU/LFeFx8/+f9H/5/9nKQoADN8ffn8p+qd//WOU16XP9a3llehxzUzfjjYal6OH1W4/j66tP4oOW6NRvP4z03eih7W2/jDabr+IHle1/Dy4MD8X3U+rVTz/R/37aGJ8PHrv3t3ofo57PGn/Z2aK13fw8+1B0vtrY+OH6LD+/qtUKtH0+v5u8mo0fQ4HAAAAAAAAAAAAAAAAAAAAOCtGywIAAAAAAAAAAAAAAAAAAAAAAAAAQzTy9NnG7t8W3c7PccOgTrdbruDdV6lUohfLZlkWrY59EE3qtVq5Kly8WPx8+v3D+vdvX0T/+V/+LQoADN8ffn8p+qd//WOU13U6xef91vJK9KQajeL5npm+Ez2sdvt5dG39UXTYJievRG/d/Dh6WGvrD6PtdvG57bgOel7yPI8utYrXodfrRQ/r1s2PopOTV6P7Oe7xpM/FzeZsdKxajR7VTnlcm5s/Rk/6vA5Kz3Pj8uVovf7653YAAAAAAAAAAAAAAAAAAACAN220LAAAAAAAAAAAAAAAAAAAAAAAAAAwRCNffPnVbrmGc6teq5WrQuXib4pWKtELWRatVqvRpF5//ffetH//9kX0n//l36IAwPD94feXon/61z9GeV2n0422lleiw9JoFM/7zPSd6GHt9HrRVqvYnzzPo8MyM3072mhcjh7W6uqD6Nb2dvS4JievRG/d/Dg6qN/vR+8vLkWPevzNT2ejB33OXVt/GG23i8+jR3Xc53FQOt7/3n4eTfuTbj+p9PfAxMRvo2l/xwb+LgAAAAAAAAAAAAAAAAAAAAA4LaNlAQAAAAAAAAAAAAAAAAAAAAAAAIAhGvniy692yzW8MVmWRceq1WhSq39YrgoXK5VopWx24e//3nn179++iP7zv/xbFAAYvj/8/lL0T//6xyiv63S60dbySnTYGo3i+Z+ZvhM9rJ1eL9pqFfuV53l0WGamb0cbjcvRg6THXyr3p1fu33Ed9PjHPf70OfvzzxaiaXs/a+sPo+128bn0qNLre+vmx9GDHu+wtra2i26X3fpLdFjvg2r598T/Vu5/rV6Lvit/ZwAAAAAAAAAAAAAAAAAAAABnx2hZAAAAAAAAAAAAAAAAAAAAAAAAAGCIRr748qvdcg0Hqlar0QtZFs3KVsc+iCb1Wq1cFS5erEQrlaIU/v3bF9F//pd/iwIAw/eH31+K/ulf/xjldZ1ON9paXomelvQ5cq45G02fIw+y0+tFW61i//I8jw7LzPTtaKNxOXqQ9PhL5f70yv07ruanxfNRr7/++Tlpt59H19YfRQ8rPd8L83PRg2xu/hh9/ORp9KjS483MFM/nWLk9bFtb20W3y279JTqs90X6e2Vi4rdFx8ej+70+AAAAAAAAAAAAAAAAAAAAAAcZLQsAAAAAAAAAAAAAAAAAAAAAAAAADNHIF19+tVuueYfVa7VyVaiOfRDNsix6oWy1Wo0m9frrv8dw/fu3L6L//C//FgUAhu8Pv78U/dO//jHK63Z6veji4lL0tKXPm3PN2Wj6PHqQtJ+rqw+i/X4/Oiwz07ejjcbl6EHyPI8utVaivXL/jiodf7N8PsYGPo8nm5s/Rh8/eRo9rEajeP/PTN+JHmRrazu6tv4omo7zqG7d/Cg6OXk1eto6nW50a7vY/07n5+hxX5dB6XVKf1dNTPw2mv5eqlQqUQAAAAAAAAAAAAAAAAAAAIBBo2UBAAAAAAAAAAAAAAAAAAAAAAAAgCEa+eL/z97/QMd113f+/2eC+CE7Ekg/28cRIJIRHhnbGjtxTpOS5NuwUiRvceI9stJC+Z7zawNCckv9/bKGdFS6sNu0J6upiUtraG11gN3zPadsd5HFhvibtRVNQ2lCk/3GiS3/iTRCN0EEEWyvBBK2+CKOfve+7/0kliJZc//MzJ2Z5+Ocd14j2bFmPvdz73zujO57hp5ecG4jRCoqKiSrq6oktZradzm3bPrP9d+vePvy/x/C6bvDr0t++E9OSgIAgODd07RR8slH2ySxvMnJH0uOZr4vOT8/L5krVc569fadOyT1enY1+n69cOq05OzsrGRQtm7ZLFlXd5PkaoK6P5WVlZJ33nG75Erjcf7Cy5KTk/Y6MlvR6M2SDdFbJFczNzcnefrMOUmvj0tv561b7XHN93mK3j4XL16SnJqetnPqp5L6cfqlt1+tc75WW1NjZ62d+s8BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAED5ucFJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQoMhTQ08vOLcRgKqqKsm3V1RIVjhZVX2jpFZbU+PcslVX2/+f/vsoLlNT086txebm5iSvOqnNz89L/s+XXpfc93+NSwIAgODd07RR8slH2yRxfTOzs5KnTp2W1OuWXNHr3507d0hWO+vp1ej79YJzP2ed+x2UO+64XTLf90efT9zujMdK5wdnzpyTvHjpkmS2tm7ZLFlXd5NktsaNVyQN41VJr+rr3yPZEL1FstDnP3q9rtfzU9NOTv1UUv+5X5WVlZK1te+S1POqptY+L8x2ngEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgOJzg5MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACBAkaeGnl5wbpe12poa55atqvpGyYqKCsm3O1lVVSWp1dYu/v8QDnNzc5JXr9qpzc/PS87MzkouNTvzc0n997Srzr+n/92gjL1i/3uf/dZPJAEAQPDuadoo+eSjbZLIjl4PvXDqtOTsCuunoOh1986dOySrl6y7V5Kr+1no+7Nh/XrJ7du3SS7l9+ds3bJZsq7uJsls6XX0+fMjkl4fnx7fhujNkvX175UMGz3OU1PTdk7bqc8b9NdB0eel+nxUf11dbc+/yspKSQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUDxucBIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQo8tTQ0wvO7aJSWVkpucZJrab2Xc4tW3VVlWRFRYXkmjX239f/P/Jjfn5ecmZmVnKp2Vn7+790/p42Nzdn59VfSC41NT3t3CpOY6/Yj++z3/qJJAAACN49TRsln3y0TRLu6HXcaGZMcnLydclc0ev2nTt3SOr1/GpydT+93p8ZZ3176tRpSX3/3Kqrs+fv1i0fkFxK/7vPPPucpNufs3XLZsm6upsk3Ro3XpGcmHhN0uvj1OdnDdGbJb3en0KZmrLPS/R5zdTUTyX1PNDnNX7p+VhbUyNZVX2jpP66unrx+S8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAACi8G5wEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABijw19PSCczsvqqqqJN9eUSFZueYddlZWSmq1NTXOLVt1tf3/VTj/H4IxMzsrOf/LeUltbm5O8qqTS01P/dS5tZj++/r/x/WNvWKP02e/9RNJAOFXU/kryem5t0kCCL97mjZKPvlomyT8mZj4oeRo5vuSuaLX/Y2x90vW1d0kma3zF16WnJx8XdIvfX927twhWe2c16xGr7dPnTotOT+/eN2dra1bNkuuNA5+f85q//5q9M8bzYxJ+h13fX7YEL1Z0uv9Cgt9fjQ1NS2pt9eUc14163wdFD1+tbXvktTzVZ+P19YuPt8GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC5c4OTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgQJGnhp5ecG5fV21NjXPLVlV9o2RFRYXkmspKyUontdraxf8frm9ubk7y6lU7l5qannZuLTY783PJ+fl5Se2Xztezs7OSCJexV+zt/Nlv/UQSQPg9/MEfSX5v3H5+++fX10oCCK97mjZKPvlomySCcfHiJcnzF0Ykl65Dg7Z1y2bJurqbJLN1/sLLkpOTr0v6pc9/du7cIVldVSW5mhlnPX7q1GlJr+O12jhMTdnnC6detH+OW17HeSl9XjNuvCLpd/z1eWZD9GbJDRvWS+rtUSr09tPnffo8T38d9H6mz/P1+b2ez/o8fun5PQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcO8GJwEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQIAiP/nJxQXrRkVFhXxjzZpKycpKO2Gbmpp2bi02Ozsr+cv5eUlt3vl6dubnkkvNOP+f/nsoT2OvzEl+9ls/kQQQfs9/7nHJ//G97ZJf+MdbJAGE1z1NGyWffLRNEsHS69ozZ85Jzs3Z65tc2bpls2Rd3U2S2Tp/4WXJycnXJf3S5087d+6QrK6qklyNHq9Tp05Luj0fyPbnTk7+WPL8hRFJt6LRmyUbosE8z+l5MW68Iul3O+hxqKuz9+/31b9XstTPY/U46vNTPZ+mpn4qqc9Pg6LHWc+zmtp3SdbW1EhWV9vf138PAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC81Q1OAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAEUWTM7tojIzOys5/8t5SW1+3v5a//lSszM/l9R/T7s6Nyc55ySQD2Ov2PPts9/6iSSA8NpW8wvJf/nKJyUvnHtQ8o5H90gCCK97mjZKPvlomyRyQ6+vz5w5Jzk1PS2ZK/X175FsjG2SzNbk5I8lz18YkfSrqqpK8vadOyQrKiokV6PPV06dOi259PxkNfrn7HR+brVzP5by+3jr6uz9R49zto9vNfq86wcTP5ScnHxd0u04LLVh/Xo7N6yTrKu7SbLcTE3Z+5/eD/V5sP7a7zgvpfeD6uob7XS+rqmtkVxpfgIAAAAAAAAAAAAAwsJQhmH+1/rP+Al1ImN9b1SNjsofms6q4eHVf+U8Eo+rJue2amxUjVbGdqldDUpFo1HrP8r8LwAAAAAAAAAAAAAAQNmh4Z+Dhn8oBBr+AcWDhn9A8aLhX37o9TUN/2j4lw0a/uUWDf8AAAAAAAAAAAAAACsyDJU2htT4iYw6PjqqhoeHnT/IH2kM2Niodu/apVqizVYfQAAAAAAAAAAAAAAAgJIWWMM/3TBgZmb5RnsrNfzQjR7mrtqNlJbKdaMQoJBo+AcUj4c/+CPJL/xhj6T265/6O8lz0++QBBA+NPwrjPMXXpbUjdxyRTek27rlA5LZKpXGf5WVlZJ33nG75Eo/1+/j1Y9vx/ZtkvrnBkU/7osXL0n+YOI1ydkVGrlnS4+Hnie6AWC5N6DT5+G6MaCeh0sbAwattsZuAFjlNAbUX1dX29sj6HkFAAAAAAAAAAAAALAYKp0aUieOH1f9BWjul61IvEPt3b1L7WuhASAAAAAAAAAAAAAAACg9NzgJAAAAAAAAAAAAAAAAAAAAACh2htXkL6HaYzG1aVOr6urtDXWzP8vCcL/q7+1Sra2bVCzWrtoTKZU2nD8EAAAAAAAAAAAAAAAocpHvjxsLzu1Fpqd+6txabGZ2VnJ+fl4SwMoqKiokq6uqJLXKNe+QHHtlTvJ//9tw/xIVAKW++fH/Kbmr5bCk9siXeyUPfu/dkgDC556mjZJPPtomicWmpqYlL166JNkY2yQZlMnJH0uevzAimSt1dfZ23rrlA5LZCvr+VTnrvtt37pDU68HV6POsU6dOS7o938r25/r9Ofrf3bpls+SGDeslc0XfX72dLl68LDk3Z6+jvaqsrJTcsGGdZF3dTZJL1+3lTh8fZp3tMOW8TqC3i9/tsJTeLno71Na+S1LP79raGkkAAAAAAAAAAAAAwHIMZaSPqAOHj6nh4WV/PbwoRSJxtTexX+3rbFZR53sAAAAAAAAAAAAAAADF5gYnAQAAAAAAAAAAAAAAAAAAAADFxDBUOtGuNm1qVa1d/SXV7M+ysDCs+nu7VOumTao9kVJpw/kDAAAAAABQdAwjrVKphEok2lV7+5JKmN9PpTn3BwAAAAAAJSvy1NDTpfVbHYAPlZWVkmuc1Kqqb5SsqKiQfLuTVVVVklp1tf21/nur+e7w65If/pOTkgDCp6byV5ITX31IcqkTQ/slH/zar0kCCJ97mjZKPvlomyQWm5qaljz14mnJ2poaye3bt0lmu65Zjf45Z4bPSc7Pz0sGzev9v3jxkuT5CyOSfu+fXifevnOHZLb3Y2Z2VvLUKXt7uL0f+ufeecftkivRP+fMGXt7zM3NSbpVV2fvX42xTZJBzZfV6Ps/OfljyYsXL0t6fRyaPh/YsGGdnevXS9bW2vMKi+nxnpmxt8fUtL2fz878XFJ/HTQ9z2tr3yVZ/cbX9nbS2xEAAAAAAAAAAAAASpp1gfyRw6q3f9j5RvmIxzvU/kNJ1Rx1vgEAAAAAAELNSCfUge5janghu0vaI+a5f8I89+/k3B8AAAAAAJQQGv4B16DhH4ClaPgHFD8a/l0fDf9sNPyj4Z+Fhn/u0PAPAAAAAAAAAAAAAArBUOnEAdVVho3+lop39Kj9+zpp/AcAAAAAQJilE2pTV7/zRfYikbhKnByg6R8AwBXDMKTGT5xQGTWqRkft7w8Pr/yaejwel2xsbFQqtkvtaoiqaLNZ8l1ci/EFwsLeF42hcXUiY+6Nzs7IvpgLzlgb5lifyCjrwCejffbsig3NrXOZpibrVqM53tZw71IN0ahqNgvInzfn7rg5d7N93n5z/i45XpjzlykcDBr+oajpRg+6AZ9W4zR+0HQDv6WNHwrduIOGf0D4de+4KPnFP/qM5ErqP/F1yem5t0kCCA8a/l3f0oZ/mm7cttNpWKcbavmlG4SddhrNzTqN24JWqIZ7S3m9Hyttl2zpRnxbt3xAciX68enGf14btOl19tYtmyULtc7W2083cNSNAP3OM73ddCNA3ViSBnPZ0dtl2pnX+uupqZ9K+m3UuJTeXno76QbuS7cbAAAAAAAAAAAAABQbI51QB7qPrXjxSDmyLjjYm9iv9nU2c0ESAAAAgAIzVDo1pE4cP65G1Vk1PPzWc7dIPK6arAv+d8fUrpYSb2BumOMxdMQcD6sZwmrjsUvtazHP6zixK0FplYh1q36Pr2VEOvpUJtnsfAUAZcJ5Dj1sPoeu1IxFN2JpbNytdpX5h6IYRloNDZ1Qx48fW3a94YeM897dav++lrJt0sT4AmFhKCM9pIZOHFfHj63caM6reEeH2r1rn2qhAaCQY9+RwzkZa3lv0zz27eLYtzrWRC7Zx4kj5nFiNAdzV4vHO3gdxyca/qGo0fAPQK7R8A8ofjT8uz4a/i1Gwz8a/i2Hhn/+0PAPAAAAAAAAAAAAAPwyVCpxQPX2r/xJ+7mkL5RY2fKNG/IpEu9QRw8ly/wijnyxLhY5og4cDv4CzzCQC62ODij6SQAAAMCVdEK1e2jQHu8ZVAOdJXgiY47Hpq5+54vssBYvUR7mwrUiEfN8P2Oe7ztfA0Cp8/qhLx19Y+X1HGqkVerI4by/Zh7v6FH7y6GZEOPrmuy7JfSaOe+5hIXVVN5qdpbfuWU189p/aJ+5/ctsAnDsCxXWRNlymvwdTqr+AjwHyWs5fDifazT8Q14tbdCnGzHoBgyabiij/7zi7XYG1WgmLGj4h1K2reYXktF3/b+ST7xaLZlv998849zy5vdaXpbc1XJYciWPfLlX8sKPvD1O3Sjwn19fK5lvurHh0dMbJIFSQsO/68u2sZxu5FZXd5OkX7rR3PnzI5IXL9kN2oIWtsZ/d95xu2S2Jid/LHn+gj1ObmXb+E8bN16RNIxXJb3SP7cxtkky23HPFb39dCNA3dgwqIZzevvWOo3HlzaWK/TjDzu9ffTx6M0Ggfb20V/73Q+XWrrd9PkmjRwBAAAAAAAAAAAAhIqRUu1tyZx9Ar8lEo+rpsZG1RjbpXY1RJ1P4tfpnmEYb+b4CXUiM6pGR3PfFNC+oOCQSpZis4zQSKtEe3dBLhjJJxpKAAAAwK10Iqa6+t2vkyPxHnVyoLPkLor2PB4dfSpDx7+SYqTaVWuv94YZ1rl+4uSA4lQfQMkz0ipxwPvrbiXbRHgJI50qWEOba9nNsEqvGRrj643XtW/Y8Tp5oRS2ede1rPcP9+4/pJIl3okuLMc+Gm06WBNlxzBU+sgB8/mnMB8YuJxSXR/lwg1OAgAAAAAAAAAAAAAAAAAAAABCwrooflNrb+DN/qxftu/p6VODg4NqbGxMZQYG1EAyqZKdzaq52Wr0573Zn8X+/6Pmv2X+e53mv5s0//2BjPysMfNnDvb1qJ6OuIpHIs7/EYyFhWHV39uq2hNpZbccRLAMlSqDZn+WhYV+dSLtfAEAAAAA8MzInHVueWOd62c4yQdQ0gyVTiVUe1t5vO7mldWMKdEeU61dvaEYp+HhftXVuqlkXotmfH1IJ0qy2Z+F18nzzGrelWhX7bG20OyLC8PDqr+rVcXaEyqVLsFFudVYLkTHvoU3jn0pVYrDvTrWRFkx523KPFZsam0NVbM/y5vP3+U6h7MXeWroaWY5VlRbU+PcslWueYedlZWS2tK/t2aN/edL/x4W++7w65If/pOTkkApefiDP5Lc8r7/Jfnxf2iSzDd9P77whz2SYTP7s0bJT/b+n5JPvFotmS/ban4h+dR/PChZ9/v/ThIoJfc0bZR88tE2SSw2NTUteerF05Krqauzx3Prlg9IBmXceEXSMF6VDJpel27fvk2yuqpKcjUzs7OSp07Z4zM/Py/pldfxm5z8seT5CyOSbrn9uXpe6J83Nzcn6VZFRYVkQ/Rmyfr690qGjX58+nFPTTs59VNJr49fq3LmW23tu+x0zp+qq+3vc96UHb0/zs7YqbfTzMzPJWedPw+Knr96e+njB5Z36fJlyWefeVayXNTX10vedtutkgAAAABQ7v7H//gf6vd///eVYfCbGgAAAADgj3VRT7CfyB/v6FG7d7WoztB9pL518cYRdfj4MTUc4MUbkXiHOjqQVM3O1wiAkVLtbcnAG1CGVbxnUA10hm1/AQAAQFilEzHzHM79WjkS71EnBzpVqa08PY9HR5/KJDmTKyVe58K1OD8DULKsRkMHgmlqU7LHSqupzZHDqjdkDW2uFYnE1d6jh1QydK+9Z4Hx9S2ItU6YsQ7LA6vR35Fg3xPMFeu9xkPJUjh/td6bNMe8l2NfaLAmyoLznnryWFG8VytzOLFf7etsLrnXvIJwg5MAAAAAAAAAAAAAAACeWc3+XnnF/nAPAAAAAIBXVrO/tkAu7InE46qjb1ANjo2pgWRnCJv9WaKquTOpBgYyamxwUPV1xFU8EnH+zLuF4X7VHUuoND3pg2NkyqbZn+VshskDAAAAAP4YanzUuQkAuIbVsKVdbWrtCqSxTaky0gnV3tYd6mZ0loWFYdXf1araU8X1eiLjG4TSX+vwOnkuWc8F1n4YzHuC+TDc36vaYu2qyA53i1mN5drNMQ9xsz/LG8e+RNqcKaWMNVFW3pi3/UXzXq3M4d4u1dbO+/XLiTw19DQzvgRUVFRIVldVSWpV1TdK6j9/u5NVS/5edbX9tf57yI/vDr8u+eE/OSkJlJLB/+NpyaYt/yxZ9/v/TrJQ7r95RvLvev5KsuqdhT2DvnDuQcm2Q7slp+feJplvD3/wR5Jf+MMeye5H7PH5+5FaSaAU3NO0UfLJR9sksdjU1LTkqRdPS2ZLrydv37lDMqh15OTkjyVHM9+XnJ+flwyKvp87nfu9dP28En0/Xjhlj9Ps7KykV3V19rzcuuUDktnS43P+woikW9HozZIN0VskV6Mf97hhX6Q9MfGapFeVlZWSDc79qKu7STLs5ubmJGdm7O0+NW3vN1NTP5X0Ox/0uOj5qM/jamtqJDlfc0cf1/R2mp35ueSMs5309nSrpfle5xaW8+Wv/I3kSy+5ez4pFQ899LuSd991lyQAAAAAlKve3l71x3/8x2qhjC7+BwAAAIBgGSrV3qZ6fV7QEIl3qMT+fSFt8JcNQxnpI+pA9zHfFy1EInGVODmgOot1KELESLWr1pBfABWkSEefyiSbna8AAACA60snYqqr3/35SyTeo04OdKpSO2XxPB6sw0tMMK9zxHsG1QAn9gBKhZFSiQPJwJvalNax0vpQnANF0wDrWtZa5qS5lgn3lmB8gxPMWifMSvV8peBy9FyQL9Z7b3uPHlLJYnsf0mp0GsB7j/lmve97dCCpSu5MmTVRVqwGvUG8Z15IvF//Vjc4CQAAAAAAAAAAAAAA4FlPTw/N/gAAAADAM+siO38XhlkXfPT0DarMQLKIm/1ZoiranFQDmYwa7OtQ8UjE+b57CwvDKtmWUGnD+QYAAAAAAMiTqGpodG760NhAVwAApcB6/bddbWrtLdoGT/lhN1ArxmZ0loX+LtXWnjIfRVgxvkChWR+uVOzPBdZ7b/1drSpRRG++WU3TNnX1F2XTtIXhftUdS6i083XxY02ULfkwtiKdt9ey369vVymewN8QeWroaWZ/AVRWVkqucVKrqX2Xc8um/1z/fa22tsa5hWL23eHXJT/8JyclgVJQU/kryYmvPiSpdT/yV5J/P1IrWSj6/p08cFxyy7ZvSubLV7/xeclPPxGTLLTnP/e4pB6HsN0/IAj3NG2UfPLRNkksNjU1LXnqxdOSblVUVEhuj2+TDGqdOjM7K3nqlH2/5ufnJYOi7/fOnTskq6uqJFej78cLzv2ade6nV3V19vzcuuUDktmanPyx5PkLI5Jubd2yWbKu7ibJbOntcv68/XP9Pn59ntMYe7/khg3rJYuV3p/0uExN/VRSj9vc3JykV1XOPK2uvtFO52v9fc4Ts6O3w8yMvV309pl2ttfUtL0dl2ppvte5heUcPPiY5MjoqGS52dxo/zbSww9/RhIAAAAAAAAAAABwK52Iqa5+77/WHe/pU4c6m1VpXgJvXfhxwNdFiJFIhzqaSapm52u4JxeV9BbnhaBeRDr6VCbJjAEAAEB2vJ7TReI96uRAZ8mdy3keD9bhJSeIc8mOvjHFtABQzIx0Sh3oTua0WUu8Z1ANdBb7iiKtEu3dJdH8x1rTnDSfvMK1RRjf4NkNFP18kFPYsT4Plt/3AsOoGNbqpfL+Vim818maKHul+L5sJBJXiZMDqgQ2j283OAkAAAAAAAAAAAAAAAAAAAAAyDPrF/a9XuATiXeovsExNVCyzf4sUdWcHFBjg32qIx5xvufOwkK/6m5PKcP5GlhNU4yrTQAAAADAr2iD/aHaXllNLXbRYwZA0bI+yKRdtXb15rSxTWlIq0SsNJrRWRb6u1RbIu18FQaML1Boft4LDLNj3e0qFeI334x0omSaphX3e52siVwpoXl7rYWFYZVsS5irEkSeGnqaPSELVVVVkm+vqJCscLKq+kZJrdr5e/rPK95up/4+ytPc3Jzk1at2ai+euyT5b/7ie5JAKejecVHyi3/0GUntq9/4vOSnn4hJhsWX7s9IfuJ3/kwyaLM/s9+U+A9HuiWPnt4gWWjban4h+S9f+aSk9qMffEhy8x9/XBIoBfc0bZR88tE2SSw2NTUteerF05J+NcbeL1lf/15Jv+bn5yVfOGXfv9nZWcmgbd2yWbKu7ibJ1QR9v+rq7Hm6dcsHJLM1OfljyfMXRiTdcvu4l9I/f9x4VVKve72qrKyUbIjeLOn1foWVHp+ZGXu+zDjzZnrqp5L6az2/vNLnr9XO+ao+H9Xfr62tkcT1vbl97ONkUMe1UnXw4GOSI6OjkuVmc6O97n/44cXnQQAAAAAAAAAAAMCq0gm1qavf+cKdeEefOpQs5UZ/y7EuCDmguvq9XeQQMccsY44Z3LMuRivFi0tW0tE3ppgqAAAAyFY6EfPUvCES71EnBzpL7rzO83hwzlaCnAZDHps6MCcAFCurwdCB7mN5a2oT7xlUA53FuqIwVKq9TfXmqBldJB5XTWY2Otd9vGlUWZfADA/n7jXPcLzGyPjmTm7HNgyK+9gSJqU9V6wm3UczSRW6VbuP92DDrNjOkVgTuWSkVHtbMifjFTefs80nbNWoYiq2q0E1ON/Xxk+cUFb3nVHzCTyXz9+c5yt1g5MAAAAAAAAAAAAAAAAAAAAAgLxJq0T3Mee2O3KxQrLcmv1Zoqo5OaAGe+LO1+4s9HepRNr5AliBdXHarvK+zgQAAAAAAtKs9iWsNkDuRSJxldjHyRmAImOkVSrRrlq7+vPW2KbYpRPBNsGynj86evpU3+CgGhsbU5mBATVgVjKZXFL2962/M2b+3b6+HtURjzj/SjCOdberlOF8USCML7yytvXuFpr9BcIYUsfPOrdzxGq+aTXzerOC3d+uZ2GhX3WH7s037+/BrsTaJ+IdPaqnr08Nmse1Qev49pYyv2/9mfl3eno6crIdiua9TtZEHhgqdSC4Zn/WccF6zh4ctOen9bw8IM/RnaqzuVk1L6lO5zk818/fvF9vbpunhp4uyb2itqbGuWWrXPMOOysrJbWlf6/i7RWS1VVVkigN8/PzkjMzs5JLvfHns8v/+dzcnJ1XfyG51FX9505ma+wV++9/9ls/kQRKwTc//j8ld7UcltR+9IMPSW7+449Lhs3HNk9JPvbpr0hWvXNU0qsL5x6UPPDV+yT/+fW1kmHx8Ad/JPmFP+yRXOo3DxyRDNv9Bry4p2mj5JOPtklisampaclTL56WDEpdnT3ujbFNkhUV9jrbr/MXXpacnHxdMmhbt2yWrKu7SXI1eh155sw5yalpezy90uO2dcsHJLM1OfljyfMXRiTdcvu4l9Lj8IOJH0pOTLwmqb/vlT5/e1/9eyT1/QtqPoWVPi+Zdc5f9NdTUz+VnF3hvMWtKue8t7r6Rjudr/X3a2sXny8D13Pw4GOSI9bHb5Whzc4nkT388GckAQAAAAAAAAAAgNUZKtXu7UK7jr5BlWzmIisjnVAHuo+5vujBauZ2NJNUtAxwx0i1q9beYeer0mVdsLX36IC5jznfAAAAALKQTsRUV7/787tIvEedHOgsuWbunsejo09lWIyXIMOcEwfMOZH9OSXnZgCKkdfXK4MgHxDTWXwriiBfc4zEO9TRQ/tUc9TvONjPW4ePnQ1kWxZyvcf45pr393mKQdxcm1sfPIUAmM8Pm7r6nS/8sdbJTXsb1e5du1SLuT9GV90nDWUYZg2dUIePm89ROZyvHX1jIVm/B7tvWk3+9u/rNI9/zjdcM497qSPqcDK4NYI1DxInB1RYn/pZE3kT1PN23HzO3n8o6WPOLsM8jqSPuDuvv55SfT0sWzT8o+FfWaDhH5A/NPyz0fAPCA8a/l0fDf8Wo+EfDf/CgIZ/KEY0/KPhHwAAAAAAAAAAANzx+gv7NPtbIp1Q7V6a/tFEwjWvc5YGiwAAACgHNPxbjIZ/WJbVIGDoiDp8fFSps29t8hOJx1VTo9W8ZJ9qaY6W7YX/AIqQkVaJA92qv4BNv4qyuY2RUu1tSd/NgOwmsYeCf93c3K4pc7sG0TCqIE2wGN88cN9UTMZzr30NUpjFzPVYJ+9FBSeAhn/+m845rH3vyGHVG1DTrmuF5fw2sKZpQY35G4Jt/BfK82fWRD6YYxczx87H3JBGkOZzdk6P3yXx/F14BW/4pxs1LG2wV+U0HNB//nYndeMBbc0auwHE0kZ+CCfdEO/q1eUb4+nGFb9coTHItNPgYiV+G6zkGw3/UEpqKn8lOfHVhyRXEvZGco/8q1ck/23nFyS9+p3P2Y0Dn3i1WjJsnv/c45Jbtn1TcqmvfuPzkp9+IiYJFDMa/l1frhr+aXr9vmP7Nsmg1u0TTmO50cz3JYPmtQFeUA0Jo9GbJRuit0hm6+LFS5K68Z/bhnv1TmM93ajRK/1zdSPCHzgNAN02yF6JboxYX/9eyXJt2K73X30epRsDzsz8XJLGgMgnGv7R8A8AAAAAAAAAAABuePuF/dBcoGAYynBuviFawAvxPV4oVc4XEXhBwz8AAABgZTT8W4yGfwCA8hBs4x4/iq+5jftGacvJx1oqiMZR+X+NlPHNDw8N/0p0/Y9VeHwfy2qIvXd/DhpuWnLUmK3g770F0Ow0Z41OtQDHPjzvdbIm8svr6yhavp9fAnn+LuPnxBucBAAAAAAAAAAAAAAAAAAAAADkWDrhvtmf1fQg3xcnGEZapVMplUi0q/b2mNq0aZNdra2qdWk5fxaLWX83oRIp8/813tIWMDeak2qwJ+58kb1j3QmVdm4DAAAAAAAAQNasZj3tbaqrtz+QxjZWc6GOnh7VEYk43ylx6SP+m9F19OWlSUy0c8DT68/XWljoV4dTeXq93ML4AkUtEu9QfYODKjMwkLumc9FmlRw4qfo6/O1/Sx07nFKF3BvTR/w3+0uczOG4W5yx74n7f84v9HgL1kQBSKsTx5ybHhSicV4Qz9/q7HE1VKZP35Gnhp52tbdUVlZKrnFSq6l9l3PLpv9c/32turpKsqKiQhKFMTM7Kzn/y3nJpaamp51bi83P239/dubnkktdnZuTnHMS1zf2ij1On/3WTySBYta946LkF//oM5Ir+eo3Pi/56SdikmHz/Ocel9yy7ZuSXv1l6hHJL/zjLZJhsa3mF5L/8pVPSq7kRz/4kOTmP/64JFDM7mnaKPnko22SWGxqyl73nXrxtGSu6PX/9vg2ydraGkm/9P0/M3xOUq9Xg1JXZ8+frVs+IJmt8xdelpycfF3Sq61bNkvW1d0kmS293j91yt6ubsfF6+NezeTkj520x2Wl8w639Hnn++rfI7lhw3rJpeej5UZv95kZez7o8dbnc3qeBHX+VlVln+9XV99op/O1/n5Q+z3C6eDBxyRHRkcly83mxkbJhx++/vkQAAAAAAAAAAAAoIyUam9zd7FJ/n5J31BGekgdOXxc9Q/7+0T+a8XjHWr3/n2qpTma08eQTsRUV7+7C0niPYN5b6RYrIxUu2rtdT8vIpEOdTSTVM3O1wAAAEAp8nI+YinERdn54Hk8OvpUJsnZAwAgzAyVTh1QXR5eJ1tJvKNH7d/XqZqjaZWIuf+wGEtxvc5pqFR7m6+GdIVYM3h9fVTL3+ukjG/+uB/rUl3/YxXphNrU1e98sTKr0V9i/z7Vmctmc8vwu/9dSzfMK8hTUpbjvBI5jpw0jyN5u+/+j9eWjr4xVZjTaNZEQfGzDxb6fVivr/9o5fpe/Q1OAgAAAAAAAAAAAAAAAAAAAAByxlCpAy6b/Vm/pJ/ri7+MtEol2tWmTa2qtas30GZ/luHhftXbZf7bmzap9kRKpXP0Sf3NyaOqIxJxvsrO2eQRlXZuAwAAAAAAAMCKjLRKtLcF1tjGaorU0zeoBpJWYxvnm+UgfcRfMzqrYVoBOhtFOwdUX4e715+vtbDQrw6ncvTi+LUYXyB8ojEVv877V9bzQUdPnzo5kMx7sz+Ltf8N9sSdr/xZWBhWx4cKsS8aKnX4mHPbPbtRYT6b/VmiqnPgpOqJez/2WY4dTpmPPs9YEwXIUEPHzzq33dt7tHDN/ixe3p+/1tnjQ/mfvyEQ+f64Iau1NZWV8o1KJ7Xa2hrnFnJhbm5O8upVO5eanZ2V/OX8vORS01M/dW4tpv++/v8RTmOv2Nv9s9/6iSRQSN07Lkr+h31HJaveOSoZNl/9xuclP/1ETDIoNZW/kpz46kOSfl0496DkHY/ukQzKPRuvSB76xFOSW7Z9UzJsTgztl+z8+52S03NvkwQK4Z6mjZJPPtomicWmpqYlT714WjJfotGbJRuit0j6NeOse8+fH5EMeh1cV2fPo61bPiCZrfMXXpacnHxd0qutWzZL1tXdJJktPS6nTtnbd36F84qV6MfdGNskWVFRIRkUff8mJn4oefHiZUm393MltTX2+ax+HBs2rJcM+nEUOz3eMzP29piato8LszM/l9TbSZ+/+lVVVSVZXX2jnc7X+vu8DlGcDh58THJkNJznEbl26607JP/wU38gCQAAAAAAAAAAACwrnVCbuvqdL7LT0TemcnZtndXo78hh1dsfbIO/bMTjHWr/oRxcOGOkVHubu6aK8Z5BNdBZdlePuGak2lWrhwt3pGllprAXmwAAAAC5lk7EVFe/+8Yq0lAl103eC8DzeHT0qUwBGswAALAqD687Xk/cfM47ZD7nLV4DpFUi1q36PfyM4nmN01Cp9jbPDenshkwDqnAP1fs2suR+7cf45ndt7X68S3X9j9WsPLfjHT1q/75wNDnzeh63VEHmuYf3YK+V0/djVxPAGiOv9581UbB8jGdonlN87H/l+j7yDU4CAAAAAAAAAAAAAAAAAAAAAHIkfeKYcys7VqOD3FycYah0ql1tau0qSLM/y/Bwv+pq3aTaE+lgP7U/2qn273VuZ+ls8ohKO7cBAAAAAAAAYDFDpQ4E09gmEu9QfYNjauAtjW3KhDGkjp91bnvQlDhUwGZ0lmaVPOryBehrLAz3qiO5fDGa8c3t+AKeNatdS6a21WCzo2/QfD4IR7M/S3PyqOqIRJyvfDh7XA0F+ubfaszn6cPu3oO9Vu7ej81StFMdSjQ5X3hz7ES+Dn6siQJnZDyP5979IWkg27xP9cS9HTsWFvpV3qZviEQWTM7tsjY/Py85MzMrudTc3JzkVSeXmp35uaT+d5aamp52bgFvGnvFnk+f/dZPJIEwuGfjFclDn3hKcsu2b0oWyuzPGiU/86VPSf79SK1k0Lp3XJT84h99RnIlF849KFlff0ay6p2jkiv59U/9neS56XdIBqWm8leSh/7NBcnf2vMXkoX2yJd7JQ9+792SQBjc07RR8slH2ySxmF6/nj8/Innx0iXJfNmwfr3k1q2bJSsqKiS90o/nzJlzkkGvw+vq7PnUGNskme39nZz8seT5C/Y4e7V1iz1OdXU3SWZLn8+cdsZldnb5856VVFVVSd6+c4ek3+20mosX7Xk4Ofm6ZNDzsramRnLDhnVO2vOwsrJSEstbet6s9y99PjzjzCs93/zS8666+kY7na/192tr7e2IcDh48DHJkdHrr49Xs7nRXv9v3mxnsbjr7rsk16+zjysAAAAAAAAAAADAW7j8dH7rQp/EyYHgL64z0ipxoFv1D4fnV8itCzoSh5IBPlbzMcbMx+ji1+TjPYNqoLBXMoaekWpXrb3uG0RGIh3qaCapCnmtFAAAAJBr6URMdfW7P8+KxHvUyYGQXKAdIM/j0dGnMgXttAAAwHLcv964lPV6796jh1Tyul2dvP+cYnl90+sawRKedZOhUu1tqtfja+y5XO8wvvleT7q/r6W6/kcWjJRKHEjK+3Nxc54eCmmTM6/vBS3V0TeWvyZ6Lt+DvVZ43sPyt9bI3+NgTRQ0z6+fhOz9Vz/HjnJ8n/4GJwEAAAAAAAAAAAAAAAAAAAAAOZA+4u5Ck6bEocCb/RnphGpv6w5Vsz/LwnC/Sra1q0TacL7jV7Pal2hybmfn7PEhFdRPBwAAAAAAAFBCjHE16tz0wmrsdDIzsEpjm3KQVieOOTc92Ls/LE3Soqpz/17ntgfHTpgjkQuMr8jZ+AI+RTtVciCjxsbG1EBIm/1Zop37VUck4nzl3eh4/t51c/se7LWaEvtC0jCtWSWPej/2LSz0qxP5OPixJgqYoca9DujeXaFp9meJtuxW8QCOHeUismBybofKzOys5Pwv5yW1+Xn7a/3nS83Nzdl59ReSS73x7zr/DlBIY6/Y8/Wz3/qJJBAmNZW/kjz0by5I/taev5DMlwvnHpR86G92SZ6bfodkrnzz4/9TclfLYcmlvvqNz0t++omYpB6f/9b1Xclfv/Nrkks98uVeyYPfe7dkrnTvuCj5H/Ydlax6p5+lcvZ+9IMPSX7iSx+V/OfX10oCYXJP00bJJx9tk8T1Xbx4SfL8hRHJfK2bq6qqJLdu3SxZ7Xzt12hmTHJi4jXJoOj7e/vOHZIVFRWSq5mc/LGkHl+vtm6xx6mu7ibJbOntef68/fMvXrK3d7b049we3yZZW1sjmWv6fuv5OTU9LXnx4mXJoOap3q61te+S3LB+vWS+Hmep0NtjZsY+/9bba3bm55L6vFyfv/ult1t19Y12Ol+/uT3Zfvlw8OBjkiOj/tahex643849D0gCAAAAAAAAAAAApSGtErFu1Z/lr23n4hP5rWZ/rV39zlfh1dE3ppKBPHC3Yx5XiZMDgTdZLCVGql219g47X2UvF/MZAAAACJt0Iqa6+t1fqhuJ96iTA2FprhIcz+PR0acywZwUAgAQnHRCbfLw2mok3qES+/epzqyb2rh7TfNa8Z5BNRD2Fzc9jqMlfGsmQ6Xa21Svxw/XCe518Gswvm/Iyfguy/39LNX1P0qL1/eDrpW/czvvz53he//K+2Ox5GXMWRMFrJQep499sQxfC7rBSQAAAAAAAAAAAAAAAAAAAABAwIzUYVe/3N6U2Bdssz/rwpwiaPZnOdadUGnntj/Nal+iybm9uoWFYZU8EsxPBgAAAAAAAFC+rA8X6ejpUycHki4a25Q+Y3zUueVe0+6WkDVIi6qW3dm//rzUsRPBvxbN+L4pF+MLlJNoy24Vj0ScrzwaHVeGczOn0ic8N8gL+v1Y/9y9t/kWx04E9B5rcFgTrcIYV16fvRsbwjaeURXzMX3LTWTB5Nxe1tzcnOTVq3YuNTU97dxabH5+XnJ25ueSS111/l397wPlaOwVe/5/9ls/kQTCrHvHRcn/sO+oZNU7vZ/4X89Xv/F5yU8/EZPMtZrKX0lOfPUhSW32Z42Sn/nSpyT/fqRWciVfuj8j+Ynf+TNJ7cK5ByXveHSPZK5tq/mF5Nf/4ITklm3flAzaiaH9kp1/v1Nyeu5tkkAY3dO0UfLJR9skkR29Tj9/fkRypXV/0CoqKiS3btksuWHDekm/Jid/LHn+gv14glJVVSV5+84dkvr+ryao+6PHqa7uJkm3xo1XJA3jVUm3otGbJRuit0gWytSUPT8vXrokOTX1U8nZ2VnJoNTW1EjW1L5LUn9dW2sn3NGvG8zM2NtJH2f06wgzzvYL6nUDvb9WV99op/O1/j7b0Z+DBx+THBn1d56w54H77dzzgCQAAAAAAAAAAABQ/AyVam9TvcPZXWxiXfiQODmgAvtA/nRCbSqSZn9aR9+YCuZD/NMqEevO+kKfSKRDHc0kQ3ZxT3hI48jeYeer7DGuAAAAKAfpREx19btvMhCJ96iTA50ha7Din+fx6OhTmWBOCAEACI6L11jjHT1q/75O5a2njbvXM68V7xlUA4G9qJwL7l4nv1bgr5kHxUip9rakGvawvYJ/zZTxvVb+XpN2P+6luv5HqfF+TNHysx/6OfaF9L0rH8c+S3Dvsa6ANVGwPG7vsD5381pQ9m5wEgAAAAAAAAAAAAAAAAAAAAAQJGNIHT/r3M7G3v3B/XK+dZFA9zHnC/8i8bjq6OhQHT19qq9vSfWY3zf/LG7+Hb9Gxw3nll/Natde52YWFhb61Ym08wUAAAAAAAAAWKIxFY9EnC+WZzVe6ekbVANJr41tSp2hMm5eJ79W027VEsYxjbao3U3ObddGVWAvgwvGd7GgxxcoN1HV0OjcDDUfx769u8LX7M/i69in1LFcv9HJmggIRORfnvt/pDXi7OysfANA/rz2uv1E9qlvvCoJFIN7Nl6RPPSJpyS3bPumpFezP7NX+5/50qck/36kVjJfundclPziH31G8sK5ByUPfPU+yX9+fa1ktj62eUrysU9/RbLqnaOS9Z/4uuT03Nsk8+VL92ckP/E7fybp1yNf7pU8+L13SwLF4J6mjZJPPtomCW8mJn4oOZr5vmS+RKM3SzZEb5H0a8Y57zl16rTk/Py8pF9VVVWSW7dulqx2vl7NxYuXJM9fGJH0en923rZDsra2RtItv/ejtsb+ufrxV1ZWShba3Nyc5NTUtJ3TTk79VFL/eVD0ONTUvktSf11dbc+HiooKSbij5+PMjL3/6u04O/NzSb1fB7U99f5cXX2jnc7X9fXvlcTyDh58THJk1F7/erXngfvt3POAJAAAAAAAAAAAAFDsjFS7au0ddr5aXUffmArmA+wNlWpvU73D7j9F/1qReIdK7N+nOt1ekWGkVerIYXX82Fk1vODuPgT6Kf7phNrU1e98sbp4z6AaCKzjYmlxO5e1SKRDHc0kw3nhFAAAABCQdCKmuvrdn39F4j3q5ECnKrWzEM/jEeT5IAAAQbE+WKUtueLrnHHz+euQ+fzl//k8rRKxbtXv8vVUS+hf11xlDK8nzI/N62umlkAfF+P7Fvl5XO7fhynV9T9Kj5/9z5KX94Zcvgd4reDejw2en7HP+TGGNVGwPM5hq6li4uRAcB8iGBBeC8reDU4CAAAAAAAAAAAAAAAAAAAAAAJjqKHjZ53bq7MuwtgX0O+ypxP+mv1Zjf76BsdUZiDpvtmfJdqsOpMDaiBzUg32dah4xP6Q7Lxr3qU6XPzss8eHzK0GAAAAAAAAAI5og2p0bl5Lv4Y6EEhjm9JmDB331IzO0tgQ3tGNtuz2/Nr32Uxwr0Qzvm8V5PgC5SjasNwzX7ikTxxzbrljNSPcFeLeYn6OferscTWUy8MfayLkQFOs/GZN5Kmhp73/JgdQ4ioqKiSrq6okNf39quobJbU1lZWSlU5qa9Ys//3vDr8u+eE/OSkJFJP7b56R/Majn5L06i9Tj0h+4R9vkcy3b378f0r+7OdrJA/89y2S03Nvk/RqW80vJL/+Byckv3r8g5JHT2+QzLfJv/1zyap3jkq6deHcg5J3PLpHEigm9zRtlHzy0TZJ+DMzOyt5/vyI5Kzzda7V1tRIbt++TVKvx7yan5+XfOHUacmgHoe+Xzt37pBcuo5ciR7XU8790fcvW15/7lL65545c05yanpaMlv6fjREb5asr3+vZFjNzc1JzszY468f7+zMzyXdPv7VVDnbpdo5j9DbSX+/ttae5/BnasrZjs5+pfevGWe7et3fW5rvdW5hOQcPPiY5MuptvanteeB+O/c8IAkAAAAAAAAAAAAUt7RKxLpVf5YX2sV7BtVAEB/Fb6RUe1vS0wV+kUhc7U0cUskg7scihjLSR9SB7mOr3q+gP8U/nYiprv7sxsJ6/ImTAyrwh18CjFS7au0ddr7KnnXh1NFMUoX42ikgINZxbkgNncio46Ojanh48f5iHV+amswbjY1qd2yXamlpVtEiO9YYhvkYjSE1bj7GjBpV9q+JnDUf68rH2Hg8bt8wH3ejiqlduxrMxx2Vgg/mtkgPDakTGXNLWBvirLkdlnl+jZjjb087c/TNedfQElXNjP0bDCOtjKHxN8fRtHTf1d7Yh03XjqfMZ/vbZck+LhjmceHENceFlcfRsvS4EDOPCw3mODI3vbCeewzzucccf3PwR1c4JnMsyA835x3Xshq/nxzoLLljiefxCPh8MNzYh8sJa+mwYL+Dd9e+NmbNkb37D6mklw9KuS53rydfK7DXlnPE+2uLYX+91vs2C3IdyPi+VX7W2YZKtbv78KVSXf+jBKUTalNXv/OFe7l/b8jH8SH0553ujy3X6ugbM9cozhc5wJooQD7e08/1dnbP+7wN32PJPRr+AdehG5bQ8A94Kxr+XR8N/4DwoOFfsGj4d31eG+/R8K8waPhXmmj4Vxg0/AMAAAAAAAAAAACW4eJimOAurPP+y/R5ubjPSKvUkcOqt3/liw8Dv9DG5UVJ5XhRQTZo+BcgadR1RB0+Prpiky6L1dyhcfd+tS/kjeGMdEodOXx8xUYJFrtZQqPavX+f6gz8wq/rMcc6ZY/1ik28rPvWuFvt39epPN0187iWOHBY9V+nudX1xDt6vP/sHDPSaTV04rA6fmzleeqHXAxojv2ufS1F2Dxj9bn1xj7cae7Dzvf80Pua17l2rUi8QyV271ItAd23YmE1+Bs6csKc06s3IHZDmgHutY4jxTiX3bAa5OjGpuYYerzo+Hr0WO7e1ZLn5ws/8vBcs4j+ef63QbkeC3KFhn+LFU/DP/bhfJL1zAnz3ME8FbreGspcRBX98ypr6ethv0MRs5p3mls/d7td6Ta38bw2KILXFr0/tuBej2d83yov73eYRwQa/qFk+WhEZsn58aWkGqW9lddjnyUvawLWRMHwMY/D9zi9bbP8PF+HDw3/UBJ0Iz3dcE+rXPMOO5d8XzfWWNooJt8NNmj4h2L2pfszkp/4nT+T9KrQjeR048InXq2WzJV8/ZylPrZ5SvLoF/5PSb9+/VN/J3lu2j6+AsWAhn+5NZoZk5yYeE0y1/S6Tjf+89rYTtMN7vTjmJy012d+FXvjP21i4oeS48arkm7vj25k1xh7v2SxNrTLVQO5lazWGLC62s6l5zNwZ6XtOnfVbty8tOEjDf+uj4Z/gHcTExOSV65cldQNc9euXSuZb1euXJHU64AfOPfvqvP9bK1x7v/76uslC/24io2eF5cuXZbUXy9V74zv+vXrJPXXpebSZWccfmCPw0rjsZpyGS9Nj9tlZx69uX97G7+l1jnjuH7desnNmxslcX1Lt8vIiN1QfzWbN2+W5HgKAAAAAABQXNxcjBHYhS8um9tp+f5lesO8nwe6l2+2E/yFNu4uMMh/g4niQMO/gHi4eCfMF7t4uegsf/uYuwtu3Y+z+e8nDly3gakbYbk4y2qIdsRHA0M/7OaHxdCwxO3c8nMcNMz97IA6nKNGMZYwN50MhNVk9Uhux/Ba1rGkae9+dWhfc6ibtWbNatY8dEIdTy6/bsu18M/PXD/XvCnXx+eSPxbkgedGJDT8WyS/52Psw/njbqy1omrUYGItnQ32O+D6SrW5jbfnAUsxrJW8vm5qCea1cMZ3Jblv6uV+7Gn4h6IR8oZ/3t+zCu/7LYt4fL/XUhrvc5ZJwz8fjzN0zyeef0ehPN9HvsFJAAAAAAAAAAAAAAAAAAAAAEAgDDXu5vOy9u4K4BfZDZU6fMy57c7eo/m9uCXanFQDJ4+qjnjE+Y7NutAmFvj9iKpYk3MzG6Pj5kgCuWEMHXd9gdzCwrA6PhTGWZlWJ7wcco6dMP/PfDBU5qxzMwtuxtlqWtoeawus2Z/lbPJInsZlOYbzmGKqtbWrIA1KLMP9vaqrtVXF2ttVIm3eJ+f74eNubplPLGrc9YOxGv21q02bWlWXOc9y2WjNHvdNqj2RUuawlwyrwUuiPaY2mXMq12N4LetYMtzfZe5LRTymVpPElH1M2GQeE3p7+wvS7M+i52esPaFSoRzM3D3XvMGZy7k+Pr95LEiH+PgLBI19OH/crp9sZ48PFcExibW0O+x3QHny9jwgGhtC3xgt2uD9A7NH3b9gsAzGdyXBjC9QpoxMwV4Pyobh9cDXtFu1hP3AZ4nGVDyy+D3UrOXtPRj45/I97GudPa7C9NZh2tMbhqZAfkei+ESeGno6vEdYlJzKykrJNU5qVdU3SlZUVEhqtTU1zi1bxdvtP6+uqpIsdt8dfl3yw39yUhIoJiP/8WuS737f05J+/fqn/k7y3PQ7JBGMr33EPln5rT1/IenXI1/ulTz4vXdLAsXgnqaNkk8+2iaJ3JiampY8f2FEcm5uTjJX9LqxMfZ+ybq6myT9mpj4oeRo5vuSfnm9nzOzs5KnTp2WnJ+fl8yW/rk7d+6Q9Lt+1tvz/Hl7+05N29vbrQ3r10s2Ntrjoc8PSoXeD2ad7ae348zMzyX194Oix09vX31epc+jqqvt7y89z4I7ejuWynlorhw8+JjkyKibK5beas8D99u55wHJcnHp8mXJr3/tP0l69dDHf09y/bp1kvl25coVya993X4cV69clXTrox/9bcn6+npJv5559lnJZ5/5nqRb+n7o++XViy++ZOdLTjpfX726eJw2N9pvSD/88Gckc0Vvr2eetcflWWec9HooV9Y58/O2W2+VvPvuD0oGtb29+i//5b9KTkxMSLrldd7qeaDn6ciIfRxdOi+ytWbNGsnbbnPG9667JDdv9v6LDvmkj4dPDQ5J6v3lsvP9oOnx0uOjx0uPX1jpefqyM19GRux1uh6nXO/Hq6mvf6+k3s/vutse10I9P/ndv++7r0XS7bzQP+8Z5/kn6Pmsx1nfPz1/AQAAAAAAECbuPpG/o29MJf3+NrvXT87v6FMZ3z/cK6uh0QF1+NhZuXAo3jOoBnLQedBItavW3uwu8o5EOtTRTLIsLy64HjdjeC3GczGv45irfcMfd8c5LX9zwv39y2acvW7D1VgNTxMn89t8VY7BqSPqcPJYKC/etMZk79FD5vNjcc99t9vWahhzoLsw2yS8Y+6C1eDlwOGCNdtZTtxcax0y11phH1WrSeKRkI3dUvGOHrV/X6cKzxTNzXONzVApc50cZHPZbMlxyzwWdBbzsaAA0omY6up3f+yOxHvUyYHO0B8j3PI8Hnk9P2Ufzh/3Y20J9/7BWtob9jvg+rwdLy3hfO1I8/64CvvadZaMlGpvS3p6Pgjm8TG+K8n94zOfe9rbVO9w9vetVNf/KEEe33PUcjvX3e97WlEc94SPY7u1Ns37ew1BK9U10Vt5ff3EEp7HWj7bKyg3OAkAAAAAAAAAAAAAAAAAAAAACEL6RNa/1G5deBEL4PfYvXxyvjTeKuiFLVHVnBxQA5mMGhsby9kv9Ecb3Hw4zqgaN5ybAPAGq0Fpbpr9WRYWhlUmj8ceI51SifY21dXbH8oGJRZrTPq7WlWsPaHS5XBcthrVtcdUa1fhtoke8/ZEypzxxcZqutOuNrV2ha5h3XB/l2qLtatUSCeyfTww514Ix26p4f5e1d3WrhIlflCwGn+2x9oK0rDIYh0Leq1jQaocDr5A8NiHSx9r6fBhvwNQUNEG5fmj2UfHi/D8O88YX6AgjPFR55ZHjQ05bGxpqMxZ56ZLTUG8IZsXURVrcm6ipDXv2uvccu9s8ohKO7cLyUgd9ticskPtL8Nmf5bIU0NPh/PVBBRUZWWl5BontarqGyUrKioktdqaGueWreLt9p9XV1VJYnnfHX5d8sN/clISKAb3bLwi+eShfZIruXDuQcmz32+Q/K09fyG5kke+3Ct58HvvlkQwJv/2zyWr3rn8SdWPfvAhydTj/1rywP/va5Ir/X29Xe94dI8kUAzuadoo+eSjbZLIrfn5ecnz50ckL166JJlr9fXvkWyMbZL06+JF+36fv2A/Dv24/Nq6ZbNkXd1NkqvRP/eFU6clZ2dnJbOl1+2NsfdLZvtzVzM5+WPJ0cz3Jb2OT12dvX82RG+R1OchpW7G2Y6zM3ZenZuTnJ76qaT+86DmnZ4H+vyspvZdkvrr6mo7y2X8kRsHDz4mOTLq78X8PQ/cb+eeByTLxeOPf9vObz8h6VWhx29kxN7+B79ozwevgn4cQc3Pf/+FfydZX18vuZIrV+zz5qeeGpJ85tnvSV6+fFkyW6m/O+rcCsbS+zXo5NWrVyULbXOj/Tb8Rz/625KrjXNQJiYmJP/0Efv82au77vqg5Mcf+j3JpQo9/ve1tEju2WPvX2vXrpUstEvOfvH1r/0nSb/7aVDWrVsnqcfr7rvukiwUPV/0/HF7PAkLvZ989CP2fp6vedj5yW7nljf6+PTww5+RXErv3y++9JLk44/bz+f53k719e+V/NSn/kByvTOPAQAAAAAAUDhGKvumUNJ0L5NU/truefvk/LL51HwjpdrbkllfiN/RN6YK2gcxhNzM6WsFM79Lh9dxDOe+6u24k7854f7+rTzOVrO/NtXVn9vLb/Jz7LEeywHzsRSmEYZXVnPcvYlDKhmK/cDd3LLue+LkgLreXbcalBzoPhaqhjGRuLmvHjL31WJYJljNEg+Y22Q4POO3knhHnzpk7uihGFZz3FJHDhesMY5f4XhuDPK5xhK+Y3So5mzIpRMxT2uFSLxHnRzoLLkx9jwe5pzL5O1kjH04f9yPtSV8+wdraf/Y74Dr83a8tIRjfbwS748rv2sDrwyVam9TvR7OSYN5rmN8V5L7tYT7+1aq63+UHq/vZWg5Pb64fM/vWsX0/p/X82pL8b/PWapromX4mM+Wwj/eUl+H5MYNTgIAAAAAAAAAAAAAAAAAAAAAAmBkzjq3srB3l//GV+kTrn+Rvqw+NT/aoOyP98jO6Ljh3AIA68K63Df7ywurKVq79ViKq0GJZWFhWPX3tqpYe0qV1hHaalDSrlq7+kPV7M+yMNyvutsSKh3yAbeaJba3dRdFsz/LcH+XagvBPNbjVqzN/izD5jGhPVVCR4SQHqNlzibSzlcAVsQ+XPpYS4cP+x2A0IiqBjcvPl/rbKbEXufIBcYXKARX73MuoymWw/cfjYyn11KtRti5vFtBi8aanFvu8T5nEYl2qv17ndsenE0eUIV8idRIHfbW7M/6oKR95dnsz0LDvyJXWVkpVVtTs6jq698jFY3evKh23rZjUd1xx+1SLc33Lqq777pTaudO8+9dU42xTVIN0VsWVW2t+XOvqeqqKikApefBX3tNaiV/mXpE6o5H90h9/B+apD77F49Jzf6sUWqp3/pX/48UgvGxzVNSVe8clVrqxNB+qTv/9HelDn7v3VL3/fHDUhfOPSi11JZt35TaVvMLKQBYqqKiQmr79m12xe3S38+ViYnXpJ57/gWp+fl5Ka82bFgvpdfBet3t1/kLI1KTkz+WWo0et9vN+2BVlbnGtipbehzc/tzV1NXdJKXPG/T5htvtPDn5utQzzz4nderUaampqWmpUqXPl/Q46vMqPd/u/Y27pd44L3PO3/Q4b1hvzk+zsp0Peh5MTZvjapZhvCp1ZviclB7/7/zTM1J6O4xmxqT0vJmZnZUCgHJ15cpVqaWuXLki9fjj35ZK9HxO6vFvPyF1+fJlqUKZmJiQ+tNH/lxK36+rV69KhcXI6KjUG/fTGc9cW2m7unX5krmdzdJWmxf5Hv+nhoakDn7xMSl9/wrlmWeflfrTP/0zKb39w0Lvt1//+n+W+vJX/kYq3+Omx+kf/uG/ShX6eOLXs89+T0rvD/r4VKyW7t96vhRqO01M/FBK71fFPr4AAAAAAADFz1DjLl72DOKil/SJY86t7DUl9vlvNFg0osrNdTFnM1wIA8CWTsRUSTT7K7KmaCtZGO5Vbe0JVRrtMwyVCnnTmIUFp+mf83XYGKlwNktcjT2PC9hwxzweFOO4Ladkmv4ZqVAfoxf6u1Q7jYuAlbEPlz7W0uHDfgcgZLw3ZRpV9GRaHeML5Ju79zmX09iQu856huc716hyeLcCF/Xc7ZT3OYtN8y7vHf+sBu/JQr2Gb73O3Ovx/Y29+1W5fC7hcmj4BwAAAAAAAAAAAAAAAAAAAACBMVTmrHMzC/4vekkrt/3+rE/N391STr9FH1WurosZHS9cEx4AoWE1EyuFZn9GOqE2lUhzL8vCcL/qLvZGJUZaJdrbVG8RNI2Rpn+FbE63Amn25/ViwhAoXNM/c+51u28UHWZW07+i7qcjTaSSoT9GD/d3Ffc4A7nCPlzyWEuHEPsdAABAjrl7n3Mp6z3IAD7rbEWG1zvXFFNF9c5oNKbikYjzhUu8z1lcmvepnrjHbW2yX8PP93me99eZrWNEYl/5fCzhcmj4lycVFRVStTU1i6qubqNUNHrzotoe3ya187Ydi6ql+d5Fdfddd0rt3Gn++TXVGNsk1RC9ZVHV1po/95qqrqqSAoBs/ebd35XSZn/WKPU7n/uK1Bf+8RappY6e3iD1W//hgNSFcw9KaVu2fVNqW80vpODPfbe+JrXUI1/ulXrwa78mNT33Nint3PQ7pO54dI/UV7/xeamlPrzlshSA4jc/Py81NzcnFbQNG9ZL3XnH7VJ6HZwrs7OzUs88+5zUjHnbKq/0ejno+3/+wohTL0utRp9P3G6u9a2qMu+TVW7pnzuaGZPyS98vfb6hx0mf57g1NT0tderF01J6O05M/FAqV/M0rCorK6X0+Zse5+3bzfNFs/R46/NDfd7YGHu/VH39e6Synbf6eKC3w8TEa1J63jz//AtSQ+nvSJ06ZW4ns/Q81ttpasr8/80CgFL3zLPPSiV6Pif1+LefkLp69aqUXyMjo1JeTUxMSP3FwcekLl++LFUs9Hh+7ev/SapY6O32p4/8uVTQ88Iv/Xx98IuPSeXbf/kv/1Xq61//z1JhGZfVvPTSaSk9bleuXJHKtStXrkqVGr3d9fEpX+Ppl96/c3XcD4q+P8V2/AQAAAAAACg5xrjK9lX+QC56SZ9Q/W4vcm7arcqq358pGmtybmXhbIYLYYByl04UdTMxzWpQ0trV73xVOoq6UYmRUu1t3aq/CJr9adKcLkTdSnLZ7M9am8XjiytXCtL0z8u6sQgc6y7S44HVsKj7WNE0kTrW3a5SLJKBN7EPlzzW0iHEfgeUnyJpWBR19Wkzb1pYGFaZQj5AxhfAcly8z7m8RuX7s85WZKhxf3cOCKGo6ty/17ntTX7P86wPNOr2/DpzU+KQ6iyz31NYioZ/AAAAAAAAAAAAAAAAAAAAABAUI+PiwmP/F70YHq5sadrdosrt9+i9XhQIoAxZDdm6jzlfFLEcNyh5ozFaR4fqWFo5bpRmsS9gy3OzNL+k2V+yaBqUXGuhv0u1h6BbSdDN/uIdPaqnb1ANDo6psbExlckMqIGBxWV936rBwUHV19djzvHg5na+myl6WTdmw9rf9f7f09dnjtOS6rHGzTk2RCLO/xWchYV+dbjYuumYx+hN5jG6mI4HVrOM5JGibA8FBI99uPSxlg4f9jugPJXBB7OMjhfwETK+AJZhDB33t+ZqiuXwPUhDZc46N91qbCiu90ajDcrzO5t8sFnxad6neuL+Xjd9o+lfTje+oVJWsz+PH2gUifeoQ+Xe7c8UeWro6eI5s82j2poa55atoqJCsqr6RkltTWWlZKWTWm3t4v8fWM53h1+X/PCfnJQE/Kip/JXk9NzbJIN2z8Yrkk8e2if5L899XPK3+v43Sa8/90v3ZyQ/8Tt/JvnIl3slD37v3ZLwZvJv/1xyYmK75IGv3if5z6+vlXTrY5unJB/79Fckn/mfuyQf/NqvSeZKruc1ysM9TRsln3y0TRKLTU1NS54ZPifZEL1Zsr7+vZK5MjHxQ8nRzPclc60x9n7JoB7X+QsvS05O2us5v+rq7Hm6dcsHJLPl937o857t27dJ6vOeoMzNzUn+wNne+n7Oz89LerVh/Xo7N6xz0v466PtfqvR2mZmZtXPWzumpn0pedf5c/z2/qqqqJPX5sz6v1vOvutr+c7ZfcTt48DHJkVF/vxC654H77dzzgGS5ePzxb9v57SckvSr0+I2M2Nv/4Bft+eBV0I8jqPn5qT/4fcmnnhqS9Pvvrebhz35GcvNmd2/PTExMSP6F87ivXr0qWezuuuuDkh9/6PckgxLUvC1W+TpuPPPss5Jf//p/lix2+rxC76dr13p7vWU15TI/Nzfax7mHH7bHM2idn+x2bnmzZs0ayWI9nj700O9K3n3XXZIAAAAAAADIDzeNaKxfaj850OnrApN0Iqa6+rP/lXDrovLEyYHy++R858LwbJTtGF2H1wZLkUiHOppJqmbn63LndRzjPYNqIHQTMq0SsW7V7/LCv/zNCff3zx5npVLtbarX48VKXgV+3MlBYznrPu5N7Fa7WlpUc9TNHTWUkTbU0InD6vixs4E36Ih09KlMMp9HGXdz641tq4LZJvF4h9q9f5dqsbaBWYu3hDnWhvlfY0idOHxc9Q8H1xjPUvDnRxfP5dcTiZtzef8hta956fi5Y6RT6sjhpOeLG6/V0Tem8jGNg2qYaDVK3L2rRbV4HkPruDBkjl9w87Qw89Pjc03DEd9zWebx7v3mMdncBsseC8waGlcnjgczR6+Vr/labNyel2lBnBOGkefxyOvzOvtw/ng8dyjU/sFaOofY74Dr83a8tITztSPNagzj7bWWonlt0cdzh/9tx/heT273DfdjX6rrf5QWr+dzWk73u9AeD3KhDI7vKyrVNdEqAjoXlfPPo4fMc5Cgx8HcLn6a/RX6/YUQucFJAAAAAAAAAAAAAAAAAAAAAEBRSasTx5ybWWtUDeX4i/TRmIpHIs4XALA8I3XAc7M/62IlqwlWX1+fGhwcfLPMr/t6OlRHPO78zeUEeWxOq0SADUrkMQ2OqUxmQCU7O102KLFEVbS5WXUmB9RAJmOOR485FsEdjxf6u1Qi7XwRVj4v1IvEO1RP36AaGxtTAwNJ1WmO51ublFjs5iXNzZ0qOTBg/n1r/nUE9vy3sDCskkcKNdjmvO52vehZxNpHO3oG1UlzbKyLHf3uclEZZ2tO+x/jY90J8xGG1xvHN/NYIPMw2WnOQz9jaB0XnHlqHif7Oq53fMyONT+PDxnOVyGWOaLafczleIf1HGMek6153Nksx+TljwXmn5nHbGuOWscCa4yDOhYcO5xSRTDSQG6wD5cB1tKhw34HlICoanD3GfjXGFXj7ECrYHwBBMnL+46LNZblm5Bhw/G9KEU71aFEk/OFd9brpP1drardPNkLbBpY73FYTRg9vn9m2XuUZn9a5Kmhp4N51aFAqqqqJN9eUSFZ4WRV9Y2S2prKSslKJ7Xqavv/1/8f4MXM7Kzk/C/nJVcy6/y9X87bf+/sy9OS//vfBvupaSgv92y8Ivngr70m+eknYpJB+9L9Gcmf/fztkl/4x1skg/KxzVOS991qP46P/4P/hUg5uv/mGcm9v/6q5IH/vkVyeu5tkn7VVP5K8r91fVfyt/r+N8mg/v2l9Lx7PrNe8u9HaiUBN+5p2ij55KNtklhsaspej5x68bSkVltTI7l162bJpevooOh11PnzI5J6vZQrdXX2fGiMbZL0ex4wOfljyfMX7Pvv14b19vFOj3u29+/8hZclJydfl3RLb9/t27dJVjvnWUGbd9bBetx+MGE/78/NzUn6pcdvw4Z1Ttpfc77njd5eMzP2fjk1bR8v9Paamfm5ZFD7rd5Oev7V1L5LUn9ducaep7man/Dn4MHHJEdGRyW92vPA/XbueUCyXDz++Lft/PYTkl4VevxGRuztf/CL9nzwKujHEdT8XLNmjeTVq1clc+0jH/ltydb7WiRXc+WK/frAnz7y55KXL1+WLDVux2U1Qc3bYtfb+6jk+nX2OiooExMTknpelpr7Wux5+NGP2vMyVzo/2e3cKm3//gv/TrK+vl4yKOUyfitZ5+zXSWc/BwAAAAAAQH6kEzHV1Z/dr2hHOvpUJtnsfOWBhwZCkXiPOjnQuczFzaUurRLWhQhZjlVH35jys2lKjZFqV6297n/nORLpUEczScVQ2ryOY7xnUA2E7ioYd/uUlr854f7+xeNxNTzsYZ6b/9/e/YekgVg2DCOtho4cVr39b/4s388H13DzPHQ91vPF0UNWUxLnGwEz0il1oDuYZir5PdZ4m/teWI3+Evv3SWM1fwxzXhww50Uw164U4jnS77zO9Xy2xjjV3ua5YaglyOPAStw+DwU3B7PgszGmJf/rjvwdD6yGUfv3+ZzD5vNP6kC3r3mqsVZ+K6/HqVI9P/M8Hnk4Fr6JfTh/PJ47FGD/YC2da+x3wPV530fC+drRm/wcX8P+2Gzet10Q6x/Gd2W5XV+6fy2gVNf/KCHphNrU1e984V7O15Y+7l8xrsc8n1tH4ipxspibq5Xummh1/l9nvpa8vnso6WsuGOZ+d6D7mK/zz+LfLsG6wUkAAAAAAAAAAAAAAAAAAAAAgC+GGnfxOURNsQL8YntjAxeTAcAyvDT7i3f0qZMDAyrbZn+WaLRZdSYH1NjYmBoc7FN9fYPqZFBXGqYTvhuUWBcDdpj3KTOQy+Zo5jg0d6qBzEnV1xF3vuPdwkK/6k6kna9KQ7zHmlvJgBqtRVWzOecG+zpUPBJxvufdscMpc8WTRz7ntb2f5nY+W2PcOZDxN5+PHVapHA9stKHRuXV9VoOcvsEx8zgQ1BzMQtQ6JhxVHXHvc9Q6FpworUOBHJN7zGPyQDKAOWw9/wwEc9w9VmoDDeQI+3CRYS1dEtjvgHCKxpqcW+6dzeT1DLQoMb7FxlCGYVY6rdK6UgmVSGRZqdSb/5/175gFBMNQqcPHnNse7d2Vxw9iwEoWFoYVh/diZb3OfFR1BPA6vmVhuF/1tm5S7eY5n/spYX2QULtq7eqn2V/AIk8NPe3v1YcsVVVVSb69okJSq6l9l3PLpv9c/31tzZpKycpKO1FeZmZnJed/OS+5klnn7/1yfpW/N/NzyflV/t4bP3eVv+fV2Ctzkp/91k8kAS++dH9G8jfv/q7k5j/+uGTQaip/JTk99zbJXMnXz0FxGPmPX5N85qU7JD/+D95fdEL5uqdpo+STj7ZJYrGpqWnJUy+ellyqwlmfN0Rvlqyvf69kroxmxiQnJl6TzBV9vrFj+zZJv+cZet146pQ9jn7Xj/r+3b5zh6TeDqs5f+FlycnJ1yW9ijrbuyF6i2SuXbx4yc5Ldvq9/0vp8dywYZ2T6yWrl5x3wh99PJmbs89z9H6hz7+mpu0/D0ptTY1kVfWNknp76v25ttb+c+THwYOPSY6MurhyaRl7Hrjfzj0PSJaLxx//tp3ffkLSq0KP38iIvf0PftGeD14F/TiCmp/55nYcgppHYbdmzRrJZO+jkmvXrpX0Kqh5W+zua2mR/OhHf1syKLne//R8eF99veRS+drve535uH6dvd4MWrEex9zK1Tzs/GS3c6u85XqeAgAAAAAA4FruPnXf9y+4pxNqU1e/80V2yveX6tMqEetW/VlekMDFB4sZqXbV2uu+GVok0qGOZpJc4OXwOo7hnI/u9iktf3PC2/3LltVEYu/RQ64a/eWe/8dsPa7EyQGV7+nmdd+4Vv7ue+7nVk4fh5FS7W1JXxfoWTr6xsz573yRU/7GuxDHz3Qi5rlZUKSjT2VyObCrbH+r0d/+fblujrgaf9s852O4SI6PB/EeaVaZi83hZ55aWOO9ldcxzeV2LiTP48E+nJXi24e9jXV+9w//84G1dDbY73juxPV530dC/1qmh9exteLYf7xvu0DWP4zvinK7vnT3flA+xONx1di4W+3a1aKi5sl9iI8KCCOfrxvmY03pZ+2bv9czg1Nuj/dNJbwmylrw507WPprt+1pGOqUOdPt/H4H325d3g5MAAAAAAAAAAAAAAAAAAAAAAF8MlTnr3MxCY4O/X3A3xt1/WIjfn1m8oirGZ84CCIC+cDFczf6si/8O+7r4Sy7wLkCDEku0c0AN9sSdr7xZWBhWySNp56viJA1KMjneBtFONXB0r/OFd8dO5Ges/cxr66L+QlxM2Jw8qXriEecrl44dVinDuZ0L0Ra1e5n1kNXor29wTA0kC93sz9Kskn7m6LETqriPBLa41ZQih02ufM1T08JCv8rTYQAoSuzDxYe1dPGvpdnvgJCLxlQ84m0fKvn9Z3Rc+T4NZHxXFsT4FpHh4WHV39+rurpaVeumTSoWa1ftiZRKG+U0CvAqfcRnc6+m3aql4K8rLc96TT8W0vuWK6Pj7PfFrVklTx5VHR6f35djnff1m88PsfaESqVXmB9GWqUS7aq1q5dmfzkUeWro6UWjW1lZKbnGSa2q+kbJiooKSa22psa5Zat4u/3n1VVVkigO8/PzkjMzs5IrmZubk7zq5Er035u7+gvJlczM2j9P//xyM/aKPU6f/dZPJAEvRv7j1yTf/b6nJX/zwBHJf359rSRQjO7ZeEXyyUP7JH/0gw9Jbv7jj0sCbtzTtFHyyUfbJLHY1NS05KkXT0uuRq//t27dLKnPH4Km79f5CyOSen0ZNH1+sz2+TbK2dvH5jVv6fp4+c05y1lnvelXlnFfdvnOH5NLzsZVMTv5YUo+fV/rn6+2dr/M8fX5w8eIlJy/becn+Oih6PPW8rq19l2SNMw84r80NvZ/o8099Xjg99VNJfb4Z1H6v53G187qGPm7p7V5dbf95tvsXlnfw4GOSI6PuLya61l13fVDy7rvukgybzZsbnVvBevzxb9v57SckvdrzwP127nlAMt9GRuztf/CL9nzwKujHEdT8zLdsx+HKFfv8KdHzOcmrV69KBqW+/r2St916q2R9fb3k2rXLv+4wMmKvP34wMSH50kvZrTPdCmqeBDVvg7JmzRrJ9znjvFSu5rH+uYf/+kuSfj3z7LOSX//6f5YMin6eaL2vRVLPx9VMOPPxvzvH26Dnpb5fH3/o9ySD9uKLL0k+9dSQ5Jq1y8+TdevXSa5ft15SW+v8/WzHa6lLl+31+MQP7HF88SX7/uj7FdRxZ906+/4nex+VDErnJ7udW+GwuXH59USun6c+8pHfltT7DwAAAAAAAHLJ3aftd/SNqWSz84UH6URMdfVn/8v2uklVef5uvaFS7W2qdzi78eIihMWMVLtq7R12vsqeND3IJJWPaV5SvI5jOOeju+Odlr854e3+rSa0x1EjpdrbvF+QGZbH5XUf0fLzOHI0tzr61ElzUZCvTVAUY+1jXkvzxBw2fFmVn/tuNavxs0BcjXnfEgeSqt9cE8XjHWr/oX2qOVrgnX8Zbte5Wn6PZ7k5HuTted/vc0eu52qR8TxnC328yhHP45HXecU+HPaxztv+4XcsWUu7wH7Hcyeuz/s+Ev7XMt29NrtU+Pcf79sumOc7xncluV1P+Bv3fLOe65v27leH9jWrEL4MgILzv07z+55nNorj9aLg+Fnjh39tcD2lvCZyyTDH4oA5Fjl4rrE+BGb/Pv0BMIZKp46ow8ljns93NGt/23v0UOg+NCtMbnASAAAAAAAAAAAAAAAAAAAAAAAgMAsL/aq7vV21h60SKWU49xFwY+/RcF4UmD7ivemEJSyPK9o5oPo6Is5X7i0sDKvkkbTzVfGIOxfW53MTRDsPqZ64v7E+PpTbI6kxdNzTvJYLeA8VuHlWtFMdSjQ5X7h07ITK6Sw271tyIKPGxsbUwEAylM3+LM279jq33LHmZqaIn+Q7+vJ4UbI5F/Z7G2ZbrucqUITYh4sTa2lbsa6l2e+AYhFVDct/XnJ2jh1WqVCv86Mq5vEUMBiML1ZnPdcP93ep1tZNKtaeUGmDV8jxJiN12FezP+uDfnbR1zhwUV8Hd5SEaLNKDpz09Vr+Sob7e1WX+Zwg75vG2lRXb7//Zn/xDnX05ADN/lYRWTA5t5EHc3Nzklev2rmS2dlZyV/Oz0uuZHrqp86t5en/X/97CJexV+x58Nlv/UQScOOejVcknzy0T1L7y9Qjkl/4x1skgWL08Ad/JPmFP+yR1H7zwBHJf359rSSQjXuaNko++WibJBabmpqWPPXiaclsVVRUSDZEb5asr3+vZNDmnfXs+fMjkhcvXZLMlajzeBqi/p5H9f0ezYxJTk6+LumVHu+dO3dIVldVSa5Gb98zw+ck9f3yKqjx8Urf/4sX7XkwNW0/vosXL0v6fXxL6XGvramxs/ZdklXO+NfW2t9HbujtOTNjn8/q7a3Pq2dmfi4Z1PluZWWl5Bona5ztrfe3DRvWS2J5Bw8+JjkyOipZqtasWSP57//95yXXr1sn6dfjj3/bzm8/IenVngfut3PPA5L5NjJib/+DX7Tng1dBP46wz891zjzS66nNmzdL3n3XByXXrr3++U9Q80fT8/wPP/UHkps3+3tz6NJl+3n6K1/5G8mJiR9K+qXv5+G//pKkV0HNW7duvdVe1912262Seruvdly5csV+Peipp4Ykg9ru2sOf/Yyk3+0e9H730EO/K3n3XXdJ+vXMs89Kfv3r/1kyKH/9V38pudp+WypytX+n/u6ocysYnZ/sdm7lhz6u33arvX/ffbd9PK+vr5dcid6/n3n2e5L/8A//VTIo97W0SH70o78tCQAAAAAAgBwyUqq9LbsLxaUZzUl/F4WnEzHV1Z/9r4NbF9wczSRVeV5zY6hUe5vqHc5uvOI9ebxgvAgYqXbV2jvsfFUaIvEedXIgvw2hvI5jOOdjWiVi3a4vAszfccjb/bue0B4XXDz3LCd8j8vftgvi+fX6gp1bBR1/n3Mnt8dR7+Mcnjnt/TF09I2pZNlfJO1u7Xat/M2BYI8HVsOivF8M6+M4kPvjbXFxe26mFWJNmg+ex8Npgpsf7MP524e9jXVe9g/W0osU21qa/Q6lx/s+Er7j0TLSCbWpq9/5wr38rhPc8n7+EtjzHeO7rNyuJ7zfr7CQxkyHrA8DcL6BMuV/jZav5yHP55rF+v6oj2N7UawNVlTiayJPDHM6HDDnf3jfq7U+2OhQnj/YqFjd4CQAAAAAAAAAAAAAAAAAAAAAoGgYary0P5MrYFHV4O8zb1Bqzh5XQ4ZzG1iFddFzWC8SM4aOe2/YFulQ+0P3uJrVvkSTc9u9hYVhdbxIdm7rAriCzqtop9q/17ntRQ6Po0bqsKcLOsM1p73P5WMn0s6tchZVLbu9jd/ZTPE9wRekYZHFx3HAOt4W4VADOcE+XLxYSy9WTGtp9jugCEVjKh6JOF+4t9DfpRKhPVUyVMa5VTCMLzxYGO5XXa2bVLu58XmKKl9eX4fT8rcu5n1RN4rx9TFcT1Q1JwfUYF+Hr+f7XLAam/eY52cDNPvLWsk2/Jubm5Oampq+bk1M/FBq3HjlunXq1Onr1nPPvyA1lP7OdeuZZ5+TOvWi+f9dp0Yz35cyjFevW1PT5uO4Ts3OzkoBKD1tW38itdS//uAZKaCY/da/+n+klnrw116TAlB48/PzUnrdqtfFM+ba06qgVFRUSG3fvs2uuF36+0HT6+wzZ85J6cfplr5/W7d8QCoavVnKK30/3I5zbW2N1J133C5VVVUl5ZUeH31ec/HiJal80eNaV3eTlB7fe3/jbqk7zMdoVWPs/VIb1q+X0v+fW3rcL14yH6dZb8x357xNn+fp7TKaGZPS46LPS+GN3m56HjdEb5HS213P65bme6V23rZDSm//+vr3SNXWmP+/Wat543UE53z6jePBsHk8MAuwXL16VerypctSQDbWrVsnddddH5R66KHflertfVQq6dQffuoPpFrva5Fau3at1GqeefZ7Un6tWbNG6o8e/ozU5s2NUn6tNx+7VQ9/9jNS9fXvlfJL748vvviSVFjp7b/ngful9HbX2/vuu+6S0uO0Gj0v9ux5wC7n3w3KyMiIlFeXLl+WGhkdlfLrIx/5bSk9TkHR/57+94Py4kvmfDSrXOh5+9GPfEQqKCMj5vwxK+z0cVMf3//9F/6dlD6uf/Sjvy1VX18vtRq9f+vngU/9we9LBWViYkIKAAAAAAAAAPCmYmpkgMKKxHvUyWSz81XYpNWR5Fnntnt7jyZVGB9ZtPOQ6ol7v2jt7PGh0F+kLE0kQzCvmvclPF8gmLvjqKGGjnub102JfaGa09HO/arDy/geO2Hu3Yh67dY8Ol5UjQriPQVqWORo3uW98+foOGspgH24mLGWXk4xrKXZ74AiFW1RHnt6v+FYdyJ050pGOqUS7d2qf9h7s6xAML7wYbi/S7W1J1SKp6nyY6TUAR9rYkvYXo8DSlm0OakGTh71dc4XJOtDjU5mBlRnAc/PilHJNvwDAAAAAAAAAAAAAAAAAAAAAAAAvIpE4ipxqFOF9lKl9AnVv+DtgmerkeG+0F6JGVWd+7030VBnj6sw9/MMVRNJnw0BctIQxhhSXvr9RSIdan9n2PbWZuWtH8yooheMKRrz1pDybKZoGv5ZDYsGCj1vm3d5a0xpOpthoqK8sQ8XOdbSywv5Wpr9DihmPo9PpoWFftXdHpKmdEZapRLtqrWrNyTN6Bhf+LMw3K96Wzepdrr+lZX0kaQa9rgmtoTz9bgS4vW1MZS2aLPqHMiowb6Ogs2PSLxD9Q2OyYcacQRwz3fDv/n5eampqenr1sTED6XGjVeuW6dOnb5uPff8C1JD6e9ct5559jmpUy+a/991ajTzfSnDePW6NTVtPo7r1OzsrBQA5Mu//uAZqaW2bPum1LaaX0gBxUTPWz2Pl/rNu78rBSB89Lr4eXOtbpVe3wdtw4b1UnfecbtUbU2NVNAuXrok9YJ5DmLVjLnWt8qrhugtUtvj26QqKiqk3NLnX/r8aHLyx1KrqayslNLjFo3eLOXV3Nyc1Jnhc1L6/ujzv0KprqqSqq9/r9T27eZ4m3Xvb9wtdfddd0pt3bJZqr7+PVJ+55Ge/xMTr0npcdHnpd/5p2ek9Djp/ePiRXOemaXHE/7U1prb0Sy9/Rtjm6R27twh1dJ8r9Qd5j5gld4f9f6g54HX/RMAbr11h9RDD/2uVG/vo1JJpz7+0O9J3X3XXVLr162T8mpiYkLq8uXLUn599KO/LVVfXy8VtLVr10p96lN/IBWUkZFRqbBYZ25Tq/Q80Nt/z54HpPxu96X0v6t/rl9+x/PFF1+S8ks/ntb7WqRyRf/7QY1fUI+/2Gze3Cil12GlSs+TPQ/cL7X0+B708fO2226V2txojq9ZAAAAAAAAAACgMJoSh1SYr1dMnzjm3HJv7/4QNzK0+GiisbAwrI6HtEuJNPsbCNPY+20IE3xjNWPouKcLjZsS+1QY++40e+j4F+Y5nFfRBlXK75ZGOvoK37BIeG1MaRodL5rmikDQ2IeLH2vp5YV6Lc1+BxQ/H8cnzWpKJk3pCrYzGSqdaFebWrtUb/+w872QYHwRgOHeVpr+lYt0QnX1+2uoufdoMpSvxwHlINqcVAOZk6qvI+58J/esD8nq6BtUmQFz3w/1SXG4+W74BwAAAAAAAAAAAAAAAAAAAABAuBlqPDyfI4SQOJvhwkWsLDzNJFaSVl57lEQiHWpX6K/EbFb7Ek3ObffCuH9b4340VM3+HL4awvSrE2nni0AYauj4Wed29qwLDXe3hHR/jcZU3MP48hxliaqYp8PAqBoP+fBJ889keA7EXhpTihw0/QSKAftwKWAtfT2hXEuz3wElwt/xSZOmdG3tKpHPrnRGWqWsRnSbWlVXaBvRMb75FVUNJdqlXpr+JQJ9wQehk1aJbu8NsC3W+mxf6NfFQKmLqubkgBob7FM9eWj8ZzWIHz0xVMDGwKXhhlOnTqvr1VD6O9et7/zTM1KnXjT//nVqNPN9KcN49bo1NT193ZqdnZUCgHK1reYXUlu2fVNqJR/eclkKKCarzdt3v+9pqXs2XpECEF56ff/c8y9IzZhreKuCUllZKbVz5w6pxtj7pSoqKqSCos8/9PnR5OSPpbzasGG9lL7f+nG4NT8/L3X+woiU2/vVEL1F6o47bpeqqqqS8kqfr71xDuiM19SU+X2zwkKPd13dTVKNsU1Senu0NN8rpcdl65bNUvX175GqramRcjvP9PbS46T3jzPD56SeefY5qTfOr53xGzdekdLbN+j9qFxVm3PdKr0/6v1Bz4N7f+NuqbvvulNq523m980CAO3WW3dIPfTQ70r99V/9pdQffuoPpO6+6y6p9evWSeXKyyOjUn6tM++jVfp+55oel7vu+qCUXyOjI1KFVl//Xqlk76NS+RpP7bZbb5Xy6wcTE1JevfTiS1J+BfV4snXffS1Sfo2Y+6RV5Wpz42apUrN0/96z5wGptWvXSuXaXXebx0uz/Lp0+bIUAAAAAAAAAGCxpliYm7mhkKQxW4iaSSwrfUL1Lyw4X7jTlNiniuFazGjLbk+N0sSxEypslyXvPZoM6bh7bapmGw2ys5oxpDz0+zMn9W4V1n5/5kRWu72M7+g4zWBKtHlCKJt/emxMCZQj9uESwVr6+kK2lma/A0pLtHO/56bz17IazvR3tapYe0KlctV1xjBUOpVQ7bGY2tTapXqLoBEd45tfzfsSJft8MNzfpdpTvDJRqtKJbs/rYYv14RuJQ/lenxkq4+V1Q6AcRJtVZ/KQ6stD07/h/l7V1bpJGsOaT+Xw4AYnAQAAAAAAAAAAAAAAAAAAAAAAgLIX3sZsbzLGvX0YlXUx5u7QdkZbwmujNDGqguxD51ck3qP2hXZSRVWL94FWZzPBDbQxdFwNe7nYuLEhXA1gFvHYtO7scTXEBZOlqSkWvvkabVDeeiuG61gL5AX7cElgLb2akM0N9jugxDSr5NG9zm3/Fob7Va/VmC7WrhIpn41nzP/ZSKdUKtHuNKFrVV29/d7OUwuG8c2raKcaOHlUdcTf2mDJWjfEze9b1dHRoTp6+lRfX58aHBx0akyNja1Sb/zdQfl/+3rMf6fD+jfz02RwuLdVJcL2iRrwL51QXf3+9rumxCHVWSTLYqD0Wc+vVgPdNnPfzl/zXKsxbFubsz5wvofsRJ4aerqIVz9AcRt7ZU7ys9/6iSTKw8c2T0l+4N0/lXRra/Si5K6Ww5Ir+dEPPiT5Dye9/VbAzNz/R/Lg994tCWTj4Q/+SLK68v+VdOsjbfZZ/7vf97TkSk4M7Zc8b2yQdOvlH71L8u9HaiVR2u5p2ij55KNtklhsampa8tSLpyVzLRq9WbIheotk0GZmZyXPnx+RnHW+Dlp9/XskG2ObJL2an5+XPHPmnOTUtL09vPI7vuPGK5ITE69J6vvnV21NjaS+f7W19tfFTo/PzIw9z/T2m5uz1/lzV38h6Xe7rkSPa+Wad0hWV1VJVjlZKuOM4nDw4GOSI6Pefvml2Dz82c9Ibt4czMdHP/74t+389hOSXu154H479zwgmW8jI/b2P/hFez54FfTjCGp+fuoPfl/ytttulSy0r339P0k+++z3JL26r6VF8qMf/W3JfHnxxZckv/I3fyvpV+rvjjq33Alq3m5utI8HDz9sHx/yLSzjuf//+LTk1atXJb0K+ji7momJCck/feTPJf3667/6S8m1a9dKlouwPp91frLbueVNoffvS5cvS/b0fE7SL6/7NwAAAAAAAFwwUqq9LZnVBWrWxV6JkwO+LohJJ2KuLsoJ4mcWL0Ol2ttU73B249XRN6aSYe/2lUdGql219ubvIpF8yfd29jqO8Z5BNRC6HTetErFu1e/ygtxIpEMdzeSjmZ63+2eJdPSpTOgPAO6OadeyGs+dHOgMcXO0xdw+110rN/uOx7kf9nF3sYZZKsjH5nV7h/1528vxv7zXbW/yMifyN3aldDzw9rzCPH2T1+NXsT0vZ8vzeOR1HcY+XL7HS9bS2WAtvRqeO5EL3vYRSzhfO7o+P8eobETicdXU2KgaVUzFdjWoBuf7bxg/oU5kzBwdVaPqrBr28LwQhFwd4xhfW7E9d7tlGGllDJ1Qh48fy9kY89xVarw/12iF26+83/f8vS8RMD+v2RbF+xwrKa81kR9Wo78Dh3P3HJAt67li79FDKtnMk0U2bnASAAAAAAAAAAAAAAAAAAAAAIASZajMWecmYLIu8NpVrNc6IWfkAtZ9xTAxfBzTGhuK6iLn5l17nVvunc0Yzi2sKtqgPH8k29mMOSODYKhxD5/naO23sRK8jnBhYVgxhRF2zFOguJXvPsxaOhuspXOD507gTc3Jo6ojEnG+Ct7C8LAa7u9X/f29qrerS3UtrV7rz8yy/l4hm+Tk6LmF8XUU2XO3W9Fos2ruTKqBgYwaGxtUfT0dKh7wdreeu5IHUgG99oNCSyd8NvuzXj8/VKgmmlEVa3JuAuXOSKtEe0y1dvUXvNmfxXqu6O9qVbH2hErzhLEqGv4BeVBVVSVVW1OzqN5V8y4plJf/+9V3Sr3zxl9K/dvOL7iqXS2HpVbz7vc9LbXcv3G9+tcfPCP1f19YJwW48XcvbpT6YPwVqeXm2PVKz9vV6P1guX/jeqX3u78fqZUCkH+G8arUc8+/IDUzOysVlGpzzWXVnXfcLhWN3iwVtImJ16ROnTotNT8/L+VWRUWF1M6dO6Tq6jZKeaXH9/yFl6XcaojeIqXHz+/90aamp6VOvWiOl1l6+09O/liqWOntV1trru/N0uO3dcsHpPR2bWm+V+ruu+6U2nmb+X2z9PzcsH69lD5vyJYe18nJ16VGM9+X0uM8lP6O1DPPPid15sw5qXHjFamLFy9Jzc3NSQEArm/t2rVSYXH50mUpvzZvbpTKt6B/7sTEhFS5qn9fvVRQRkZGpbJ16fJlqatXr0r5le95WV9vjp9ZQZmY+KEUEIT169ZJAQAAAAAAAEHhomLAZl2UtvdoUtHvD2/RtFu1FMNVwMa48tAXTewttk6X0Zj3C5VHx7kYOWt+LpgdVeNBDLQxpI57ar7TqBpCvt9GG7y9Bz4ayMAC2Ygqj9MUQCiwD7vCWjo7rKVXwX4H+NeskicTgTcng8b4lp+o3fwvc1IN9gXb+G9huFcdSTtfoHilE6qr319jsKbEIdVZyl00gdAzzF25XW1q7VL9IWj0t9TCcL/qat2k2lNpzievg4Z/AAAAAAAAAAAAAAAAAAAAAJBnQTTfi3rozFO2jWNcXNBvNYOLccFSICKRDtU3NqbGQlaZzIBK0u0PRcwYOq6GF9xfzFWUx7dog/LcR+NshovKsua9YUlgDYWNjKd5rZpi5r0vTWfp1AwAQOBYS2eJtTSAfIh2qoGje2lKlyuMb5mKqmiz3fivryPufM+/Y90JRc+/ImakVHv3MecLbyLxHnWIbn9AwRjphGqPtamu/mHnO+E13Nul2trN5w1OKpdFwz+UtdqammWrvv49UtHozYtq65bNUjtv27FstTTfu2zdecftUjt3mn/vmnpPLCqF8jI99zapTz8Rk+p+5K+kZn/WKFUoX/3G56XueHSP1Lnpd0gBbuj53frXH5L6y9QjUoWi9yu9n+n9DsCbKt5eIZVvs7OzUs8//4LUuPGKVNAaordI6fVaZWWlVFCmpqelnnn2OakZ8zFZ5dXWLR9wyl53ejU5+brUc+bYWjU/Py+VLT1O+v7cfdedUnV1G6X80tv//IURqe/80zNSo5kxqbm5OalSo8e1ttY87zBLz8/t27dJ6fMGfR6h562eD/o8RZ+3VFSY+69Zq9HjefHSJSnDeFXqzPA5KT1/h9LfkTp16rSU3h4TEz+Umpoy57tZAIBwGBkdlfJr/fp1Uvm2du1aqTVr1kj5deXKValytX6duR3NKpTLly5L+RXUfCi0K1euSAEAAAAAAAAoU34upPYg6rUzDwCgqBmZs84ttxpVQ9FdwhBVHvrbOkZVufa59cJLI2EtiIbCxrjH34NobAh/w79ojAYLAACEBGvpbLGWBpAnVmOyk4myPWdqynWvCcbXuVWOoubmH1CDfR2BbP+FhX51OMXioDgZKnUg6e2DNhzWhzsdHegM/2twQEky9+FEu2rt6ve1H18r3tGnBgfHzOeIHtURz80aYWG4X3W3tasEXf/egoZ/AAAAAAAAAAAAAAAAAAAAAFAAQTTHcevs8SFVlr9Wb2RcXARRjBfxAygfhvLaF001xYrwosyoor9tftBIGChvfpp+Aig89uFssZZGcNjvgABFO9XAyaM5aziTa5F4R7jvO+Nb1qIBNn08mzyi0s7twBhplUokVCJUlVJpo3TeSUsn2lTvsL8mYXuPJlWzcxt55Oq9zcXKu+FpCTFSqj1m7sP9w843/Il39Ki+wTE1kGxWUXOKRJs7VXIgk7PGfwsLw6q/q1W1J9Ll+fsJK4g8NfR0MK0bgQBUVlZKrnFyqZradzm3FquuqpKsqKiQ1Nassf8d/e+GzXeHX5f88J+clER521bzC8mv/8EJyS3bvimZK7M/s1+p/cyXPiX59yO1kkAu3H/zjORj+/6b5Lvf97Rkrlw496DkQ3+zS/Lc9DskUZ7uadoo+eSjbZJY3szsrGRm9PuSU9PTkvlW5azrtm7dLKnXeUGZn5+XHDdekZyYeE0yaFu32Pe/ru4mSa/0djl16rSkvv9u6fXw9u3bJP2O69zcnKQex8lJe10bND0f3l1n78cbNqyXDOv6vlD0vJiZseeL3n/1dpqZ+bnkrDOfgqK3g55PVdU3Suqvq6vtZHuVh4MHH5McGfX6GzC2devWSa53MmzWrF0j+fGHfk9y7dq1kn49/vi37fz2E5Je7Xngfjv3PCCZbyMj9vY/+EV7PngV9OMIan4+/NnPSG7eHI7ffOr8ZLdzy5/U3x11bhVGUNvnoYd+V/Luu+6SzFZQ83Zzoz0vHn7YnieFkuj5nOTly5clvXI731988SXJr/zN30p6VehxDGo+Fup4rOfzyMiI5CVnHly+5G8+ZOuNn+dz/gU9fn6Pl2HZv0vluA8AAAAAAFAeDJVqz/7CmXjPoBro9HHBhfVL/m1JVxd7RCId6mim/C7MMVLtqrU3u4shynWMrsfN+F2LsVzM6zj6PlbkRFolYt2q3+XFZvmbEx7vX7xHnRzoDHkjD3fPNdeKdPSpTLL49kiv+46lo29MBfuQS3hupRNqU1e/84U7QcytdCKmuvrdz+t4T5861BLuvVYZQ+pAt7s1m6U4jkm55WVeRCJxlTg5oHL/1FlaxwOvx9rgj7PFyesxrFT3c8/jkde1CvuwJT/7cJjGmrW0G6ylr4/nTgTP2z5iCedrR14Y5jrigLmO8HbcyjerEV1i/z7V2Rz1vP7J7zGB8S1rPl73uVagYxrQfcqVUpg/ftaCWnieY3ys5fP2WlHAfOwjxb02YE1kCWL/1azn1KOHksp8Sr0uw5xzB7qPeW40eT2l+hqYFzc4CQAAAAAAAAAAAAAAAAAAAADIo7MZn59lH21Q7j+uaFSN8xH619cU42IDACFmqMxZ56ZLTbHiPLpFG9w/28GDaEzFIxHni3wz1LjHz2Yb7u1Sra2t4a6u3pxcJBkehjIMQ6XTaZVOJVQiYVZ7u2rXFYupTZs2eSovDR0AAFgZa2kACLeoak4OqMG+HtURL9T56eqspjk9fYMqM5CUZnTFg/Eta81JNdgTd77w7tiJtHPLL0OlDh9zbodTcI+1QNIJ383CrAZdh0qkeRpQPKwGvcE0+7v2OTWbp9So+VwxkDmp+no6An+vYGG4V7W1J1SRH1kDEXlq6Gle9caqKisrJdc4qVVUVEhWVd8ouVRtTY1za7Hq6ipJ/f+Xq+8Ovy754T85KQlc60v3ZyQ/8Tt/JhmUC+celHzob3ZJnpt+hySQDzWVv5JMfeyU5K6Ww5JB+eo3Pi/56SdikoDlnqaNkk8+2iaJ7Fy8eElyNPN9ybm5Ocl8i0ZvlmyI3iIZNP04z18YkZyfn5cMSl2dPf8aY5skva5/9f164dRpydnZWUm39M9vjL1fsq7uJkm/9P37wcQPJScn7XVuruZNVZV9PvFuZ3w3bFgvqc9bcH0zzvyZnbHzqrOdpqd+Kqn/POj9QZ8f6vNHfX6pt2dt7fLnjygOBw8+Jjky6vG3bh17Hrjfzj0PSJaLxx//tp3ffkLSq0KP38iIvf0PftGeD14F/TiCmp8Pf/Yzkps3F/YXny5dvizZ0/M5Sb9Sf3fUuVUYhT5+BDVvNzfa8+Lhh+15UihBjedHPvLbkq33tUiuJqjjWKnI9fFYz9tnnn1W8tlnvydZKoIev85Pdju3vAnL/u33cWiFPu4DAAAAAACUi3QilnVjkkhHn8okm52vvDBUqr1N9Q67+5XweM+gGiizC3Tyu11Kj5HydmFJJNKhjmaSitG0eR3HcO6zaZWIdat+lw2s8jcnPN6/eI86OdAZ7qafRkq1tyU9NQ8r2uN/OqE2dfU7X7gT/GMu4bnl8bFZ/D8+b2uaUhfOeWMoI22oofETKnN8VPUP+7/wNGiRSFwlTg6o3B/uSut44HWd0tE3plg6W09V2Z9vXKs4nh/c8zweeT0XYx+25GcfDtFYs5Z2hbX09fHcieB5Pycr2mPUKgzzGHbg8DE1HIJzRes8o2nvfnVoX7OKvmWovZ3T5u/cZXmMbzny//pLcK/vej/m5UtRv7/hY92rhfHxez7XLNbjgY+1fHGvOct5TRTc6+Txnj51qNN8XnW+ds9qPHjA3OeCff2X946VusFJAAAAAAAAAAAAAAAAAAAAAEA+jY4rw7npTVQ1ePi8orMZfz+1+Bhq3MXn5TTFuAIQQGlqbCjS41s0puKRiPMFSpOhMmedmwgfw1DpVEK1t8fUpk2tqrWrS/X29oey2R8AALnCWhoA8i/anFQDAxk1ONinOuJx57v5FY93qJ6+QZXJDKiB5HLN6PxoVIV8emF8y1FUdR5K+FobLCz0qxNp5ws/jHHl4m0buJJWCd/N/qwGeeXdkAvIv2Ca/UXM59a+wTE14KvZnyWqmpMDakzWCcGdU1rPI93tCfNIVb5o+FfkqqqqpGprahZVXd1GqWj05mVr5207lq17f+NuqZbmexfV3XfdKbVzp/n3rqnt27dJNURvWbZqa837s0xVVFRIAVjZp5+ISXU/8ldSfp0Y2i91x6N7pM5Nv0MKyKfpubdJPfi1X5P66jc+L+XXI1/uldL7DQD/NmxYL3XnHbdL6XVkvtdxhvGq1HPPvyA1MzsrFRT9OPV6d8N682uzgjI5+brUC6dOS83NzUm5pcf9dnMNbpVe77s1Pz8vdf7CiFMvS+nve6Xvnz4P0OO5PW6eL5il76/+e37NmnPAqtHM96WeefY5KT1Pxo1XpIKeL6Wi2jyHtKqu7iYpvd30eZ4+L9Slzxf1cUBvT30+mq2p6WmpiYnXpPT2O/Xiaamh9Hek9PY8Ze4zVuntefHiJSm2K4Byd/nSZSm/1qxZIwUs5+qVK1IIhyvmtrDqy1/5G6mDX3xM6tlnvycFAAAAAAAAIHyisSbnVhbOZnw2/FOqedde55YLxw6rVFn1/HPXRKhoL+IHUB6MjK+LNQEgO06Tv1hMbWptVV29/WrY58WmAAAUHGtpAChK0WizSg4MqLGxQTXY16M6OuI5a2YaicfNf79H9Q0Omj9vTA0MJFVn82qvFxd3E3vGt8xEO9V+D28rXetYIB3/isGoGi+699KshmHdqt/nmnfv0QHVWUJvlS0sDKti/Cw0w82nmaHIBdPsL97Rp06az62rPrW6IeuEjLlG6AhsfbAwbDX9S5mPujzR8A8AAAAAAAAAAAAAAAAAAAAAAhJtaHRuZSOAi4WiMde/XG9d2HJ8qIx+hd4YN0c6O5FIXMXo9wcAJeNsMV7JWTBR5aZvcaBcPFcjx4y0SiXa1aZNTpM/miIBAFC2WEsDCJ+oijZ3qmRyQA1kMnaDusE+1ddjNanrUB3xuIpbtcrr5fJ3rLL+H6v5XF+f+e/YDegyAwPmv9+pmqN5eJG4KWY+ojBhfMuFpw+SutboeNk2aQq7dCKAhmE9gyrZ7HwRMq4+dK2M8V5n8fG771rbvKNvUA2YO2+uNn20OakGTh5VPfGgmv71qrZEuTSQXYyGfzlSVVUlVVtTs6jq6jZKRaM3L6rG2Puldt62Y9lqab532brzjtuldu40/941tXXLB6QaorcsW7W15v1ZpioqKqQAAMiHd974CykA4aXXh3odqdefG9avl8qX2dlZqeeff0FqNDMmNT8/L+WXfpzbt2+T2rpls5T+vl/6/j9n3nerLl68JOWWvj96va/PI7yanHxd6oVTp6VmzPtoVVA2bDDniVn6/t77G3dLbY+b42yWPj8KepwN41UpPV++80/PSJ05c05qcvLHUnNzc1JYnt4u+nxRHwf09tTHA31+eod52yq9/+jzXX0+nO121ttlanpaSm/PM8Pm9jNLb9eh9HekTplz16rzF16WGjdekZqaMv9/s4I6TgBAqXlffb0UgPCamJiQSvR8Tuqll05LAQAAAAAAACgCLhrwWY33fF87HW1Ruz1c33L2+FDZXJBlDB130aymUTVwEQyAElTUF/lFG8yjM1CGGhvy15zAMFTaavTX2qV6+4edbwIAAAtraQAIq6iKRptVc6fVpC6pkgMDasAqaVY3tmLJ37HK+n+s5nPNzea/4+NA77WJfT7P+TxhfEtW8y7V4fKDpBY5m6HhXwilEzHV1e+v2V+ko08NdJbmnjPq+xPY8s/InHVuoZQZqXZf+24k0qGOnhxQyeY87LvmuqBzIKMGe+LON/xZ6O9S5djzj4Z/AAAAAAAAAAAAAAAAAAAAABAUlxdS+7/AJKpaPHT8sz41/0iZ/AK9qwtimmJcBAgAAEKhKU+dhYx0QrW3takuGv0BAAAAgHtGxsUHzrwpX+d8RY/xzYFmtWuvc9OTUeX/ra1iaEpcPB8Q5bdhmCUS71Enk83OV+EUbaCVdXb4cLOikU6o1l7vr8lG4h3qaCap8tHr71rRzgE12NeR9YcgXs+x7oQqt55/Zdfwr7KyUqq2pmZRbVi/XioavXnZ2nnbjmWrpfneZevOO26X2rnT/HvX1NYtH5BqiN6yqOrr3ytVW2ven2UKQPm679bXpPza1XJYqqbyV1JAGPzmh74l5Vfbr41KAcg9vZ7evn2blF4XV1VVSeXLxMRrUs89/4LU1NS0VFDq6m6S0ut6fd7g1/z8vNSZ4XNS48YrUl7p8wi9HSoqKqTcmp2dlXreHEur/N6v1WzYYJ5/maXPj+79jbul9OOor3+PVFDzSo/7xUuXpM5fGJF65tnnFtX5Cy9LTUz8UGrGHBOrkJ1qc1tZpfcffb6rz4f1dl66vfV5d13dRim3231q2tz/zZqcfF3KMF6VOvXiaanv/NMzi+rUKfP7Zul5Pjn5Y6mgjyMAAKB81dfXS3l15coVqb84+JjU1atXpQAAAAAAAAAUk6iKuei/dzbj96oo8ye27Pb0C/Xh+AV6Q6US7WrTpk0q1p4yvwqaocbd/HpZYwMN/wAAQMFFIh1qf2euVyX2Oqy1q99T8wQAAAAAgHlm5eoF6Dc10o0pK4xvbjT76Pi3sDCs/L+15e69tIIokg+Ispr9+WkYZpGmYQOdJf3+WBDvx+aXy/c3r8WHmxWJtEp0H3Nuu2e9fnx0IKkK1aYz2pxUAycTvpv+LSz0q8OpYts//Sm7hn8AAAAAAAAAAAAAAAAAAAAAkDtR1dDo3MzGsRP+m+5FW9RuDxdGWb9A350oYMs/I6XaY22qt9+5EOnscTUU9O/zG0Pq+FnndhaaYlwCAwAACisSiau9R3N8saaRVon2a9ZhAAAAAABPjIyLF6Ad1nkfL0Vnh/HNkWjMd5Mmf6KqxcsbW3nUtLsl9E3TjHTCf7O/AjcNc6Xg8xYIjpE6rPo9fgiL9TyXOBmC/TbaGUjTv7PJA6qcev6FpuFfZWWlVG1NzaLasH69VDR687K187Ydy9a9v3G3VEvzvYvq7rvulNq50/x719T27dukGqK3LFu1teb9WaYAINd+80PfklrN7M8apVbzkc3/SwoopI9tnpKqeueo1Eqynde/fufXpGoqfyUFIH/0uvjOO26Xaoy9X6qiokIq1+bm5qROvXhaajQzJjU/Py/llz5P0ecN+vEFxTBelTp1yrz/Znm933o76PtZVVUl5ZW+X889/4LU1NS0VK7px9EY2ySl55U+v9seN8/bzKqvf4+U38ep6Xk0Ofm61Gjm+1LPm4/dqu/80zNSejuNG69IXbx4SSqo+VYu9PFBb2993r11ywek9HbX5/F3mLet2rpls5R+PUC/bpDt8UZvp6lpcz6bpef5+QsjUvo4MpT+jpSe/2fOnJMCAABYzZo1a6Q2b26U8uprX/9PUlevXpUCAAAAAAAAUJyiMTcXKY2qcd+/wB5Vnfv3OrfdWejvUoXo+WddhNTellTD11zIsLAwrI4H3fHPyCz6GddjXRyxu4WrAAGUJusYmynWC6aMcfPZ0psmru5GEbHWIvGOPnXy5IBK5vJqTavZX1u36h/2dkEpAFyfoca9PnEDIcVaGgCwMq9rn0bVwGE2C4xvzkQbzFHybtT/G1sq2nlI9XXEna/CxXp95lBnyCeR1eyvq9/5wpvQNA3Lh9Fx84hSTAzlod+prbEh9M0qy56RUgeSXjewUnuPDqjQHKICaPqXk98RCLHQNPwDAAAAAAAAAAAAAAAAAAAAgFIQbcj+MqnAfoG9eZfq8PiL9Me62/P4qfmGSifa5SKkbBvx+ZE+ccy5lQ0uAgQAlDMfF5H65eMi83jPoBobGyuJymQG1ECyWUVzuh5xmv3lYB0Wj8dVR0eP6unrU319g2pw0KrlH2u21dfh/UJRoCiczRRZwwEAAAAs5vFceu+u8miw5RvjW9qiqjk5sOzrAYUueX3GuZehZH2oVreb97/eym72F6KmYdnw06iy2M6/adxd0tJHFn8onhuRjr7cfliMF1bTv6PePpxQO5s8ogrwGYUFsWrDv4qKCqnampplKxq9ednaHt8mtfO2HYvq3t+4W6ql+d5Fdfddd0rt3Gn+vWtq+3bz3zGrIXrLslVba96PZUrfb8AyPz8vNTU1nVVdvHhJatx4xVWdOnXaVY28MCwFLOdjm6ekqt45KrXU7M8apbof+Sup+/74YakL5x6UWknr7a9IAYV0362vSa3kX577uJSe17/zua9I/egHH5JayUc2/y8pAIVTX/9eKb2+r69/j1S+TEy8JvXc8y9I6fVdUPTju+OO26Wqqqqk/JqaNu+nWfp+z8zOSrlVbd4Xq243z6WsqqvbKOXVrHkfrDr1orl+Nev8hZel5ubmpPJFn99t2LBeqjG2SepOcxtYpc8z9XmnPi/V561+zw/fOJ9wtpNhvCp1Zvic1Hf+6RmpZ559TurMGfP7ZunzBH1+ke9xKxV6XtfV3SSlXw/Qrxvo7f/G6wpL5sGG9ea8Mcvt/qrn/8VL5vYzCwBKzaXLl6UA+Ldu3TqpP3r4M1Jr166VcmtkZFTqpZdOSwVNn8/ceusOqT0P3B9obW5slAIAAAAAAADgcNl872wmiEtMmtW+RJNz2x2r6WCyLQ9N/4y0SrS3qa7+lX9/Npix0NLKVb8/LgIEAMCbxoZwXwQNh6FS7cE1+7MuDO/o6VF9TlO/gYEBlUx2qs7mZtXcHFXRqFXOXwZKXDTm7VwMizXRlAAAABQbjw2ZWPdkifHNoajiNKZIGSlp9ufnQ7Ws13T2Hi2yZn+CeZuNRj7dLORcvn99jUikQx0NXbc/R3PS14e3LCz0qxNl0vFv1YZ/AAAAAAAAAAAAAAAAAAAAAAA3XF5wcuxEIJ9YH+3c76rR4LWspn+9rZtUe066/hkqnUqo9rZu1T8cTIOZrKRPuGpow0WAAIpCNKbiHo/1o+O57uyaI0bG8wWsXNxY+oJtFlzajNQB1RvAWiwe71B9g4MqkxlQyc5O1cxuBvgwqor16XllhsqcdW4CYcNa2hXW0gCwOmPouKfjLMfY7DC+wBJWs7+2pK9mfxar2V9Ye4blTpGdf3tcx1vNHHm7M9yM1GHPH8jSlNgX6g+va96X8HzObTlWJh3/Iv/rf03JDFizplK+UVlpJ3Ctubk5yatX7VzNG3/fydXovz939ReSq9H/rv7/itXYK/b9/+y3fiIJXOtrH7Hf2fitPX8hqV0496DkQ3+zS/Lc9Dskl/rS/RnJT/zOn0lqsz9rlKz7/X8nCRTC5N/+uWTVOxd/rsJfph6R/MI/3iK5VE3lryRTHzsluavlsKR2Ymi/5INf+zVJwHJP00bJJx9tk0Rh6HXb+fMjklPT05L5Ul//HsmGqH18qaiokAzKuPGKpGG8KhmUrVs2S9bV3STp1cWLlyTPX7DHf35+XtIrPX56XN9X/17JoMc1aHoezszM2jlr5/TUTyXzdZ6hx6m6qkqyqvpGSf21Pi+vra2RRG5MTdnHoTfmhTMfZmd+Lqm/1vtLS/O9kljewYOPSY6MevncrDfteeB+O/c8IFkuHn/823Z++wlJrwo9fiMj9vY/+EV7PngV9OMIan4+/NnPSG7ebJ9XF8qly5cle3o+J+lX6u+OOrcKo9DHj6Dm7eZGe148/LA9TwolqPH8yEd+W7L1vhbJ1QR1HKt31pUf/chHJMNu3fp1kuvX2enX177+nySfffZ7kn7pefnQx39PMqj7uZKwPp91frLbueVNWPZvv49DK/RxHwAAAAAAoNwYqXbV2jvsfLW6jr6xYC6uSSfUpq5+5wtvIvEOldi/T3X67t5iKCN9RB3oPpb1RSmRjj6VCegqo3Qiprr6s/y5kbhKnBxQnVwEsyy381mLRDrU0Uwy1Bef5JPXcYz3DKqB0E3OtErEul1flJS/OeHx/sV71MmBThXqQ4GPizrDOZey4PG5LTfHdubWcvzPLUOl2ts8NaMLcu1Q0gK4IDy4NWL23KzntPyt60rreOB1nRLYeUyx83EeVnJj6ON4k9+xYB+25GfMQzTWrKWzxlp6dTx3Inje9hFL0R6jip63c1leM80W45tb3l+LsXDcKYAAXtuxdPQNmmuR4t12Xl4r0oppHVbe78uV8prIx+vgRbJt/eyj5fIcfoOTAAAAAAAAAAAAAAAAAAAAAICARFt2u/oE+8A+sb45qfo6vH9yvmVhuF/1drWqWHu7SqTSynC+nzXDUOlEu2qPtanWrn5XFx81xYK6CCOtThxzbmajabdq4do0ACXubMb1ET0UjHF/H3qGLBkZ3xcMI7zSR/xdEB7v6FMnB5J5bfYHlIvR8eJ8fl6Rx+cTq7lYYKeDQA6wlgYAvJWhMmedm240xcL9gQChwfgCbzDSKhFAsz+rGVoxN/uzRGNNzi33iuf821BDx70cAE0cA8PNGFKeN21iX1E0wmvel3D1OxKLjapSe5lsOTfU1tYoqyorK6WQW1NT065q3HglqxrNjEmdOnU6q3ru+RekhtLfyaqeefY5qVMvmv9/FnX+woiUYbyaVU1Ovi41NW0+7ixqbm5OCihlv/mhb0lpX/3G56XueHSP1Lnpd0it5NNPxKR+53NfkZr9WaNU1TtHpT62eUoKyCc97/Q81PNSz9Mv/OMtUiuZnnub1INf+zWpR77cK6XtajksVVP5KykA4aHPN3bu3CG1Pb5NKl/nIRMTr0npdfDFi5ekgtIQvUXqjjtulwrqcel19fkLL0vNz89LubVhw3qpu++6U2rDevNrs7zS90Ov5/X5gj4/8Xo/c01vFz0eervpeanH597fuFtq523m981qjL1fqq5uo1RtjXkebZZXenz0+Y2en3p76/OqpedjZ86ck9LjrOfxzOysFNzRr4fU1d0k1RjbJKXng54Hel4AQFisX7dOCovV19dLlbuR0VEpv95njqVV+bZ2zVqpzZsbi6KC3h9ffPElKb/q698r9fDDn5HiuAEAAAAAAAAUULRBNTo3s3LshAqo5Z9qTh5VHZ5/kf5NC8PDqr+3S7Vu2qRisXbV3p5QiURCpdJplV5UKfl+or1dbTL/7qbWVtXVP1zYpkHpE6rfxc9v2t3CBTAAioPb55drjY67b+IaAoanq7stjaqBg3vW/DSDafQ90FHV4HViF+m8zisjpQ67aYS8hNXsbyDZzFoJuJ5ozPPFzMXaRGwlNBdDqLGWdoG1NACsyuVr0BqvRWeJ8QUcVrO/bk/7w7WsZn8DncW/d0Q9v4hYTOffHhueWhobOAaGmY8P3fH/GnyeRFvUbo99ORcWhlWJvUy2rBucBAAAAAAAAAAAAAAAAAAAAAAEplnt2uvczMLCQr86nArqN9ibVfKkn0/PfyvrF+yHh/tVf3+/6u3qUl2Lqle+3z887PxtbyKRuNrdEsTFCoZKuehqE9zPBXKj1JrAwK+oinm8WMqcTEXYpMRQnvsGNcW4uNEFr81grOfRWCEHuijndX6ljyQ9X0gacZr9AVhFGTYRWwnNxRBurKWzxloaAFaVPuG+szqvRWeP8c01Hw3FTEXTeKropVWinWZ/i/houF8059/GuPK+jGffDDMvz22WSKRD7Sqal2ijqsVrxz/T6Hjpv9pftA3/pqams6qLFy9JjRuvZFXnL7wsderU6azqO//0jNRQ+jtZ1akXzf/PRRnGq1nVxMRrUlPT5uPOomZnZ6UAhM/HNk9Jab/zua9IffqJmJRbT7xaLbXl3/6x1L8893Gp+259TQrIJz3v9DzU81LPU7cOfu/dUr954IjUhXMPSn1k8/+SAhBeGzasl7r7rjulotGbpSoqKqRyZW5uTurM8Dm7ztg1Pz8v5Vd1VZXUnXfcLlVf/x4pvyYnX5d6wTwHsUo/Drf0+G7fvk1q5207pCorK6W80uOnz0/0eZI+v/J6fwtFj1NtbY1Uff17pbZu+YDUzp3muJnV0nyvlJ7Hejz1fK6r2yhVZc4Jq7zS43fxknl+a5YeZz2Pn3/+Bak3zjud81U9/vp8V58nF9v2KDS/+wcAhN2ly5elCiWon7927VopFEZ9fb2UX1euXpEqNxMTE1JXr16V8uvjD/2eFAAAAAAAAIBwaHbT8c909vhQcBeaRDvVwFF3P7/gmnarQK7FSx9RvcMuLn4K6ucCQF5EVYP3jkKq+K6V8nHxcWMDTUqy5qMZTEANmqKeu+/g+tLK43WkciHpUZr9AVkqtyZiK6G5GMKOtXTWWEsDwCo8nmvxWnSWGN8wK/iHP5QNQ6WsZn9u3u9aRkk1+7P4abh/9rgaKoI1vTF03POHd9CMM8zK5zWTqPcT77JQtA3/AAAAAAAAAAAAAAAAAAAAACDUmvepnnjE+WJ1C8O96kja+SIIzUk12BN3vgg36wKxxKHOAC5WMFTqsLsrAZt2t3ARO4CysLAwrDLF1qTEGFfer4Hj6J49H81gArrY0PtFgMXYfCeP0idUv8cLhJsS+xTt/oBseW8itrDQr04EeR5YSMaQOu7x+YTzMoQda2kAwLWM1GFv51o0VM0K45sHPtYJyAer2V+buw+3Wka8o6+0mv0J7w33i2VNb3h8odb68I5dvJhXmort+S0aU/FI9r8jca2zRXfi7d4NU1PTKpu6ePGS1LjxSlZ1/sLLUqdOnc6qvvNPz0gNpb+TVZ160fz/sqgzw+ekDOPVrGpy8nWpqWnzcWdR8/PzUgAQtC3/9o+lnni1Wsqv6bm3SbX+9YekLvzg/ysF5NNTL71HSs9DPS/9+ufX10q1Hdot9dp0pRSA1c3Mzkrp9fvc3JxUvjVEb5G6+647perqNkrl2sVL5nmOWc88+5yUPu/xq6KiQqoxtklq5207pCorK6W8mjW3lVXPPf+ClN/7W1tbI6XHPRq9WUrff7/0+ZUeX33+Nzn5Y6lSoberHk89n7du+YDUnXfcLtXSfK/UHeZtq7bHt0npcd+wfr1UVVWVlFf6fFWPvz7f1efJenu8cX7tbJfRzJjUxMQPpfTrAZz3AkA4rVu3Tsqvy5cuSxXK5cvmzzfLr7Vr10iVq4mJCamguB3PtWvXSvml1yHl5sqVq1J+6eNCfX29FAAAAAAAAICwiKqW3e6uOjl2OKWC/DX2aOeAGuzrcL4KJ2n2d3JABXLNUfqIqwugrItf9pfcxU4IK8/NrEbHAz0uoPhFvV7RaBotss5oxtBxNezhAm/ruWV3C8f3rPm5yDuoiw09XgRYlM138sgY97ZlWSMB7vl5fj5WIh3/vD5vWxobOOYgP1hLr461NACsxlBDHrocy+vg++jEtDrGNy+MjOe1u7l6VyzfcymYZn8Rq9lfshT3CffvvV4r/OffaXXC3eeavSmgD2ZBrvj40B2UlBucBAAAAAAAAAAAAAAAAAAAAAAELNqy21XzmoXhXnUk4GtNos1JNTbY4/mT9HMp3tGjTmYCavZnXQR12OVVMHt3KS4BBFBs3D63XOvs8SHzaFk8DM9XwHHhsRt+GjQ1xQIa6GiDudW8KZVGWcHz1iRBsEYCXPPc3Nly7IQq/iOZ92OO1ZwlqKcTYDWspbPBWhoArsvlh868oWm3op9qFhjfvPD6AQGCpmI5ZKh0Iphmf5mSbPZn83X+HfYPWEqfUP1eX6fd3cK+iaIX2PsNIXbDqRdPq2zqzPA5KcN4NauanHxdamp6Oquan5+XAoBy9/cjtVLTc2+TypWD33u3FJBPen7nit5vnni1WgrA6uZ/aa7DzdLr92eefU5q3HhFKt/r9IqKCqmtWz4gtfO2HVK1NTVSuaIfpz7vOXPGrqAef22tef/NuvOO26Xq6jZKebX0/o5mxqT8aojeInX3XXdKRaM3S+nt4pc+/zt/YUTqO//0jNT5Cy9LXbx4SarUVVdVSW3YsF5Kj/v27duk9Dxpab5X6g7ztlXb4+afm6W3i94vKisrpbzS22Vi4jWp0cz3pfTrAXo76Tp1yvy+WXreTUz8UGpqyvx3zAJQOFeuXJVCeVi/bp2UXyMjI1L5NjIyKhWU+vp6qXJ16dJlqaC4Hc9169dJBWViYkKqXAS1H9bXv1cKAAAAAAAAQAhFO9X+vc7tLB3rTgTf7MG8HwOZk6qnI+58o7Cshg4dfYNqINkZ2MUnRuqAq4ugrPuQ2EcrGxSBs5miaiqBPPDRGE2dPa6GimZCpdUJl31c30CzMhd8NIUzNQbWDSaqYk3OTbfCfqFuwRjKa5+fvbvYgwDXmnepDo9NxBYW+lXR9y41hpTnpxOasyCfWEuvjrU0AFyHhw+dcdCIKRuMb374ey2Isc4dq9lfV7//Zn8nS7jZn4jGvH/IWcjX9GmPi3jrPc/dnFgDReEGJwEAAAAAAAAAAAAAAAAAAAAAOdC8L+HqwhOr2cPhVC6uNomqzuSAGuzrUR1xjxfC+CSN/nr61MnMgEo2B3jhiZFSB5IuL1Dbu191cu0L8snzRWijajxsF6AZ4+a9QuF4b4y2sDCsjhdLl5L0CdW/4O0C16ZYyA7wYW7c6aNBUyTSoYLrCxdVLbs9Tuyiar4TftZ6LWy7EFAcfDQuNR07nArvc0UWjKHjatjr8zYNQ5BXrKVXE7q1NACESfqIqw+d0WjElCXGNz/8NOs2BffhD7hWOtHuv9lfvEea/ZX8Foq2KK8vI4Z7Te+jabdqVOyaQHGg4R8AAACAtzCMV6WeefY5qXHjFal8q/3/s/c/0FHVh97vv0PH5UycqTM3yQ9jHTXUJMcajuCzTlsxz2MPkXCPAmchbbW9v3VP1Qi0ltYKaTi1pae2KwcaobZUCxilvXfdVtsiPkAXP4HkcK4VT/s8BSlaTwIlaKyIIc/EZkzG5XTxm/ns/VWTEsnM7Pmb96tnn/dMQJzs/d3/BuebUFDLNddcreUjV9Zr8Xq9WrJl4PRpLeb77+9/RUumPB6Plo9c+Tda/nbmVVrM19PV3/8nLb/57e+0xGIxLekyr2dGzeVarpvzMS01NZdpyfT1GvF4XMvJk6e0/P7IC1r+/f99RssfXvxPLQMDie2RWKaqgN+vpaqqUovZLma/MNunae71Wq6Znfh6YjH7i9luoWBif0os6e4/ZntFhoa0mHHXe/SPWg4eOqylq/vftZj95+DBxNcTizmOnDz5mpZIJPHnJBYA7unv79eCqSEcDmvJ1KHnntOSa4cOJf69iSVT4fAlWqY6t7ZjRUWFllRVJv6Z5OKWZ555VgtSc2nimJBcAAAAAAAAABSomhZrxc3O40l6ft09Vlbm/Euomdtirdt+NKcT/42Z6K/F7Q8b9Vmd96xLaXKJ5OtpW+7aDEXA5NTMsOqch6lIfgDtaEF9/qzbamtObZ+D2zKYGC3h+V91FcGEQolj+8b0Pt1YiB/yTk7mu6ytuyDXeyYTNFkNta6e02tmpHOULLLJd3KJyVmBHMvs/HzmyFprU7fzpOh0W5tSnYDdweQsyD2upd8P+yQAvJ9uq21ZmjMx8cNnJoH1myuZvBfk7g9/gNHXmZzs74jzLD2a7G97i8t//1aoaqw030aUgr2mz2DSbuvm+Ra7ZgnrPV4E96Hv0Xc07fPMVJhUlgn/AAAAAAAAAAAAAAAAAAAAACDL5i5vs2aWTX5yveTENeua26xszvdgJv47tnevtWX1kpRe32QkPyQ+c8lqa8veY9bRrEz0Z+vrvMdaeyTFDw3wAUAUmSeeKpTZX/qszsXL0v/QGVyT7sRo8vyvrIKfG62vy/pVevMGWVbDTVYhzlFyZttSq7mt0GZy6rO60l7RyVXd5O65fe58a0ma1yPFMflOsaizpsDnKoGsqGm6KaP7qic2dhblsayvc2P614cFet5GaeNa+n2wTwLAhNK95uGHz0wO6zdX0p+sW1z+4Q9Ijv3F1ry1TPaXqrnzU/xJa+9VoNf03U+lP2k3x8ES9/zRonq/qO94ej+GJjmWa6fAgYwJ/wAAAABMKB6Pa+nre0nLMwd+o+Xkyde05Fp19UVaPvbR/6KlpuYyLdlivv/eo3/UcvDgYS2xWExLpqqqKrVcN+djWqoqE88TS7qi0aiW3/z2d1oGBk5ryZTH49Eyo+ZyLdf/t+u0fOTKei2hYFCLW8x6P3nylJbfH3lBy7//v89o+f3vE88TixmHbm2PUhEKJbZHYjH7i9lu11xztRYz3prmXq/lmtmJrycWsz3NfmW2q9fr1ZIqs10iQ0NazHHkDy/2aDl4KLE/JZau7n/XYo4vZj873ndCi9nOkUjiz0ksACbW09urZWRkREuu9PQk/r2JZceOnVqQG+FLL9GSqf7+V7SY7ZhtZnw+c+CAlkyFw2EtU5VZn4cOPaclU+FwYlwllnTV19VpyZQZH+b7Q3EYGRnVAgAAAAAAAOB91LRYK1L87MmZM9usZYtzMOFDTY01t2Wdtf3oUevYsb3W3i1brNVLllhLZs5MabKKmYnfv2TJamv1lsSfccye5G/7uhZrbjb/w/zutpQ/CFVWtsTavI4PviAfaqzaBudhqnqPF8SHebrbmlOfYBPZkcHEaJpUdlOhTTw3VvemddaRNCcOcn0SOhclJ/1b3FlAH83r3pT2Pp388N1Nrs8GM9dK+7O6xTD5DoDSV9Nk3ZTu9V7CmSNrrQI/Rf+1vk7rngwmDCnk8zZKGNfSE2KfBIAJZHLNww+fOTfWb85kNFl3AtcK7mKyvwyU2jV94ji4Mb35/pI7JpN2F4X0/44w+d8MFMzPBZuEvqPpvk80NX4QDRP+AQAAAAAAAAAAAAAAAAAAAEAOzF23OeUPnyQnfGhuy+V/wV9j1cyda7WsW2et277dmQTwmJa9e/f+9eL8WnLZnvj969a1WC1zE3+G86dlVV+ntXhZ6p9+aWhbbjHdH/KjxpqR7s9PKoCJrLrbFltLtzHZX+HIYGK0pCc2WoU079wYGXy4MTuT0LnryNp5Vk5P7RPqszrT/hRpUnY+fDc3zYFdDJPvFI9e6ziTJwJpqrGaMpnxL+GJZW1WMR3NMplYLDkZ+wpmZ0FecC19NsVwLQ0A+dFttTWnd82TPLa2Lefd6PfH+s2dbmtTBpN1c63grr40fqDVeGUzl1ibp+Jkf1Ja1/RM2j0VZPB3hAm9RfOGbbf1VNqTV9ZOibHMhH9ACvx+v5ZQMHjWpabmsrMudbUf1nLN7KvHLLV1H9YCAABQLGKxmJY/vNij5ZkDv9ESiQxpyRWPx6NlRs3lWq6b8zEtVZWVWrIlMpT4PhPLb377Oy39/a9oyZT5fv72b6+yl5n2Yr6eqng8ruX3R17Q8ocX/1OL+bpbqqsv0nLNNYnr28RitkM4/CEtXq9Xi1vM6x84fVrL+HFoFvP9njz5mhYzbnF2oVDifiaxmO1p9qvx27Vp7vVazP3MR66s12Lue8x9Ubrb3Wwns5/19b2kxWzng4cOa+nq/nctZnsfPJj4emI53ndCi9nuuT4uAZkIh8Na3LJvX5eWbDl06DktHR3r7eV+e+np7dWC3Jg9a5YWtzz2+ONaRkZGtGTLY4//XMvo6KiWTLm9HoqN2d/dWp/19fVa0jVr9iwtmTLfjxkvmJyX+/u15MrpwUEtj279sZZ9XYnxmFgAAAAAAAAAnMtca3lb6pM/nNm21FpcAJ86qamp+evF+bWcS072l8aHAMtmrrY2MKEE8qimNr0JYJITWf0qjzP+9XUmJ/vL7MOHcF+6E6MlFe7kaH1W5z3pf7jRunmFVQyH+W1Lr8j/pH/dm6y1R9Jcz0k3z8/OBLpz56c8QbKRvGYqjMkU4Qb73JPBGAXypKZlRdrHsaQzZ7ZZyxZ3Js6IhS/j/TRb5xJgEriWPosiuZYGgNxKHFsXL7O2cWzNEtZvLnW3ZbCuk1jf7klO9rd0m/MkPckJ1DdvXzel76lK5po+MR7SvbdmIv3iku7fESY9v25TcfyAiO6n0j/X1M1gwj8AAAAAAAAAAAAAAAAAAAAAgHtqWjZYq2emPvnDkbXzCmLSv4KQ7mR/ZTOttg0tU+KDAihcNTPqnEepy9eHeZITucxby2R/BWnu8rTOKUYhTo7W13lP2pPQ6Ti/vHg+4vrEssV5XP99VufGJ5zHqcvuup5rZfBZXeuJjcUxSVYhS37g+WieV2Jf8oPvnHtQtDI7jiWdObLWai70GUwT92X3rHveeZK6YjtvowRxLT0G++RU0Gd1d7ZZbYsXW4sX11pXXHHFXy2Lk7/W1ml193FFDRjdbc0cW7OI9ZtDGUwoZtw8n/XtisS2WLws/fflkjTZ356pPdmflMQ1fWbv0zKRfnHJ5O8Ikz8gYmPB/7cCmY3nqXKeYcI/FKVQMHjWpaqyUktNzWVnXT5yZb2Wa2Zffdbl+v92nZamudefdfnYR/+LlmuuSfz+sywzai4/6xIOX6IlFEq8zvcs5/UzUyIAAJaVSURBVF0Y0AIAAFCsYrGYloOHDtvLQXuJRIa05IrX69Xyt397lRZzfef3+7W4LR6Pa+k9+kctv/nt77QMR6NaMlVVlbiuTSzXzfmYFnO9m66TJ09pMa8zW9vHbIe62iu0mNf/0cQ1dHIJhz+kxfw+t5nxaL7fP7zYo+WZA78Zs/zhxf/U0t//iha3tttUYe5nqqsv0mLue8x9kdnu5j7K7I/mfszcn5lxne54MNs7MpQYz4mlr+8lLWa7m+NSV/e/azHb3xynjved0HLy5Gtacn3cAt6rvLxci1t27NylpaenV0u6Tg8OatmxY6eWttVf0/LgQz/S0tOb+PMTC/LDjJv6ujotmTLnxQcfTGzfxDIyMqIlU+bPeXTrj7UcOPCslkz5fD4t9fWJ7z+x5NvIaOL7TCy58syBA1rM/u6Wv0msy+SSrtmzZ2lxixkvZvy4NS5LldmPs8WcV8z2WJ04JyQXt/ZrAAAAAAAAYGqpsVo2tFkzy9Kc9K+te2pPYpPmZH9JDW0brBZm+0O+zZ1vLUlj/0/Kx4d5mOyv0NVYTTc1OI/T88SytrxMJHlWGU4cZDXcZDUV0XE+Oalavib9y2QyGMnyup67PL1rpaSimCQrV2pmWOn+VwBPPJW/dajJ/pZuc54BxSmT45iRnHigYCd9z+C+7B03r+D+DHnGtfQYRXYtjRQlJxaqbbaWrt1mbTtyxDoywb3AkeSvbVtrLZ03z6pdzETaQHfb4swmSON6532xfnMoef2e6QRzM1dbzK/oAmdbZHIv9c5kf4z/BBeu6fP8w0OY+HSKqanN6P2ifP1gsMnKbBL6JdZUmVeWCf8AAAAAAAAAAAAAAAAAAAAAIJdqWqztm292nqTmyLalVvMU/cBtcuKxK+atTeuDUGVLtljb+fQfCkKNVZvB58+eX3ePlat5X5IfumWyv8JX07Ii7Ukkk5ITSS4riPNKt9WWwcRB+nDjhpbEHlZckpP+bVt6hdXWncMtkOlkMAk3r8jyuq5psjL5rG5ykizm/EvK4JzzxFN5+fAok/2hZCTu+Vakd8s3hiZ9L7RJ/1yY7I9JCVAouJa2Feu1NCYpzYmFkhNp31OoE88CWddndbfVWku3pf++mCbkWsf1ztkV6frV5Km1Vm3tYquts4h+MJML1+9JWX8vaCpw616Kyf7GyPiaPp8/PCRxXGHi0ykm0/e9k/eghfrGd6Z/73DzfGuqXDmV7evan9lZGVNaKBh0Ho3l8XhUf+ACdTyf16t6nY4XCPhV8+eUqqePnFJvvHePiqntquBb6o1XDqodz16sAkid2Z/ubHxZvXtXrYqpqbFhurq7vVnFWJHIkHrw0GHVbdXV9vqfUXO5OtH1X7adPPma2nv0j2o8HlezpabmMtV8327p739FPd73kprp91FVWal+5CP1aq6vv2OxmDowcFqNRN6wO2SPy2xvp4mY+xxzP2Oem/uUfI3jqcIcl8z4GHU65IwP89z8eraY7WzuX6+55moVZ9fRsV7t6e1V01VRUaFWOi0W4XBYvfXWT6upGhkZUb/05a+obps1yx6/s2fPUisr7OO/cXrQPg73v2yfZw4995w6OGjfn+baooUL7C5aqGbKrfHZumqlWl+f7s89z45nDhxQt279ieoWn8+nXjdnjmrGTzh8iVpeXq4aZhyb65VDh+xxZF7f6Oio6pY5c65Vb7/tc2q6enrscdFxvz1OMmXW26XOccGMl/p6+3qrvNz+dXPcmKz+/n51774u9cCBZ1W3mO36zTXfUDPl1n43njlPXOds/3fHZWrr0xg/bo2XnfU96vz6aed4OHh67HHRHPfT/fcbbo/DTPcPM97+03ld+5xxl6vzglv7t9Fy5zLnUXrq6+z9uLXVPg/kS6bfh9H58GbnEQAAAAAAAApFcgK7dCfT0od6Nm+wWqbEp3r6rM62e6y1aX4AUB/+O7puynxIIBvSHaus+7PLZN9PKpu52tqzPZsfsEzsc4ubrbVH3P+4Se7GRLfVVrvM2pbihyezv26zI9MxlZTX770vOUFJ6tvrvZITux7NyQe90xtbkzFz9d7sT07rxrrO1X7c3WZdkeHEb0u2HLOKYX6FvsR26euzrJq5c13fB+1JFNLb3jkZk+/hxrEsyf7w+/YcfOC5tM416a7/YtnP8sO9c0ahjJvkpJz3pDFp1Hi5Pr6cHftwUm724cJe11xLJ15/gV9Ls99lJpMxzvs6uZb+tUNhnFtLRPK4ek9iO2T4vhjXyRMo2vX71++XJu89b968IfE6Cnffc+v6PXfXCiXMtcn+cvF+R/Fx45o+5+fS5CSiGeyfpT0eSvuaqCjH6zmlv82SptrxbZpTAAAAAAAAAAAAAAAAAAAAAEAO1bRssLYsKXOepebMmSPW2qXzrMVt3Vaf87VS1NfdaS2ubc5ssr89fDAZhaWm6SZrZll6+37SmSNrrebEvp8VyQ/dZmmyP2RPTcsKa0kGYypJ42pxm9Wd65OKPuya/gfBkvRhsOXFf6Q/snaeVbu4M3vndRcmg0lqaFuem/Pq3OXW6pmZjettS6+wFncW6pVSn9XdtjhxnVNrzZu31Fq6dKnVXJvYB51fdcvc+Tc7j1L3/LpNrr+eiSTXRaYfdAUKz1xreVuD8zgzOk/XLrY6c36iNpKTsCf206XbMp8sZOZqa8NU+QQ3igLX0qVxLY2J9R193nmUjl7reKFeTgNZkJwcTcfVDN8XS06Oxtxof62o129fl/WrcYfT5N/RbFuafC+nLY/X6ROx33Nw5fqdawUXJN+TY7K/bHLjmj753myu3kfU8TDDyTgb2jYwHopUsY3Xc9LfK2b49w4NN1lNU2g8l+3r2p/Z2RkFIRQMOo/G8ng8qj9wgTqez+tVvU7HCwT8qvlz4K6nj5xSb7x3j4qprfXaV9Xmv+tV5/3gEyqA1Jn96VN//z/Vj7YvUjE1NTZMV3e3N6sYKxIZUg8eOqxmW3W1vT3qaq9Qc32dGY/H1eN9J9T+/j+p2eL329fTdbUfVkOhs1+3pyoWi6l/+EOPGhmyt2O6zHaYUXOZGg5foubbcDSqDjnjNBJ5QzVfN+sh18z6Cjjb19xvvfvcrnmO7DLHMTMeRp0OOePFPM90vDTNvd55hLPp6Fiv9vTa9zNT1TfXfF0Nh8NqqtpWf00dHBxUp6pFCxfYXbRQzZRb47N11Uq1vr5OLTRTbfysXduuVlZUqOnq6bHHRcf99jjJlwrn+zDfz2lnO+Zqe9522z+p182Zo2Yq3+u1vm7sfvpyf786OjqqumXOnGvV22/7nJous71XO/uxW8y4mj1rllpe7lPHM+vHbDe311O6bmhqUs1xt7y8XE31ONxy5zLnUXrMeGpttc8D+ZLp92F0PrzZeQQAAAAAAIDC0md1Zji5VvKDPje3rbCWt8y1Sua/he/rtjo3bUx7or8kPgDlnr7O9Cbh0YSLR5lw8a9lvt8nzVyyxdqwzr39Pvkhs3sy/JDZueRuTHRbbbWpf8AoOQHNnu0tRXksTXc/HU/Hzs0brJa52V8LfZ2JMbcu8zE3c/Vea3vODvbpja1UlM1cYrVtWOfu+UuTwWT2weKknO8jLr1ut4+XmehLXONsumejte3I2ffXJVuOuTxxQWZjNvvb3J1z4nvl7hqwtM416Z5H3B+zpcb9MT5zyWprxfIWKwen6oQ+q7tzk7XRhfN1UmHdo7EPJ+VmHy78dc21dK5GNPtdUm7PnZmfhzjX51L61+653Zcz1Z0YlxutX+lxnXXTiuVWU+K4mddX78J70Ub+j1ms36zobrOuWLrNeXJ2yeuAQvh7muQPUdq0cV3GEysaxXV8KUzdbbXW0m3pbw+Nrc3bOR+fg1vX9Nl9HzF5j32PtTTD11n6f/dW+tdE7o3X1Ynxmr/rjuQ5555lmb9/P9XuOaY5BQAAAAAAAAAAAAAAAAAAAADkXI3Vsn2PtXpm+j/J/8yZI9a2tUut5trFVltnt1UgP88/PckP/7Uttq6Yt5TJ/lDCEvv9ipudx+k7si2x3y9us7oz3emd/W7e0m2uTOSC/Khp2ZDRucRInlPWLp1nLW7rzHxsTSA54Vnb4lpr3trMx1zyg94bSuxgf+bINmvtvCsS28CNc3ryQ6TJ8+paV/bvm1fk+MODNS3WhrYG50n6dLxMXidla1CfU3I7tFmLaxPjPnGNM9Fkf0m9x91+jXOt+Rmccs4cWWs1uzIW/1ryA6GLa92dCA0oPInrvg1t1syyzM/RxpFta62lyfOErgOzdVwzx61ma6kL52ujoW0D92goSFxLA0Du9HVuTNwDHLGOaEnc/yaOm/OusK9tOnN9z5a4lup24b1oQ+9Jb8jvBKWs3+zoO27/YPX3Y/6eJrm+a531ncs1nrzH1jXC0rWuTfZXtmQLk/1lqq/T2viE8zhNDTffZM23uq3u7iJdsnbfOlZNywpriQv33ubvXdw+Ztr7aPIeO/Pj4c2b+UFbxc698bo2T+972+f45Dkn4/vSxLlmqk1oWravaz/viLvI7/er53k86njB0IXOo7HM7zf//Hg+n1f1eu2iNDx95JR64717VExtv/3aDvXKq36phu/Yqg7FPqACmLzx+9PH73pYfWHofBVTS2PDdHV3e7OKsSKRIfXgocNqrnic699w+EPqpeFLVPP1XInFYuof/tCjRobs9ZEt5vudUXO56tb329//inq87yU1Ho+r6TL3HR+5sl4NhYJqoTHfpxnHw9GoOhR5Qx11tq/ZzvkSCtrrz+uzz0MB577P3P8FAnZzPf6nKjNezLgw4+Rc46Zp7vXOI5xNR8d6taf33H+BVcpaV61U6+vr1FQ9uvXH6oEDz6pT1aKFC+wuWqhmyq3xmen2zbZDh55TH3zoR2qpcnt89PTY46LjfnucTDUVFRXqurXtqttK/fwQdu5jvrnmG2qm2lZ/TR0cHFRxdrNmXa1+8a4vqOfScucy51F66uvs435rq30eyJdMvw+j8+HNziMAAAAAAAAUpj6rc7F7E54kf6L/iuUt1txi+SxWcsKxTRvd++Afk/25qq9zsTXPhQ8hFYPkvrNhXS4+OOr2Pr8l8brnpva6M9zvkvtaQ8Pz1pEUvoeysiXW5qO5+DBat9VWu8zaluIHjZITXuzZnt8PZmekr9Na3LzOtUl5ktw8nyQ/1Lhp4zr3PnScs/H0XumNrXRpP7t5Rer7d0Jfd5t1z8YnUtpH30/yg3dH8/LJO3ePl2UzZ1o3r9hgLU8M6qzu68mJDbo2WRt/ldo2mLl6r/sfpk+MhSuWbnOepCd5fNy8waVrSxev+84md9eCpXWuSfd6b8mWY1PuQ7lpcWE/fD/J8/VN85uspgyObclJxPq6nkr5uDVZ+TuPTIR9OCk3+3CRrGuupXOA/S4pt+fOzK+lOde/V3JC3E3WU0edp67rtXqfeD6t41DZzCXWza7+Z+611vysvLc8uTGp7+em+dbypsS9cBZ2frePqUnJ+5CbN2/P8/7C+s2WTN4f1/sQN63IyvpOXsN3Je6vf5XmseP95OdaofR0t9VaS7e5u22KUc7uB12+904eL9tWLLdaMjghun4dXxD31lwTucHtv3vNyfveyfe8N92TOK6587qn6rmGCf9cxoR/SAUT/uG9mPAPcA8T/uG9mPDv/THhnz2hFxP+jcWEf+5iwr/CwoR/2cGEfzYm/HMHE/6lhwn/0sOEf0z4lwkm/MsPJvzLDBP+AQAAAAAAFAN3J7RJ0gfh2rLzYbLM2R8I2bjuCdc+jKYPBuxZl5UPXkxlbn/opNAV64fPkvQBtJvmW01NiZ0gsdOP3RX6rL6+Pqur6ynrVxlO5GImU5qxKbUPLObuwzulNZFEKrK1vya3ecPNN1kr5jclhlZibE1iJSXHW19fl/XUxl9Z2464+5rMGMz95K7pjS03mA+Iz0/s39oGztcNs76PP/Ur1ydSy/sH77IwAU/SzMQx86YViWNmTebXSVr/XYnx/qvMxnt2zkHuXGO+c13ZklhfztdS4vIHQieSu+MDkxYlMQnQ5OXymnpm4pxRp//modaqnT/DmmF/+R3Hjz9lHU1+ML+31+q1UpvAOR2FOe7Zh5OY8G8srqWzjf0uiQn/ipX779sWA/e3f3rrUffDdTfpuqYpeRA9yz3xhHQ87bO6Etc/mb4f9n4KY19h/WaLm9cI9rW6vb5nvLOu3+/6ILmOk0nUOm499dTRxGV89tZ1Uv6uFUpN/t5HLES52o+zNcli8n3Eupvm2+/NJp4nr+v/ivbTxP+6Evtqhu8Rnk1hXBdzTeSe7K3L9/5d4VnHago0uWzy7xVd/Pt8ozCun3KvZCf8MxMlmIkUxvMHLlAnmlDBTMgwnue89/9zgVQw4R+Srgq+pf7Hg3eqxqrv2h+I3ny4SgVwbhPtT/f9cK3a8ezFKqYWJvx7f/ma8G88c10+o+Yy1UyckWtmffzhRXsCwGxNFGcm1Kur/bBaVVWpZipbExia+6MaZ/sU6gSAEzETAw4P2xMCRp2JAc3EbtHhN1UzYWCmEyama/x9rJkw3ueMFzNuim39FztzXGC9vz8m/LNlOiHcVJ94zGDCv8yU6sSRbk+sZkz1/e6uL3xenT17luq2087Edd/61rfV0dFRtdS4NYHajh077e7cpeLsUp2Ajwn/xmLCPwAAAAAAgGLRZ3W3ZW8ilJlLllg3zU9ObJP5f+Cfuj6rr7vP6npqo/WrJ553/UMBhfoB8FKQy8lJCkHuJrYqzg9GvfcDn6l+eI4J/3IjWx9qPJvkh5THyv6kQUn5+yBYumNrptWQo3WTDQXxwbssTJI6nrZTXZ2lKbJq51szxs+QlXT8uPWUZslKzpPVmxjy7l7TZG3SWZfX38wlqxPXlE1W01wzOcHZ2RMhbrI2ZnHih/GY8C89xTFpUfHL5Tm6UCQ/ZL55ex4njZ0Q+3BSbvbh4lrXXEtnE/tdUm7Xf+bvO3Cud+TgfqQQub//pXcceD9/fSxNyu3995ItexP7SSEcpVi/2TKV3h9nsj8XZekHWBSrnP2gpSL9e5dzyfsPZDG4JnJXjo4T9t9P1Fn2x2Nqrfnzz/rGt3X8qaOW/fMh3H/fe7yZq/da26foyWaaUwAAAAAAAAAAAAAAAAAAAABA3tVYc9dtt/ZuWWLNLCtzvuaeI9u2WWuXLrXmzZtnXXHFFVbt4sXW4rY2q62z0+ru7ra6k5OyJJZMaGKXvsSflfjzOpN/dvLfUVub+PfNs+Yl/t1rtx1x/QMCM5dsKcgPf6NY9VrHM9sNJqnGatnQlpV9PVv4wGdxmLtuj7V6Zm7G1ZEjiWP6mCX7H6RMfhCs+CacuMnasH2ztSRH28VNBbO+566z9q4+20QD7jmTHMOJa6VtyeultUutpYnrlr9a1q7VrycXjXmXr2kaarN0gE2svy1L3Bt/R7atTVxTJq7tEteT71xTvmdJfi25JK85l65Nrqv011NycsFcHdOAbJu77qir+2KhK9zJ/oCJcS0NvCv5HkC2Lk+LTd/xzH74fNF6/qiVk7fIMvDXx9LcHE+TkvtI4Uz2lx2sX1vNDPsHiJc63vt1Wd9RJvt7r97jOTqnFN/fu5yLvW8Wxr0110Quq2mxtm++OevjVe97H7Hf0962be3Z3/deutZam8X3vd8rOQHoVJ3sLylnE/6FgsGzLlWVlVpqai476/KRK+u1XDP76rMuTXOvP+ty/X+7Tss11yR+31mWutortMyoufysSyiUeH1nWQJ+vxYAcMuNVw5qGW/efzmhBcDkTbQ/ferv/6cWAH/N5/Nq8XrtJV/i8biW3qN/1PLMgd9oOXnyNS25Yq77r5vzMS11tR/W4vF4tLglFotp+f2RF+zl9/Zi1kO6zHY09z1/O/MqLZm+/sjQkJaDhw7by0F7iUQSX08shc58/2b7hsOXaDH3hWZ9mfvId+4nnftOMw7C4Q9pMfezma7X8cz2N+u7r+8lLX94sUeLWf9d3f+u5Te//Z0WM36O953QMjBwWstwNKoFmTHjBsiF+vo6e6mzl2Lh8/m03NDUpAX5destn9ZiznfFznwfratWaoE7zP46e/YsLdlSWVGh5autK7WY40Wp6e/v15KpG25IbJfEUmzryeyn31zzdS2zZl2tBQAAAAAAAEDmauaus7bvyf7EQO9MbLPW+Y/75817ZzJAs9TWjp3A5a+X5GR+7/7+5D8/b5794QB9MCCLHwpIfshl9Za91vZ1c5nsD645c+aIdTRXn2auabE2tDU4TwobH/gsJjVWy/bcTVSSS8kJSor3g2BzrXVFtl0K7YN3NS3bsz7pXymbuy5xbZmlD4/aHxp9d3FLclLn7ev4b5JQWuxJ/0r/WFY2c7W1h8n+UJS4lgYwXp/V9avnncdTTa5+KEbxMe+TlfJkf/nE+s29srIl1mbe+0U25XIS2eQkantKY9K/wvp7Ga6JsiL53wRsvtl5UvqSf+ewZ4rPQp+zCf8AAAAAAAAAAAAAAAAAAAAAACmoSU4MdNTau2VJXj+Ukpz87L0TuPz1kr2f8P9+khPA7Dm63WrhQ38ocskJrLYsKewPnpXN5AOfxae0JipJfrBxSXKC16IfhMWzXQr1g3elPulf3YxsjvHEtWURfXhUkxJN8Q9/onTNXVfaxzLdq21vYVJ2FDGupQFbnZXVy1MUPPd/KEaNVVscP/fifRXu+2Ss36yZOz9rE+gXAk3WfXSdxV+1oKSUwKR/hTXZ39SW9R8UNneddSzP/z1ALuj9In6InzUtFAxa713C4Q9pqam5bMxSV/thLdfMvvqsy3VzPqalae71Z12uuSbx+86y/O3fXqVlRs3lZ12qqy/SEgolXt9ZFiAf4vG4lkhkKKPl7TeGtWBq+9Tf/08t481v2qgl6P2LFgDnNtH+dOVVv9RyVfAtLQDe5fV6tZjr+Y9cWa/FfD1fYrGYlj+82KPlN7/9nRZzHZUr4fAlWsz6MfdLbhs4fVrLMwd+o6W//xUtmaqqqtTi9uuPDCW2Q2I5eOiwvRy0l5MnX9NS7DwejxZz32nGQV3tFVrM/ez1/+06Le/c9zr3x2Y/MvfTVZWJ7ZBY/H6/FrdEo1EtZvz09b2k5fdHXtDy28Q+k1y6uv9di9lOf3jxP7Uc7zuhxexXZr8DkH+33vppLT6fT0uhuqGpScu6te1aZs+epQX5VV5erqV11Uotc+Zcq6XY1NfVaTHfh/m+kBkzHsxxJlfC4bCWr7au1FLox7d8MeM819snVWb7LVq4QMs313xDi9nOnA8AAAAAAACA7KhJ/mT/PXusLUtKd0KIVCQ/8Lc6+UF1PhCAEjJ3XeFOJqEPfG7nA5/FyZ6opNjPH+aD3utKZhAW/nYp9A/eJSf9K8UPQCaPt8uzPb9d4royOZl0IWNSIkwVOpbtXW0tKZEJxZLe2X+5V0NJ4FoasBpqOZ4D49gTG/M+WbYU7vqda80vnvnzU5KcbP8ok3WjVBXxpH+FO7ksskb/PUBxT1L5fswPd2FIW9Y0pwAAAAAAAAAAAAAAAAAAAACAQlVTY81dl5wQYou1eopO/Fc205484uj2dVYLn6jMqZoZdc4jZI89mUShTfrHBz5LgX3+SE7wVYwfFCvdD9I722V14Z3Ti+aDd/oA5OaSmSgrOUlW24bcHG+Tk0kX6qR/TEqEKaemxVpXAhOKJc1cspr9FyWIa2lMbQ03NRX+fQGKTI3VdFOD87i4FMcPoWH9ZtPcdaXzHkRScp1v2XuMyfZR+jTpX3Htv8n7a67jp6jkeD26p6T+WwCd4znfjFF2JsF5DLwjEhlyHmUmGo2qb8fjarpisZjd0bfUdI2aP8dpvh07Yb+OVU++rmJquSpoj+f/ePBOdSLL7vu++tOekArgrwW9f1H7H7lNnciq765XNx+uUjE1NDZMV3e3N6tIzcDAabW//09qZMid68RMhYJBtabmMjUUsp/nirme/MMfetRsrRfzfdbWfVgN+P1qprL9+r1er1pdbe9/F1dfpJqvwzbs3C/F37bvl8x2GH//k6/9zow/f+AC1edsP78zDgMBux6PR8XU1tFhX2f19PaqU1XrqpVqfb07H3Tp7+9Xv+us39HRUTVf5sy5Vl20aKFaWVGhGj099vbvuN9+velatHCBXeffkym3xqfb2zfXDh16Tn1064/VfI+n8Xw+n2q2+7wbmtRsc2vcFqp8rddzGRkZUR97/OfqgQPPqsXmhiZ7fd5666dVtz1z4IC6detP1Hwx48iMnxuclpeXqxPJ1vVBfZ19HG5ttY/L59Jy5zLnUXpS/fdlS6bfh9H58GbnEQAAAAAAAEpCX7fVuWmjtXbbEecLpSv5QYC2FcuZ5C+f+jqtxc3rrCNT5OMHmnRpz3YrP5896bO62+6xluZ5357sOuhuq0281smPi7KyJdbmo+usuc7z7Om22mqXWdtSHLNlM5MfpCvhCQ6L6NxRuMf+LIytAjnG5vfYl5m+zjbrnnVPFOV5Krneb25bYS1vycOkBgV2fk9OSrThrJM79Fmdi5uttUcm9zpzN5ZL61zT17nYmrc29fPDki3HrMRmgxsS5+m2exJjapJjvVAU7/0a+3BSbvbhElnXXEu7gP0uKbfnztSuo8bjPP+uVN//KBXZubZO71iQL3m9Z0sL6ze77PduNz7xfNG+V651vnkDE3XnwhT7e5Vzyf81XWH83cv7KYb9k2si5ws50NfdaW3auK7o3icyiu8cnzvTnAIAAAAAAAAAAAAAAAAAAAAAikXNXKtl3Xbr2LG91pbVS6yZM8ucXygdM5estrbsPWYd3b6Oyf7yrabJuqnBeTwl1Fkz8jbkaqy5iX1775bEfl2Wn/06OeHSnqPFOekXzsE5d+zdstpaUqDnjeSHwJasTozBqXTsr2mxth/dY61eMtP5Qu4V+35f07JO63BLHtdhqt4Z64n1vi5fH3pMjr09m/M69pKSkxIlr/m2n3WyP2AKSZyn120/WtDn6fcqm5k4jm3Zy/0apg6upTHFJCfsn89kf++oqZ1Sb4y9q+Emq8n1w0nimmfzzXl732uyCuKeLS2s3+yy37vVexDJv5cp8PX8Xlrniet3rXOuE3Jjyv29yjnUzcjzvm7vv8f2binI63nz/myh759cE+VOzdwWa9324jvfJL0znpns76zKziQ4j6eEWCymjo7aTdc7f47TdMXjcTU6/KaaqcjQkPMIxeDYCXv8rHrydRXFpXH6iBr0/kVN1Q0zX1Pv+My31Yk81bVC/XHX36jp2vVSwHkEFJ5lVw+oH/rf0jsfXvL/iaqfWvRddSIvvvBJ9f/37N+q6Vrzb5c7j1AMGhumq7vbm1VkJhKxrzf7+l5SC+X6s7ra3s4zauz90+v1qrli1ssfXuxRzf2C22pqLlMvDV+iejweNVO52q5VlZV2qyrU6uqLVEyOuX8cHrbPe9GoXXNfau4rh52vm9+fbWYcBvx+1R+4QPU5+6Hf+XogYNetcYvC8thjP1f3dXWpU43P51PXrW1Xy8vLVbeMjNj3n489bq/nAweeVbMl7Jxn5syZo14351r1XN9Xf3+/+q37vqOm67bb/km9zvn3Z+rRrT9W011v2d6+uWbG06HnnlP37bP32/7+V9RcMePshhua1NmzZqm5Xr89Pb1qx/3r1XSZ78d8H2b95nq9mvE6z1mvZv0W+rg9PTio7tixUz10yF5/o6Ojar5UVNjXrfX1dWOa6/FqxqlZPz299vNsmTXranX2bPv7TPd4nK3zV6rniRVfultNdzzNcc6Dt9/2OTVfvnWf/T5yuseV+jp7/La2rlQBAAAAAABQwvr6rO6uTdbGdU9YR4r0PxOfOXOJddOK5XwwvSB1W22Ll1nbjpT+RxDKlmyxjq4rgE+193VbnZs2Wmu3HXG+kF3J/W/FhnVWKrtfd1uttXTb5MdEcsKAzUcT/w7nefYkxmttYrymeCwsm7na2rO9Zcp8AKmvu9PatHFdQezXyQ8d37x5g7U8MQALe/1nd2zlepskJ1rbnOJ+X/CS10Ob7rE2PvF8QV4PJSc0XrG8KbHOC2ul5+N4kJws7OYVGybxQeY+q3Nxs7V2kq8teTxp25OLCSxL61zT17nYmrc29WuOJVuOJbah8wSu6ktcC266Z2Niv8zNteBkJc8dbSVxz8Y+nJSbfbg0r825lk4H+11Sbs+dqV1HvdfM1Xut7fwkgPdIb/wWu+yOg8S9W1vh3buZ+5TCP6aeC+s3Vwr1ut0onev3ItXdZl2xdJvzZOrK3Xs1k1co1/P2e4UtRfT+LNdE+dKXOJ7cs/EJ60gB3IOeje5L21ZYy5nk75ymOQUAAAAAAAAAAAAAAAAAAAAAFLOaGmtuyzpr+9Gj1rFje60tq1dbS2bOdH6xMCU/4LdkyWpry969idd8zNq+fR0fPCtYc6112/dYe7cssWbOLHO+VnqSH67aUyiz1tTMtVrWbbeO7d2S1X05+T1v2Wvvf6nufnPn3+w8mqSG2hx90KfGqm1wHqag4aamKfVBpJq5LYn9OnHOSIyx1UtmWjPLcr9vJyeaTJ4Djh7drkm/pvoZwGwTHWuzuD3Mfn80jf2+4CWvhxLHzuT10N4tyWuh/J6zdK2TuCZLrm9d66xLfoC38Fb6u2Mv++ssOdnA6i2J/X67vd+fW401w/4ZZ5NUZ83IySourXNNTdNNKR93khP5zmeyv6ypSVwLrkvsJ/a9XX6vwe1j2RZrb+I4ljx3lMY9G/tw7vbh0rw251o6Hex3OT93dm9Ka7K/5GQdNzWVwrHeTYnz8p62vOzr+TJzyRZrQ1Ynthl/75a/97GTY/7d++RSeX+C9Zsr7163H7P25vG64L1K8/q9SM1dl7heLPy/q8smTQK2eUNBTfaX9M57YVn++5ezSa4T7aPJv5fRe4XOLxQFronypSZxPNmue1DnfaIC2AZjzvHJ+1Im+5uUsv/1vyJnvUuLDA05jzITHX5TjcfjarqGo1E10z8HKCTHTsTUVU++rqK4NE4fUTfcsU+98qpfqoUi+mf7bzNXPnCX+tOekAoUoqD3L+ovlj6tfvxjj6qFwuxPd679srrrpYCK4tDYMF3d3d6swl2xmH09c7zvhHry5Ck136qr7e1eV3uF6vF41Fzp739FPd73kur2fYzX61Xraj+sVlVVqm6JROz7wT7n9bt1fzie2S5VVRV2K+3vw+3vZ6oz29Psr6NOhyJvqOa5+fVsM9s94Per/sAFqs8Z137n64GA3Vzvv8hMf3+/OjIyqk4V4fAlanl5uZptIyP2/eih555Te3p61cHTg+rIqP3r5nxk+Hw+9dJwWK2otI+/9fX29W59fb1aWWF/PV2nB+3XYV7PZJWX268v7Lw+t5n1lKpcb998Mdut/2V7Pzb788tOR1Pcr33O9jTjzWxXM94KZX2acdFx/3o1XfV19vfV2rpSNcz+avbHnp4edaL9pKf37OO0wtkvzf45fv2a/des31Jhto8Zh2ZcTna9jT/uGePXn88Zj++O18Le7834OXTIPg9MtF4mYo7/ZjzlavyMf92jzv5xLub1pbtdxu+HqSqU/SrT72OqnM8AAAAAAABwDn19if/rsrqeOmr9qrfXOnLkiPMLuZP8j/4bbq6z6mrnW/ObagpyshugoCX24+6uTdbGXz2R2IdT/8D+eyU/9HnzTSus5S58+Kavr9vq2rQxcWyxznpsSf67Gqw666YVy3P+4dK+7k5r01O/spJ/pTTRcW9m8sN8dTdZK5Y3cVxKSmzPzq6nrF+5MM7Oxj4XJNb3/MT6LsoPG3dbbbXLrG1nUls3ZTNXW3u2t6S8vyX3r033bLS2uXDeTk4Ik9wPm0rsg/WTlTwedCWOB7964nnrSIrbb7I0vhsS1zo3lcC1jovnnKTkurm5LXHeaUqcd9JcLTqmb0wc063ENjzLa0oez+tcOreloqTONe9s98Q38/zZ9xVzXk+O81yvayQl7uu6k/d1WT6eJbdzYszelDhfl/p5g304d6bEtTnX0pPCfpcrfVbn4ub0JvxbssU6Wig/DAFTTvI+uC9xLH0qsV+5cS88keQ9svbLDO5RihHrN5fMtXvy72Syc21gTKXrdyCbktep2brfTu6nN990kzW/qdgm+ENhs881ur/K4vtEhvm7/pvmz7eaaqbyOT4zTPgH5BET/hU3JvwD3MOEf8gmJvzLLib8Ozsm/JscJvzLDSb8Qy4x4V9uJtBhwr/0MOHf+2PCPyb8K0RM+Hd2TPiX2nZhwj8bE/4BAAAAAADg/fT19SX/n9VnHbeeeuqovtb7nvdeJ/rg9XvZk9s4T5IfYNZba7XW/PkzrOR/6a//8R/8Ay5LfognsRx/ynrqaK8mSrAmmADp3ckFEvvljCarhg99IhXJCSwSy/HEWDv6zlh7//ODPeaSkueEWqs2cT6YkTgRFPXkZ+/I7YR/7/XOh/Kd7fC+k6O8Z93zwbuzS14DJZfjTyXGtjXJsf3ea57EBU+dc71Tk7zeKfGVbI+/xPXi0cTaclbWOccg5x0gJ7R/9h1PHM+OpnU8q0vewNUm99epcTwDcopraeRTd5t1xdJtzpPJS54j2vZst1oYcigY9r1b8npH718nDqb24fTs74MZ9n1JUvJ4mrjcmT9fx1Nd7zi/giTWb05pXSeW4/b99WTXN9fvQD4k9lX9HczY98MmmuQ56Z1reed9Q/u9WfZT5JZ539vS3x8mvjDJc03S+PN7YhAnzjfJv+5nHLupbF/X/vffEgCyhgn/SoOZqOxfbjiu3vGZb6v58uILn1Rve2i++sLQ+SpQTO77e3visK+0rFHz5T9+c7v6qS3/VR2KfUBFcWHCv9wyE4a97ExAYCYAzNfE3WaisHD4Q+qlzgQHuZpAzHzfZkLE/v4/qW4LBYPqRz5iT4RhJgR0S64mADTGTwRovj8zESATwGWX2d5m/JoJ+M2E/maCwKjz9Wwz25sJAgEA2ZDtCf8AAAAAAAAAAAAAAKUmfxP+AQAAAMDk9Fmdi5utteeY0ONsZq7ea21ntj8AAAAAyIppTgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFCsujelNdlfcqLyDUz2BwAAAABZU7ava3/qd2sAXHHsRExd9eTrKkrDZ+sj6vq7H1T9H+xVs+2Rn31DvXtXrQqUggWXDasPr/6+mqv96Xud96lr/u1yFcWtsWG6uru9WUVuxeNx9eX+V9T+/j+p5uu55vF41Bk1l6nh8CVqrsRi9vVfb+8f1YHTp1W3hcMfUmfU2Mcx8327xXwfx/tOqAMDg2qutmsoGFSrqirUYMh+HvD7VeTWcDSqxkbtcfHOc2ecxEbfUiNDQ2qumHHi9Z1v1+tVzdd9Pvu5+ToAYGrq6bHvMzvuX6+mq76uTm1tXakCAAAAAAAAAAAAAEpVt9VWu8zadia1j2MlJ87Ys73FYuoMAAAAANnVZ3Uubk55wr+ysplW257tFvP9AQAAAED2THMKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAYtS9KeXJ/pIa2jYw2R8AAAAAZFnZvq79qd+xAXDFsRMxddWTr6soLVcF31K3fuEp9cqrfqm6JfrnOnXlA3epP+0JqUApCnr/ov5i6dPqxz/2qOoWsz/dufbL6q6XAipKQ2PDdHV3e7OK/IrH4+rAwGn1eN9LaixmXxflmtfrVWfUXKZWV1+k5kokMqT2Hv2jGo1GVbd4PB7VfH/h8CWq28Zv15f7/6S6/f2ci/l+Q8Gg3dCFajBkPw/4/Sryy+zvo6N2I0P2fmC+Hhu1r6PN13PFjBszjvyBC1QzbqqqKlUAQGnp6elVO+5fr6arvs6+r2xtXakCAAAAAAAAAAAAAEpVt9VWu8zadia1j2OVzVxt7dneYjF/BgAAAIDs6bM6FzenPOEf9ysAAAAAkBvTnAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKDYdG9KfbK/splW2wYm+wMAAACAXCjb17U/tbs2AK45diKmrnrydRWl7YEFR9U7PvNtNV0vvvBJ9baH5qsvDJ2vAlNJ67Wvqmu+uFpNl9mfmjfcpA7FPqCitDQ2TFd3tzerKEwnT76mvtz/JzUajaq55vV61Y9cWa+GQkE1V8x66D36RzUej6tu8fv9al3th9Vsf3+xmH29OzBwWn315Ck1X9vX4/GooaD9ffsDF6jmeSBgrx/z+1AYzH4wPGyPGzN+Rp3xFR1+Ux12vu72fmM0zb3eeQQAKCU9Pb1qx/3r1XTV19Wpra0rVQAAAAAAAAAAAABAqeq22mqXWdvOpDiJxszV1p7tTKIBAAAAIFv6rM7FzSlP+Ddz9V5rewt3KgAAAACQC9OcAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoJh0b0p5sr/kxOQbmOwPAAAAAHKmbF/X/tTu3ABMmsfjUQN+vzrei3+Mqsv/7+MqSttn6yPq5jVfVtP1yM++od69q1YFpqIFlw2rP2u/S03XL3Z8Vb398QYVpamxYbq6u71ZRXGIRIbUvr6X1MiQ/TzXQsGgWlNzmRoK2c+zLR6Pqy/3v6L29/9JNV93S1VlpVpX92HV6/Wq2RaLxdSBgdNOB9V8befx/M71eyBwgV3nufl6rsYB0mP2k+Fh+34rGrU76oy76PCb6rDz9cnuV01zr3ceAQBKSU9Pr9px/3o1XfV1dWpr60oVAAAAAAAAAAAAAFCquq222mXWtjOpT6SxZ3uLxVQaAAAAALKhu63WWrpt8vcpZWUzrbY92y3m+wMAAACA3JnmFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMWir9Pa+ITzeJIa2jYw2R8AAAAA5FjZvq79qf1IKaAAeTweNeD3qxPx+s636/WqEzF/jvlzJxIKBZ1H6Xn6yCn1xnv3qChtj97yvPqpRd9V0/Xqy59Q6//5dhWYih5YcFS94zPfVtMV/XOdWv35r6soTY0N09Xd7c0qitNwNKr297+injxpX0flWihoX/995CP16rmuK90Si8XU430n1Gx9/zU1l6mXhi9Rz3U9nC0DA6fVyNCQGh1+UzXPC4XZ/j6nwdCFqrmf8Prsr5/rPgWFIRKxx5fZ30adDkXeUK+55moVAFBaenp61Y7716vpqq+z7y9bW1eqAAAAAAAAAAAAAIBS1W211S6ztp1J7eNYZTNXW3u2t1jMpwEAAADAbd1ttdbSbZO/R+H+BAAAAADyY5pTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFIO+TmvjE87jSSgrm2m1bWCyPwAAAADIh7J9XftT+5FSmNI8Ho8a8PvViXh959v1etWJmD/H/LkTCYWCzqPS8vSRU+qN9+5RUdpO/ug7qv+DvWqm/uGeTeqvT5WrwFTS86+Pqhdful/N1LL7vq/+tCekorQ0NkxXd7c3qygNsVhMPd53Qh0YGFTj8biaK9XV9viaUXO5eq7rX7cMR6Pq0d4/qpGhIdUt5vo8HP6Qemn4EvVc1+25EonY36/5vqPDb6pmvZjxUWjM+PA5DYYuVM9z1qvfuT/y+exfz9V4AgBgKuvpsd+n6bh/vZqu+ro6tbV1pQoAAAAAAAAAAAAAKFXdVlvtMmvbmdQ+jlU2c7W1ZzuTagAAAABwV3dbrbV02+TvT2au3mttb+HOBAAAAADyYZpTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFLq+TmvjE87jSUhORL6Byf4AAAAAIG/K9nXtT+1HSiEnPB6PGvD71Yl4fefb9XrViZg/x/y5EwmFgs4j5MLTR06pN967R0Vp+mx9RN285svqRB752TfU3x6tVNff/aDq/2CvOp75/XfvqlWBqaBx+oi6e8NydSLf67xP/e3xCnX98l+oF1+6Xx3vFzu+qt7+eIOK0tLYMF3d3d6sYqxIZEj9w4s96qXhD6nV1Rep57p+LBTxeFx9uf8Vtb//T6r5eq5UV9vjra72CjVX62/8dozFYqpbzPcRdsbHpeFL1EIfH2a9RKNRddRZL9HhN1Xz3O315Ta/cz/nc+77/IELVPPc3A8GApO77wMAAO/q6bHfd+m4f72arvq6OrW1daUKAAAAAAAAAAAAAChV3VZb7TJr25nUPo6VnFhjz/YWi6k1AAAAALilu63WWrptcvcmZWUzrbY92y3m+wMAAACA/JnmFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIWsr9Pa+ITzeBIa2jYw2R8AAAAA5FnZvq79qf1IqRLj9XpVn9OJ+AMXqB6PR51IwO9Xz/X7QqGg8whT2dNHTqk33rtHRWl69Jbn1U8t+q5qRP9cp9659svqrpcCqnFV8C116xeeUq+86peq8erLn1Dr//l2FZgKHlhwVL3jM99WjXPtT0HvX9TOzx5U5zdtVA3zz1d//usqSktjw3R1d3uzirEikSH14KHDqmGuZ6uqKtQZNZer5vq50MXjcXVg4LR6vO8lNRaLqdlm1l84/CH10vAl6rnuE9xy8uRrara+b/N9VFfb+5f5/oplfEzE7A9m/AxHo6p5Hh1+U33bPHd+vVCFgvZ9p9le5r7W3P+a7RUITO4+FgCAUtTT06t23L9eTVd9nX1f2dq6UgUAAAAAAAAAAAAAlKpuq612mbXtTGofxyqbudras73FYn4NAAAAAG7obqu1lm6b3H0J9yMAAAAAUBimOQUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECh6uu0Nj7hPD6HsrKZVtsGJvsDAAAAgEJQtq9rf2o/UipNXq9X9TmdiD9wgerxeNSJhIJB59HZec6z//mA368ChejpI6fUG+/do6I0nfzRd1T/B3vV//jN7eqntvxXdSj2AfVcHlhwVL3jM99WjX+4Z5P661PlKlDKev71UfXiS/er6e5Prde+qq754mrVWHbf99Wf9oRUlIbGhunq7vZmFWNFIkPqwUOH1XMx1+Hh8IfUqqpKtVicPPma+nL/n9RoNKpmm7m/Mevt0vAl6rnue9xivu/jfS+psVhMdVt1tb2/VV90kRoKvf99W6kw63N01G48HleHnfFlnkeH31TfNs9zNP5SZfZzMz7Nfbq5nzf394GAfb+dq3EMAEA29PTY79d03L9eTVd9XZ3a2rpSBQAAAAAAAAAAAACUqm6rrXaZte1Mah/HKpu52tqznUk2AAAAAGSuu63WWrptcvckM1fvtba3cCcCAAAAAIVgmlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUor5Oa+MTzuNzSE48voHJ/gAAAACgYJT98Xjf+07fHgoGnUdn5znPowb8fhXA5D195JR64717VJSWz9ZH1M1rvqx+r/M+dc2/Xa6ma8Flw+rDq7+vPv6rW9S7d9WqQClqnD6i7t6wXL3vh2vVjmcvVtNl/txH7n5Mfea5j6q3P96gojQ0NkxXd7c3qxgrEhlSDx46rKbK6/Wq1dX2er40fInq8dj3CYXOfP99fS+pkSH7ebaZ9Taj5jK1uvoiNVdOnnxNPe5837FYTHWb+T4vDX9INd9nsYyPXInH4+rwcFR953l07PPo8JvqqLO9srXd0mXePzDb1x+4QPU548CMh0DAfv+AcQAAKAQ9Pb1qx/3r1XTV19Wpra0rVQAAAAAAAAAAAABAqeq22mqXWdvOvO/Hsf5KcqKNPdtbLKbaAAAAAJCR7jbriqXbnCcTK5u5xNq8fZ0113kOAAAAAMi/aU4BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQiOYut1YvmWnNLCtzvvCusrLE12cusVZv2WsdZbI/AAAAACg4ZWcSnMcAcuzpI6fUG+/do6K0tF77qvriqwF110t23RL0/kX9lxuOq3fvqlWBUrTs6gH1hdcuUH99qlx1i9mf7rm2X13zb5erKA2NDdPV3e3NKsaKRIbUg4cOq26prrbXezh8iRrw+9VCNxyNqv39r6gnT9rXa9nm9XrVGTWXqdXVF6m5cvLka+rL/X9So856yJaqykq7VRVO7ecej0dFamKxmDo6atdsv7fjcTU6/KYad55Hhuz9vlCEgkHVbH9/wD7f+5z9wuwfgYB9HGGcAADc1NPTq3bcv15NV31dndraulIFAAAAAAAAAAAAAJSqbqutdpm1LcWPY5XNXG3t2d5i1TjPAQAAAAAAAAAAMLVMcwoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFxUdibBeQxkLB6Pq8PDUTXbYrGYOuo026LDb6rm+8zUi3+019Py//u4CgAA3NfYMF3d3d6sYqxIZEg9eOiwmi2hYFCtrra3R3X1RWqhM9ebx/tOqAMDg6pb14MT8Xq9al3th9Wqqko1V8y4OPnaa3ZPnlKzrarS/j6rqiqc2s89Ho+K7DDb+537uah9n2Kem/sgc99l9ot8M8cVMz78gQtUn7P/mP0oEPCrjCMAwNn09/er37rvO2q66uvq1NbWlSoAAAAAAAAAAAAAoFR1W221y6xtKX4cq2zmamvP9harxnkOAAAAAAAAAACAqWWaUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4KKyMwnO44ISj8fV4eGomm2xWEwddZpt0eE3VfN9ZltkaMh5hEJy7IQ93lY9+boKAADc19gwXd3d3qxirEjEvk48eOiwmisej0cNhz+kXlx9ker1etVCZa7fX+5/Re3v/5Oa7ev6UDCo1tRcpoZC9vNcMfdLr558TT158pRqvp5t5vuvqqpQg873H/D7VeTHcNS+X4+/bY//qPP8bWd/GIq8oZrn5tfzzYwncxzyBy5Qfc7xxxyHAgF7fJnfBwAobY899nO1v79fTdWiRQvV+vo6FQAAAAAAAAAAAABQqrqtttpl1rYUP45VNnO1tWd7i1XjPAcAAAAAAAAAAMDUMs0pAAAAAAAAAAAAAAAAAAAAAAAAAGBCNVZtg/MwBQ03NTHZHwAAAAAAAAAAwBRW9rvfPZfSj5SKDA05jwBk6tiJmLrqyddVAADgvsaG6eru9mYVY0Ui9vX9wUOH1Xyrrra3V/VFF6mhUFAtdCdPvqYe73tJjcXs67xsCQXt9VJTc5mar/Vkxs/J1+zvf2BgUI3H42q2eb1eNRS60K6zXsz6ML+OwmL2j9FRu9FoVH3bGTdDkTdUo1Deh2iae73zCAAAAAAAAAAAAAAAAMBU19fdaW166ldWb69lHTlyxPnqWDNnzrSsupusFcubrLk1TPcHAAAAAAAAAAAwlU1zCgAAAAAAAAAAAAAAAAAAAAAAAAA4h5q5Lda6ddut7du3W8eOHTvrkvy17etamOwPAAAAAAAAAAAAVtm+rv1nnMcAcuzYiZi66snXVQAA4L7Ghunq7vZmFWNFIkPqwUOH1ULj9XrVGTWXqVVVlarH41EL1cmTrzk9pUaG7PWcLaFgUK1x1lMoZD/PF/P9DwwM2j19Ws01M35CoQvVgN+vBp31Y56jOMTjcXV4OKrGYvb91KjT6PCbqvl9bu93TXOvdx4BAAAAAAAAAAAAAAAAAAAAAAAAAAAAADB505wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXle3r2n/GeQwgx46diKmrnnxdBQAA7mtsmK7ubm9WMVYkMqQePHRYLXQej0etrra366XhS1Sv16sWKrOe+/v/pA6cPq1mSygYVGtqLlNDIft5vg0M2N+3+f4HBgbVeDyu5otZX/7ABWrA71f9AbvmOYqTGV/Dw1E1FrPvw0adRoffVM3viwzZ++t4TXOvdx4BAAAAAAAAAAAAAAAAAAAAAAAAAAAAADB505wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXle3r2n/GeQwgx46diKmrnnxdBQAA7mtsmK7ubm9WcXbD0ah68uRr6sDAoBqL2dcrhS4UDKrh8IfUqqpKtVCZ9Xq874R68uQpNVvM+qmpuUwNheznhcKMv4GB0+pQ5A01MjSkFgq/36/6vF7VH7hANevXc55HDTi/D8UtHo+rw8P2+Cy0/QYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUBymOQUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4q29e1/4zzGECOHTsRU1c9+boKpOOq4FvqjVcOqh3PXqwCpaD12lfVZ48H1V+fKleBVDQ2TFd3tzerSM1wNKr297+iDgzY55t4PK4WKq/Xq1ZX29v/0vAlqsfjUQuNWZ8vO+u5v/9ParbWs1k/M2ouU6urL1ILVSQyZHfI7lDkDdWMz0Idj36/Xz3PGXfB0IWqeW5+3XOe/TzgPAcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKVjmlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOCisn1d+884jwHk2LETMXXVk6+rQDpar31V/dTf/0/1o+2LVKAU7P3SfvXZI5era/7NLpCKxobp6u72ZhXuGBg4bfe004FBNR6Pq4WqutoeD9UXXaSGQkG10Jj1aNbz8b6X1FjMvn50m8fjUcPhD6kXV9vrx+v1qoVuOBpVo8N2333+phoZGlKLhVnvPqde3/l2x3/dPPeNfQ4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArHNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZfu69p9xHgNTltfrVX1Oz8UfuED1eDzquYSCQefRWIdeOK3+43efVYF0/PZrO9Qrr/ql+vG7HlZfGDpfBYpR0PsXtf+R29QXX/ik+tH2RSqQisaG6eru9mYV2TUwYF/fnDx5Sh04bT8vVH6/X700/CG1qqpSnex1Xq6Z9dvf/yc1MjSkZkt1tb3/VF90kRoKnf26tlgMR6NqbDSmmufR4TfV0Zj99ajz9WI1/v7G67OvC83XA864N+M8EBj7HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuGeaUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4KKyfV37zziPgYz5/X71PI9HPZdg6ELn0fszf57588/F5/OqXq/dQvX0kVPqjffuUYFUBL1/UfsfuU01Vn13vbr5cJUKFKNlVw+o9391pWp8/K6H1ReGzleByWhsmK7ubm9WkVvxeFwdGDjtdNDuaft5ofE4153V1fa4uTR8iVqo15WRyJB68rXX7J60ry+zxawHs34urr5ILfTr7nQNR6Nq/G17HEeG7PVtxnV0+E31bfPc+f3Fbvx9nT9wgWr2j4Dz6+88D4x9DgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3jXNKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcFHZvq79Z5zHKCB+v189z+NRzyUYutB59P7Mn2f+/HPx+byq12sX7nr6yCn1xnv3qEAqll09oN7/1ZWq8VTXCvWTj/6dChSjX97+P9T5TRtV474frlU7nr1YBSajsWG6uru9WUVhiMfj6sDAafXkSfu6KDI0pBaaUDCoVlfb46m6+iK10MRiMfXVk6+p/f1/Us36zpbx66eqqlL1TPJ+ptSY9T08HFXfeR4d+zw6/KZqjP/1YjX+ftYfuEA14yHg/Po7zwNjnwMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUEqmOQUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC4q29e1/4zzuCT5/X71PI9HPZdg6ELn0fszf57588/F5/OqXq9dIOnpI6fUG+/dowKp+OXt/0Od37RRHS98x1Z1KPYBFSgGQe9f1P5HblPHe/GFT6ofbV+kApPR2DBd3d3erKKwxWIxdWDgtPrqSft6KRqNqoXCXNdXV9vj6+Lqi9RCu96Px+OqWZ8v9/9JzdX6rKqstFtV4dR+7pnk/dlUZ/aH0VG7ZnsOj9t+Q5E3nEc28+vm9xebUDDoPLKNv08f/+uh0NjnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUkmlOAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAi8r2de0/4zx+X36/Xz3P41HPJRi60Hn0/syfZ/78c/H5vKrXaxcoZk8fOaXeeO8eFZiMoPcvav8jt6kTWfXd9ermw1UqUAyWXT2g3v/VlepEwndsVYdiH1CB99PYMF3d3d6sojjFYjF1YOC0+upJ+zoqGo2qhaKqslINhz+khkJBtdBEIkPqyddes+usz1wx938XV9v7Z9BZT4FJ3hciNfF4XB0eHru/RIbscWBEh99Uze8fdfY7s/8Vi1Bw7H43/v2J8b9eqPspAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKA0THMKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABcVHYmwXkMIMeePnJKvfHePSqmhsbpI2rQ+xc1VR+dMah+pWWNOpGnulaoP+76GzVdu14KOI+Ac1tw2bDzKD1f/sffqR//2KPqRL7XeZ/62+MVaqqGYh9Qf32qXEVpa2yYru5ub1ZRWoajUfXkydfUgQH7PBmLxdR883q96qXhD6nV1RepHo9HLRTxeFwdGDitvtz/JzXqrN9cMeulqso+voeCQbshu2Z9Ij/MOBkeHjsuIkNDziOb2f9io2+pbzv/XK7HU7rMuDP8gQvUutorVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUjHNKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcFHZmQTnMYAce/rIKfXGe/eomBoap4+oG+7Yp1551S/VQhH9c536L5uWqZsPV6nAZHy2PqKuv/tB1f/BXrVQvPjCJ9XbHpqvvjB0vorS1tgwXd3d3qxiahiORtWTJ19TBwYG1VgspuZbdbU9LqsvukgNhYJqoTHr6+X+V9R8r0ev16uGQheqAb9fDTrrzzxHcYhEhpxHtqiz374dj6tmnMVG31LN183vy7Wmudc7jwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAmLxpTgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgIvKziQ4jzEFDUejavztuIqxos76eTuenfXz/H8Oqf/Hj46omFqC3r+oG/7xRfVTi76r5suLL3xSveeRG9RfnypXgXSY8f2LpU+rH//Yo2q+PPKzb6h376pVMbU0NkxXd7c3q5jazPXvyZOvOT2lxrN0vTdZfr9fvTT8IbWqqlL1eDxqoRm/HgcGBtVYLKbmWygYVP2BC9SAs379AbvmOUrD+PtaMw5HnZr9Ozr8pmpEhuz7sclqmnu98wgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgMmb5hQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALio7I/H+844j8eIx+NqdPhNtdRFhoacR0DuHDsRU1c9+bqKqW3Z1QPqvyzfrPo/2Ktm2y92fFW9579fqQ7FPqACbrrv70+oX2lZo2Zb9M916soH7lJ/2hNSMTU1NkxXd7c3q8DZDAyctnva6cCgau6Lcs3j8ahVVRVqOHyJGvD71UIVi9nXt++sT2c9Fur9VigYVL2+8+16var5us9nPzdfR2kbjkbV+Nv2fm/G86jTGTWXqwAAoPSNjIyo/f2vqONVVNrX6ZUVdoFi1NNz9vefy8t9ajgcVgEAAAAAAAAAAAAAAAAAAAAAAABkbppTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgorJ9XfvPOI8B5NixEzF11ZOvq0BS4/QRdcMd+9Qrr/ql6pbon+vUf9m0TN18uEoFcmHBZcPqw6u/r/o/2Ku65cUXPqne9tB89YWh81VMbY0N09Xd7c0qxhqORtW+4y+pVVUVTitVj8ejTlUDA6ftnnY6MKjG43E11/x+v3pp+ENqsW2nSGTI7pDdocgbqnle6ELBoGrWtz9wgRpwtov5eiAw9jmA7Onpsa8nX+7vV0dH7Psp8/V0hcNhtbzcp1ZU2ufHygr7uBsOX6KWl5erAIDiNuKcPx57/OfqgQPPqudSX2e/z3brrZ9WzfkDKEQ7duxU9+7rUkdHR9WJVFTY1z+33/Y5tb7eHu8AAAAAAAAAAAAAAAAAAAAAAAAAUjfNKQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcFHZvq79Z5zHAHLs2ImYuurJ11XgvYLev6gb/vFF9VOLvqum68UXPqne88gN6q9PlatAPpjx/YulT6sf/9ijarp+seOr6u2PN6jAezU2TFd3tzerGCsSGVIPHjqsjuf3+9WLq+31WFVVqXq9XnWqGhg4bfe004FBNR6Pq7ni8XjUqqoKNRy+RA04263YDEej6pAzLs3z2OhbamTI/nqxCQWDziNbMHSh88g2/tdDobHPAVjWoUPPqc8cOKD29PSqo6Ojar5UVIw9/tbX16uzZ89SK51fBwAUtke3/lg9cOBZNVXmPPDNNd9QgUJirp+2bv2Jmiqfz6d+85v2+Ob6BgAAAAAAAAAAAAAAAAAAAAAAAEjdNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZfu69p9xHgPIsWMnYuqqJ19XgbNpnD6i7t6wXE3XL3Z8Vb398QYVKAQPLDiq3vGZb6vpWnbf99Wf9oRU4L0aG6aru9ubVYwViQypBw8dVifL6/WqVVUVanX1RWrA71enmng8rg4MnHY6aPe0/TzX/M52uDT8IbWqqlL1eDxqsRuORtXYqH09/c7zmP08NvqWar5utk+xMtvzPGf7+QMXqGZ7+pz90eyXPt/Y50AxeubAAXXHjl3q4KB9XC024fAl6g03NKnXzZmjAgAKw8iI/b7bl778FTVT31zzdTUcDqtAIfjWffb7bv39r6jpuuWWT6vznOsaAAAAAAAAAAAAAAAAAAAAAAAAAJM3zSkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBR2b6u/WecxwBy7NiJmLrqyddV4GweWHBUveMz31bTFf1znVr9+a+rQCHo+ddH1Ysv3a+m6xc7vqre/niDCrxXY8N0dXd7s4qxIpEh9eChw2qmPB6PWlVVYbeyUg2Fgqr59akiHo+rAwOnnQ7aPW0/zzWzPaqr7f2iqsp+PlWY8W5EhsY+jw6/qZrtNhqzr9djTouV2e8Cfr9qBEMXOo9s5tff+f2Bsc+BbOrv71cf3fpjtb//FbXU3NDUpN5666dVAEB+9fT0qh33r1cz1bpqpVpfb78PBxSCljuXOY8ys2jhAruLFqoAAAAAAAAAAAAAAAAAAAAAAAAAJm+aUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4KKyfV37zziPAeTYsRMxddWTr6vA2fT866PqxZfuVzP1ma89qO56KaAC+dA4fUTdvWG5mqnon+vU6s9/XQXeq7Fhurq7vVnFWJHIkHrw0GE126oqK9VQ6EK1qsp+7vV61akiHo+rAwOnnQ7aPW0/zxWPx6NWVVXYdbaP2S4Yy2y34eGoakSj9vO3nV83vy86/KZqDDu/z/x6sTH7qc+pGT/+wAWqEQoGnUe2QMCvmt8PvNczBw6ojz32c3V0dFQtVfV19nVra+tKFQCQXz09vWrH/evVTLWuso/v9fX28R4oBC13LnMeZWbRwgV2Fy1UAQAAAAAAAAAAAAAAAAAAAAAAAEzeNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZfu69p9xHgPIsWMnYuqqJ19XgfdqnD6i7t6wXJ3IL3Z8VX3x5f9Nvef/fFT1f7BXHe+Rn31DvXtXrQrkwwMLjqp3fObb6njRP9epKx+4S/2bi99Qv9KyRp3Isvu+r/60J6QCSY0N09Xd7c0qxopEhtSDhw6r+eL3+9VQ6EK1uvoiNeB8faqIx+PqwMBpp4N2T9vPc8Xj8ahVVRV2KyvtVtmFu8x+aMRi9n3CqFMjOvymasbJ206j0ahaLLxer+pzasabP3CBaoSCQeeRLRCwjwfm96O4PXPggLp160/UqWLWrKvVL971BRUAkF89Pfb7Zx33r1cz1bpqpVpfb7+vARSCljuXOY8ys2jhAruLFqoAAAAAAAAAAAAAAAAAAAAAAAAAJm+aUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4KKyfV37zziPAZyD1+tVfU5T5fWdb9f555//zyH1//jRERV4rwcWHFXv+My3VSP65zr1XzYtUzcfrlKNxukj6oY79qlXXvVL1Xj15U+o9f98uwrkQ8+/PqpefOl+1XjxhU+qtz00X31hyD5uGgsuG1YfXv191f/BXtX4xY6vqrc/3qACSY0N09Xd7c0qxopE7OuRg4cOq4XG4/GoVVUVdisr1VAoqJpfL3XxeFwdGDjtdNDuaft5rpj1HQra699sF7M9zHUu8ms4GlXjb9vjxogM2fu7EYvF7I6+pRrjf1+hmuj+LBi60HlkC/j9qhm/Pp/9+xmv+XHo0HPqgw/9SJ1qFi1cYHfRQhUAUBhWfOludXR0VE3X2rXtamWFfZ0MFIKOjvVqT+/Y99FSddcXPq/Onj1LBQAAAAAAAAAAAAAAAAAAAAAAADB505wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXle3r2n/GeQxMmtfrVX1OU+UPXKB6PB41VaFg0HmUGs959r8v4Per+fb0kVPqjffuUYH36vnXR9WLL92vvvjCJ9XbHpqvvjB0vnouDyw4qt7xmW+rxj/cs0n99alyFciFxukj6u4Ny1XjkZ99Q717V616LkHvX9RfLH1a/fjH7P0l+uc6tfrzX1eBpMaG6eru9mYVY0UiQ+rBQ4fVYmOuC6uqKpxWquZ6tdTF43F1YOC000G7p+3nueZ3rrNDoQvVqkp7e4RC6V2/ozDEYjF1dNSuERmyjx+G+X2x0bdUY/zvKzTmvnT8feL4+9YZNZerSM/pQfv49K1v2fclo6Oj6lTTumqlWl9vX7cCAArDjh077e7cpabqhqYm9dZbP60CheTQoefUBx/6kZqqcPgS9Ztr7PfvAAAAAAAAAAAAAAAAAAAAAAAAAKRumlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOCisn1d+884j5EBv9+vnufxqJPlcX6/P3CBmqqA8+81f85kec6zf7/555EfTx85pd547x4VSGqcPqLu3rBcfeRn31Dv3lWrpuuz9RF1/d0Pqo//6hY10z/3XFqvfVXtePZiFYVp2dUD6ubDVWq2PLDgqHrLTY+rKx+4S/1pT0hNlxlna764Wl123/fVTP9clIbGhunq7vZmFWNFIkPqwUOH1VLh9XrVqqoKu5WVaigUVEtdPB5XBwZOOx20e9p+ni+hoL3+g6ELVfM8EEjvvgbFKRaLqaOjdo3IkH08MqLDb6pmPL/tNBqNqrnWNPd65xHS0dGxXu3p7VVzpb6uznlkq68f+9zo6Xn/1+XW6/7B97+nlpeXqwCAwrJ3X5d64MABtb//FXW8igr7PuO6OdeqixYtVIFCdujQc+qOnTvVica3z+dTZ8+epd56y6dVrl8AAAAAAAAAAAAAAAAAAAAAAACA9E1zCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXFS2r2v/Geexqzwejxrw+9VU+QMXqObPmSyf16t6naYqFAo6j4Dse/rIKfXGe/eoQFLrta+qf/pfPvWnPSHVLVcF31K/dfPv1U8++neq28y/54m2/0et/+fbVRQWs532/WuHWv35r6vZ8sCCo+q/7JuhDsU+oLqlcfqIeu2MIbXj2YtVTG2NDdPV3e3NKsaKxWLqH/7Qo0aG7P2nVJn7i1DQvu6vqqpQzX1AuvcRxSIej6uRiL2dB06ftjswqJpfzxe/c/8YcO4Hzf2k+Tr3azib4WhUjb9tj19zXBt1agxF3nAe2cyvm99/Lk1zr3ceIRXPHDigbt36EzVb6uvq1BtuaFJnz56lZsvpQfu4OXja7siIfR3a39+vjoyMquZ5rl4XAAAAAAAAAAAAAAAAAAAAAAAAAAAAgMIzzSkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBR2f/6X5EzzuOz8vm8qtdrF4B7nj5ySr3x3j0qUEpar31VXfPF1eo/3LNJ/fWpchWFYfx2Wnbf99Wf9oRUoBQ0NkxXd7c3q5icSGTI7pDdocgb6nA0qsbjcbXU+P1+NRS6UK2uvkgNOF8vdWb7DgycdjqoRp2vFwqznXzOfao/cIEaCgZV7mORDnNcGx4eO97NcXBGzeUqUtO2+mvq4KB9PHGLz+dTv3jXF9T6+joVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArNNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZWcSnMcAcuzpI6fUG+/dowKl5Ldf26FeedUv1Ud+9g317l21KgrD+O30ix1fVW9/vEEFSkFjw3R1d3uzCncMR6PqUGRIjUTesDtkP4/H42qp8Hg8alVVhRoKBtWqqkrV/HqpMtszYra3s53Ndo8646FQ+f1+9TxnOwVDF6rmufl1z3n284DzHED6Dh16Tn3woR+pbqmosI/Dra0r1UrnOQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUqmlOAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAi8rOJDiPAeTY00dOqTfeu0cFSsFVwbfU/3jwTtV49eVPqPX/fLuK/GI7YSppbJiu7m5vVpEbsVhMjUSG7A45jbyhml8vFX6/X62qqnBaqQacr5e6eDyujt/e0eE3VfO82Hg8HnX8dgyGLnQe2cyvm9/vOe/s/xwwlfzwwYfU5547rGbK5/OpX21dqYbDYRUAAAAAAAAAAAAAAAAAAAAAAAAAAAAACt00pwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEVlZxKcxwBy7Okjp9Qb792jAqWg9dpX1TVfXK2O9w/3bFJ/fapcRX6wnTCVNDZMV3e3N6soDPF4XI1EhuwOOY28oUajUbXYeTweNRQMqlVVFWooZD/3er3qVDHsbNfosN13n7+pmudmfJQav9+vnueMC6/vfLvjxoEZL0YgYP9zZjwBhWxkZET90pe/orrllls+rc67oUlFdvT396sjI6PquVRU2ue1ygq7SI/Zb/r7X1HHC4cvUcvLC/v+rKen13k0Vnm5Tw2Hwyre30TrsVj2t3MdR+rr65xHyIfTg4Pq4Gm7k1UsxyG3TLQfToTjXGFLdXtOtfGebec67nBeAAAAAAAAAAAAAAAAAAAAAAAAU8U0pwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEVl+7r2n3EeA8ixYydi6qonX1eBUvDbr+1Qr7zql+p43+u8T13zb5eryI9zbadHfvYN9e5dtSpQzBobpqu725tVFJdIZMjukN2hyBuqeV7s/H6/GgpdqFZVVqqhUFCdqmIx+zp5dNRuNBpVR52vR4ffVM1z8/unCq/Xq/qcGkFnHBkBZ3x5PB6759k1Xwey4dCh59QHH/qRmqmKigp13dp2FZkx2+fQc3b7+/udvqJmKhy+xGlYra+vU2fPmqWWl5er+TIyMqLu29elpsp8X7Nn29/PZJl/r1nvZjv09PSqo6Oj6mTV19nrdZbzOszrqXT2F7dN9PrNuBkcHFQny+zXZnxcN2eOap7nyl5nHIw6399kVVTar9+87lSN3w/NOEh1PZr9rb6uXr1hXpOarXFguHUc8fl86jvHCWccp7teM2W2Q09Pj5oq8/rNcSJfzPZ45plnVfO8p9f+/txitt+lzvdr9gvz/f+Ns13zvT7GO+3sZ+8eh+3tne7x+FzM8dpXPnZ91dfb+63Zj7N9fjTf94FnDqipSvf8l2252p5mO5r1cN1116qFMr7fPS/b+/tk+Zxxd90c+/tJdxy+u/6d42ivvR3cOi8UynUkAAAAAAAAAAAAAAAAAAAAAABApqY5BQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALirb17X/jPMYQI4dOxFTVz35ugoUs6uCb6n/8eCd6kRefOGT6kfbF6nFJuj9i3rjZX9Wf9oTUovFZLfTqy9/Qq3/59tVoJg1NkxXd7c3qygtw9GoOhQZUiORN+wO2c/j8bharKoqK9VQ6EI1GAqqAb9fxVixmH19PTpq953nTs14iA6/qRpmvExVoaA9rgyv73y7Xq9qjP99gYA9Dj0ejwokPbr1x+qBA8+qmVq0cIHdRQtVTM7IyIi6b1+Xutfp6Oiomms+n0+dPXuWarZnZUWFmiuPPfZzdV+XvT7StXZtuzrR6z89OKju2LFTdWt/OBe39pf+/n7VjJtcvf76ujr11ls/rYbDYdVtzxw4oG7d+hM1Xbfd9k/qdXPmqOPlaz+cM+da9dZb7PVYXl6upsqM43177ddt1lu2X3+Fs1+Z12+OG9m24kt3q+l+f2b8trauVHPFbJcdO3apg852KxT5Pv739PSq5njc02s/LzSzZl2tmvU00XElXT988CH1uecOq+nqfHiz8yg/8nV+nYg5Xi1aZJ9/3d5u52LO19+67ztqum5oalLN+Xci+TovmOOIOX7Mu8F+vQAAAAAAAAAAAAAAAAAAAAAAAMVmmlMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOCisn1d+884jwHk2LETMXXVk6+rQD41Th9RH7n7MfXiS/erheY/fnO7+qkt/1Udin1AzZVlVw+o8/7LCfWTj/6dmitmO224Y5965VW/VAvNU10r1JafXqPmejsB79XYMF3d3d6sYmqJxezrrUhkyO6Q08gbqvn1YuPxeNSqqgo1FAzaDdn1er0q0mPGxejo2PFhxo8Rj8fV6PCbqjHq/PPFOr7SZcZlwO9XDX/gAtX8us8Zn+PHqRm/KG7fuu/ban//K2qm1q5tVysr7OMd3l9PT6/66NYfq4ODg2qhWrRwgd1FC9Vs6+hYr/b02uspXa2rVqr19XXqaWc979ixUz1w4Fk1X8LhS1TzOsvLy9WJmNe/9VF73GS6fjLl8/nUr7barz8cDqtuMdtpx85darrGj9+REft+fd++LnWv09HRUTXXUl2P5vU/9vjP1XyPY+O22/5JvW7OHDVbWu5c5jzKTOfDm51H2dHf36+a47xb59tcG38cdYsZxzt22Pv3vi57Pyw2NzQ1qbfe+mk1U26d/7I9vifi1nE72yqc69Xbb/uc6vb4Hs9c93Xcb2/fdNXX2a+z1TlfGOZ489+d9f/cc4fVfJsz51rVrGcAAAAAAAAAAAAAAAAAAAAAAIBiMc0pAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwUdm+rv1nnMcAcuzYiZi66snXVaAQBL1/UTs/e1Cd37RRzbfvdd6nrvm3y9V8+eXt/0M16yV8x1Z1KPYBNVfMdtrwjy+qn1r0XTXf7vvhWrXj2YtVoBA0NkxXd7c3q8B7xeNxNRIZsjtkNzr8pmqeFxuv16uGQheqVZWVaigUVD0ej4rcGo5G1fjb9rgzxo8zMy7NODTeNl93/pxSZ8axz6kZt/7ABaoR8PtV8+ue8+yaryO3Wu5c5jzKTEVFhbpubbuK9/fMgQPq1q0/UYvNrFlXq7ff9jm1vLxcdVtHx3q1p7dXTVfrqpVqT0+Pundflzo6OqoWivq6OrW11X69xsjIiLpjxy51X5f9+guNz+dTv/nNb6iVznEhUzt27LS70/7+07Vo4QI1HA6rj279sVpo48AcT7+55uvq+P3LjF+zXgrt9Rt3feHz6uzZs1S3uXX+6nx4s/PIXf39/ep3neNYoW6nyTL7z6JFC9VMmeNax/32+unvf0UtVhMdv9Pl1vkvW+N7PLM9H3zwR2qmrztfbrvtn9Tr5sxR3dbTY68XM+7TZcbbXXfZx9nHHv+5euDAs2qhcvs4AgAAAAAAAAAAAAAAAAAAAAAAkG3TnAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABeV7evaf8Z5DCDHjp2IqauefF0FClHrta+qa764Ws2V6J/r1DvXflnd9VJAzZeg9y9q/yO3qcaq765XNx+uUvPls/URdf3dD6r+D/aq2fbqy59Q73jgVvXXp8pVoJA0NkxXd7c3q0A6IpEhu0N2hyJvqMPRqBqPx9Vi4fV61VDoQrvBoN2QXfPrKA6xmH1fMTpq13jn604N8/XY6FuqUazjebLMODe8vvPtjhvv439fIOBXPR6PirPr6bGvPzvut6+PMzVnzrXq7bd9TsXZPXPggLp160/UYhcOX6J+c803VLd1dNjjs6c3s/sln8+njo6OqoXulls+rV4aDquPbv2xOjg4qBa6+jr7/YHW1pVqpnbs2Gl35y41XcU2DmbNulq99dZb1K2P2uMg0/0hVyoqKtR1a9tVt7Xcucx5lJnOhzc7j9zR39+vftc5fhXLeDuXu77weXX27Flqptw6vhcKt6+D3Fo/bo/viXzrvm+r/f2vqMXuttv+Sb1uzhzVLW5df5vj68jIiFpsx5m1znmh0vk+AAAAAAAAAAAAAAAAAAAAAAAACtU0pwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEVl+7r2n3EeA8ixYydi6qonX1eBQtY4fUR95O7H1Isv3a+67T9+c7v6qS3/VR2KfUDNt2VXD6j3f3Wlavxix1fV2x9vUPPtquBb6tYvPKVeedUvVbc91bVCbfnpNWqhbCfgbBobpqu725tVIBuGo1F1KDKkmueRyBtqLGZf9xULr9erBvx+NRS6UPW/8zyoYmow4zn+dlw1os7X346P+/rwm2p83Nff+XPGfb1YNM293nmEszl06Dn1wYd+pGZq0cIFdhctVDFWT0+v2nH/erXU3NDUpN5666dVt3R02Ourp9def1OFz+dTR0dH1WK1dm27WllRoaZrx46ddnfuUqeaYh8Pt932T+p1c+aobmm5c5nzKDOdD292HrnjW/d9W+3vf0UtFT/4/vfU8vJyNV2luj/fcot9/pt3g30+zJRb5z+3x/d4jz32c3VfV5daalpX2e8r19fXqZkq9evBycrWdSMAAAAAAAAAAAAAAAAAAAAAAIDbpjkFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuKtvXtf+M8xhAjh07EVNXPfm6ChSDoPcvaudnD6rzmzaqmfpe533qmn+7XC00v7z9f6jjv9/on+vU6s9/XS00Dyw4qt7xmW+rmbrvh2vVjmcvVoFi0NgwXd3d3qwC+RCL2dd9w8NRNTI0ZDfyhhqN2l8vNqFgUPUHLlADfr/qD9g1z4H3M+yM//jbcdUw+8Xb8XFfH35TjY/7+jt/zrivu6Vp7vXOI5zNjh077e7cpWaqddVKtb7evt6GbWRkRP3Wfd9RBwcH1VLl9jjo6Fiv9vT2qiguixYusLtooZout49XyK1Zs65Wv3jXF1S3tNy5zHmUmc6HNzuPMvPMgQPq1q0/UUtFRUWFum5tu5oucz5sW/01dXR0VC0V31xjv88YDofVTLl1/nNrfI/X02O/ro777ddZqtwa/8ZUWW/n4vP51I0/eEAFAAAAAAAAAAAAAAAAAAAAAAAoVNOcAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAF5Xt69p/xnkMIMeOnYipq558XQWK0ckffUf1f7BXTdV//OZ2dd4PPqEWquH/5/90Hp3dsvu+r/60J6QWmky304svfFL9aPsiFSgmjQ3T1d3tzSpQyCKRIbtDdqPDb6rmeTweV4tNKBhU/YEL1IDfr/oDds1zIBuGo1E1/vbY/SfqfP3tcftVLGbfp8VG31KNa6652nmEs9mxY6fdnbvUTLWuWqnW19epsLm9nsfz+XzqpeGwGnZaXm5/fWRkVO3v71dfdjo6an/dbfV19vZvbbXHQ6Y6OtarPb3p3Rchv9waD9nej5AbnQ9vdh65o+XOZc6jzLj1ur5137fV/v5X1ExVVFSo1825Vq2vr1cna2RkRDXHf+P04KA6eNquMdFx9oamJvXWWz+tpmvvvi718cd/rmbKnP9mz56lmuuPyopKdbLeOS8668vo6Rm7PiY6f4bDl6jfXPMN1S1unf/c3u8Mt8f7eGb8m/VrrnOMd7ebvT2yfZ1wyy32+J93g70/pMuMq4777e071X1zzddVc/0KAAAAAAAAAAAAAAAAAAAAAABQaKY5BQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALirb17X/jPMYSJvX61V9Tt0SDF3oPHKHeX3m9brFc55HDfj96mQ9feSUeuO9e1SgmHy2PqJuXvNlNVPhO7aqQ7EPqIVist/nL3Z8Vb398Qa1ULi9nT5+18PqC0Pnq0AxaGyYru5ub1YxViQypP7hxR7VXC+Z6zBzfeP12V9P9XoH7hqORtXosN3IkL39hoffVKPOrxcrvzO+zDj0By5QQ8Gg6nPGodvX8wAy19GxXu3p7VUz1fnwZucR3mvFl+5WR0dH1UxVVFSot97yaXX27Flqqg4dek7dsXOn2t//iuqW1lUr1fr6OjVdbo/TyTLrefYse/2a9RwOX6KWl5erpwcH1Z4e+7psx45d6qDz9Xwxr7O+rl6d7Ovft69LdXs8ZHp82LHDHqc7dtrrN1d8Pp9q1p8ZD+FLw2qlM05GRkbUnh57nGZrv0rV+HFs9sdzvX4zDtze79w6Lhgtdy5zHmUm0/Fp1t+XvvwVNVNz5lyr3n7b59RS8cMHH1Kfe+6wmi5zHDPjyRzPSo1b5z+3r8/McaLjfvv1uWXWrKvVW2+9RTXHp8ky+6E5fu116vb117q17Wq6srX+JsvsP++eF+zrhIpK+/sz6z1X1ze3ONez825oUgEAAAAAAAAAAAAAAAAAAAAAAArNNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZfu69p9xHuM9/H6/ep7Ho6bL4/zz/sAFaqZCwaDzKDM+n1f1eu0iP54+ckq98d49KlBMHr3lefVTi76rZmrVd9ermw9XqW75bH1EXX/3g6r/g71qoXnkZ99Q795Vq7rF7e103w/Xqh3PXqwCxaCxYbq6u71ZxViRyJB68NBhdbLM9bLPuZ4017sB5+te53rTPEduxONxdXg4qkaG7O0bHX5THY7aX4/FYmqxM/dHXt/5dp3xaMaduR8Lhdy5jwIwsY4O+3q+p9ed6+3Ohzc7j5B06NBz6oMP/UjNVDh8idq6aqVaXl6uuuXRrT9WDxx4Vs3UnDnXqrff9jk1XW6P0/F8Pp86e/Ysdd4NTWo4HFZTNTIyon7rvu+og4ODarZk6/V33G+v9/7+V9RM/eD731PTHbc7duy0u3OXmi2zZl2tXjdnjmrWa7p++OBD6nPPpXbdni63X7/bx4W7vvB5NdPXZbTcucx5lJlMz189Pfbxyew36TL788YfPKCWmhVfulsdHR1V0+X2OCpUbp3/3L4+c/u4dttt/6Sa45Zb+vv71e866zHTcWdkOv7cOl6cS0VFhXqdcz025zp7/VY6X0+X29t/0cIFdhctVAEAAAAAAAAAAAAAAAAAAAAAAArNNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZb/73XNnnMeT4vF4VH/gAjVTAb9fNX9uujzn2f+8+fOAYvD0kVPqjffuUYFicvJH31H9H+xVx4v+uU59+Of/X/UrLWvUiTzVtUL95KN/p7rtquBb6tYvPKVeedUv1Xwx62flA3epP+0JqW4713Z69eVPqJ07/nf1nv/zUXWi3//iC59UP9q+SAWKQWPDdHV3e7OKsSKRIfXgocNqtvid6/SAcx/h9XrVUDCo+nz2c/N15IbZ/tFoVB2NxdTo8JvqsPP1eDyuFjszvnxOvb7z7TrPx9+fBgLu3K8CU0FHx3q1p/fs15Gp6nx4s/MISY9u/bF64MCzarp8Pp+6bm27Wl5errptZGRE/dZ99v3I4OCgmi7zujf+4AE1XW6PU/O65t3QpN7g1O31+syBA+rWrT9R3ZKr19/TY6/vjvvt9Z+p1lUr1fp6+746VTt27LS7c5fqljlzrlUXLVqoVlZUqG457exHq1d/TXVbtl+/OS60Oa9/dHRUTdeihQvsOq83Uy13LnMeZSbT85db+3t9nb1/tLba+0upKZTtVSzcOv+5vb7c2o6zZl2tfvGuL6jZ4vb57IYm+7x7662fVlPl9usxzPHDXBfMnj1LdZvb54VcjQMAAAAAAAAAAAAAAAAAAAAAAIB0TXMKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABcVHYmwXkMIMeePnJKvfHePSpQDD5bH1E3r/myOt5TXSvUlp9eow7FPqA2Th9RN9yxT73yql+q44Xv2Kqafy5bHlhwVL3jM99Wc+XFFz6p3vbQfPWFofNVt6W7na4KvqVu/cJT6kTb6eN3Paxm6/UDbmpsmK7ubm9WMVYkMqQePHRYLRShYFD1+uzjjNfrVc3XfT77ufk6csOMl3g8rg5Ho2osFrM7ap9HRs1zp6XC7/er53k86vjxaZhxajBeMRV0dKxXe3p71Ux1PrzZeYSkFV+6Wx0dHVXTtWjhAruLFqrZ9syBA+rWrT9RM/XNNV9Xw+Gwmiq3xukNTU3qokX2+iwvL1ezreXOZc6jzOTr9bet/po6ODiopqt11Uq1vr5OTdWOHTvt7tylpqu+zv7333b759TKigo129wax/l6/T988CH1uecyu/9w+3jm1v6V6fnLrfFp9vNbb/20WmoKZXsVC7eOG26tr54e+3V03G+/rkytXduuFttxOBy+RP3mmm+oqXJrPVY46+2Ld31eTfc6K12Pbv2xeuDAs2q6zHmttdW+TgAAAAAAAAAAAAAAAAAAAAAAACg005wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXlZ1JcB4DyLGnj5xSb7x3jwoUg0dveV791KLvqsZ9P1yrdjx7sTqRoPcv6oZ/fFEd/+es+u56dfPhKjXbPlsfUdff/aDq/2Cv6rZHfvYN9e5dtWq2jd9O0T/XqRv+r9vVc20n44EFR9U7PvNt1Zjs9gYKQWPDdHV3e7OKsSKRIfXgocNqsQoFg6rXd74a8PtVv9NAwK7H41GRW8PRqBp/O67GYjF11Gk8bn89OvymakSG7PFZqrxer+pzOl4wdKHzaCzz+80/b3jOs8e3Gf9ALnV02NfxPb3uXE93PrzZeTS19ff3q9+67ztqptaubVcrKyrUXGm5c5nzKDO33fZP6nVz5qipcmuctq5aqdbX2/dZucLrt2X6+nfs2Gl35y41XYsWLrC7aKGaK7x+m9uv363jVKbnL7fWT32dvX+0ttr7S6lxa3v94PvfU8vLy9VS5dbx163rs2If54cOPac++NCP1Eylu157euzt2XG/vX3Tle/jhVvrs9SPewAAAAAAAAAAAAAAAAAAAAAAoPhNcwoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFxUdibBeQwgx54+ckq98d49KlAMTv7oO+qfhy5W73jgVvXXp8rVVC27ekD9l+Wb1Wf+x3z1k4/+nZorVwXfUrd+4Sn1yqt+qaYr+uc6deUDd6k/7QmpuWK2U3//36r3PHKDmu52WnDZsPrw6u+rz7/YqM77wSdUoJA1NkxXd7c3qxgrEhlSDx46rJY6j8ejBvx+1R+4QPV5varf+XogYNf8fhSG4WhUjb8dtxu3a75uxGIxu6P2+d145593/rmpJhQMOo/GMvvB+PFu9guvU8PnO/vXMbV1dKxXe3p71Ux1PmzfH0x1hw49pz740I/UdIXDl6jfXPMNNdfcGh+LFi6wu2ihmiq3XkfrqpVqfb1935crxf76H3vs5+q+ri41XZm+/h07dtrduUtNV6bjMV3F/vqfOXBA3br1J2q63H79LXcucx5lJtPzl1vHfeOba76uhsNhtVS4tb3ytR/kmlvnD7euzwrluiBdIyMj6pe+/BU1U+me13p67PXXcb+9PtNVX2f/e1tb7deRa6XyfQAAAAAAAAAAAAAAAAAAAAAAAJzLNKcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMBFZb/73XNnnMdZ4Q9coHo8HjVbznP+fL/fr2ZbIGD/e7L9faG0PX3klHrjvXtUoJB9tj6i3nzdMbXlp9eoQ7EPqJlqnD6ibrhjn9q84SbVrT9/sszr2L1huZquR372DfXuXbVqriy4bFi9+eMvqff89ytVt9Zj0PsX9RdLn1Y/teW/qrneTkAqGhumq7vbm1WMFYkMqQcPHVYxlrneDzj3Geb+xuf1qub+g/uD4jYcjarxt+OqEXW+/nZ83NeH31Tj474+GoupMadTzfj9Zbxg6ELn0VihYNB5NBb7VXHp6Fiv9vT2qpnqfHiz82hq27Fjp92du9R0zZlzrXr7bZ9Tc82t76O+rk5tbV2ppsqtcdq6yv7319fbrydX3FqPU/31u/U6Fi1cYHfRQjVXDh16Tn3woR+p6crX6+/psfe/jvvt/TFdbr/+ljuXOY8yk+n5q7+/X/3Wfd9RM+Xz+dR5NzSpc66bo1ZWVKjFqm3119TBwUE1U7NmXa3+ozOewuGwWircOv+5dX32rfu+rfb3v6KmK1/nM8OtcZju9+HW8TTT6yu3ZHocLpTvAwAAAAAAAAAAAAAAAAAAAAAAYCLTnAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABeV7evaf8Z5jAIWCgadR7kRDF3oPMoNn9erep3mis+Xn3+v8fSRU+qN9+5RgUIW9P5FHYp9QM22XP/7jAcWHFXv+My31XS9+vIn1Pp/vl0FkD+NDdPV3e3NKsaKRIbUg4cOq8iMx+NRA36/6g9coJrrXb/z9UDArvn9mBrM/jZeNBpV347HVSPuPI8Ov6mON+z8c+b3TRVmPzpv3P7j9Z1vd9z93UT3m57zxu6vyExHx3q1p7dXzVTnw5udR1PbY4/9XN3X1aWma9HCBXYXLVRzbe8++/U//rj9/aSrvq5ObW1dqabKrXHausr+99fX268nV3bs2Gl35y41XVP99bv1OvK1X/X02OO34357PKeL1z9Wy53LnEeZcev8teJLd6ujo6Oq2yoqKtRw+BL10nBYDTstLy9Xc32cmKxHt/5YPXDgWTVbzHnHrJeKSnu9vbu+7PVn1lehcuv859b4dmt/++aar6tm++SaW+s13eOZW8fTTK+v3JLpuCiU7wMAAAAAAAAAAAAAAAAAAAAAAGAi05wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXle3r2n/GeQwgx/50qky962cvqQDyr+dfH1UvvnS/mql/uGeT+utT5SqA3GtsmK7ubm9WMVYkMqQePHRYRW55PB414Per/sAFqs/rVf3O1wMBu+b3A+8nHo+rw8NRdbxYLKaOOh1vKPKG82gs8/vNP1+qxu+XxjXXXO08wtl0dKxXe3p71Ux1PrzZeTS1ubVeb7nl0+q8G5rUXOvpsV9/x/3295Ou+ro6tbV1pZoqt9Zn6yr7319fb7+eXNmxY6fdnbvUdE311+/W61i0cIHdRQvVXHFrf+L1j9Vy5zLnUWbcOn89uvXH6oEDz6r5Fg5f4jQ8prNnz1IrKyrUXHFrHLnF5/OplzrrxRyf6uvrneb2eDueW+c/t8Z3oe1v6XJrvaZ7PCuU6yu3ZDouCuX7AAAAAAAAAAAAAAAAAAAAAAAAmMg0pwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwEVl+7r2n3EeA8ixYydi6qonX1cB5E/j9BF194bl6kRefOGT6oHfX6ne8ZlvqxN55GffUO/eVasCyL3Ghunq7vZmFWc3HI2q8bfjatR5/nbceT78php3nr/z+53nyA2Px6MG/H7VH7hA9Xm9qt/5eiBg1/x+IBcikSHn0VjjjxvjjT++GO8cfyb453Ktae71ziOcTUfHerWnt1fNVOfDm51HU5tb67V11Uq1vr5OzbWeHvv1d9xvfz/p8vl86sYfPKCmqtjX544dO+3u3KWma6q/frdex6KFC+wuWqjmilv7E69/rJY7lzmPMuPW+ev04KD6rW/Z7zuNjo6qhSocvkS94YYmdfasWWp5ebmaLW5ff2SLOX9dN2eOesM8ez1VVlSo2ebWenJrfBfa/paufJ9P3Dqe1tfZ59PWVvv8mi+ZjotC+T4AAAAAAAAAAAAAAAAAAAAAAAAmMs0pAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwUdm+rv1nnMcAcuzYiZi66snXVQD5c9/fn1C/0rJGHe97nfepa/7tctVYcNmw+vDq76v+D/aqxqsvf0Kt/+fbVQC519gwXd3d3qwiO4ajUTX+dlyNxezrnFGn5nls9C11/NeRHR6PRw34/ao/cIFqvh4KBlXPeWN/H1AM3jnOjJ79OBIZGnIejRWP28ep6PCb6njvHM+c32c0zb3eeYSz6ehYr/b0jr0eTtfate1qZUWFOlW5tV5bV61U6+vr1Fzr6bFff8f99veTqc6HNzuPUlPs63PHjp12d+5S0zXVX79br2PRwgV2Fy1Uc8Wt/YnXP1bLncucR5lJ9/g0kWcOHFC3bv2JWix8Pp96+22fU2fPnqW6bWRkRG1b/TV1dHRULRY3NDWpixbZ47m8vFx1m1vnP7fGd6Hub6nK9/nEreNpfZ19Pm1ttc+v+ZLpuCiU7wMAAAAAAAAAAAAAAAAAAAAAAGAi05wCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAXMeEfAAAJ//u1v9diRP9cp+UzX3tQy5p/u1zLeLteCmi58iv/rOU/fnO7FuPiS/draZw+ogX4/7dzv7FxlfeewJPUiBnjbG053MA2A4S2nu2SQpI3FU5UKhKiXRT7JbDa1UIQCN32trdV4l62pclCqyhdJ1rpqrSKEgH7DtC+QHakSG3chVaYbVc3f2C7e8dlG+gE3S141m5Jx76t1az9O2duceT8mzmeOPbng776PtiTmeec85w/fjOwWK1sa4t0dLRHbr75psjta2+L/MvP/IvIxo13RTZ1fy6y5d57IrX/37hh+vfTufOzd0TWrr01Uih8ItLRPv3+HwkXNzU1FRkbH4+Uy+9FTp9+N3L8xKnIz3/+d5GhH78WeX34Z5Hjx6d/P52RX74d+dXpdyIffDAaGRubft/pwNWQy+UitevO+aldf85P16c/Faldj87PPZ/fFKldn2rh4orFrkhWKqOVCAAsZZu6uyM7djwcuVZMTExEnv3+DyLPPf9CJGutra2Rr/ftjBQKayLXimNDQ5H+/Qci1Wo1AgAAAAAAAAAAAAAAACw+vvAPAAAAAAAAAAAAAAAAAAAAAAAAAOaBL/wDYEm7o/0fI5+5479G/vvPHo185mv/IXLk3ZWRSxmf/Fjkvr/9QuQ/H34mUnP37eMRAOaWy+UiHR3tkRtvXBW5fe1tka5PfyqyceNds7Ll3ntmZeOG6Z9/JF2f/mRk7dpbIzeumn7f6XS0T3/OdGqfy2yTk5ORsfHxSLn8XuT06Xcjb771i8jxE6ciQz9+bVaOH5/++Ufyq9PvzMoHH4xGxsam3386U1NTEYCa0cpoBABYtmxTd3dkz+6nIsWursi1Ynj4jciLL74cyVqhUIj07doZ2bplS+RaUS6fifTvPxCpVqsRAAAAAAAAAAAAAAAAYPHwhX8AAAAAAAAAAAAAAAAAAAAAAAAAMA984R8AS9r9n6lEnvnevsh9f/uFyPjkxyL12v3fbov8m288G/ncZ96LADC/OjraZ6VQWBO5fe1tkTvvvCOyceNdkU3dn4tsufeeyD2f3xTZuGH699O587PTr5/O2rW3zkpH+/T7fyQtLS0REmPj47Ny+vS7s/LmW7+IHD9xKvLaT16PDP34tcjrwz+LHD8+/fvpvPnm9Oun86vT78zK2Nj0+38kk5OTEaD5CoVCJCuV0UoEAPiz2v22r29nkl1JurvvjuTz+chCdWxoKFIqjUSy1traGnnooQci+/btjfT2bI90dnZGFqpy+UxkYOBIBAAAAAAAAAAAAAAAAFg8fOEfAAAAAAAAAAAAAAAAAAAAAAAAAMyDj/37hx/5j+kYaLL/Nz4V/cO//3000HzDZ1bOStZGfnt95OUTn4gAzXfLX7RF/7stn4yGi1mxYkUkn89FbrihNdLR0T4rN99806zcdustkdvX3hb559M/u6WwZtmNq1bF72+8cdWy1av/Ylnree+1fPq/fG76s9JMTU3F5//pT39KZ7Q0zeyHmUxOTkaq1WpkfPy3s/IP//c3s1Iuvxc5ffrdWRkfm37tP0y/Js3Y+PiyD0ZHl3149myMa5n5zJnP+cMf/vBPnz2TmbUAXNzU1B+Xtba2LnvttZ+kP2lcpVJZtmlTd/p/S9Pw8BvRM/uiEZu6k/24alVndLPV5l/bnnoVu7qWrersrHtdXOv7s1QaSXok6Xot9flnNY9isSvtYnSzZHY+mf8sA4NH0lFjent70lFzzJwHM9mwYX3k/n/9ryIz+2XmXJm5Ztb21cx45ln3uuuui+feq6kyWolzcb7v8zPPJjOZ2R8zuW/rlsjMvvrCPZ9fdtNNNy1bt+6OZR//+D9bVigU4t/MjH/3u9/F+Gr51enTke7p/VOdmIhtaERW97+s1ve1er6d78SJU8v++MepOFaNqPd6ltX1dObaMONqP3c3ui4WynYAAAAAAAAAAAAAAABcyIq0AQAAAAAAAAAAAAAAAAAAAAAAAIAMLT829Oq5dAw02dvvTEbveuX9aAAge5vXrY4+undbNFxLJieT58WJiaRrxsbH01Hi7Ie/j56amor+Y9pnz56NJlstLS3RK9vaomtqP29beUP0+Tra29PRbPl8LjqXSxquZY89/kQ6ysbhQwfT0dLU338gujQyEl2vBx98IPq+rVuim61USubfvz/ZnnoVu7qi+/p2Rl+prPZn367k84vFZD7NMjAwmPTgkeh6LfX5ZzWP3p7tSff2RDdLVueT+c+W1f3rWrlvVavV6HL5TPRoZTS6MlqJrh2nRq+Xl7Jv397oVZ2d0QtNuVyOHk33S+3/f512bf9VKsnvs5bV/Tur+19W63uxnG9Z7dd6r2cL5fkqK42ui4WyHQAAAAAAAAAAAAAAABeyIm0AAAAAAAAAAAAAAAAAAAAAAAAAIEPLjw29ei4dL0gd7e3piBm5/PVJ53LRi911LS3RbW1t0YtFPp8cv//xy99G3//NH0YDANnbvG519NG926JhKZucnIyemEi65uzZs9F/nJqKrjn74e+jp877+UT6PrX3Y37V/v7LX+DvwEv9nbgy/XuqJf376nwdHf7uJjv9/QeiSyMj0Y360hf/MnrDhvXRS83AwGDSg0ei69Xbsz3p3p7oZstqO4pdXdF9fTujr1RW67NvV/L5xWIyn2bJaj8u9flf6+dVqZSs3/79yXqul/nP9tjjT6Sjxhw+dDAdLS6vDw9Hv/jiy9ETExPRjdqx4+HoTd3d0deq2rp+8aWXosvlM9GNWr/+rui/+tIXo+uV1f0vq/Wd1fm2Z/dT0YVCIbrZstqv9Z4HWV1PG32+ykqj62KhbAcAAAAAAAAAAAAAAMCFrEgbAAAAAAAAAAAAAAAAAAAAAAAAAMjQ8nPT0jHQZD996zfR93/zh9EAQPY2r1sdfXTvtmhg/o2Njaej2SYnJ6Mn0j7f2Q9/Hz01NRV9vrHxud+X+ZHL5aLzaZ+vbeUN0S0tLdHn62hvT0eztVyXvH5lW1s0i8uLL74cfWxoKLpRxa6u6L6+ndFLzcDAYNKDR6Lr1d19d/SjOx6Jbras1kWj29HffyC6NDISXa++Xcl6LBaT9dksWa2HpT7/rObR27M96d6e6GYplZL1278/Wc/1Mv/ZHnv8iXTUmMOHDqajxalcLkc//cx3oht1tdbhfHv6mW9Hl8tnouvV2dkZ/d19e6PrldX9L6v1ndX+uVr3s5ovf+Wr0RMTE9H1qnc7srqeLpTn7Uavw0v97wYAAAAAAAAAAAAAAGDhW5E2AAAAAAAAAAAAAAAAAAAAAAAAAJAhX/gHAABApjo62ufMzTffFLl97W1z5s4774hs3HjXnNly7z0XzcYN06+7SNauvfWiuXHVqkhH+/R850gul4ssFZOTk5Gx8fE5Uy6/Fzl9+t05c/zEqTnz85//XWTox6/NmZ9N/24mx49Pv36O/Or0O3OmXD4TGRubnt8cmZqaijC/NmxYH8lKaWQkSSnJUtO5qjPSqHK5HLlasvr8VZ2dEQCujkKhECl2dUUaNVqpRBabrVu3RBpVmd43M1lsWvOtkUaVSqVIs9XW7cTERKRRWT3vAQAAAAAAAAAAAAAAsLD5wj8AAAAAAAAAAAAAAAAAAAAAAAAAmAe+8A8AAIBFoaOj/aK5fe1tF82dd94R2bjxrjmzqftzkS333nPRbNww/fqLpOvTn4ysXXvrnCkUPhHpaJ+e90WSy+Uii83Zs2cjY+Pjc+b06XfnzMgv/0/k+IlTc+a1n7weGfrxa3Pm+PHp182RN9/8RYTLUyx2RfL5fCQrL770UmSpWdW5KtKocvlMZLRSiTRLtVqNlEZGIo0qFAoRAK6u2v2+UZXRSmSxyer+vVhltX5OnDwZabZSqRTJyqrOzggAAAAAAAAAAAAAAACLmy/8AwAAAAAAAAAAAAAAAAAAAAAAAIB5sGJsbHxZPfnw7NkIAAAA8GcdHe0XTaGwJnL72tvmTNenPxXZuPGui2ZT9+ciW+6956LZuGH69RfJnZ+9I7J27a0Xzc03r450tE9vx0VyrRobH58zH4yORrgym7q7I1kpl89Ennv+hchSUSx2RbIy/PpwpFlOnDwZyUrhlkIEALh2da7qjDSq9nw4WqlEmmX49TcijSp2TT/nTQcAAAAAAAAAAAAAAIClYUXaAAAAAAAAAAAAAAAAAAAAAAAAAECGlh8bevVcOr6qcrlcdD7tRuXy1yed0futbGuLbmlpiW5UR0d7OmIp++lbv4m+/5s/jAYAsrd53eroo3u3RQNwdU1NTUV/+OHZ6AuZnJyMnkj7Qmqvm5z4x+gL+fBs8nm1z79SW+69Jx1xOUYrlegnn/xGdNa6u++OfnTHI9GLXX//gejSyEh0vfL5fPR39+2Nbm1tjc5atVqN/pv0+E9MTETXq7OzM7o273pltR/7du2MLha7optlYGAw6cEj0fVa6vPPah69PduT7u2JbpZSKVm//fuT9Vwv85/tscefSEeNOXzoYDpa3L737PejT548FV2vq7UO59uJEyejn/3+D6Ib1ei6yur+l9X6zvo5cf36u6L/6ktfjJ4vWR/XRtd/VtfTYldyP+3rS+6vV0uj1+GFsh0AAAAAAAAAAAAAAAAXsiJtAAAAAAAAAAAAAAAAAAAAAAAAACBDy48NvXouHXMNamlpiV7Z1hbdqFz++qRzuehG1eZVm2ejVq7M9v2utp++9Zvo+7/5w2gAIHub162OPrp3WzSzTU1NRX/44dnoC2m5LtvnToCFamxsPB3N1tHRno64Es89/0L08PAb0Vnr7OyMfujBB6I3bFgffbWVy+XoanUiulBYE93a2hp9pX50bCj6pZdejm5UbT59u3ZG1zuv81Wr1ej+/Qeiy+Uz0Y3aumVL9EMPJce5Xv39ybxKIyPR9artt2KxK7pZBgYGkx48El2vpT7/rObR27M96d6e6GYplZL1WzvP6mX+sz32+BPpqDGHDx1MR4vTaKUS/fTT346emEjuc/V6ML1/37c1uc4vFlndb4pdyXWury+57tUrq/lkvb6ffiZZR1k9L8zXda32XPef0v3Y6Lqv2bP7qehCoRB9pbK6nma1zhrV6HV4oWwHAAAAAAAAAAAAAADAhaxIGwAAAAAAAAAAAAAAAAAAAAAAAADI0PJjQ6+eS8ew5HS0t6ejxrS0tES3rbwh+nL9z78fj/63P3grGgDI3uZ1q6OP7t0WzWxjY8nzyPETp6KzlsvlovNpX65c/vqkr/Df1T7nSv9dy3XJ89zKtrZoALIxWqlEP/nkN6LnW2dnZ/SG9eujC7esiV7VuSr6So1WRqMro8l21Py6XI6eqE5E17azkvb5avP67r690VdqvvZjbV69vduja/uttbU1+lKq1Wr0iZMnowcGjkRfaD/Ua1+631al861Xf/+B6NLISHS9+nbtjC4Wu6KbZWBgMOnBZD/Xa6nPP6t59PYk501vb090s5RKyfrt35+s53qZ/2yPPf5EOmrM4UMH01Fj/um+ct79p9nnbU05ve899/wL0eXymehG7dn9VHShUIi+UrX1VFMoJPf9y72PZaV2P3zxpZejh4ffiG7U1i1boh966IHoemV1/8tqfdf86NhQ9EvpfstKsSs5T2rXhys9b2rrvTa/rI5nTW2d7tn9reh6ZXU9re2vvr7k/nq1NHodXijbAQAAAAAAAAAAAAAAcCEr0gYAAAAAAAAAAAAAAAAAAAAAAAAAMrT82NCr59Ix0GRvvzMZveuV96MBgOxtXrc6+ujebdHMNjY2Hn38xKlo6tPR3p6OrkxLS0t028obouuVz+Wic2nXa+XKtujavACyMjAwmPTgkeilqm/XzuhisSv6Sj33/AvRw8NvRM+XYtfF51edqEaXy2ei50ttHn19yX5rVH//gejSyEh0vRo9jvXK6jxa6vPPah69PduT7u2JbpZSKVm//fuT9Vwv85/tscefSEeNOXzoYDpqzNPPfDv6UtfZ86/XhUIhurU1H12Tb22NviX9/YWMVkajK6OV6BMnT0Znfb3v7OyM/u6+vdFX6kfHhqJfeunl6AspFNZEt+aT7c+n++VC+6FYLKajuVWrtftfOfrXadfW9cTERHRW9ux+Krp2XOuV1f0vq/VdU9uff/PkN6Kz3n81+fzFj3tN7XjO1zxqdux4OHpTd3d0vbK6nmb9vHWlFst2AAAAAAAAAAAAAAAAXMqKtAEAAAAAAAAAAAAAAAAAAAAAAACADC0/NvTquXQMNNnb70xG73rl/WgAIHub162OPrp3WzSzjY2NRx8/cSoa6tHW1hZ9XUtL9OVq7/h4Oro8tfevfd7lyudz0blc0sDV099/ILo0MhK91Dz44APR923dEn2lRiuV6Cef/Eb0Yrdv397oVZ2d0Y3Kav317doZXSx2RTfLwMBg0oNHouu11Oef1Tx6e7Yn3dsT3SylUrJ++/cn67le5j/bY48/kY4ac/jQwXRUn2q1Gv2Vv/5a9GK1Y8fD0Zu6u6Ov1GJ/nih2Jde3vr7keteorPZXo+v7QrK6Li90hcKa6D27vxXdqKyup1mvtyu1WLYDAAAAAAAAAAAAAADgUlakDQAAAAAAAAAAAAAAAAAAAAAAAABkaPmxoVfPpWOgyd5+ZzJ61yvvRwMA2du8bnX00b3bopltbGw8+viJU9HAn3W0t6ejy5PLX590Lhd9Kde1tES3tbVFX66VK5PXt6T/Hq4l1Wo1un//gehy+Uz0UtHdfXf0ozseia7XwMBg0oNHoheb3p7tSff2RGelvz9Zd6WRkeh69e3aGV0sdkU3S1bHfanPP6t5zNc6vZRSKVm/tetovcx/tscefyIdNebwoYPpqD5Z7Z+FqlBYE71n97ei6/Xlr3w1emJiInqx2bP7qehCoRDdqKzuf42u70t5+plvRy+258N8Ph/99b7k/pXVcc3qelHsSu6nfen8mm2xbAcAAAAAAAAAAAAAAMClrEgbAAAAAAAAAAAAAAAAAAAAAAAAAMjQ8mNDr55Lx0CTvf3OZPSuV96PBgCyt3nd6uije7dFM9vY2Hj08ROnooHFp6WlJXplW1v05Wrv+Hg6ujy196993qXk87noXC5plpZqtRrdv/9AdLl8JnqxKxTWRO/Z/a3oRj33/AvRw8NvRF/rurvvjn50xyPRWevvT9ZbaWQkul59u3ZGF4td0c0yMDCY9OCR6Hot9flnNY/enu1J9/ZEN0uplKzf2vWzXuY/22OPP5GOGnP40MF0VJ+s1udCk8/no7/el5y/hUIh+kqVy+Xop5/5TvRis2PHw9Gburujs5LV/a/R9X0po5VK9NNPfzt6YmIi+lo3X8c1q+tpsSu5n/al52ezLZbtAAAAAAAAAAAAAAAAuJQVaQMAAAAAAAAAAAAAAAAAAAAAAAAAGVp+bOjVc+kYaLK335mM3vXK+9EAQPY2r1sdfXTvtmhmGxsbjz5+4lQ0wELX0d6eji5P28oboltaWqIvJZ/LRefSrunouLLP5fJUq9XoF196OXp4+I3oxapQWBO9Z/e3orPy3PMvRF+r+6+7++7oR3c8Ej1f+vsPRJdGRqLr1bdrZ3Sx2BXdLAMDg0kPHomu11Kff1bz6O3ZnnRvT3SzlErJ+u3fn6znepn/bI89/kQ6aszhQwfTUX2yWp8LRT6fj/56X3LeFgqF6HpltX4Wmh07Ho7e1N0dnbWs7n+Nru/LVS6Xo7/37A+iK5VK9LWitu4feuiB6Pk6rlmdD8Wu5H7al56nzbZYtgMAAAAAAAAAAAAAAOBSVqQNAAAAAAAAAAAAAAAAAAAAAAAAAGTIF/4BAAAA14yx8fErSrn8XuT06XcvK//rf5cix0+cmhXmR2tra+TRHY9EvvTFv4x0dnZGFova9tS2M2u1933wwQci+Xw+slDV5lc73vO1XwCuNcViMXKtW7/+rsh39+2NFAqFSKMKhTWzcq2qPRf07doZ2dTdHSFRWy97dj8Vqa2nha7Y1RX5et/OiOMKAAAAAAAAAAAAAADAR/nCPwAAAAAAAAAAAAAAAAAAAAAAAACYB8uPDb16Lh0DTfb2O5PRu155PxoAyN7mdaujj+7dFs1sY2Pj0cdPnIoGYG5b7r0nHdFMrw8PRx87NhRdLp+JXqgKhTXRxa5i9KZNd0cXCoXoZqlWq9EDA0eia/txYmIiutk6OzujN3Un+2Pr1i3Rra2t0c3y3PMvRA8PvxFdrz27n4pu9nEdGBhMejA5rvXq27Uzuljsim6WhTL/2vnw/PP/JbpeDz74QPR96XpullJpJLp//4HoevX2bE+6tye6WRbq/L/8la9G13udLHYl67GvL1mfjRqtVKKP/Si5/504eTK6kv58oelOr++buruj5/v6UrvPvZ5ez4fT83qhPifUng9q97/afmqW7z37/eiTJxv7u//woYPp6OqoXT9+dOxYdKPb06jaed+dPu81+7iWy+Xop5/5TnS9tm5J1uVDDyX3tWarnc9f+euvRdcr6+swAAAAAAAAAAAAAABA1lakDQAAAAAAAAAAAAAAAAAAAAAAAABkZtmy/w95ZGu4lP8oOAAAAABJRU5ErkJggg==";
                        docjspdf.addPage();
                        docjspdf.setFont("arial");
                        docjspdf.setFontSize(16);
                        docjspdf.text(38, 38, "LEGEND OF EUROPEAN ATLAS OF THE SEAS");
                        var canvaslegend2x = Canvas2Image.convertToPNG(canvaslegend2);
                        var ctx2 = canvaslegend2.getContext("2d");
                        // ctx2.webkitImageSmoothingEnabled = false;
                        ctx2.mozImageSmoothingEnabled = false;
                        ctx2.imageSmoothingEnabled = false;

                        ctx2.fillStyle = "#FFF";
                        ctx2.fillRect(0, 0, ctx2.canvas.width, ctx2.canvas.height);
                        ctx2.drawImage(canvaslegend2x, 0, 0);
                        var canvaslegend3x = Canvas2Image.convertToPNG(canvaslegend2);
                        docjspdf.addImage(canvaslegend3x, "png", 38, 60);
                        docjspdf.addImage(eclogoBase64SecPage, "jpg", w * eclogow, h * eclogoh,225,24);
                    }

                    docjspdf.output("save", "Atlas_" + layouttemplate.substring(0, 2) + ".pdf");

                } else {
                    function downloadCanvas(base64Img, filename) {

                        // Function to download data to a file
                        if (window.navigator.msSaveOrOpenBlob) { // IE10+         
                            // convert base64 to raw binary data held in a string
                            // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
                            var byteString = atob(base64Img.split(",")[1]);

                            // separate out the mime component
                            var mimeString = base64Img.split(",")[0].split(":")[1].split(";")[0];

                            // write the bytes of the string to an ArrayBuffer
                            var ab = new ArrayBuffer(byteString.length);
                            var ia = new Uint8Array(ab);
                            for (var i = 0; i < byteString.length; i++) {
                                ia[i] = byteString.charCodeAt(i);
                            }
                            // write the ArrayBuffer to a blob, and you're done
                            var blob = new Blob([ab], { type: mimeString });
                            window.navigator.msSaveOrOpenBlob(blob, filename);
                        }
                        else
                            download(base64Img, filename, "image/jpeg")  //Creative Commons Attribution 4.0 International License, attribute to "dandavis".

                    }
                    downloadCanvas(base64Img, "ATLAS_" + layouttemplate.substring(0, 2) + ".jpg");
                }

                jpgpdf();

            }, "image/jpeg");
        };

        jpgpdf = function () {
            var val = $("input[name=pdgp]:checked").val();
            if (val === "PDF") {
                //$("#printButton0").empty();

            } else {
                //$("#printButton0").empty();

            }
            printInfo.then(handlePrintInfo, handleError);
        };

        $("input[name=pdgp]:radio").change(jpgpdf);

        ////////////// LANGUAGE CHANGE FUNCTION 
        lchange = function (ya) {
            //lang = $("#language").find(":selected").val();
            if (typeof legendDijit !== "undefined") {
                legendDijit.destroy();
            }
            ;
            var ndiv = domconstruct.create("div", {
                id: "legendDiv"
            });
            document.getElementById("legendPane").appendChild(ndiv);
            que = [];
            main_functions.url2que();
            yay = ya;
            //   if (lang !== urlobj.lang)
            translation2(lang);
            //  urlupdate();
            basemapx = basemapGallery.getSelected();
            if (basemapx.id != urlobj.bkgd)
                basemapGallery.select(urlobj.bkgd);

            translate_article();

            if (lang == "EN") {
                document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_en.htm";
            } else if (lang == "DE") {
                document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_de.htm";
            } else if (lang == "FR") {
                document.getElementById("legalNotice").href = "http://ec.europa.eu/geninfo/legal_notices_fr.htm";
            }

            translate_measurement(lang);
            if (measurement.activeTool && measurement.activeTool != null) {
                if (measurement.activeTool == "distance") {
                    measurement._createDistanceUnitList();
                    measurement._switchUnit(measurement.currentDistanceUnit);
                } else if (measurement.activeTool == "area") {
                    measurement._createAreaUnitList();
                    measurement._switchUnit(measurement.currentAreaUnit);
                } else {
                    measurement._createLocationUnitList();
                    measurement._switchUnit(measurement.currentLocationUnit);
                }
            }
            if (measurement1.activeTool && measurement1.activeTool != null) {
                if (measurement1.activeTool == "distance") {
                    measurement1._createDistanceUnitList();
                    measurement1._switchUnit(measurement1.currentDistanceUnit);
                } else if (measurement1.activeTool == "area") {
                    measurement1._createAreaUnitList();
                    measurement1._switchUnit(measurement1.currentAreaUnit);
                } else {
                    measurement1._createLocationUnitList();
                    measurement1._switchUnit(measurement1.currentLocationUnit);
                }
            }
            if (measurement2.activeTool && measurement2.activeTool != null) {
                if (measurement2.activeTool == "distance") {
                    measurement2._createDistanceUnitList();
                    measurement2._switchUnit(measurement2.currentDistanceUnit);
                } else if (measurement2.activeTool == "area") {
                    measurement2._createAreaUnitList();
                    measurement2._switchUnit(measurement2.currentAreaUnit);
                } else {
                    measurement2._createLocationUnitList();
                    measurement2._switchUnit(measurement2.currentLocationUnit);
                }
            }


        };

        showInfo_preview = function (sip_layerId,event) {
            var event = event || window.event;
            showinfolayerModal(sip_layerId);
            event.preventDefault();
        }

        worldartic = function () {
            if (urlobj.p === "w") {
                urlobj.p = "a";
                urlobj.bkgd = articBackgroundLayers[0];
                CLEAR();
                urlobj.c = "undefined";
                urlobj.z = "undefined";
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
                var prueba_boleano = true;
                location.reload();
            } else {
                urlobj.p = "w";
                urlobj.bkgd = worldBackgroundLayers[0];
                CLEAR();
                urlobj.c = "undefined";
                urlobj.z = "undefined";
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
                var prueba_boleano1 = true;
                location.reload(true);
            }

            toggledMenu = !toggledMenu;
        };


        /*    cliccatoMaps = function () {
             // myToggleButton1.set("iconClass", toggled1 ? "dijitEditorIcon dijitEditorIconCut" : "dijitEditorIcon dijitEditorIconPaste"); //rimosso perche in automatico
             
             if (toggledMenu === true) {
             $('#MapMenu').hide('fast'); 
             $('#LayersMenu').show('fast');
             $('#br').hide('fast'); 
             //     resizeMap();
             //   ajaxOnComplete();  
             
             } else {
             
             $('#MapMenu').show('fast'); 
             $('#LayersMenu').hide('fast');
             $('#br').show('fast');               
             //                        initsli();
             }
             
             toggledMenu = !toggledMenu; 
             };
             cliccatoMaps();
             */

        // to show LayerMenu
        document.getElementById("zoomIn").addEventListener("click",function () {

            map.setZoom(map.getZoom() + 1);
    
        });

        document.getElementById("defaultView").addEventListener("click",function () {
            map.setZoom(4);
            var centerPoint2 = new esri.geometry.Point({
                "x": "1253866.2175874896",
                "y": "7033312.218247011",
                "spatialReference": new esri.SpatialReference({ wkid: 102100 })
            });
            map.centerAt(centerPoint2);
        });
    
        document.getElementById("zoomOut").addEventListener("click", function () {
    
            map.setZoom(map.getZoom() - 1);
    
        });
    
        document.getElementById("measure").addEventListener("click", function () {
       
            $("#measure_container").slideToggle();
    
            if($("#info_window .esriPopup").hasClass("esriPopupVisible")) {
                var feature_info = $('.esriPopupWrapper').height();
                if (feature_info == 0) {
                    var totalHeight = 0;
                    $(".esriPopupWrapper").children().each(function(){
                        totalHeight = totalHeight + $(this).outerHeight(true);
                    });
                    $("#measure_container").css({"top":totalHeight+15});
                } else {
                    $("#measure_container").css({"top":feature_info+30});
                }
                
            }
    
        });

        document.getElementById("basemap_next").addEventListener("click", function () {
       
            $("#basemapGallery").css({"left":$('#basemapGallery').position().left-125});

            var pppp = $( "#basemapGallery" );
            var positionBasemap = pppp.position();
            if (positionBasemap.left <= -220) {
                $("#basemap_next").css({"display":"none"});
            } else {
                $("#basemap_previous").css({"display":"block"});
            }
    
        });

        

        document.getElementById("basemap_previous").addEventListener("click", function () {
       
            $("#basemapGallery").css({"left":$('#basemapGallery').position().left+125})
            var pppp = $( "#basemapGallery" );
            var positionBasemap = pppp.position();
            if (positionBasemap.left >= 0) {
                $("#basemap_previous").css({"display":"none"});
            } else {
                $("#basemap_next").css({"display":"block"});
            }
    
        });

        document.getElementById("centralPane").addEventListener('click',function(){
            if($("#measure_container .esriButton").hasClass("esriButtonChecked")){

            }
            else{
                $('.esriPopup').appendTo('#info_window'); 
            }
        });

        document.getElementById("info_button").addEventListener('click',function(){
            toggleModalInfo();
        });

        document.getElementById("search_button").addEventListener('click',function(){
            toggleModalSearch();
            translate_article();
        });
        
        document.getElementById("share").addEventListener('click',function(){
            if (navigator.userAgent.match(/ipad|iphone/i)) {
                $("a[href='#exportPane']").hide();
            }
            toggleModalShare();
            closeinfolayer();
            download_data();
        });

        download_data = function () {
            
        };

        clickLangBtn = function () {
            $('.langContainer').click(function(){
                toggleModalLanguages();
                return false;          
            });
        }
        clickLangBtn();

        clickSocialNetBtn = function () {
            $('.socialNetContainerParent').click(function(){
                toggleModalSocialNet();
                return false;          
            });
        }
        clickSocialNetBtn();

        $("#createPaneList").scroll(function() {
            closeinfolayer();
        });

        $("#createPaneList1").scroll(function() {
            closeinfolayer();
        });

        listalayercreation4 = function (checkboxElem) {
            if($(".modal").hasClass("show-modal")){
                main_functions.listalayercreation3();
            }
            if($(".modalSearch").hasClass("show-modal")){
                main_functions.listalayercreation4();
            }
        }

        changeCheckboxLayer = function (checkboxElem) {
            var idOfLayer;
            var opentip_text;
            $("#predefinedmapCont").hide();
            if( !$("#buttonshow i").hasClass("fa-angle-left")){ //OPEN LAYER PANEL
                cliccatoTool();
            }
            if($(".modal").hasClass("show-modal")){
                idOfLayer = checkboxElem.id.replace("cb","");
            }
            if($(".modalSearch").hasClass("show-modal")){
                idOfLayer = checkboxElem.id.replace("cbb","");
            }
            if (checkboxElem.checked) { //ADD LAYER
                opentip_text = article["LayerAdded"];
                $("#"+idOfLayer+"Cont").css({"background-color":"#ececec"});
                $("#"+idOfLayer+"Cont1").css({"background-color":"#ececec"});
                addLayersId(idOfLayer);
            } else { //DELETE LAYER
                opentip_text = article["LayerRemoved"];
                $("#"+idOfLayer+"Cont").css({"background-color":"#ffffff"});
                $("#"+idOfLayer+"Cont1").css({"background-color":"#ffffff"});
                removelayer(idOfLayer);
            }
            setTimeout(function () {
                main_functions.listalayercreation2();
            }, 1000);
            
            addRemoveLayerMessage(opentip_text);
        }

        addRemoveLayerMessage = function(opentip_text) {
            myOpentip2 = new Opentip($("#container2"), {
                showOn: "click",
                extends: "alert",
                removeElementsOnHide: true,
                background: "#014489",
                stem: "false",
                borderWidth: 0
            });
            myOpentip2.setContent(opentip_text); // Updates Opentips content
            myOpentip2.show();
            $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("opentipOther");
            $(".opentip-container, .opentip, .ot-header, .ot-content, .ot-close").addClass("added");
            $(".ot-header.opentipOther").find("h1").remove();    
            $(myOpentip2.container).addClass( "transition" );
            setTimeout(function() {
                if (myOpentip2) {
                    //myOpentip2.hide();
                    $(".opentip-container.transition").remove();
                } 
            }, 1000);
        }

        changeCloseHomeCheckbox = function (checkboxElem) {
            miStorage = window.localStorage;
            if (checkboxElem.checked) { //DON'T SHOW HOMEPAGE AGAIN
                miStorage.setItem('closeHomepage', 'true');
            } else {
                miStorage.setItem('closeHomepage', 'false');
            }
        }

        //////////////////MODAL
        var modal = document.querySelector(".modal");
        var modalShare = document.querySelector(".modalShare");
        var modalInfo = document.querySelector(".modalInfo");
        var modalSearch = document.querySelector(".modalSearch");
        var modalLanguage = document.querySelector(".modalLanguage");
        var modalSocialNet = document.querySelector(".modalSocialNet");
        var searchPage = document.querySelector("#searchPage");
        var HomePage = document.querySelector("#HomePage");
        window.addEventListener("click", windowOnClick);
        document.querySelector("#close_home").addEventListener("click", toggleModal);
        document.querySelector("#close_share").addEventListener("click", toggleModalShare);
        document.querySelector("#close_info").addEventListener("click", toggleModalInfo);
        document.querySelector("#close_search").addEventListener("click", toggleModalSearch);
        document.querySelector("#close_language").addEventListener("click", toggleModalLanguages);
        document.querySelector("#close_socialNet").addEventListener("click", toggleModalSocialNet);
        //document.querySelector("#copyUrl").addEventListener("click", copyUrl);
        //document.querySelector("#copyIframe").addEventListener("click", copyIframe);
        //document.querySelector("#export_button").addEventListener("click", exportMap);

        function toggleModal() {
            if(worarc==arctic){

            }else{
                closeinfolayer();
                modal.classList.toggle("show-modal");
                if ($( ".modal" ).hasClass( "show-modal" )){ //WHEN OPEN THE MODAL
                    main_functions.listalayercreation2();
                    main_functions.listalayercreation3();
                    main_functions.listalayerspredmaps();
                } else {
                    $("[tooltip]").addClass("show_tooltip");
                    setTimeout(function () {

                        $("[tooltip]").removeClass("show_tooltip");
                    }, 3000);
                    $('.k-animation-container').hide();
                }
            }
            
        }
        
        function setEmbedText() {
            var iframe = "<iframe src=\"" + window.location.href + ";e=t\" width=\"930px\" height=\"565px\" frameborder=\"0\"></iframe>";
            $("#embedTextArea").val(iframe);
        }

        function setUrlEmailText() {
            $("#urlText").val(window.location.href);
        }

        function toggleModalShare() {
            modalShare.classList.toggle("show-modal");
            if ($( ".modalShare" ).hasClass( "show-modal" )) { //WHEN OPEN THE MODAL
                $("#export_error").hide();
                //cliccaPinterest();
                //ADD IFRAME TO EMBED
                setEmbedText();
                setUrlEmailText();
                var twitter_str= window.location.href.replace("#","%23");
                twitter_str = twitter_str.replace(/;/g,"%3B");
                //twitter_str= twitter_str.replace("localhost/maratlas/","194.30.43.115:8032/Atlas/");
                $("#twitter_link").attr("href", "http://twitter.com/share?url="+twitter_str);
                
                $("#facebook_link").attr("href","http://www.facebook.com/share.php?u="+twitter_str);

                var linkedin_summary="The%20easy%20and%20fun%20way%20for%20professionals,%20students%20and%20anyone%20interested%20to%20learn%20more%20about%20Europe's%20seas%20and%20coasts,%20their%20environment,%20related%20human%20activities%20and%20European%20policies.";
                $("#linkedin_link").attr("href", "http://www.linkedin.com/shareArticle?mini=true&url="+ twitter_str +"&title=European%20Atlas%20of%20the%20Seas&summary="+ linkedin_summary +"&source=LinkedIn");
                
                //$("#instagram_link").attr("href","http://pinterest.com/pin/create/button/?url="+twitter_str+"&media="+twitter_str);
                /*var media = "http://assets.pinterest.com/images/PinExt.png";
                //media = media.replace(/\//g,"%2F");
                //media = media.replace(/:/g,"%3A");
                $("#instagram_link").attr("href","https://www.pinterest.com/pin/create/button/?url=http://ec.europa.eu/maritimeaffairs/atlas/maritime_atlas&media="+media);
                */
            }
        }

        function toggleModalLanguages() {
            modalLanguage.classList.toggle("show-modal");
            if ($( ".modalLanguage" ).hasClass( "show-modal" )) { //WHEN OPEN THE MODAL
                main_functions.listalanguages();
            }
        }

        function toggleModalSocialNet() {
            modalSocialNet.classList.toggle("show-modal");
            /*if ($( ".modalLanguage" ).hasClass( "show-modal" )) { //WHEN OPEN THE MODAL
                main_functions.listalanguages();
            }*/
        }

        toggleModalSocialNet2 = function () {
            toggleModalSocialNet();
        }

        function toggleModalInfo() {
            closeinfolayer();
            modalInfo.classList.toggle("show-modal");
        }

        function toggleModalSearch() {
            closeinfolayer();
            modalSearch.classList.toggle("show-modal");
            if ($( ".modalSearch" ).hasClass( "show-modal" )){ //WHEN OPEN THE MODAL
                main_functions.listalayercreation2();
                main_functions.listalayercreation4();
                main_functions.listalayerspredmaps();
                if(worarc==arctic){
                    $("#predefinedNameCont").remove();
                    $("#createPaneNameCont").addClass("active");
                    $("#predefinedPane1").remove();
                    $("#createPane1").show();
                }
            } else {
                $('.k-animation-container').hide();
            }
        }
        
        function copyUrl() {
            if (navigator.userAgent.match(/ipad|iphone/i)) {
                var el = document.getElementById("urlText");
                var oldContentEditable = el.contentEditable,
                oldReadOnly = el.readOnly,
                range = document.createRange();
                el.contenteditable = true;
                el.readonly = false;
                range.selectNodeContents(el);
            
                var s = window.getSelection();
                s.removeAllRanges();
                s.addRange(range);
            
                el.setSelectionRange(0, 999999);
            
                el.contentEditable = oldContentEditable;
                el.readOnly = oldReadOnly;
            } else {
                $("#urlText").select();
            }
            document.execCommand('copy');
        }

        function copyIframe() {
            if (navigator.userAgent.match(/ipad|iphone/i)) {
                var el = document.getElementById("embedTextArea");
                var oldContentEditable = el.contentEditable,
                oldReadOnly = el.readOnly,
                range = document.createRange();
                el.contenteditable = true;
                el.readonly = false;
                range.selectNodeContents(el);
            
                var s = window.getSelection();
                s.removeAllRanges();
                s.addRange(range);
            
                el.setSelectionRange(0, 999999);
            
                el.contentEditable = oldContentEditable;
                el.readOnly = oldReadOnly;
            } else {
                $("#embedTextArea").select();
            }
            document.execCommand('copy');
        }

        function exportMap() {
            $("#export_error").hide();
            var a = new Object();
            printer2 = new Print({
                "map": map,
                //"templates": templates,
                "url": printUrl,
                "content": {
                    "f": "pjson"
                }
            }, dom.byId("export_button"));

            //var form = document.getElementById("export2");

            a.layout = $('.radioButtonsExport:checked').val();
            a.label = a.layout;

            a.exportOptions = {
                width: map.width,
                height: map.height,
                dpi: 96

            };

            a.format = "PNG32";

            var copyri = "\&#169;<span></span> European Union, 1995-2018";

            a.layoutOptions = {
                "authorText": "",
                "legendLayers": [], // empty array means no legend
                "customTextElements": [{
                    "physAddress": "EUROPEAN COMMISSION"
                }, {
                    "ownerOne": "European Union"
                }],
                "copyrightText": copyri,
                "titleText": "EUROPEAN ATLAS OF THE SEAS",
                "scalebarUnit": "Miles"
            };

            printer2._printButton = document.getElementById("export_button");

            printer2.printMap(a);
            $("#export_loading").empty();
            $("#export_button").hide();
            $("#export_loading").show();
            $("#export_loading").html("<img style=\"position:relative; width: 60px; height: 60px;\" src=\"img/loader2.gif\" alt=\"loading..\" />");

            printer2.startup();

            printer2.on("print-start", function () {
                console.log("The print operation has started");
            });

            printer2.on("print-complete", function (evt) {
                lastprint(evt);
                $("#export_button").show();
                $("#export_loading").hide();
            });

            printer2.on("error", function (evt) {
                console.log("ERRORR printing:" + evt.message);
                $("#export_loading").hide();
                $("#export_button").show();
                $("#export_error").show();
            });            


        }

        function windowOnClick(event) {
            if (event.target === modal) {
                toggleModal();
            }
            if (event.target === modalShare) {
                toggleModalShare();
            }
            if (event.target === modalInfo) {
                toggleModalInfo();
            }
            if (event.target === modalSearch) {
                toggleModalSearch();
            }
           if ((event.target === searchPage || searchPage.contains(event.target)) || (event.target === HomePage || HomePage.contains(event.target))) {
                if ($(".k-widget.k-multiselect.k-header.required").hasClass("k-state-border-down")) {
                    if (event.target !== document.querySelector(".k-multiselect-wrap.k-floatwrap")) {
                        closeinfolayer();
                        $('.k-animation-container').hide();
                        $(".closeSearchButton").hide();
                        $(".search_text").show();
                        $(".k-widget.k-multiselect.k-header.required").removeClass("k-state-border-down");
                        $(".k-input").val("");
                        $(".k-input").blur();
                    }
                }
            }
        }
        //////////////////////

        changeLayerLegend = function () {
            $('.a').click(function(){
                $(' .a').removeClass('active');
                $(this).addClass('active');
                $('.lay_leg_content').hide();

                var activeTab = $(this).attr('href');
                $(activeTab).show();
                moveShareButton();
                closeinfolayer();
                return false;          
            });
        }
        changeLayerLegend();

        // $("#layerPane").click(function(){
        //     moveShareButton();
        // });

        changeShareOptions = function () {
            $('.b').click(function(){
                $(' .b').removeClass('active');
                $(this).addClass('active');
                $('.modalS_content').hide();
        
                var activeTab = $(this).attr('href');
                $(activeTab).show();
                return false;          
            });
        }
        changeShareOptions();

        changeSectionOptions1 = function () {
            $('.d').click(function(){
                closeinfolayer();
                $(' .d').removeClass('active');
                $(this).addClass('active');
                $('.section_content1').hide();
        
                var activeTab = $(this).attr('href');
                $(activeTab).show();
                return false;          
            });
        }
        changeSectionOptions1();

        moveShareButton = function() {   
            var leftheight=$('#leftPane').height();
            $("#share").css('top', '0').css({'top': leftheight+$('#share').position().top+101});
        }

        changeSectionOptions = function () {
            $('.c').click(function(){
                closeinfolayer();
                $(' .c').removeClass('active');
                $(this).addClass('active');
                if($('#predefinedPane0').css('display') != 'none'){
                    $("#scroll").css({"overflow-y":"scroll"});
                }
                if($('#createPane').css('display') != 'none'){
                    $("#scroll").css({"overflow-y":"hidden"});
                }  
                $('.section_content').hide();
        
                var activeTab = $(this).attr('href');
                $(activeTab).show();
                return false;          
            });
        }
        changeSectionOptions();

        var top = $('.modal .multisearch').offset().top;
        var blue = $('#blue_home').offset().top;
        
        var topp= top-blue;

        $("#scroll").scroll(function (event) {
            var y = $("#scroll").scrollTop();
                if (y >= topp-$('.modal .multisearch').height()-10){
                    $('.modal .multisearch').addClass('fixed');
                    $('.modal .multisearch').css({"width":$("#white_home").width() });
                    //$('#blue_home').addClass('fixed');
                    //$(".breadcrumb, #home_sep, .tit_des, .subtit_des").addClass("home_scroll");
                    //$('#blue_home').css({"width": $("#white_home").width() });
                    $('.fixed').css({"top":$("#contenedor_cabecera").height()-1});
                    $('.section_tit').addClass('fixed');
                    $('#createPaneList').addClass('fixedd');
                    
                    $('#closeHomeCheckCont').addClass('fixed');
                    $('#closeHomeCheckCont.fixed').css({"top":$(".modal .multisearch").outerHeight()+$("#contenedor_cabecera").height()-7,"z-index":"9","background":"white"});
                    $('.section_tit.fixed').css({"top":$(".modal .closeHomeCheckCont").height()+$(".modal .multisearch").outerHeight()+$("#contenedor_cabecera").height()+5});

                    $("#Feedback a").css({"color":"red"});

                }
                else{
                    $("#Feedback a").css({"color":"blue"});
                    $('.section_tit').removeClass('fixed');
                    $('#closeHomeCheckCont').removeClass('fixed');
                    //$('#blue_home').removeClass('fixed');
                    $('.modal .multisearch').removeClass('fixed');
                    $('.modal .multisearch').css({"width":"auto" });
                    $('#createPaneList').removeClass('fixedd');
                    $(".breadcrumb, #home_sep, .tit_des, .subtit_des").removeClass("home_scroll");
                }

        });

        changeMeasureOptions = function () {
            $('.e').click(function(){
                closeinfolayer();
                checktopLayerCursor();
                $(' .e').removeClass('active');
                $(this).addClass('active');
                $('.measure_content').hide();
                
                measurement.setTool("area",false);
                measurement.clearResult();
                measurement1.setTool("distance",false);
                measurement1.clearResult();
                measurement2.setTool("location",false);
                measurement2.clearResult();
                var activeTab = $(this).attr('href');
                $(activeTab).show();
                return false;
            });
        }
        changeMeasureOptions();


        
        cliccatoMapMenu = function () {
            $("#MapMenu").hide("fast");
            $("#LayersMenu").show("fast");
            $("#br").hide("fast");
        };
       // cliccatoMapMenu();

        // to show MapMenu
        cliccatoLayerMenu = function () {
            $("#MapMenu").show("fast");
            $("#LayersMenu").hide("fast");
            $("#br").show("fast");
        };
       // cliccatoLayerMenu();


        cliccatoMaps = function () {

            if (toggledMenu === true) {
                cliccatoMapMenu();
            } else {
                cliccatoLayerMenu();
            }
            toggledMenu = !toggledMenu;
        };
        //cliccatoMaps();


        $("#LayersMenu").show("fast");
        

        cliccatotime = function (togg) {
            /*
            // myToggleButton1.set("iconClass", toggled1 ? "dijitEditorIcon dijitEditorIconCut" : "dijitEditorIcon dijitEditorIconPaste"); //removed because it  works in other way
            toggledTime = togg;
            if (toggledTime === true) {

                $("#timeInfo").hide("fast");
                $("#timeInfohr").hide("fast");

            } else {

                $("#timeInfo").show("fast");
                $("#timeInfohr").show("fast");
                //                        initsli();
            }
            */
        };
        cliccatofish = function (toggfish) {
            //  myToggleButton1.set("iconClass", toggled1 ? "dijitEditorIcon dijitEditorIconCut" : "dijitEditorIcon dijitEditorIconPaste"); //removed because it  works in other way
            toggledFish = toggfish;
            resizeMaxLayerPane();
            if (toggledFish === true) {

                $("#titlePane1").hide("fast",function(){
                    if( $("#buttonshow i").hasClass("fa-angle-left")){
                        moveShareButton();
                    }
                });
                
            } else {

                $("#titlePane1").show("fast",function(){
                    if( $("#buttonshow i").hasClass("fa-angle-left")){
                        moveShareButton();
                    }
                });
                
            }
        };

        cliccatoTool = function () {
            //        myToggleButtonLegend.set("iconClass", toggledLegend ? "fa fa-angle-double-right fa-inverse fa-2x" : "fa fa-angle-double-left fa-inverse fa-2x"); //removed because it  works in other way

            $("#leftTool").toggleClass("leftArrow");
            toggledTool = !toggledTool;
            if (toggledTool === false) {
                $("#leftPane").hide("slide", {
                    direction: "left"
                }, "fast");
                $('#buttonshow').appendTo('#tol').css({'top':'114px','left':'0px','position':'absolute','margin':'0', "text-align":""});

                $('#buttonshow .fa-angle-left').addClass('fa-clone').removeClass('fa-angle-left');

                $("#share").css({'top': 0}).css({"top":"166px"});

                $("#lay_ind").show();

                closeinfolayer();

                // $("#map_zoom_slider").css({
                //     left: 22
                // });
            } else {
                $("#leftPane").show("slide", {
                    direction: "left"
                }, "fast");
                var leftwidth = $("#leftPane").width();

                $('#buttonshow').appendTo('.lay_leg_tit').css({'float': 'right','margin-top': '-30px','position':'static', "text-align":"right"});
                $('.fa-clone').addClass('fa-angle-left').removeClass('fa-clone');

                moveShareButton();

                $("#lay_ind").hide();

                // $("#map_zoom_slider").css({
                //     left: leftwidth + 22
                // });
            }
        };

        /*cliccaPinterest = function () {
            var a = new Object();
            printer3 = new Print({
                "map": map,
                "url": printUrl,
                "content": {
                    "f": "pjson"
                }
            }, dom.byId("instagram"));

            a.layout = "MAP_ONLY";
            a.label = a.layout;

            a.exportOptions = {
                width: map.width,
                height: map.height,
                dpi: 96
            };

            a.format = "PNG32";

            printer3._printButton = document.getElementById("instagram");

            printer3.printMap(a);

            printer3.startup();

            printer3.on("print-start", function () {
                console.log("The print operation has started");
            });

            printer3.on("print-complete", function (evt) {
                lastprint3(evt);
            });
        };*/

        /*cliccaPinterest = function () {
            var mapHtml = $("#map_layer1").html();
            var obj = {
                payload: {
                    HtmlInput: mapHtml,
                    BaseUrl: location.href.substring(0, location.href.lastIndexOf('/'))
                }
            };
            
            var data2Send = JSON.stringify(obj);

            jQuery.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: imgproxyconf,
                data: data2Send,
                'async': false,
                dataType: "json",
                success: function (response) {
                    //$(response.d).removeAttr("style");
                    //$(response.d).removeAttr("id");
                    var mapHtmlFinal = "<img src='"+$(response.d).attr("src")+"'/>";
                    
                    var canvas = document.createElement("canvas");
                    canvas.width = 900;
                    canvas.height = 500;
                    var ctx = canvas.getContext("2d");
                    
                    var image = new Image();
                    image.onload = function() {
                      ctx.drawImage(image, 0, 0);
                      //var img    = canvas.toDataURL("image/png");
                      canvas.toBlob(function(blob) {
                        var newImg = document.createElement('img'),
                            url = URL.createObjectURL(blob);
                      
                        newImg.onload = function() {
                          // no longer need to read the blob so it's revoked
                          URL.revokeObjectURL(url);
                        };
                      
                        newImg.src = url;
                        //document.body.appendChild(newImg);
                      });
                    };
                    image.src = $(response.d).attr("src");

                    //$("#other_social").append(canvas);

                    
                    html2canvas($(response.d)[0], {
                        onrendered: function (canvas) {
                            //layouttemplate = ioArgs.content.Layout_Template;
                            var canvaslegend = Canvas2Image.convertToPNG(canvas);

                            //canvaslegend2 = canvas;
                            //canvas = null;
                            //$(newMode).remove();
                        }
                    });					
                    
                    html2canvas($(response.d)[0]).then(function(canvas) {
                        
                        // Export the canvas to its data URI representation
                        var base64image = canvas.toDataURL("image/png");
                        
                        // Open the image in a new window
                        //window.open(base64image , "_blank");
                    });
                    
                },
                error: function (a, b, c) {
                    alert(a.responseText);
                }
            });
            
            
        };*/

        var lastprint3 = function (evt) {
            var myurl = evt.result.url;
            myurl = myurl.replace(":443", "");
            console.log("The url to the print image is : " + myurl);

            var instagram_url = window.location.href.replace("#","%23");
            instagram_url = instagram_url.replace(/;/g,"%3B");
            instagram_url = instagram_url.replace("localhost/maratlas/","194.30.43.115:8032/Atlas/");
            //$("#instagram_link").attr("href","https://www.pinterest.com/pin/create/button/?url="+ instagram_url +"&media="+ myurl);
            $("#instagram_link").attr("href","https://www.pinterest.com/pin/create/button/?url=http://ec.europa.eu/maritimeaffairs/atlas/maritime_atlas/&media="+ myurl);
        }

        $("#tmbutton").click(function () {
            //Some code
        });
        $("#slbutton").click(function () {
            //Some code
        });

        $("#language").change(function () {
            //do not do anything if the language is the same
            if (this.value!= lang) changeLanguage(this.value);
        });

        changeLanguageText = function (languageValue) {
            var langName = "";
            $.each(IDIOMS_CONFIG.languages, function (key, value) {
                if (languageValue == value.CODE) {
                    langName = value.NAME;
                }
            });
            $(".langName").text(langName);
            $(".langCode").text(languageValue);
            $("#lang_row").find("a").removeClass('is-active');
            $('#lang_row a[lang="'+languageValue.toLowerCase()+'"]').addClass('is-active');
            $('#lang_row').find(".fa-check").remove();
            $('#lang_row a[lang="'+languageValue.toLowerCase()+'"]').append("<i class=\"fas fa-check\"></i>");
        }
    
        changeLanguage = function (languageValue) {
            if($(".modalLanguage").hasClass("show-modal")){
                toggleModalLanguages();
            }
            changeLanguageText(languageValue);
            
            
			//do not do anything if the language is the same
			if (languageValue== lang) return;
			lang = languageValue;
			 hash2 = true;
            //translate_measurement(lang);s
			//load all language files
			allLoaded= false;
			var fTranslate = $.Deferred();
							
			var fLang = $.Deferred();
			var _langConfig;
			
			var fGroups = $.Deferred();
			var _groupConfig;
			
			var fServices = $.Deferred();
			var _serviceConfig;
			
			var fThemes = $.Deferred();
			var _themesConfig;
			
			var fFish = $.Deferred();
			var _fishConfig;
			
			
			$.getScript( './languages/translation_'  + lang + '.js?_=' + Date.now(), function( data, textStatus, jqxhr ) {				
				translate_article();
				translate_measurement(lang);
				fTranslate.resolve();
			});
            
            $.ajax({
                url: './languages/languages_'  + lang + '.json?_=' + Date.now(),
                crossDomain:true,
                type:'get',
                dataType:'json',
                success: function(data) {    
                    _langConfig= data;
                    fLang.resolve();	
                }
            });    
			
			$.ajax({
                url: './languages/serviceinfos_'  + lang + '.json?_=' + Date.now(),
                crossDomain:true,
                type:'get',
                dataType:'json',
                success: function(data) {    
                    _serviceConfig= data;
                    fServices.resolve();	
                }
            });
            
            $.ajax({
                url: './languages/groupinfos_'  + lang + '.json?_=' + Date.now(),
                crossDomain:true,
                type:'get',
                dataType:'json',
                success: function(data) {    
                    _groupConfig= data;
                    fGroups.resolve();	
                }
            });
            
            $.ajax({
                url: './languages/themes_'  + lang + '.json?_=' + Date.now(),
                crossDomain:true,
                type:'get',
                dataType:'json',
                success: function(data) {    
                    _themesConfig= data;
                    fThemes.resolve();	
                }
            });
            
            $.ajax({
                url: './languages/fishlist_'  + lang + '.json?_=' + Date.now(),
                crossDomain:true,
                type:'get',
                dataType:'json',
                success: function(data) {    
                    _fishConfig= data;
                    fFish.resolve();	
                }
            });			
			
			//when we are sure all files are loaded change the url 
			$.when(fTranslate,fLang, fGroups, fServices, fThemes, fFish).done(function()  
			{				
				GROUP_CONFIG = _groupConfig;
				SERVICES_CONFIG = _serviceConfig; 
				FISH_CONFIG = _fishConfig; // parse fishinfo file
				LANG_CONFIG = _langConfig; // parse languages file
				THEMES_CONFIG = _themesConfig; // parse themes file				
				
				world = GROUP_CONFIG.world;
				arctic = GROUP_CONFIG.arctic;
				if (urlobj.p === "w") {
					worarc = world;
				} else {
					worarc = arctic;
				}	
            

            // //  var lang = this.value;
            // lang = this.value;

            // translate_article();
            // hash2 = true;
            // //    translate_measurement(lang);
                allLoaded = true;
                window.location.href = "" + "#lang=" + lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            });
        
        
        // changeLanguage = function (languageValue) {
        //     lang = languageValue;
        //     translate_article();
        //     hash2 = true;
        //     window.location.href = "" + "#lang=" + lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
        //     
            $(".lanValue").removeClass('active');
            $("."+ languageValue +"").find("a").addClass('active'); 
        }

        url2quex = function (xxx) {
            if (typeof xxx === "string" && xxx.indexOf(",") == "-1") {
                xxxx = [];
                xxxx.push(xxx);
                main_functions.url2que(xxxx);
            } else {
                if (typeof xxx === "string" && xxx.length > 1) {
                    xxx = xxx.split(",");
                }
                main_functions.url2que(xxx);
            }
        };

        layerinitx = function () {

            var uniqueQue = [];
            $.each(que, function (i, el) {
                if ($.inArray(el, uniqueQue) === -1)
                    uniqueQue.push(el);
            });

            que = uniqueQue;

            imageParameters.layerDefinitions = layerDefs;
            layerinit.layerinit(intlayer, imageParameters, labmapservice, que, alphax);
        };

        fishupdatex = function () {
            if (speclayer) {
                if (timeSlider.timeStops)
                    fishallinupdate();
            } else {
                //lchange("1");
            }
            ;
        };


        function limitMapExtent(map) {
            map.on('extent-change', function(event) {
                //If the map has moved to the point where it's center is 
                //outside the initial boundaries, then move it back to the 
                //edge where it moved out
                var currentCenter = map.extent.getCenter();
                if(event.delta != null){
                    if (event.delta.x !== 0 && event.delta.y !== 0) {
            
                        var newCenter = map.extent.getCenter();
            
                        //check each side of the initial extent and if the 
                        //current center is outside that extent, 
                        //set the new center to be on the edge that it went out on
                        if (currentCenter.x < maxExtent.xmin) {
                            newCenter.x = maxExtent.xmin;
                        }
                        if (currentCenter.x > maxExtent.xmax) {
                            newCenter.x = maxExtent.xmax;
                        }
                        if (currentCenter.y < maxExtent.ymin) {
                            newCenter.y = maxExtent.ymin;
                        }
                        if (currentCenter.y > maxExtent.ymax) {
                            newCenter.y = maxExtent.ymax;
                        }
                        map.centerAt(newCenter);
                    }
                }
            });
        }

        urlupdate = function () {
            /*      for (var l in lai) {
                 var checkop = urlobj.theme.substr(urlobj.theme.search(lai[l].idd) + lai[l].idd.length, 1);
                 if (checkop == ":") {
                 //  var urlthemepar = urlobj.theme.substr(urlobj.theme.search(lai[l].idd + ":"), lai[l].idd.length + 5);
                 } //correggere!!!!}
                 else {
                 var urlthemepar = lai[l].idd;
                 var urlthemerepl = lai[l].idd + ":" + lai[l].opacity;
                 urlobj.theme = urlobj.theme.replace(urlthemepar, urlthemerepl);
                 window.history.pushState(urlthemepar, "Opacity change", "" + "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme);
                 }
                 }*/
        };



        urlupdateok = function () {
            var urlthemepar = [];
            if (layerInfo2.length > 0) {
                for (var l in layerInfo2) {
                    if (layerInfo2[l].idd) {
                        var urlthemerepl = layerInfo2[l].idd + ":" + layerInfo2[l].alpha;
                        urlthemepar.push(urlthemerepl);
                    }
                }

                urlobj.theme = urlthemepar.toString();
            }
            if (embedurl) {
                if($(".modal").hasClass("show-modal")){
                    toggleModal();
                }
            }
            urlobj.z = map.getZoom();
            urlobj.c = map.extent.getCenter().x + "," + map.extent.getCenter().y;
            
            window.history.pushState(urlthemepar, "Opacity change", "" + "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z);
        };



        // on document ready load other script

        $(document).ready(function () {

            // submenu of layers expand and collapse function
            var togglemenuchange = function () {
                if ($(this).is("li.yyy.changed")) {
                } else {
                    $(".yyy").removeClass("changed");
                    $(".zzz").hide();
                }
                $(this).toggleClass("changed");
                if ($(this).is("li.yyy")) {
                    $(this).nextUntil(".yyy", ".zzz").toggle();
                    return;
                }
            };
            var togglemenuchange2 = function () {
                $(this).toggleClass("changed");
                if ($(this).is("li.xxx")) {
                    $(this).nextUntil(".xxx", ".yyy").toggle();
                    if ($(this).next(".yyy").is(":visible")) {
                    } else {
                        $(this).nextUntil(".xxx", ".zzz").hide();
                        $(this).nextUntil(".xxx", ".yyy").removeClass("changed");
                    } // hide submenu if groupmenu is hided
                    return;
                }
            };
            $(document).on("click touchstart", "#LayersMenu .chosen-container .chosen-results .group-result", togglemenuchange);
            $(document).on("click touchstart", "#LayersMenu .chosen-container .chosen-results li.disabled-result", togglemenuchange2);
            $(document).on("keyup", ".chosen-container-single .chosen-search input[type=\"text\"]", function () {
                $(".zzz").addClass("showed");
            });



        });


        $("#LAYERSELECTION1").on("change", function () { // FOR MAPS

            var endvalList = $(this).val();
            var z = $("#LAYERSELECTION1");
            var val = $(z).find("option[value=\"" + endvalList + "\"]");
            var endvalName = val.attr("id");
            //var endvalBG = val.attr('valueBG');

            //urlobj.bkgd = endvalBG.toString();
            urlobj.theme = endvalList.toString();
            basemapGallery.select(urlobj.bkgd); // SELECT BG from theme.json
            window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            var newque = urlobj.theme;
            que = [];
            url2quex(newque);
            translation1();
            layerinitx();
            fishupdatex();
            hashcheck = true;

            map.infoWindow.hide(); // to hide info popup window from map            
            closeinfolayer();
        });
        
        thematicSelection = function(layersIDs,themeName,themeIndex) {
            var endvalList = layersIDs;
            theme_Index = themeIndex;
            loadedLayers = [];
            urlobj.theme = endvalList.toString();
            basemapGallery.select(urlobj.bkgd); // SELECT BG from theme.json
            window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            var newque = urlobj.theme;
            que = [];
            url2quex(newque);
            translation1();
            layerinitx();
            fishupdatex();
            hashcheck = true;

            map.infoWindow.hide(); // to hide info popup window from map            
            closeinfolayer();
            if($(".modal").hasClass("show-modal")){
                toggleModal();
            }
            if($(".modalSearch").hasClass("show-modal")){
                toggleModalSearch();
            }
             //close modal
            $("#predefinedmapCont").show();
            //$("#predefinedmapCont").removeClass();
            $("#predefinedmapName").text(themeName);
        }

        function clearGraphics() {
            //first remove all graphics added directly to map
            //map.graphics.clear();

            //now go into each graphic layer and clear it
            var graphicLayerIds = map.graphicsLayerIds;
            var len = graphicLayerIds.length;
            for (var i = 0; i < len; i++) {
                var gLayer = map.getLayer(graphicLayerIds[i]);
                //clear this Layer
                gLayer.clear();
            }

        }

        CLEAR = function () {
            $("#map_graphics_layer g").empty(); //REMOVE GETFEATUREINFO CONTOUR
            $("#predefinedmapCont").hide();
            clearGraphics();
            for (var i = lai.length - 1; i >= 0; i--) {
                lai[i].setVisibility(false);
            }
            urlobj.theme = "";
            window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            var newque = urlobj.theme;
            que = [];
            url2quex(newque);
            removeAllLayers();
            intlayer = [];
            listlayerx2ok = [];
            //  que.push(endval);
            //         translation1();
            if (intlayer.length > 0)
                layerinitx();
            lchange();
            checkb();
            map.infoWindow.hide(); // to hide info popup window from map            
            closeinfolayer();
            hash2 = true;
            if ($("#measure_container").css('display') != 'none') {
                $("#measure_container").css({"top":"165px"});
            }
            if( $("#buttonshow i").hasClass("fa-angle-left")){
                moveShareButton();
            }
        };
        $("#clearbutton_label").click(function () {
            // $(".esriPopup").remove();
            $(".titleButton").click();

        });
        //	$("#myubutton").click(function(){
        //     $("#highcharts-0").hide();
        //    });
        $("#LAYERSELECTION3").on("change", function () { //FOR LAYERS

            var x = $(this).val();
            var z = $("#LAYERSELECTION3");
            var val = $(z).find("option[value=\"" + x + "\"]");
            var endval = val.attr("id");

            var urllay = window.location.hash;
            var themeurl = urlobj.theme;
            main_functionsx.seturlobj();
            var urltheme = urlobj.theme;
            //if (que.includes(endval)) 

            /*if (que.indexOf(endval) > -1)
                {
                } else
                if (endval) {*/


            if (urltheme) {
                urlobj.theme = urltheme + "," + endval;
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            } else {
                urlobj.theme = endval;
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            }
            ;

            var newque = urlobj.theme;
            que = [];
            url2quex(newque);
            translation1();
            layerinitx();
            main_functions.fishtrasl(); //to empty fish list
            //document.getElementById("LAYERSELECTION3").value = "";
            fishupdatex();

            //    urlupdate();
            hashcheck = true;

            //}

            map.infoWindow.hide(); // to hide info popup window from map            
            closeinfolayer(); 
        });

        addLayersId = function(layerID2) {
            var endval = layerID2;
            var urllay = window.location.hash;
            var themeurl = urlobj.theme;
            main_functionsx.seturlobj();
            var urltheme = urlobj.theme;
            if (urltheme) {
                urlobj.theme = urltheme + "," + endval;
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            } else {
                urlobj.theme = endval;
                window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;
            }
            var newque = urlobj.theme;
            que = [];
            url2quex(newque);
            translation1();
            layerinitx();
            main_functions.fishtrasl(); //to empty fish list
            fishupdatex();
            hashcheck = true;
            map.infoWindow.hide(); // to hide info popup window from map            
            closeinfolayer();
        };

        //   basemapGallery.on("selection-change", function () {
        dojo.connect(basemapGallery, "onSelectionChange", function () {
            var basemapx = basemapGallery.getSelected();
            urlobj.bkgd = basemapx.id;
            window.location.hash = "#lang=" + urlobj.lang + ";p=" + urlobj.p + ";bkgd=" + urlobj.bkgd + ";theme=" + urlobj.theme + ";c=" + urlobj.c + ";z=" + urlobj.z;

        });

        main_functionsx.seturlobj();
        var llla = urlobj.lang;

        resizeMaxLayerPane = function() {
            var fishPanelHeight = $("#titlePane1").height();
            if ( toggledFish ) { //IF HAS NOT FISH PANEL
                fishPanelHeight = 0;
            }
            var maxHeightLayer = $(window).height() - 276 - fishPanelHeight;
            var maxHeightLegend = $(window).height() - 240;
            $("#layerPane").css({"max-height":maxHeightLayer});
            $("#legendPane").css({"max-height":maxHeightLegend});
        }

        $(window).on('resize', function(){
            resizeMaxLayerPane();
        });

        translateThemeName = function() {
            if ($("#predefinedmapCont").is(":visible")) {
                inputlistofmap = THEMES_CONFIG.services;
                $.each(inputlistofmap, function (key, value) {
                    if (key == theme_Index) {
                        $("#predefinedmapName").text(value.THEME);
                    }
                });
            }
        };

        searchThemeById = function(idLayer) {
            var themeName = [];
            inputlistofmap = THEMES_CONFIG.services;
            $.each(inputlistofmap, function (key, value) {
                var arrayThemes = (value.LAYERS_LIST).split(",");
                for (var i in arrayThemes) {
                    if (idLayer == arrayThemes[i].split(":")[0]) {
                        themeName.push(value.THEME);
                    }
                }
            });
            return themeName;
        };
        
        var start = function () {

            resizeMaxLayerPane();
            // sostituire con la reinterpretazione dell hash con urlobj
            if (hashcheck !== true && hash2 == true) { //check if add.maplayers is done, if not don't restart it
                resizeMaxLayerPane();
                translateThemeName();
                main_functionsx.seturlobj()
                var temptheme = [];
                var temptheme1 = [];
                var themetemp = [];

                if (urlobj.theme.length > 0)
                    temptheme = urlobj.theme.split(",");
                for (var i in temptheme) {//get the theme number for all theme in string
                    var themetemp = temptheme[i].split(":");
                    if (SERVICES_CONFIG.filter(function (item) {
                        return (item.ID == themetemp[0]);
                    })[0] || themetemp[0] == "") //check if requested theme number is in our list
                    {
                        console.log(themetemp[0] + " exist");
                        //  if (temptheme1.includes(themetemp[0])) 
                        if (temptheme1.indexOf(themetemp[0]) > -1) //if it exist in list in complete listcheck if is it in used list
                        {
                        } else {
                            temptheme1.push(themetemp[0]);  // if is not used put it inside the list
                        }
                    }
                    else {
                        alert("Layer " + themetemp[0] + " does not exist");
                        console.log(themetemp[0] + " does not exist");
                    }
                }

                var temptheme2 = [];
                for (var i in temptheme1) {
                    temptheme2.push(temptheme1[i] + ":0.75");
                }

                // urlobj.theme = temptheme2.toString(); this instruction restart the opacity to 0.75
                var basemapx = basemapGallery.getSelected();
                if (basemapx.id != urlobj.bkgd)
                    basemapGallery.select(urlobj.bkgd);
                var llag = urlobj.lang;
                $(".lanValue").removeClass('active');
                $("."+ llag +"").find("a").addClass('active');
                changeLanguageText(llag);
                //$("#language").val(llag);
                //  main_functions.fishtrasl(); //to empty fish list
                if (urlobj.theme.length > 0) {
                    if (llag === llla) {
                        his = urlobj.theme;
                        his.replace("\"", "");
                        if (listlayerx2ok)
                            if (his !== listlayerx2ok.toString()) {
                                var newque = his.split(",");
                                que = [];
                                url2quex(newque);
                                translation1();
                                layerinitx();
                                //       console.log(lai[0].layerDefinitions[0])
                                //  fishupdatex();  
                                quex = newque;
                            }

                    } else {
                        justtranslated = false;
                        lchange("1");
                        llla = llag;
                        if ($( ".modal" ).hasClass( "show-modal" )){ //MODAL OF LAYERS IS OPENED
                            main_functions.listalayercreation2();
                            main_functions.listalayercreation3();
                            main_functions.listalayerspredmaps();
                    }
                        else if ($( ".modalSearch" ).hasClass( "show-modal" )){ //MODAL OF LAYERS IS OPENED
                            main_functions.listalayercreation2();
                            main_functions.listalayercreation4();
                            main_functions.listalayerspredmaps();
                        }
                    }
                } else {

                    lchange("1");
                    llla = llag;
                }

                llla = llag;
                var xyCenter = urlobj.c.split(",");
                if (!isNaN(xyCenter[0]) && !isNaN(xyCenter[1])) {
                } else {
                    if (urlobj.p === "w") {
                        urlobj.c = "1253866.2175874896,7033312.218247011";
                        urlobj.z = 4;
                    } else {
                        urlobj.c = "-103858.90000000037,723739.5250000004";
                        urlobj.z = 1;
                    }
                }
                checkEmbedUrl();
                if (urlobj.z && urlobj.z !== "undefined") {
                    map.setZoom(urlobj.z);
                }
                var wkidValue;
                if (urlobj.p === "w") {
                    wkidValue = 102100;
                } else {
                    wkidValue = 102017;
                }
                if (urlobj.c && urlobj.c !== "undefined") {
                    var xyCenter = urlobj.c.split(",");
                    var centerPoint = new esri.geometry.Point({
                        "x": xyCenter[0],
                        "y": xyCenter[1],
                        "spatialReference": new esri.SpatialReference({ wkid: wkidValue })
                    });
                    map.centerAt(centerPoint);
                }
            }
            /*if( $("#buttonshow i").hasClass("fa-angle-left")){
                moveShareButton();
            }*/

        };


        $(window).on("load", function () {	
            justloaded = true;
            if (allLoaded  && justloaded) {
            start();
            }	
        });

        $('a[href="#exportPane"]').click(function(){
            if($("#export_error").is(":visible")) {
                $("#export_error").hide();
            }
        });


        $(window).on("hashchange", function () {
            hash2 = true;
            if ($( ".modalShare" ).hasClass( "show-modal" )) {
                setEmbedText();
                setUrlEmailText();
            }
            setTimeout(function () {
                start();
            }, 500);
            justloaded = true;
            closeinfolayer();
            if (map.infoWindow) map.infoWindow.hide();
        });


        listmenu = function () {

            if ($(this).is("li.group-result")) {
                $(this).nextUntil(".group-result").toggle();
                a = a + 1;
            }
        };
        $("#LayersMenu .chosen-container .chosen-results").on("click", "li",
            listmenu
        );
    });
});

$(window).resize(function () {
    if ($(".opentip-container").length >0)   $(".opentip-container").reposition();
});