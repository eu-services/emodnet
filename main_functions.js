var optlaye;
var groupname = "prueba";
define(["dojo/dom", "dojo/_base/array", "dojo/dom-style"], function (dom, arrayUtils, domstyle) {
    return {
        swap: function (theArray, indexA, indexB) {
            var temp = theArray[indexA];
            theArray[indexA] = theArray[indexB];
            theArray[indexB] = temp;
            return theArray;
        },
        showLoading: function () {
            domstyle.set(dom.byId("loader"), "display", "inline-block");
            return true;
        },
        hideLoading: function () {
            domstyle.set(dom.byId("loader"), "display", "none");
            var numVisibLayersLayers=0;
            for (var i=0; i<layerInfo.length; i++) { 
                if ((map.getLayer(layerInfo[i].layer.id) || "") != "") 
                    if (map.getLayer(layerInfo[i].layer.id).visible) numVisibLayersLayers++;	
            }
            
            var noLegendElem = document.getElementById("legendDiv_msg");
            if ((noLegendElem || '') != '') {
                if (numVisibLayersLayers > 0) {
                    noLegendElem.innerHTML = "";
                    $(noLegendElem).hide();
                } else {
                    noLegendElem.innerHTML = article["#legendDiv_msg"];
                    $(noLegendElem).show();
                }
                //noLegendElem.innerHTML =  numVisibLayersLayers > 0? "":  article["#legendDiv_msg"];
            }	
            return true;
        },
        /////////////// SORT BY ATTRIBUTE FUNCTION
        dynamicSort: function (property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            };
        },
        intersect_safe: function (a, b) {   //better performance than foreach
            mapservice = [];
            labmapservice = [];
            idlayer = [];
            intlayer = [];
            title = [];
            identifyinfo = [];
            type = [];
            fishlist = [];
            description = [];
            alpha = [];

            var ai = 0;
            var bi = 0;
            var result = [];
            if (b) {
            } else {
                b = 0;
            }
            ;

            while (ai < a.length && bi < b.length) {
                if (a[ai].ID !== b[bi]) {
                    ai++;
                }
                else /* they're equal */ {

                    // ai++;
                    var serviceInfos = a[ai];

                    result.push(a[ai].MAPSERVICE);

                    idlayer[bi] = serviceInfos.ID;
                    //  mapservice[0] = serviceInfos.MAPSERVICE;
                    mapservice[bi] = serviceInfos.MAPSERVICE;
                    if (serviceInfos.LBL_MAPSERVICE) {
                        labmapservice[bi] = serviceInfos.LBL_MAPSERVICE;
                    }
                    intlayer[bi] = serviceInfos;  ///IMPORTANTE ... CONTIENE TUTTE LE CARATTERISTICHE DEL LAYER
                    title[bi] = serviceInfos.TITLE;
                    identifyinfo[bi] = serviceInfos.IDENTIFYINFO;
                    type[bi] = serviceInfos.TYPE;
                    fishlist[bi] = serviceInfos.FISHLIST;
                    description[bi] = serviceInfos.DESCRIPTION;

                    if (alphax[bi]) {  //se esiste alpha nell`url lo utilizza 
                        alpha[bi] = alphax[bi];
                    } else {        //altrimenti utilizza quello nel json che se non esiste viene rimpiazzato con 0.75
                        if (serviceInfos.ALPHA) {
                            alpha[bi] = serviceInfos.ALPHA;
                            alphax[bi] = alpha[bi];
                        } else {
                            alpha[bi] = 0.75;

                            alphax[bi] = alpha[bi];
                        }
                    }

                    bi++;
                    ai = 0;

                    // var widget = new ServiceName ({serviceName: serviceName}).placeAt(legendDiv);

                }
            }

            return result;
        },
        intersect_safe2: function (a, b) {
            var ai = 0, bi = 0;
            var result = new Array();

            while (ai < a.length && bi < b.length) {
                if (a[ai] < b[bi]) {
                    ai++;
                }
                else if (a[ai] > b[bi]) {
                    bi++;
                }
                else /* they're equal */ {
                    result.push(a[ai]);
                    ai++;
                    bi++;
                }
            }

            return result;
        },
        url2que: function (url) {
            que = [];
            alphax = [];
            arrayUtils.forEach(url, function (quef, indexx) {
                var quex2 = url[indexx].split(":");
                que[indexx] = quex2[0];
                alphax[indexx] = quex2[1];
            }
            );
            return true;
        },
        seturlobj: function () {
            var str2 = window.location.hash.substr(1).replace("#", "");
            var temp2 = [];
            temp2 = str2.split(";");
            //   urlobj = {};
            for (var char in temp2) {
                var tempobjectx = temp2[char].split("=");
                urlobj[tempobjectx[0]] = tempobjectx[1];
            }
            return true;
            if (typeof urlobj.lang === "undefined") {
                urlobj.lang = "EN";
            }
            return urlobj;
        },
        /////////////////////////////////////////////TRANSLATION GENERIC FUNCTION
        translater: function (input) {
            for (var k in input) {
                if (input[k]) {
                    if (input[k].charAt(0) === "{") {
                        for (var i in translated) {
                            if (input[k] === translated[i].attributes.UNIQUEID) {
                                output[k] = translated[i].attributes.TRANSLATION;
                            }
                        }
                    }
                    else
                        output[k] = input[k];
                } else
                    output[k] = "null";
            }
            return output;
        },
        fishtrasl: function () {
            output = {};  //empty output
            var fishinput = [];
            for (var za in FISH_CONFIG) {
                fishinput[za] = FISH_CONFIG[za].NAME;
            }
            this.translater(fishinput);

            for (zo in output) {
                FISH_CONFIG[zo].NAMET = output[zo];
            }
            FISH_CONFIG.sort(this.dynamicSort("NAMET"));
            var selectedx = document.getElementById("spec");
            //    if (typeof selectedx.options[selectedx.selectedIndex] !== "undefined")
            //       var speciesx = selectedx.options[selectedx.selectedIndex].value;
            var speciesx = $("#spec").find(":selected").val();
            if (speciesx) {
            }
            else {
                speciesx = fish_options[0];
            }
            $("#spec").empty();


            var select = document.getElementById("spec");  //prende elemendo con id spec


            // var options = FISH_CONFIG;
            fishopt2 = [];
            fishopt3 = [];
            for (var i in fish_options) {
                for (var l in FISH_CONFIG) {
                    if (FISH_CONFIG[l].FISHCODE)
                        if (fish_options[i] === FISH_CONFIG[l].FISHCODE) {
                            var opt = FISH_CONFIG[l];
                            var el = document.createElement("option");

                            if (opt.FISHCODE === speciesx)
                                el.selected = selectedx;
                            //      fishopt2[i] = FISH_CONFIG[i];
                            fishopt2.push(FISH_CONFIG[l]);

                            el.textContent = opt.NAMET;
                            el.value = opt.FISHCODE;

                            select.appendChild(el); //inserisce in spec option opt
                        }
                }
            }

            // fishopt2.sort(dynamicSort("NAMET")); 

            this.specieslink();
            return true;
        },
        fishupdate: function () {

            timeSlider.pause();

            // var exx = document.getElementById("spec");
            //    species = exx.options[exx.selectedIndex].value;

            species = $("#spec").find(":selected").val();  //get fish code from list
            var prelay = lai[speclayer].id; //get id of fish layer from lai 
            var prelayidd = lai[speclayer].idd;
            var prelaymapid = map.layerIds.indexOf(prelay); //get id of fish layer on map
            map.removeLayer(lai[speclayer]);  //remove fish layer from map
            layersupdate2(prelay);
            //   map.addLayer(lai[speclayer]);
            //    lai[speclayer].refresh();
            lai[speclayer].serv = speclayer;
            lai[speclayer].idd = prelayidd;
            lai[speclayer].type = "fish-dynamic";
            // lais.alpha = alpha[ll];
            lai[speclayer].pos = speclayer;

            //map.addLayer(lai[speclayer], prelaymapid);
            //  layersupdate2(prelay);
            //lchange("1");
            //setDefinitionExpression
            // fishtrasl();
            //            checkb('1');
            return true;
        },
        specieslink: function () {

            var name = species;
            var LANG = lang;
            var YEAR;
            var urllink = "";
            if (currentYear) {
                YEAR = currentYear;
            } else { YEAR = ""; };

            //         var indurl =   fishopt2.FISCHCODE.findIndex(species);
            //  var indurl = fishopt2.map(function(e) {return e.FISCHCODE;}).indexOf(species);
            //        var   indurl = fishopt2.map(function(e) { return e.indexOf(species); });
            //  var indurl = fishopt2.findIndex('FISCHCODE',species);
            if (fish_options.indexOf(name) !== -1) {
                var indurl = fish_options.indexOf(name);
                var url = fishopt2[indurl].IMAGE;
                if (fishopt2[indurl].LINK)
                    urllink = fishopt2[indurl].LINK;
                // resources/TAC/%YEAR%/%LANG%/BFT.htm
                //alert(fishopt2[indurl].FISHDATECODE.getUTCFullYear());//or.. "page_" + name + ".html";
                //  var url2 = "resources/TAC/" + YEAR + "/" + LANG + "/" + species + ".htm";
                var url2 = urllink.replace("%LANG%", LANG).replace("%YEAR%", YEAR);
                var link = "<a   href=\"" + url2 + "\" target=\"same\" onclick=window.open(this.href,\"same\",\"width=1005,height=1000\")  >  More info</a>";
                var ima = "<img  src=\"" + url + "\"></img>  <BR>   " + link;

                {
                    $("#resultlink").empty();
                    $("#resultlink").addClass("g_title");
                    if (url) {
                        $("#resultlink").append(ima);
                    }

                }

            }
            return true;
        },
        ///////////////////////////////////////////////////// CHECK IF IS IT A TIMELAYER AND SETUP A TIME SLIDER
        checkslider: function () {

            var cont = 0;
            for (var i = 0; i < layerInfo2.length; i++) {
                if (layerInfo2[i].layer.timeInfo) {
                    if (layerInfo2[i].layer.visible) {
                        if (layerInfo2[i].layer.lab !== "label") {
                            cont = cont + 1;
                        }
                    }
                }
            }
            var hided;
            if (cont > 0) { // If there is at lest one timelayer , timeslider is showed 
                hided = false;
                cliccatotime(hided);
                //   initsli('1');
            }
            else {
                hided = true;
                cliccatotime(hided);
            }
            return true;
        },
        ///////////////////////////////////////////////////// CHECK IF IS IT A TIMELAYER AND SETUP A TIME SLIDER
        checkfishdim: function () {

            var cont = 0;
            for (var i = 0; i < layerInfo2.length; i++) {
                if (layerInfo2[i].type) {
                    if (layerInfo2[i].type === "fish-dynamic") {
                        if (layerInfo2[i].layer.visible) {
                            if (layerInfo2[i].layer.lab !== "label") {
                                cont = cont + 1;
                            }
                        }
                    }
                }
            }
            var hidedf;
            if (cont > 0) { // If there is at lest one timelayer , timeslider is showed 
                hidedf = false;
                cliccatofish(hidedf);
                //   initsli('1');
            }
            else {
                hidedf = true;
                cliccatofish(hidedf);
            }
            return true;
        },
        changeSliderCss: function () {
            var isIE = false || !!document.documentMode;
            var initPosSlide = -160;
            var incrementSlide = 130;
            var initPosSliderr = 120;
            var incrementSliderr = -130;
            for (var i=0;i<numSliders;i++) {
                if (isIE) {
                    $(".slide" + (i+1)).css({"background": "white", "left": (initPosSlide)+"%"});
                    initPosSlide = initPosSlide + incrementSlide;
                } else {
                    $(".slide" + (i+1)).css({"background": "white", "left": (i)+"00%"});
                }
                if (i != (numSliders-1)) {
                    $(".slide-radio" + (i+1) + ":checked ~ .next .numb" + (i+2)).css({"display": "block", "z-index": "1"});
                    $(".slide-radio" + (i+2) + ":checked ~ .previous .numb" + (i+1)).css({"display": "block", "z-index": "1"});
                }
                if (i == 0) {
                    $(".slide-radio" + (i+1) + ":checked ~ .previous .numb" + (i+1)).css({"display": "block", "opacity": "0.3", "cursor": "auto"});
                }
                if (i == (numSliders-1)) {
                    $(".slide-radio" + (i+1) + ":checked ~ .next .numb" + (i+1)).css({"display": "block", "opacity": "0.3", "cursor": "auto"});
                }
                $(".slide-radio" + (i+1) + ":checked ~ .slider-pagination .page" + (i+1)).css({"background": "#004593", "border-color": "#004593","cursor": "auto"});
                if (isIE) {
                    $(".slide-radio" + (i+1) + ":checked ~ .sliderr").css({"-webkit-transform": "translateX(-"+(i)+"00%)", "transform": "translateX("+(initPosSliderr)+"%)"});
                    initPosSliderr = initPosSliderr + incrementSliderr;
                } else {
                    $(".slide-radio" + (i+1) + ":checked ~ .sliderr").css({"-webkit-transform": "translateX(-"+(i)+"00%)", "transform": "translateX(-"+(i)+"00%)"});
                }
                $(".slide-radio" + (i+1) + ":checked ~ .slide" + (i+1) +" h2").css({"-webkit-transform": "translateX(0)", "transform": "translateX(0)", "opacity": "1"});
                $(".slide-radio" + (i+1) + ":checked ~ .slide" + (i+1) +" .button").css({"-webkit-transform": "translateX(0)", "transform": "translateX(0)", "opacity": "1"});
            }
        },
        changeSliderCss2: function () {
            var main_functions = this;
            setTimeout(function () {
                for (var i=0;i<numSliders;i++) {
                    $(".slide-radio" + (i+1) + " ~ .next .numb" + (i+1)).css({"display": "none", "z-index": "0", "opacity": "1", "cursor": "pointer"});
                    $(".slide-radio" + (i+1) + " ~ .previous .numb" + (i+1)).css({"display": "none", "z-index": "0", "opacity": "1", "cursor": "pointer"});
                    $(".slide-radio" + (i+1) + " ~ .slider-pagination .page" + (i+1)).css({"background": "white", "border-color": "black","cursor": "pointer"});
                }
                main_functions.changeSliderCss();
            }, 50);
        },
        listThemesCreationModalHome: function () {
            $(".css-slider-wrapper").empty();
            inputlistofmap = THEMES_CONFIG.services;
            var lengthThemes = inputlistofmap.length;
            numSliders = Math.ceil(lengthThemes/3);
            var inputsHtml = "";
            var labelsHtml = "<div class='slider-pagination'>";
            var nextConHtml = "<div class='next control'>";
            var prevConHtml = "<div class='previous control'>";
            for (var i=0;i<numSliders;i++) {
                if (i==0) 
                    inputsHtml = inputsHtml + "<input type='radio' name='slider' class='slide-radio"+(i+1)+"' checked id='slider_"+(i+1)+"'>";
                else
                    inputsHtml = inputsHtml + "<input type='radio' name='slider' class='slide-radio"+(i+1)+"' id='slider_"+(i+1)+"'>";
                labelsHtml = labelsHtml + "<label for='slider_"+(i+1)+"' class='page"+(i+1)+"' onclick='javascript:changeSliderMain()'></label>";
                nextConHtml = nextConHtml + "<label for='slider_"+(i+1)+"' class='numb"+(i+1)+"' onclick='javascript:changeSliderMain()'><i class='fas fa-angle-right'></i></label>";
                prevConHtml = prevConHtml + "<label for='slider_"+(i+1)+"' class='numb"+(i+1)+"' onclick='javascript:changeSliderMain()'><i class='fas fa-angle-left'></i></label>";
            }
            labelsHtml = labelsHtml + "</div>";
            nextConHtml = nextConHtml + "</div>";
            prevConHtml = prevConHtml + "</div>";
            
            slidersHtml = "";
            textOfLayers4 = "";
            $.each(inputlistofmap, function (key, value) {
                var thumbLayerSrc = value.THUMBNAIL;
                if (!((value.THUMBNAIL || '') != ''))
                    thumbLayerSrc = "img/noimage.png";
                var textOfLayers2 = "<div class='theme_img'><div class='theme_img_des'><p class='theme"+(key+1)+"_Name'>"+value.THEME+"</p><p class='theme"+(key+1)+"_Layers'></p></div><img src='"+thumbLayerSrc+"'></div>";
                var textOfLayers3 = "<div class='theme_title'><span class='theme_title_des'>"+value.THEME+"</span><a href=\"javascript:thematicSelection(\'"+value.LAYERS_LIST+"\',\'"+value.THEME+"\',\'"+key+"\')\"><div class='theme_title_button'></div></a></div>";
                textOfLayers4 = textOfLayers4 + "<div class='theme theme"+(key+1)+"'>" + textOfLayers2 + textOfLayers3 + "</div>";
                if ((key)%3 == 0) {
                    slidersHtml = slidersHtml + "<div class='sliderr slide" + (Math.ceil(key/3)+1) + "'><div class='slide_cont'>";
                }
                if ((key+1)%3 == 0) {
                    slidersHtml = slidersHtml + textOfLayers4 + "</div></div>";
                    textOfLayers4 = "";
                } else {
                    if (lengthThemes == key+1) {
                        slidersHtml = slidersHtml + textOfLayers4 + "</div></div>";
                        textOfLayers4 = "";
                    }
                }
            });
            var allSlidersHtml = inputsHtml+labelsHtml+nextConHtml+prevConHtml+slidersHtml;
            $(allSlidersHtml).appendTo($(".css-slider-wrapper"));
            this.listalayerspredmaps();
            this.changeSliderCss();
        },
        listThemesCreationModalSearch: function () {
            $("#predefinedPane1").empty();
            inputlistofmap = THEMES_CONFIG.services;
            $.each(inputlistofmap, function (key, value) {
                var $select2 = $("#predefinedPane1");
                var thumbLayerSrc = value.THUMBNAIL;
                if (!((value.THUMBNAIL || '') != ''))
                    thumbLayerSrc = "img/noimage.png";
                var textOfLayers2 = "<div class='theme_img'><div class='theme_img_des'><p class='theme"+(key+1)+"_Name'>"+value.THEME+"</p><p class='theme"+(key+1)+"_Layers'></p></div><img src='"+thumbLayerSrc+"'></div>";
                var textOfLayers3 = "<div class='theme_title'><span class='theme_title_des'>"+value.THEME+"</span><a href=\"javascript:thematicSelection(\'"+value.LAYERS_LIST+"\',\'"+value.THEME+"\',\'"+key+"\')\"><div class='theme_title_button'></div></a></div>";
                var textOfLayers4 = textOfLayers2 + textOfLayers3;
                $("<div class='theme theme" + (key+1) + "'></div>").html(textOfLayers4).appendTo($select2);
            });
            this.listalayerspredmaps();
        },
        listalayercreationin: function () {

            listalayertit = [];
            listalayerid = [];
            listalayertitt = [];
            arrayUtils.forEach(SERVICES_CONFIG, function (xxx, indx) {

                listalayertit[indx] = SERVICES_CONFIG[indx];
                listalayertitt[indx] = SERVICES_CONFIG[indx].TITLE;

            });
            // listalayertitt = [];
            output = {};
            this.translater(listalayertitt);

            for (var inn0 in listalayertitt) {
                listalayertit[inn0].TRASL = output[inn0];// Translated values
            }

            optlaye = {};

            var groupuid = [];
            var mastergroupuid = [];
            for (var i in worarc.services) {

                groupuid[i] = worarc.services[i].GROUPS;
                mastergroupuid[i] = worarc.services[i].MASTERGROUPID;
            }
            output = [];

            this.translater(groupuid);
            var groupuidtrad = output;
            layers_of_group_list = [];
            for (var indice in worarc.services) { // for every group i create an array with relative layers

                //    var groupuid = worarc.services[indice].GROUP;

                var layers_of_group_list_id = worarc.services[indice].LAYERS_LIST.split(";");
                layers_of_group_list = [];
                for (var i in layers_of_group_list_id) {

                    var result = SERVICES_CONFIG.filter(function (obj) {
                        return obj.ID === layers_of_group_list_id[i];
                    });

                    if (worarc.services[indice].LAYERS_LIST)
                        layers_of_group_list[i] = {
                            id: result[0].ID,
                            name: result[0].TRASL,
                            group: worarc.services[indice].MASTERGROUPID
                        }
                    else {

                        layers_of_group_list[i] = {
                            id: "",
                            name: "----------------------",
                            group: "..."
                        };
                    }
                    ;

                }

                optlaye[groupuidtrad[indice]] = layers_of_group_list;
                mastergroupuidx = [];
                $.each(mastergroupuid, function (i, el) {
                    if ($.inArray(el, mastergroupuidx) === -1)
                        mastergroupuidx.push(el);
                });


            }
            ;
            return true;
        },
        listalayercreation2: function () {
            this.listalayercreationin();
            $(".required").remove();
            $(".k-list-container.k-popup.k-group.k-reset").remove();
            $(".closeSearchButton").remove();
            $("<select class=\"required\" multiple=\"multiple\" ></select><button class=\"closeSearchButton\" id=\"searchButton1\"><i class=\"fas fa-times\"></i></button>").appendTo($("#multisearch1"));
            $("<select class=\"required\" multiple=\"multiple\" ></select><button class=\"closeSearchButton\" id=\"searchButton2\"><i class=\"fas fa-times\"></i></button>").appendTo($("#multisearch2"));
            //$(".k-widget k-multiselect k-header required").remove();

            var select = $(".required");

            for (var inde in mastergroupuidx) {
                $.each(optlaye, function (key, value) {
                    var group;
                    groupname = key;
                    $.each(value, function () {
                        if (this.group === mastergroupuidx[inde] && mastergroupuidx[inde] === "") {
                            var isLayer = false;
                            for (var x in layerInfo2) {
                                if (this.id == layerInfo2[x].idd) {
                                    isLayer = true;
                                    break;
                                }
                            }
                            if (isLayer) { //CHECK IF LAYERS ARE OPENED OR NOT
                                $("<option class=\"zzz\" id=\"" + this.id + "\" value=\"" + [this.id,groupname,searchThemeById(this.id)] + "\" selected>").html(this.name).appendTo(select);
                            } else {
                                $("<option class=\"zzz\" id=\"" + this.id + "\" value=\"" + [this.id,groupname,searchThemeById(this.id)] + "\" >").html(this.name).appendTo(select);
                            }
                        }
                    });
                });

            }


            $("#LAYERSELECTION3").trigger("chosen:updated");

            function detectmob() {
                if (navigator.userAgent.match(/Android/i)
                    || navigator.userAgent.match(/webOS/i)
                    || navigator.userAgent.match(/iPhone/i)
                    || navigator.userAgent.match(/iPad/i)
                    || navigator.userAgent.match(/iPod/i)
                    || navigator.userAgent.match(/BlackBerry/i)
                    || navigator.userAgent.match(/Windows Phone/i)
                ) {
                    $("#LAYERSELECTION3").chosen({ width: "99%", include_group_label_in_selected: "true", disable_search: "true", children: "true" });
                }
                else {
                    $("#LAYERSELECTION3").chosen({ width: "99%", include_group_label_in_selected: "true", children: "true" });

                }
            }
            detectmob();

            if ($(".modal").hasClass("show-modal")) {
                var idInfoIcon = "aaaa";
            } else {
                var idInfoIcon = "aaab";
            }

            //if (urlobj.p == "w") {
                var checkInputs = function (elements) {
                    elements.each(function () {
                        var element = $(this);
                        var input = element.children("input");
        
                        input.prop("checked", element.hasClass("k-state-selected"));
                    });
                    //$(".k-button").hide(); 
                    //$(".k-icon.k-i-loading").remove();
                    
                };
                var k_input;
                var required_pre;
                if ($( ".modal" ).hasClass( "show-modal" )){
                    required_pre = $(".required").get(0);
                } else {
                    required_pre = $(".required").get(1);
                    
                }
                // create MultiSelect from select HTML element
                var required = $(required_pre).kendoMultiSelect({
                    itemTemplate: '<input type=\"checkbox\" id=\"#: formatValue(data,0) #\"/><label>  #:data.text# </label><a href=\'javascript:void(0)\' onClick=\'showInfo_preview(#:formatValue(data,0)#,event);\'><i id=\"' + idInfoIcon + '#:formatValue(data,0)#\" class=\"fas fa-info-circle\"></i></a><p class="searcher_groupName">#: formatValue(data,1) #</p>',
                    headerTemplate: '<div id="search_results_container">' +
                    '<span id="search_results">Search results</span>' +
                            '</div>',
                    autoClose: false,
                    filter: "contains",
                    deselect: onDeselect,
                    select: onSelect,
                    open: onOpen,
                    close: onClose,
                    filtering: onFiltering,
                    clearButton: false,

                    dataBound: function () {
                        var items = this.ul.find("li");
                        setTimeout(function () {
                            checkInputs(items);
                        });
                        closeinfolayer();
                        if ($(".k-item").length==0){
                            $("#search_results").hide();
                            translate_article();
                        } else {
                            $("#search_results").show();
                        }
                    },
                    change: function () {
                        var items = this.ul.find("li");                            
                        checkInputs(items);
                    }
                }).data("kendoMultiSelect");

                //DISABLE BACKSPACE AND ENTER
                var input = required.input;
                input.on('keydown', function(e){
                    if ((e.which === 8 && !e.target.value.length) || e.which === 13 ) {
                        e.stopImmediatePropagation();  
                        e.preventDefault();
                    }
                });
                $._data(input[0]).events.keydown.reverse();
                /////////////////

                $(".k-multiselect-wrap").append('<span class="search_text">Search for layers</span>');
                translate_article();
                
                function onSelect(e) {
                    $("#predefinedmapCont").hide();
                    var dataItem = e.dataItem;
                    dataItem = formatValue(dataItem,0);
                    addLayersId(dataItem);
                    if( !$("#buttonshow i").hasClass("fa-angle-left")){ //OPEN LAYER PANEL
                        cliccatoTool();
                    }
                    var opentip_text = article["LayerAdded"];
                    addRemoveLayerMessage(opentip_text);
                    setTimeout(function () {
                        listalayercreation4();
                    }, 1000);
                };
    
                function onDeselect(e) {
                    $("#predefinedmapCont").hide();
                    var dataItem = e.dataItem;
                    dataItem = formatValue(dataItem,0);
                    removelayer(dataItem);
                    if( !$("#buttonshow i").hasClass("fa-angle-left")){ //OPEN LAYER PANEL
                        cliccatoTool();
                    }
                    var opentip_text = article["LayerRemoved"];
                    addRemoveLayerMessage(opentip_text);
                    setTimeout(function () {
                        listalayercreation4();
                    }, 400);
                };
                function onClose(e) {
                    if ($('.opentip-container').length == 1) {
                        e.preventDefault();
                        $(".k-input").focus();
                        $(".k-input").val(k_input);
                    } else {
                        $(".closeSearchButton").hide();
                        $(".search_text").show();
                        if ($(".k-input").val() == "") {
                            
                        } else {
                            $(".k-input").val("");
                        }
                        closeinfolayer();
                    } 
                };
                function onFiltering(e) {
                    k_input=$(".k-input").val();
                    if (e.filter) {
                        var value = e.filter.value;
                        var newFilter = {
                            filters: [
                                { field: "text", operator: "contains", value: value },
                                { field: "value", operator: "contains", value: value }
                            ],
                            logic: "or"
                        }
                        e.sender.dataSource.filter(newFilter);
                        e.preventDefault();
                    }
                    e.preventDefault();
                }

                function onOpen(e) {
                    $(".k-list-scroller").scroll(function() {
                        closeinfolayer();
                    });
                    $(".closeSearchButton").show();
                    $(".search_text").hide();
                    closeinfolayer();
                };
            //}
            return true;
        },
        getCheckedLayersSearch: function () {
            var arrayLayersSelected = [];
            if ($( ".modal" ).hasClass( "show-modal" )){
                var listOfLayers = $( ".k-list-scroller" ).find("ul")[0].childNodes;
            }
            if ($( ".modalSearch" ).hasClass( "show-modal" )){
                var listOfLayers = $( ".k-list-scroller" ).find("ul")[1].childNodes;
            }
            $.each(listOfLayers, function (key, value) {
                if (value.className.indexOf("k-state-selected") > -1) {
                    arrayLayersSelected.push(value.childNodes[0].id);
                }
            });
            return arrayLayersSelected;
        },
        alerta: function () {
            alert("allerta");
        },
        listalayercreation3: function () { //CREATE A MAP PANEL
            $("#createPaneList").empty();

            var select = $("#createPaneList");
            for (var inde in mastergroupuidx) {
                $.each(optlaye, function (key, value) {
                    var group;
                    if (mastergroupuidx[inde] === "") {
                        group = $("<div  class=\"groupsLayersContainer\" />");
                        $("<hr class=\"hrThemes\"/>  ").html(this.name).appendTo(group);
                        $("<span class=\"groupsName\" >" + key + " </span> ").html(this.name).appendTo(group);
                    } else { //LAYERS THAT HAVE TO BE VALIDATED
                        group = $("<div  class=\"groupsLayersContainer\" />");
                    }
                    $.each(value, function () {
                        var layerCont;
                        if (this.group === mastergroupuidx[inde] && mastergroupuidx[inde] === "") {
                            var isLayer = false;
                            for (var x in layerInfo2) {
                                if (this.id == layerInfo2[x].idd) {
                                    isLayer = true;
                                    break;
                                }
                            }
                            if (isLayer) { //CHECK IF LAYERS ARE OPENED OR NOT
                                $("<div  class=\"layersNameCont\" id=\"" + this.id + "Cont\" ><input type=\"checkbox\" onchange=\"changeCheckboxLayer(this)\" id=\"cb" + this.id + "\" class=\"layersNameCheckbox\"  value=\"" + this.name + "\" checked><label for=\"cb" + this.id + "\" title=\"" + this.name + "\">" + this.name + " </label><a onclick=\"showinfolayerModal(" + this.id + ");\"><i class=\"fas fa-info-circle\" ></i></a> </div>").appendTo(group);
                            } else {
                                $("<div  class=\"layersNameCont\" id=\"" + this.id + "Cont\" ><input type=\"checkbox\" onchange=\"changeCheckboxLayer(this)\" id=\"cb" + this.id + "\" class=\"layersNameCheckbox\" value=\"" + this.name + "\" ><label for=\"cb" + this.id + "\" title=\"" + this.name + "\">" + this.name + " </label><a onclick=\"showinfolayerModal(" + this.id + ");\"><i class=\"fas fa-info-circle\" ></i></a> </div>").appendTo(group);
                            }
                        }
                    });
                    if (value[0].group === mastergroupuidx[inde])
                        group.appendTo(select);
                });
            }
            var arrayLayersSelected = this.getCheckedLayers();
            for (var als in arrayLayersSelected) {
                $("#"+arrayLayersSelected[als]+"Cont").css("background-color","#ececec");
            }
        },
        getCheckedLayers: function () {
            var arrayLayersSelected = [];
            $( ".layersNameCheckbox" ).each(function( key, value ) {
                if (value.checked) {
                    arrayLayersSelected.push(value.id.replace("cb",""));
                }
            });
            return arrayLayersSelected;
        },
        listalayercreation4: function () { //CREATE A MAP PANEL
            $("#createPaneList1").empty();

            var select = $("#createPaneList1");
            for (var inde in mastergroupuidx) {
                $.each(optlaye, function (key, value) {
                    var group;
                    if (mastergroupuidx[inde] === "") {
                        group = $("<div  class=\"groupsLayersContainer\" />");
                        $("<hr class=\"hrThemes\"/>  ").html(this.name).appendTo(group);
                        $("<span class=\"groupsName\" >" + key + " </span> ").html(this.name).appendTo(group);
                    } else { //LAYERS THAT HAVE TO BE VALIDATED
                        group = $("<div  class=\"groupsLayersContainer\" />");
                    }
                    $.each(value, function () {
                        var layerCont;
                        if (this.group === mastergroupuidx[inde] && mastergroupuidx[inde] === "") {
                            var isLayer = false;
                            for (var x in layerInfo2) {
                                if (this.id == layerInfo2[x].idd) {
                                    isLayer = true;
                                    break;
                                }
                            }
                            if (isLayer) { //CHECK IF LAYERS ARE OPENED OR NOT
                                $("<div  class=\"layersNameCont\" id=\"" + this.id + "Cont1\" ><input type=\"checkbox\" onchange=\"changeCheckboxLayer(this)\" id=\"cbb" + this.id + "\" class=\"layersNameCheckbox1\"  value=\"" + this.name + "\" checked><label for=\"cbb" + this.id + "\" title=\"" + this.name + "\">" + this.name + " </label><a onclick=\"showinfolayerModal(" + this.id + ");\"><i class=\"fas fa-info-circle\" ></i></a> </div>").appendTo(group);
                            } else {
                                $("<div  class=\"layersNameCont\" id=\"" + this.id + "Cont1\" ><input type=\"checkbox\" onchange=\"changeCheckboxLayer(this)\" id=\"cbb" + this.id + "\" class=\"layersNameCheckbox1\" value=\"" + this.name + "\" ><label for=\"cbb" + this.id + "\" title=\"" + this.name + "\">" + this.name + " </label><a onclick=\"showinfolayerModal(" + this.id + ");\"><i class=\"fas fa-info-circle\" ></i></a> </div>").appendTo(group);
                            }
                        }
                    });
                    if (value[0].group === mastergroupuidx[inde])
                        group.appendTo(select);
                });
            }
            var arrayLayersSelected = this.getCheckedLayers1();
            for (var als in arrayLayersSelected) {
                $("#"+arrayLayersSelected[als]+"Cont1").css("background-color","#ececec");
            }
        },
        getCheckedLayers1: function () {
            var arrayLayersSelected = [];
            $( ".layersNameCheckbox1" ).each(function( key, value ) {
                if (value.checked) {
                    arrayLayersSelected.push(value.id.replace("cbb",""));
                }
            });
            return arrayLayersSelected;
        },
        listalayerspredmaps: function () { //CREATE PREDEFINED MAPS DESCRIPTION
            inputlistofmap = THEMES_CONFIG.services;

            $.each(inputlistofmap, function (key, value) {
                var ddd = value.LAYERS_LIST.split(",");
                var eee = [];
                for (var i in ddd) {
                    eee.push(ddd[i].split(":")[0]);
                }
                var listOfLayersNames = [];
                for (var i in eee) {
                    $.each(optlaye, function (key, value) {
                        $.each(value, function () {
                            if (eee[i] == this.id) {
                                listOfLayersNames.push(this.name);
                            }
                        });
                    });
                }
                var textOfLayers = "";
                textOfLayers = value.DESCRIPTION;
                textOfLayers = textOfLayers+"<br><br>"+article["listLayers"];
                for (var i in listOfLayersNames) {
                    textOfLayers = textOfLayers + "<br> - " + listOfLayersNames[i];
                }
                $(".theme"+(key+1)+"_Layers").html(textOfLayers);
                
            });
        },
        listalayercreation: function () { //NOT MORE USED

            listalayertit = [];
            listalayerid = [];
            listalayertitt = [];
            arrayUtils.forEach(SERVICES_CONFIG, function (xxx, indx) {

                listalayertit[indx] = SERVICES_CONFIG[indx];
                listalayertitt[indx] = SERVICES_CONFIG[indx].TITLE;

            });
            // listalayertitt = [];
            output = {};
            this.translater(listalayertitt);

            for (var inn0 in listalayertitt) {
                listalayertit[inn0].TRASL = output[inn0];// Translated values
            }
            listalayertit.sort(this.dynamicSort("TRASL"));
            //    $("#LAYERSELECTION").empty();
            $("#LAYERSELECTION2").empty();
            $("#LAYERSELECTION3").empty();

            // var selectlay = document.getElementById("LAYERSELECTION");
            var selectlay2 = document.getElementById("LAYERSELECTION2"); //prende elemendo con id spec
            var selectlay3 = document.getElementById("LAYERSELECTION3"); //prende elemendo con id spec
            // var options = FISH_CONFIG;
            for (var i in listalayertit) {
                // optgroup
                var elay = document.createElement("option");
                elay.textContent = listalayertitt[i];
                elay.value = listalayerid[i];

                var elay2 = document.createElement("option");
                //  elay2.textContent = listalayertitt[i];
                elay2.value = listalayertit[i].TRASL;
                //   elay2.text = listalayertitt[i];
                elay2.id = listalayertit[i].ID;
                //   selectlay.appendChild(elay); //inserisce in spec option opt
                selectlay2.appendChild(elay2); //inserisce in spec option opt
                // selectlay3.appendChild(elay2); //inserisce in spec option opt
            }
            for (var i in listalayertit) {
                var elay = document.createElement("option");
                elay.textContent = listalayertitt[i];
                elay.value = listalayerid[i];

                var elay2 = document.createElement("option");
                elay2.textContent = listalayertit[i].TRASL;
                elay2.value = listalayertit[i].TRASL;
                elay2.text = listalayertit[i].TRASL;
                elay2.id = listalayertit[i].ID;
                //   selectlay.appendChild(elay); //inserisce in spec option opt
                selectlay3.appendChild(elay2); //inserisce in spec option opt
                // selectlay3.appendChild(elay2); //inserisce in spec option opt
            }

            /*
             document.getElementById("LAYERSELECTION").onchange = function () {
             // alert( $('#LAYERSELECTION').find(":selected").val());
             var urllay = window.location.hash;
             var themeurl = window.location.hash.substr(15);
             
             if (themeurl.length > 0)
             window.location.hash = urllay + "," + $('#LAYERSELECTION').find(":selected").val();
             else
             window.location.hash = urllay + $('#LAYERSELECTION').find(":selected").val();
             location.reload();
             
             // or alert(this.options[this.selectedIndex].value);
             };
             
             */
            return true;
        },
        listalanguages: function () {   //CREATE LIST OF THE LANGUAGES
            $("#left_row").empty();
            $("#right_row").empty();
            inputlistoflang = IDIOMS_CONFIG.languages;
            leftArray = [];
            rightArray = [];
            var size = inputlistoflang.length/2;
            for(var x in inputlistoflang){
                if (x < size) {
                    leftArray.push(inputlistoflang[x]);
                } else {
                    rightArray.push(inputlistoflang[x]);
                }
            }
            
            $.each(leftArray, function (key, value) {
                var $select = $("#left_row");
                var btnLang = "<a href=\"javascript:changeLanguage('"+(value.CODE)+"')\" lang=\""+value.CODE.toLowerCase()+"\" class=\"langbtn\">"+value.NAME+"</a>";
                $(btnLang).appendTo($select);
            });
            $.each(rightArray, function (key, value) {
                var $select = $("#right_row");
                var btnLang = "<a href=\"javascript:changeLanguage('"+(value.CODE)+"')\" lang=\""+value.CODE.toLowerCase()+"\" class=\"langbtn\">"+value.NAME+"</a>";
                $(btnLang).appendTo($select);
            });
            changeLanguageText(lang);
        },
        assigndescription: function () {

            for (i in lai) {
                lai[i].descriptiont = descriptiony[i]; // metto il titolo tradotto nella c pos di ogni lai
            }
            return true;
        },
        assigntitle: function () {

            for (i in lai) {
                lai[i].titleok = labtras[i]; // metto il titolo tradotto nella c pos di ogni lai
            }


            return true;
        }



    };


});


/*
 ////////////RICHIESTA FIELDS DA SERVICEINFOS
 showResultsxx = function (resultsxx) {           
 identifyResults= new IdentifyResult(resultsxx);
 
 //  alert(fieldName);  
 resultsxx.fieldAliases ;
 // var getFieldInfo = resultsxx.identifyResults[0].feature.attributes;
 //FIELD_KID is a known field alias
 //alert(getFieldInfo.FIELD_KID); 
 
 //  alert(resultsxx);
 };
 
 for(var k in intlayer[posx].IDENTIFYINFO  ) {    
 
 //   var queryTaskxx = new QueryTask("http://maratlas.discomap.eea.europa.eu/arcgis/rest/services/Maratlas/translation/MapServer/1");
 var queryTaskxx = new QueryTask( layerInfo[posx].layer.url+"/"+intlayer[posx].IDENTIFYINFO[k].ID);
 var queryxx = new Query();
 queryxx.where = "1=1";
 queryxx.returnGeometry = true;
 queryxx.outFields =  [intlayer[posx].IDENTIFYINFO[k].FIELDS];
 queryTaskxx.execute(queryxx, showResultsxx);
 }
 
 ////////////  FINE   RICHIESTA FIELDS DA SERVICEINFOS
 
 */
/*   RICHIESTA FIELDS DA SERVICEINFOS 222222222     
 var requestHandle = esriRequest({
 "url": layerInfo[posx].layer.url+"/0",
 "content": {
 "f": "json"
 },
 "callbackParamName": "callback"
 });
 
 
 
 requestHandle.then(requestSucceeded, requestFailed);
 
 function requestSucceeded(response, io){
 var fieldInfo, pad;
 pad = dojoString.pad;
 
 //toJson converts the given JavaScript object
 //and its properties and values into simple text 
 JSON.toJsonIndentStr = "  ";
 // console.log("response as text:\n", JSON.toJson(response,true));
 dom.byId("container2").innerHTML = "";
 
 //show field names and aliases
 if ( response.hasOwnProperty("fields") ) {
 //  console.log("got some fields");
 fieldInfo = arrayUtils.map(response.fields, function(f) {
 return pad("Field:", 8, " ", true) + pad(f.name, 25, " ", true) + 
 pad("Alias:", 8, " ", true) + pad(f.alias, 25, " ", true) + 
 pad("Type:", 8, " ", true) + pad(f.type, 25, " ", true);
 });
 dom.byId("container2").value = fieldInfo.join("\n");
 
 //  alert( fieldInfo.join("\n"));
 } else {
 dom.byId("container2").value = "No field info found. Please double-check the URL.";
 }
 
 }
 function requestFailed(error, io){
 
 f.add(dom.byId("#container2"), "failure");
 
 JSON.toJsonIndentStr = " ";
 dom.byId("container2").value = JSON.toJson(error, true);
 
 }
 
 */

///////////  FINE   RICHIESTA FIELDS DA SERVICEINFOS 222222222