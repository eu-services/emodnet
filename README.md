# European Atlas of the Seas #

The European Atlas of the Seas is an easy and fun way for professionals, students and anyone interested to learn more about Europe's seas and coasts, their environment, related human activities and European policies. It was developed to raise awareness of Europe's oceans and seas, in the context of the EU's integrated maritime policy. The atlas offers a remarkably diverse range of information about Europe's seas, for example:

* Sea depth and underwater features
* Coastal regions geography and statistics
* Blue energies and maritime resources
* Tide amplitude and coastal erosion
* Fishing stocks, quotas and catches
* European fishing fleet
* Aquaculture
* Maritime transport and traffic
* Ports' statistics
* Maritime protected areas
* Tourism
* Maritime policies and initiatives
* Outermost regions

The atlas exists in English, French and German.

### Links ###

Atlas: https://ec.europa.eu/maritimeaffairs/atlas/maritime_atlas/
Issues tracking (JIRA): https://bilbomaticauegis.atlassian.net

### Libraries ###

* Chosen (https://harvesthq.github.io/chosen/)
* Font awesome (http://fontawesome.io/)
* Highcharts (http://highcharts.com/)
* jQuery UI (https://jqueryui.com/)
* Opentip (http://www.opentip.org/)
* Slider (http://rangeslider.js.org/)