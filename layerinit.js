var package_path = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'));

define([
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/FeatureLayer",
    "esri/layers/WebTiledLayer",
    "esri/layers/WMSLayerInfo",
    "esri/geometry/Extent",
    "esri/layers/WMSLayer",
    "esri/SpatialReference",
    package_path + "/main_functions.js"
], function (ArcGISDynamicMapServiceLayer, FeatureLayer, WebTiledLayer, WMSLayerInfo, Extent, WMSLayer, SpatialReference, main_functions) {
    return {
        layerinit: function (intlayer, imageParameters, labmapservice, que, alphax) {

            lai = [];
            lai2 = [];
            speclay = [];
            speclayer = null;
            for (var ll in que) {
                layerDefs = [];

                alphay = 0;

                if (alphax[ll]) {
                    alphay = alphax[ll];
                } else {
                    alphay = 0.75;
                }

                if (intlayer[ll])
                    if (intlayer[ll].TYPE === "fish-dynamic") {
                        fish_options = intlayer[ll].FISHLIST.FISHLIST.split(";");
                        fish_options.sort();

                        //   var exx = document.getElementById("spec");
                        //    species = exx.options[exx.selectedIndex].value;
                        species = fish_options[0];
                        speclayer = ll;  /// remember to delete speclayer when remove fish layers 
                        speclay.push(ll);
                        //  var species = "SAL";
                        var layerDefAryx;

                        //  codefield = intlayer[ll].FISHLIST.FISHFIELDNAME.split('.')[1];

                        codefield = intlayer[ll].FISHLIST.FISHFIELDNAME;

                        //  identifyParams.layerDefinitions = layerDefAryx;  // per ora non lo uso                    


                        layerDefs[0] = codefield + "='" + species + "'";

                        imageParameters.layerDefinitions = layerDefs;

                        lais = new ArcGISDynamicMapServiceLayer(intlayer[ll].MAPSERVICE,
                            {
                                "imageParameters": imageParameters,
                                "opacity": alphay,
                                mode: FeatureLayer.MODE_ONDEMAND,
                                "fieldcode": codefield


                            });
                    }
                    else


                        if (intlayer[ll].TYPE === "webtiled") {
                            imageParameters.layerDefinitions = layerDefs;
                            newURL = "http://www.marinetraffic.com";

                            lais = new WebTiledLayer(intlayer[ll].MAPSERVICE,
                                {
                                    "imageParameters": imageParameters,
                                    "opacity": alphay

                                });

                        } else

                            if (intlayer[ll].TYPE === "wms") {
                                imageParameters.layerDefinitions = layerDefs;

                                var name=intlayer[ll].LAYERS[0];	
                                if ((intlayer[ll].RESOURCEINFO.LAYERINFOS[0] || "") != "" && (intlayer[ll].RESOURCEINFO.LAYERINFOS[0].name || "") != "" ) {
                                    name= intlayer[ll].RESOURCEINFO.LAYERINFOS[0].name.toString();
                                }
                                var layerwms = new WMSLayerInfo({
                                    name: name,
                                    // title: intlayer[ll].RESOURCEINFO.LAYERINFOS[0].title.toString(),
                                    title: intlayer[ll].TITLE,
                                    // legendURL: 'http://maratlas.discomap.eea.europa.eu/arcgis/services/Maratlas/Bathymetry4/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=1'
                                    legendURL: intlayer[ll].LEGEND,
                                    description: intlayer[ll].DESCRIPTION


                                });
                                srs = new SpatialReference({ wkid: intlayer[ll].WKID });
                                var resourceInfo = {
                                    //  extent: new esri.geometry.Extent({"xmin": -106476154.32, "ymin": 2504688.54, "xmax": -6457400.14, "ymax": 7514065.62, "spatialReference": {"wkid": 102100}}),
                                    //    extent: new Extent(25.000943, -36.000000, 84.993726, 43.000092, {wkid: intlayer[ll].WKID}),
                                    //    extent: new Extent(-85.22193775799991, -180.000000, 83.6664731, 180.0, {wkid: intlayer[ll].WKID}),
                                    extent: new Extent(0, 0, 256, 256, srs),
                                    //    srs = new SpatialReference({wkid:intlayer[ll].WKID}),

                                    layerInfos: [layerwms],
                                    version: "1.1.1",
                                    spatialReferences: [intlayer[ll].WKID],
                                    spatialReference: intlayer[ll].WKID
                                };

                                lais = new WMSLayer(intlayer[ll].MAPSERVICE + "",
                                    {
                                        "imageParameters": imageParameters,
                                        "opacity": alphay,
                                        format: "png",
                                        resourceInfo: resourceInfo,
                                        version: "1.1.1",
                                        //  visibleLayers: intlayer[ll].LAYERS.toString() //lista dei layer visibili e non l'array
                                        visibleLayers: intlayer[ll].LAYERS //lista dei layer visibili e non l'array

                                    });
                                // if (lais.spatialReferences)  lais.spatialReferences[0] = intlayer[ll].WKID;
                                //    lais.spatialReference = intlayer[ll].WKID;


                                //     lais.setVisibleLayers(intlayer[ll].LAYERS.toString()); //lista dei layer visibili e non l'array);
                                //     lais.setImageFormat("png");

                            } else {
                                lais = new ArcGISDynamicMapServiceLayer(intlayer[ll].MAPSERVICE,
                                    {
                                        "imageParameters": imageParameters,
                                        mode: FeatureLayer.MODE_ONDEMAND,
                                        "opacity": alphay
                                    });
                            }
                layerDefs = [];
                imageParameters.layerDefinitions = layerDefs;
                lais.DisableClientCaching = true;
                lais.serv = ll;
                lais.idd = que[ll];
                //    lais.visibleLayers= intlayer[ll].LAYERS.toString(); //lista dei layer visibili e non l'array


                // lais.alpha = alpha[ll];
                lais.pos = ll;
                if (typeof intlayer[ll] !== "undefined") {
                    if (typeof intlayer[ll].TYPE !== "undefined")
                        lais.type = intlayer[ll].TYPE;
                    if (typeof intlayer[ll].IDENTIFYINFO !== "undefined")
                        lais.identifyinfo = true;
                    else
                        lais.identifyinfo = false;
                    if (typeof intlayer[ll].DESCRIPTION !== "undefined")
                        lais.descriptionx = intlayer[ll].DESCRIPTION;

                    //  if(intlayer[ll].IDENTIFYINFO[0].TYPE)   lais.InfoTemplate= intlayer[ll].IDENTIFYINFO[0].TYPE;
                    //  
                    // NOT FOR  WMS LAYERS
                    if (typeof intlayer[ll].LAYERS !== "undefined" && intlayer[ll].TYPE !== "wms") lais.setVisibleLayers(intlayer[ll].LAYERS);
                }

                lai[ll] = lais;


                ///////////////////  ADD AS LAST LEVEL THE LABEL OF LAST LEVEL

                if (labmapservice[ll]) {
                    var lais2 = new ArcGISDynamicMapServiceLayer(intlayer[ll].LBL_MAPSERVICE, {
                        "opacity": alphay,
                        mode: FeatureLayer.MODE_ONDEMAND,
                        outFields: ["*"]
                    });
                    lais2.serv = ll;
                    lais2.lab = "label";
                    lais2.labelname = "XX";
                    lais2.iddx = que[ll] + "x";
                    lai2.push(lais2);

                }
            }

            //    return  fish_options;
            //    var exx = document.getElementById("spec");//
            //    species = exx.options[exx.selectedIndex].value;
            //    return  species;
            //    return   speclayer;
            //    var species = "SAL";
            //    return  layerDefs;
            //    main_functions.fishupdate();

            return true;
        }
    };

});   