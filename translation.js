 article = {
    ".Mapprojection": {
      EN: "Map projection",
      DE: "Kartenprojektion",
      FR: "Projection carte"
    },
    "#arctic_button": {    //GOOGLE TRANSLATE
      EN: "Arctic",
      DE: "Arktis",
      FR: "Arctique"
    },
    "#mercator_button": {    //GOOGLE TRANSLATE
      EN: "Mercator",
      DE: "Mercator",
      FR: "Mercator"
    },
    ".Mapcontent": {
      EN: "Map content",
      DE: "Karteninhalt",
      FR: "Contenu carte"
    },
    "#slbutton": {    //GOOGLE TRANSLATE
      EN: "Thematic Map",
      DE: "Thematische Karte",
      FR: "Carte Thématique"
    },
    "#tmbutton": {    //GOOGLE TRANSLATE
        EN: "DIY Map",
        DE: "DIY Karte",
        FR: "Carte DIY"
      },
    "#clearbutton": {
      EN: "Clear all layers",
      DE: "Alles l&#246schen",
      FR: "Supprimer tout"
      },
    ".Layersselection": {
      EN: "Layers selection",
      DE: "Schichtenauswahl",
      FR: "S&#233;lection de couches"
      },
    "#LAYERSELECTION1": {
      EN: "Select from the list",
      DE: "Auswahlliste",
      FR: "Liste de s&#233;lection"
      }, 
    "#LAYERSELECTION3": {
      EN: "Select from the list",
      DE: "Auswahlliste",
      FR: "Liste de s&#233;lection"
      },
    "#opacity": {    //GOOGLE TRANSLATE
      EN: "Opacity",
      DE: "Opazität",
      FR: "Opacité"
    },
    "#titlePane span.dijitTitlePaneTextNode": {
      EN: "Measurement",
      DE: "Messung",
      FR: "Mesure"
      },
    "#titlePane2 span.dijitTitlePaneTextNode": {
      EN: "Export", 
      DE: "Export",      
      FR: "Exportation"
      },
    "#selectbasemap": {
      EN: "Select Basemap",
      DE: "Auswahl der Hintergrundkarte",
      FR: "S&#233;lection carte de fond"
      },
    "#dijit_layout_ContentPane_3": {
      EN: "Measurement Result",
      DE: "Messergebnis",
      FR: "R&#233;sultat de mesure"
    },
    "#dijit_form_ComboButton_0_label": {
      EN: "Size",
      DE: "Gr&ouml;&szlig;e",
      FR: "Taille"
    },
    ".Language": {
      EN: "Language",
      DE: "Sprache",
      FR: "Langue"
    },
    ".Help": {
      EN: "Help",
      DE: "Helpen",
      FR: "Aide"
    },
    ".Feedback": {
      EN: "Feedback",
      DE: "Feedback",
      FR: "Message"
    },
    ".Legalnotice": {
      EN: "Legal notice",
      DE: "Impressum",
      FR: "Notice l&#233;gale"
    },
    "#closeHomeLabel2": {
      EN: "Do not show this homepage again",
      DE: "Zeige diese Homepage nicht mehr",
      FR: "Ne plus afficher cette page d'accueil"
    },
    "#dijit_layout_ContentPane_0_button_title": {
      EN: "Map",
      DE: "Karte",
      FR: "Carte"
    },
    "#legendTitle": {
      EN: "Legend",
      DE: "Legende",
      FR: "L&#233;gende"
    },
    "#layerTitle": {
      EN: "Layers",
      DE: "Schichten",
      FR: "Couches"
    },
    "#galleryNode_5 span": {    //GOOGLE TRANSLATE
      EN: "Water Template",
      DE: "Wasser Vorlage",
      FR: "Modèle d'eau"
    },
    "#galleryNode_15 span": {    //GOOGLE TRANSLATE
      EN: "Ocean Reference",
      DE: "Ozean Referenz",
      FR: "Ocean Référence"
    },
    "#galleryNode_6 span": {    //GOOGLE TRANSLATE
      EN: "Base Template",
      DE: "Basisvorlage",
      FR: "Modèle de base"
    },
    "#galleryNode_20 span": {    //GOOGLE TRANSLATE
      EN: "Civil Template",
      DE: "Zivil-Vorlage",
      FR: "Modèle civil"
    },
    "#galleryNode_100095 span": {    //GOOGLE TRANSLATE
      EN: "Arctic Template",
      DE: "Arktische Vorlage",
      FR: "Modèle Arctique"
    },
    "#galleryNode_100995 span": {    //GOOGLE TRANSLATE
      EN: "Basic Arctic Template",
      DE: "Basic Arctic Vorlage",
      FR: "Modèle Arctique de Base"
    },
    ".pred_maps":{
      EN: "Predefined maps",
      DE: "Vordefinierte Karten",
      FR: "Cartes prédéfinies"
    },
    "#create_map":{
      EN: "Create a map",
      DE: "Karte erzeugen",
      FR: "Créer une carte"
    },
    "#create_map1":{
      EN: "Layers",
      DE: "Karte erzeugen",
      FR: "Créer une carte"
    },
    ".click_info p":{
      EN: "Click on the map to get feature info",
      DE: "Klicken Sie auf die Karte für Informationen",
      FR: "Cliquez sur la carte sur obtenir des information sur les fonctionnalités"
    },
    "#SharePage #contenido div":{
      EN: "Share this map",
      DE: "Teile diese Karte",
      FR: "Partagez cette carte"
    },
    "#printTitle":{
      EN: "Print",
      DE: "Drucken",
      FR: "Impression"
    },    
    "#shareTitle":{
      EN: "Share",
      DE: "Teilen",
      FR: "Partager"
    },
    "#exportTitle": {
      EN: "Export", 
      DE: "Export",      
      FR: "Exportation"
    },
    "#embedTitle": {
      EN: "Embed", 
      DE: "Einbetten",      
      FR: "Intégrer"
    },
    "#saveTitle": {
      EN: "Save", 
      DE: "Speichern",      
      FR: "Enregistrer"
    },
    "#textEmbed1": {
      EN: "Embed this form on your website:", 
      DE: "Betten Sie dieses Formular auf Ihrer Website ein:",      
      FR: "Intégrez ce formulaire sur votre site Web:"
    },
    "#textEmbed2": {
      EN: "Copy and paste the above code into an HTML page on your website.", 
      DE: "Kopieren Sie den obigen Code und fügen Sie ihn in eine HTML-Seite Ihrer Website ein.",      
      FR: "Copiez et collez le code ci-dessus dans une page HTML sur votre site Web."
    },
    "#copyIframe": {
      EN: "Copy", 
      DE: "Kopie",      
      FR: "Copie"
    },
    "#btnAddLayers": {
      EN: "Add layers", 
      DE: "Fügen Sie Ebenen hinzu",      
      FR: "Ajouter des couches"
    },
    "#btnAddLayers1": {
      EN: "Add layers", 
      DE: "Fügen Sie Ebenen hinzu",      
      FR: "Ajouter des couches"
    },
    ".addLayersButton":{
      EN: "Search:", 
      DE: "Suche:",      
      FR: "Recherche:"
    },
    "#legendDiv_msg": {
      EN: "No legend", 
      DE: "Keine Legende vorhanden",      
      FR: "Pas de légende"
    },
    ".subtit_des": {
      EN: "The easy an fun way for professionals, students and anyone interested to learn more about Europe's seas and coasts, their environment, related human activities an European policies.",
      DE: "Der einfache und unterhaltsame Weg für Fachleute, Studenten und alle Interessierten, um mehr über Europas Meere und Küsten, ihre Umwelt, verwandte menschliche Aktivitäten und eine europäische Politik zu erfahren.",      
      FR: "Le moyen facile et amusant pour les professionnels, les étudiants et toute personne intéressée à en apprendre davantage sur les mers et les côtes européennes, leur environnement, les activités humaines connexes et les politiques européennes."
    },
    "#Help1 span":{
      EN: "Do you need help?", 
      DE: "Brauchst du Hilfe",      
      FR: "As-tu besoin d'aide"
    },
    "#Other1 span":{
      EN: "Give feedback on European Atlas of the Seas", 
      DE: "Geben Sie eine Rückmeldung zum Europäischen Atlas der Meere",      
      FR: "Donner un avis sur European Atlas of the Seas"
    },
    "#search_tit":{
      EN: "Add layers to map", 
      DE: "Fügen Sie Ebenen zum Zuordnen hinzu",      
      FR: "Ajouter des couches à la carte"
    },
    ".layers_tit":{
      EN: "Layers",
      DE: "Schichten",
      FR: "Couches"
    },
    "#search_results":{
      EN: "Search Results",
      DE: "Suchergebnisse",
      FR: "Résultats de la recherche"
    },
    ".search_text":{
      EN: "Search for layers",
      DE: "Suche nach Ebenen",
      FR: "Rechercher des calques"
    },
    ".breadcrumb":{
      EN: "European commission > Maritime Affairs > European Atlas of the Seas",
      DE: "Europäische Kommission > Maritime Angelegenheiten > Europäischer Atlas der Meere",
      FR: "Commission européenne > Affaires maritimes > Atlas européen des mers"
    },
    ".tit_des span":{
      EN: "European Atlas of the Seas",
      DE: "Europäischer Atlas der Meere",
      FR: "Atlas européen des mers"
    },



    
    
      
} ;


meas_trasl ={
    
            DE: {
                NLS_distance: "Entfernung",
                NLS_area: "Fl\u00e4che",
                NLS_location: "Position",
                NLS_resultLabel: "Messergebnis",
                NLS_length_miles: "Meilen",
                NLS_length_kilometers: "Kilometer",
                NLS_length_feet: "Fu\u00df",
                NLS_length_meters: "Meter",
                NLS_length_yards: "Yard",
                NLS_length_nautical_miles: "Seemeilen",
                NLS_area_acres: "Acres",
                NLS_area_sq_miles: "Quadratmeilen",
                NLS_area_sq_kilometers: "Quadratkilometer",
                NLS_area_hectares: "Hektar",
                NLS_area_sq_yards: "Quadratyard",
                NLS_area_sq_feet: "Quadratfu\u00df",
                NLS_area_sq_meters: "Quadratmeter",
                NLS_area_sq_nautical_miles: "Quadratseemeilen",
                NLS_deg_min_sec: "DMS",
                NLS_decimal_degrees: "Grad",
                NLS_map_coordinate: "Kartenkoordinate",
                NLS_longitude: "L\u00e4ngengrad",
                NLS_latitude: "Breitengrad",
                NLS_MGRS: "MGRS",
                NLS_USNG: "USNG",
                NLS_UTM: "UTM",
                NLS_GeoRef: "GeoRef",
                NLS_GARS: "GARS",
                NLS_geometry_service_error: "Fehler beim Geometrieservice",
                NLS_calculating: "Wird berechnet...",
                NLS_length_miles_us: "Meilen (US)",
                NLS_area_sq_miles_us: "Quadratmeilen (US)",
                NLS_length_feet_us: "Fu\u00df (US)",
                NLS_area_sq_feet_us: "Quadratfu\u00df (US)",
                NLS_length_yards_us: "Yards (US)",
                NLS_area_sq_yards_us: "Quadratyards (US)",
                NLS_area_acres_us: "Acres (US)"
            }, EN: {
                NLS_distance: "Distance",
                NLS_area: "Area",
                NLS_location: "Location",
                NLS_resultLabel: "Measurement Result",
                NLS_length_miles: "Miles",
                NLS_length_kilometers: "Kilometers",
                NLS_length_feet: "Feet",
                NLS_length_meters: "Meters",
                NLS_length_yards: "Yards",
                NLS_length_nautical_miles: "Nautical Miles",
                NLS_area_acres: "Acres",
                NLS_area_sq_miles: "Sq Miles",
                NLS_area_sq_kilometers: "Sq Kilometers",
                NLS_area_hectares: "Hectares",
                NLS_area_sq_yards: "Sq Yards",
                NLS_area_sq_feet: "Sq Feet",
                NLS_area_sq_meters: "Sq Meters",
                NLS_area_sq_nautical_miles: "Sq Nautical Miles",
                NLS_deg_min_sec: "DMS",
                NLS_decimal_degrees: "Degrees",
                NLS_map_coordinate: "Map Coordinate",
                NLS_longitude: "Longitude",
                NLS_latitude: "Latitude",
                NLS_MGRS: "MGRS",
                NLS_USNG: "USNG",
                NLS_UTM: "UTM",
                NLS_GeoRef: "GeoRef",
                NLS_GARS: "GARS",
                NLS_geometry_service_error: "Geometry Service Error",
                NLS_calculating: "Calculating...",
                NLS_length_miles_us: "Miles (US)",
                NLS_area_sq_miles_us: "Sq Miles (US)",
                NLS_length_feet_us: "Feet (US)",
                NLS_area_sq_feet_us: "Sq Feet (US)",
                NLS_length_yards_us: "Yards (US)",
                NLS_area_sq_yards_us: "Sq Yards (US)",
                NLS_area_acres_us: "Acres (US)"
            },
          FR: {
                NLS_distance: "Distance",
                NLS_area: "Surface",
                NLS_location: "Emplacement",
                NLS_resultLabel: "R\u00e9sultat de la mesure",
                NLS_length_miles: "Milles",
                NLS_length_kilometers: "Kilom\u00e8tres",
                NLS_length_feet: "Pieds",
                NLS_length_meters: "M\u00e8tres",
                NLS_length_yards: "Verges",
                NLS_length_nautical_miles: "Miles nautiques",
                NLS_area_acres: "Acres",
                NLS_area_sq_miles: "Milles carr\u00e9s",
                NLS_area_sq_kilometers: "Kilom\u00e8tres carr\u00e9s",
                NLS_area_hectares: "Hectares",
                NLS_area_sq_yards: "Verges carr\u00e9s",
                NLS_area_sq_feet: "Pieds carr\u00e9s",
                NLS_area_sq_meters: "M\u00e8tres carr\u00e9s",
                NLS_area_sq_nautical_miles: "Miles nautiques carr\u00e9s",
                NLS_deg_min_sec: "DMS",
                NLS_decimal_degrees: "Degr\u00e9s",
                NLS_map_coordinate: "Coordonn\u00e9es de la carte",
                NLS_longitude: "Longitude",
                NLS_latitude: "Latitude",
                NLS_MGRS: "MGRS",
                NLS_USNG: "USNG",
                NLS_UTM: "UTM",
                NLS_GeoRef: "GeoRef",
                NLS_GARS: "GARS",
                NLS_geometry_service_error: "Erreur du service de g\u00e9om\u00e9trie",
                NLS_calculating: "Calcul...",
                NLS_length_miles_us: "Miles (US)",
                NLS_area_sq_miles_us: "Miles carr\u00e9s (US)",
                NLS_length_feet_us: "Pieds (US)",
                NLS_area_sq_feet_us: "Pieds carr\u00e9s (US)",
                NLS_length_yards_us: "Yards (US)",
                NLS_area_sq_yards_us: "Yards carr\u00e9s (US)",
                NLS_area_acres_us: "Ares (US)"
            }      
};
